/*
 * Copyright 2024 European Union
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 *     https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package eu.europa.ec.leos.services.processor.content;

import eu.europa.ec.leos.i18n.MessageHelper;
import eu.europa.ec.leos.model.action.SoftActionType;
import eu.europa.ec.leos.model.action.TrackChangeActionType;
import eu.europa.ec.leos.model.xml.Element;
import eu.europa.ec.leos.vo.structure.NumberingType;
import eu.europa.ec.leos.vo.toc.TableOfContentItemVO;
import eu.europa.ec.leos.vo.structure.TocItem;
import eu.europa.ec.leos.services.utils.StructureConfigUtils;
import eu.europa.ec.leos.vo.structure.TocItemTypeName;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import static eu.europa.ec.leos.model.action.SoftActionType.ADD;
import static eu.europa.ec.leos.model.action.SoftActionType.DELETE;
import static eu.europa.ec.leos.model.action.SoftActionType.MOVE_FROM;
import static eu.europa.ec.leos.model.action.SoftActionType.MOVE_TO;
import static eu.europa.ec.leos.services.processor.content.indent.IndentConversionHelper.NUMBERED_ITEMS;
import static eu.europa.ec.leos.services.processor.content.indent.IndentConversionHelper.UNUMBERED_ITEMS;
import static eu.europa.ec.leos.services.processor.content.TableOfContentProcessor.getTagValueFromTocItemVo;
import static eu.europa.ec.leos.services.support.XmlHelper.ARTICLE;
import static eu.europa.ec.leos.services.support.XmlHelper.CHAPTER;
import static eu.europa.ec.leos.services.support.XmlHelper.EMPTY_STRING;
import static eu.europa.ec.leos.services.support.XmlHelper.CN;
import static eu.europa.ec.leos.services.support.XmlHelper.EC;
import static eu.europa.ec.leos.services.support.XmlHelper.LIST;
import static eu.europa.ec.leos.services.support.XmlHelper.ORGANIZATION;
import static eu.europa.ec.leos.services.support.XmlHelper.PARAGRAPH;
import static eu.europa.ec.leos.services.support.XmlHelper.PART;
import static eu.europa.ec.leos.services.support.XmlHelper.PERSON;
import static eu.europa.ec.leos.services.support.XmlHelper.POINT;
import static eu.europa.ec.leos.services.support.XmlHelper.ROLE;
import static eu.europa.ec.leos.services.support.XmlHelper.SECTION;
import static eu.europa.ec.leos.services.support.XmlHelper.SUBPARAGRAPH;
import static eu.europa.ec.leos.services.support.XmlHelper.SUBPOINT;
import static eu.europa.ec.leos.services.support.XmlHelper.TBLOCK;
import static eu.europa.ec.leos.services.support.XmlHelper.TITLE;
import static eu.europa.ec.leos.services.support.XmlHelper.removeTag;

@Slf4j
public class TableOfContentHelper {

    public static final int DEFAULT_CAPTION_MAX_SIZE = 50;

    public static final List<String> ELEMENTS_WITHOUT_CONTENT = Collections.unmodifiableList(Arrays.asList(ARTICLE, SECTION, CHAPTER, TITLE, PART));
    public static final List<String> ELEMENTS_WITH_ONLY_TEXT = Collections.unmodifiableList(Arrays.asList(ROLE, PERSON, ORGANIZATION));
    private static final String MOVE_LABEL_SPAN_START_TAG = "<span class=\"leos-soft-move-label\">";
    private static final String MOVED_TITLE_SPAN_START_TAG = "<span class=\"leos-soft-move-title\">";
    private static final String SPAN_END_TAG = "</span>";
    private static final String SPACE = " ";
    private static final int MOVED_LABEL_SIZE = MOVED_TITLE_SPAN_START_TAG.length() + SPACE.length() + MOVE_LABEL_SPAN_START_TAG.length() + 2 * SPAN_END_TAG.length();
    
    private static String getMovedLabel(MessageHelper messageHelper) {
        return MOVE_LABEL_SPAN_START_TAG + messageHelper.getMessage("toc.edit.window.softmove.label") + SPAN_END_TAG;
    }

    private static Boolean shouldAddMoveLabel(TableOfContentItemVO tocItem) {
        return tocItem.isSoftActionRoot() != null && tocItem.isSoftActionRoot()
                && (MOVE_TO.equals(tocItem.getSoftActionAttr()) || SoftActionType.MOVE_FROM.equals(tocItem.getSoftActionAttr()));
    }

    public static String buildItemCaption(TableOfContentItemVO tocItem, int captionMaxSize, MessageHelper messageHelper, String language) {
        Validate.notNull(tocItem.getTocItem(), "Type should not be null");

        boolean shoudlAddMovedLabel = shouldAddMoveLabel(tocItem);

        StringBuilder itemDescription = tocItem.getTocItem().isItemDescription()
                ? new StringBuilder(getDisplayableTocItem(tocItem.getTocItem(), language, messageHelper)).append(SPACE)
                : new StringBuilder();

        if (shoudlAddMovedLabel) {
            itemDescription.insert(0, MOVED_TITLE_SPAN_START_TAG + SPACE);
        }

        if (!StringUtils.isEmpty(tocItem.getNumber()) && !StringUtils.isEmpty(tocItem.getHeading())) {
            if(tocItem.getTocItem().getAknTag().value().equalsIgnoreCase(tocItem.getNumber().trim())){
                tocItem.setNumber(StructureConfigUtils.HASH_NUM_VALUE);
            }
            itemDescription.append(tocItem.getNumber());
            if (tocItem.isSoleNumbered()) {
                    itemDescription.setLength(0);
                    itemDescription.append(tocItem.getNumber());
            }
            if (shoudlAddMovedLabel) {
                itemDescription.append(SPAN_END_TAG).append(getMovedLabel(messageHelper));
            }
            if (TBLOCK.equals(tocItem.getTocItem().getAknTag().name()) || StringUtils.isEmpty(tocItem.getContent())) {
                itemDescription.append(StructureConfigUtils.CONTENT_SEPARATOR).append(tocItem.getHeading());
            } else if (!StringUtils.isEmpty(tocItem.getContent())) {
                itemDescription.append(StructureConfigUtils.NUM_HEADING_SEPARATOR).append(tocItem.getHeading());
            }
        } else if (!StringUtils.isEmpty(tocItem.getNumber())) {
            SoftActionType softAction = tocItem.getNumSoftActionAttr();

            if(softAction != null){
                if (PARAGRAPH.equals(tocItem.getTocItem().getAknTag().value()) && (DELETE.equals(softAction))
                        && !MOVE_TO.equals(tocItem.getSoftActionAttr())) {
                    itemDescription.append("<span class=\"leos-soft-num-removed\">" + tocItem.getNumber() + SPAN_END_TAG);
                } else if (PARAGRAPH.equals(tocItem.getTocItem().getAknTag().value())
                        && SoftActionType.ADD.equals(softAction) && !MOVE_TO.equals(tocItem.getSoftActionAttr())) {
                    itemDescription.append("<span class=\"leos-soft-num-new\">" + tocItem.getNumber() + SPAN_END_TAG);
                }
                captionMaxSize = captionMaxSize+itemDescription.length();
            } else {
                if (tocItem.isIndented() && !tocItem.getNumber().equals(tocItem.getIndentOriginNumValue())) {
                    itemDescription.append("<span class=\"leos-soft-num-new\">" + tocItem.getNumber() + SPAN_END_TAG);
                    captionMaxSize = captionMaxSize+itemDescription.length();
                } else {
                    itemDescription.append(tocItem.getNumber());
                }
                if (shoudlAddMovedLabel) {
                    itemDescription.append(SPAN_END_TAG).append(getMovedLabel(messageHelper));
                }
            }
        } else if (!StringUtils.isEmpty(tocItem.getHeading())) {
            itemDescription.append(tocItem.getHeading());
            if (shoudlAddMovedLabel) {
                itemDescription.append(SPAN_END_TAG).append(getMovedLabel(messageHelper));
            }
        } else if (shoudlAddMovedLabel) {
            itemDescription.append(SPAN_END_TAG).append(getMovedLabel(messageHelper));
        }

        if (tocItem.getTocItem().isContentDisplayed()) {
            itemDescription.append(itemDescription.length() > 0 ? StructureConfigUtils.CONTENT_SEPARATOR : "").append(removeTag(tocItem.getContent()));
        }

        // contains mathJax ?
        if (itemDescription.toString().contains("\\(") && itemDescription.toString().contains("\\)")) {
            int startIndex = itemDescription.toString().indexOf("\\(") + 3; // the three dots
            int endIndex = itemDescription.toString().indexOf("\\)") + 5; // 2 characters + the three dots
            captionMaxSize = shoudlAddMovedLabel ? captionMaxSize + MOVED_LABEL_SIZE : captionMaxSize;
            if (startIndex >= endIndex || endIndex < captionMaxSize || startIndex > captionMaxSize) {
                return StringUtils.abbreviate(itemDescription.toString(), captionMaxSize);
            } else {
                return StringUtils.abbreviate(itemDescription.toString(), endIndex);
            }
        } else {
            return StringUtils.abbreviate(itemDescription.toString(), shoudlAddMovedLabel ? captionMaxSize + MOVED_LABEL_SIZE : captionMaxSize);
        }
    }

    public static String getDisplayableTocItem(TocItem tocItem, String language, MessageHelper messageHelper) {
        try {
            if (tocItem != null) {
                NumberingType numberingType = StructureConfigUtils.getNumberingTypeByLanguage(tocItem, language);

                if (numberingType.equals(NumberingType.BULLET_NUM)) {
                    return messageHelper.getMessage("toc.item.type.bullet");
                } else {
                    String aknTagValue = tocItem.getAknTag() != null ? tocItem.getAknTag().value().toLowerCase() : "unknown";
                    return messageHelper.getMessage("toc.item.type." + aknTagValue);
                }
            }
        } catch (Exception e) {
            log.error("An unexpected error occurred while getting toc item", e);
            throw new RuntimeException("An unexpected error occurred while getting toc item", e);
        }
        return null;
    }
    
    public static String getItemSoftStyle(TableOfContentItemVO tableOfContentItemVO) {
        String itemSoftStyle = EMPTY_STRING;
        if (tableOfContentItemVO.getTrackChangeAction() != null) {
            if (hasTocItemTrackChangeAction(tableOfContentItemVO, TrackChangeActionType.DELETE) && hasTocItemSoftAction(tableOfContentItemVO, MOVE_TO)) {
                itemSoftStyle = "leos-soft-movedto";
            } else if (hasTocItemTrackChangeAction(tableOfContentItemVO, TrackChangeActionType.ADD) && hasTocItemSoftAction(tableOfContentItemVO,
                    MOVE_FROM)) {
                itemSoftStyle = "leos-soft-movedfrom";
            } else if (hasTocItemTrackChangeAction(tableOfContentItemVO, TrackChangeActionType.ADD)) {
                itemSoftStyle = "leos-soft-new";
            } else if (hasTocItemTrackChangeAction(tableOfContentItemVO, TrackChangeActionType.DELETE)) {
                String initialNum = tableOfContentItemVO.getInitialNum();
                if (initialNum != null) {
                    initialNum = initialNum.replace("Article ", "");
                    tableOfContentItemVO.setNumber(initialNum);
                }
                itemSoftStyle = "leos-soft-removed";
            }
        } else if (tableOfContentItemVO.getSoftActionAttr() != null) {
            if (hasTocItemSoftAction(tableOfContentItemVO, ADD)) {
                itemSoftStyle = "leos-soft-new";
            } else if (hasTocItemSoftAction(tableOfContentItemVO, DELETE)) {
                String initialNum = tableOfContentItemVO.getInitialNum();
                if (initialNum != null) {
                    initialNum = initialNum.replace("Article ", "");
                    tableOfContentItemVO.setNumber(initialNum);
                }
                itemSoftStyle = "leos-soft-removed";
            } else if (hasTocItemSoftAction(tableOfContentItemVO, MOVE_TO)) {
                itemSoftStyle = "leos-soft-movedto";
            } else if (hasTocItemSoftAction(tableOfContentItemVO, MOVE_FROM)) {
                itemSoftStyle = "leos-soft-movedfrom";
            }
        }
        return itemSoftStyle;
    }

    public static boolean hasTocItemSoftAction(final TableOfContentItemVO item, SoftActionType actionType) {
        return item != null && item.getSoftActionAttr() != null
                && (item.getSoftActionAttr().equals(actionType));
    }

    public static boolean hasTocItemTrackChangeAction(final TableOfContentItemVO item, TrackChangeActionType actionType) {
        return item != null && item.getTrackChangeAction() != null
                && (item.getTrackChangeAction().equals(actionType.getTrackChangeAction()));
    }

    public static boolean hasTocItemTrackChangeAction(final TableOfContentItemVO item, String actionType) {
        return item != null && item.getTrackChangeAction() != null
                && (item.getTrackChangeAction().equals(actionType));
    }

    public static boolean hasTocItemSoftOrigin(final TableOfContentItemVO item, final String softOriginValue) {
        return item != null && item.getOriginAttr() != null && item.getOriginAttr().equals(softOriginValue);
    }

    public static boolean isTocItemFirstChild(TableOfContentItemVO item, TableOfContentItemVO child) {
        return getTagValueFromTocItemVo(item).equals(LIST) && getTagValueFromTocItemVo(child).equals(SUBPARAGRAPH) && item.getChildItems().indexOf(child) == 0 ?
                item.getParentItem().getChildItemsView().indexOf(child.getParentItem()) == 0 : item.getChildItems().indexOf(child) == 0;
    }

    public static boolean isFirstSubParagraph(TableOfContentItemVO item) {
        return item.getParentItem() != null
                && Arrays.asList(SUBPARAGRAPH, SUBPOINT).contains(getTagValueFromTocItemVo(item))
                && isTocItemFirstChild(item.getParentItem(), item);
    }

    public static boolean hasSameSoftAction(final TableOfContentItemVO item, final TableOfContentItemVO item2) {
        return item != null && item2 != null && Objects.equals(item.getSoftActionAttr(), item2.getSoftActionAttr());
    }

    public static int getTocItemChildPosition(TableOfContentItemVO item, TableOfContentItemVO child) {
        return item.getChildItems().indexOf(child);
    }

    public static Optional<TableOfContentItemVO> getItemFromTocById(String elementId, List<TableOfContentItemVO> toc) {
        Optional<TableOfContentItemVO> hasIndentedItem = Optional.empty();
        for (TableOfContentItemVO root : toc) {
            hasIndentedItem = getTocElementById(elementId, root);
            if (hasIndentedItem.isPresent()) {
                break;
            }
        }
        return hasIndentedItem;
    }

    public static Optional<TableOfContentItemVO> getTocElementById(final String elementId, final TableOfContentItemVO item) {
        if (item.getId().equals(elementId)) {
            return Optional.of(item);
        }
        for (TableOfContentItemVO child : item.getChildItems()) {
            Optional<TableOfContentItemVO> childItem = getTocElementById(elementId, child);
            if (childItem.isPresent()) {
                return childItem;
            }
        }
        return Optional.empty();
    }

    public static boolean isElementInToc(final Element element, final List<TableOfContentItemVO> toc) {
        for (TableOfContentItemVO itemVO: toc) {
            Optional<TableOfContentItemVO> item = getTocElementById(element.getElementId(), itemVO);
            if (item.isPresent()) {
                return true;
            }
        }
        return false;
    }

    public static List<TableOfContentItemVO> getChildrenWithTags(TableOfContentItemVO parent, List<String> tags) {
        return parent.getChildItemsView().stream().filter(child -> tags.contains(getTagValueFromTocItemVo(child))).collect(Collectors.toList());
    }

    public static int getItemIndentLevel(TableOfContentItemVO item, int startingDepth, List<String> tags) {
        TableOfContentItemVO parent = item.getParentItem();
        while (parent != null) {
            if (ArrayUtils.contains(tags.toArray(), TableOfContentProcessor.getTagValueFromTocItemVo(parent))) {
                startingDepth++;
            }
            parent = parent.getParentItem();
        }

        return startingDepth;
    }

    public static TableOfContentItemVO getFirstAscendant(TableOfContentItemVO tocItem, List<String> tagNames) {
        if (tocItem != null) {
            if (tagNames.contains(getTagValueFromTocItemVo(tocItem))) {
                return tocItem;
            } else {
                return getFirstAscendant(tocItem.getParentItem(), tagNames);
            }
        } else {
            return null;
        }
    }

    public static void convertArticle(List<TocItem> tocItems, TableOfContentItemVO article, TocItemTypeName oldValue, TocItemTypeName newValue,
            String language) {
        updateTocItemsNumberingConfig(tocItems, article
                   , StructureConfigUtils.getNumberingTypeByTagNameAndTocItemType(tocItems, oldValue, POINT, language)
                   , StructureConfigUtils.getNumberingTypeByTagNameAndTocItemType(tocItems, newValue, POINT, language), language);
    }

    private static void updateTocItemsNumberingConfig(List<TocItem> tocItems, TableOfContentItemVO item, NumberingType fromNumberingType,
            NumberingType toNumberingType, String language) {
        for (TableOfContentItemVO child : item.getChildItems()) {
            NumberingType numberingType = StructureConfigUtils.getNumberingTypeByLanguage(child.getTocItem(), language);
            if (numberingType.equals(fromNumberingType)) {
                TocItem tocItem = StructureConfigUtils.getTocItemByNumberingType(tocItems, toNumberingType, child.getTocItem().getAknTag().name(), language);
                child.setTocItem(tocItem);
            }
            child.setAffected(true);
            updateTocItemsNumberingConfig(tocItems, child, fromNumberingType, toNumberingType, language);
        }
    }

    public static List<TableOfContentItemVO> getSiblings(TableOfContentItemVO item, String tagName) {
        List<TableOfContentItemVO> siblings = new ArrayList<>();
        TableOfContentItemVO parent = item.getParentItem();
        int index = parent != null ? parent.getChildItemsView().indexOf(item) : -1;
        if (index > -1 && parent != null) {
            for (int i = 0; i < parent.getChildItemsView().size(); i++) {
                TableOfContentItemVO sibling = parent.getChildItemsView().get(i);
                if (i != index && getTagValueFromTocItemVo(sibling).equalsIgnoreCase(tagName)) {
                    siblings.add(sibling);
                }
            }
        }
        return siblings;
    }

    public static boolean containsOnlySubpoints(TableOfContentItemVO item) {
        boolean containsOnlySubpoints = getTagValueFromTocItemVo(item).equals(LIST);
        if (containsOnlySubpoints) {
            for (TableOfContentItemVO child : item.getChildItemsView()) {
                if (Arrays.asList(NUMBERED_ITEMS).contains(getTagValueFromTocItemVo(child))) {
                    return false;
                }
            }
        }
        return containsOnlySubpoints;
    }

    public static void moveChildren(TableOfContentItemVO source, TableOfContentItemVO target) {
        List<TableOfContentItemVO> children = new ArrayList<>();
        children.addAll(source.getChildItems());

        int index = source.getParentItem().getChildItemsView().indexOf(source);
        if (source.getParentItem().equals(target) && index != -1 && !Arrays.asList(UNUMBERED_ITEMS).contains(getTagValueFromTocItemVo(source))) {
            for (TableOfContentItemVO child : children) {
                source.removeChildItem(child);
                target.addChildItem(index, child);
                index++;
            }
        } else {
            for (TableOfContentItemVO child : children) {
                source.removeChildItem(child);
                target.addChildItem(child);
            }
        }
    }

    public static void manageListContainingOnlySubpoints(TableOfContentItemVO item) {
        if (TableOfContentHelper.containsOnlySubpoints(item)) {
            checkSiblingList(item);
            moveChildren(item, item.getParentItem());
            item.getParentItem().removeChildItem(item);
        }
    }

    private static void checkSiblingList(TableOfContentItemVO item) {
        if (item != null && (hasTocItemSoftOrigin(item, EC) || item.getSoftActionAttr() != null)) {
            List<TableOfContentItemVO> siblings = TableOfContentHelper.getSiblings(item, LIST);
            for (TableOfContentItemVO sibling : siblings) {
                if (hasTocItemSoftOrigin(sibling, CN) && sibling.getSoftActionAttr() == null) {
                    copyListAttributes(item, sibling);
                    return;
                }
            }
        }
    }

    private static void copyListAttributes(TableOfContentItemVO source, TableOfContentItemVO target) {
        target.setId(source.getId());
        target.setOriginAttr(source.getOriginAttr());
        target.setSoftActionAttr(source.getSoftActionAttr());
        target.setSoftUserAttr(source.getSoftUserAttr());
        target.setSoftDateAttr(source.getSoftDateAttr());
        target.setSoftActionRoot(source.isSoftActionRoot());
        if (hasTocItemSoftAction(source, MOVE_FROM)) {
            target.setSoftMoveFrom(source.getSoftMoveFrom());
        }
    }

    public static void removeChildItem(TableOfContentItemVO parent, TableOfContentItemVO child) {
        if (parent != null && child != null) {
            if (listHasAnIntro(child)) {
                TableOfContentItemVO subpara = child.getChildItemsView().get(0);
                int index = child.getParentItem().getChildItemsView().indexOf(child);
                child.removeChildItem(subpara);
                child.getParentItem().addChildItem(index, subpara);
            }
            if (listHasAConclusion(child)) {
                TableOfContentItemVO subpara = child.getChildItemsView().get(child.getChildItemsView().size()-1);
                int index = child.getParentItem().getChildItemsView().indexOf(child);
                child.removeChildItem(subpara);
                child.getParentItem().addChildItem(index + 1, subpara);
            }
            parent.removeChildItem(child);
        }
    }

    public static void addChildItem(TableOfContentItemVO parent, int index, TableOfContentItemVO child) {
        if (parent != null && child != null) {
            if (listHasAnIntro(parent) && index == 0 && Arrays.asList(NUMBERED_ITEMS).contains(getTagValueFromTocItemVo(child))) {
                parent.addChildItem(1, child);
            } else if (listHasAnIntro(parent) && index == 0 && SUBPARAGRAPH.equals(getTagValueFromTocItemVo(child))) {
                int parentIndex = parent.getParentItem().getChildItemsView().indexOf(parent);
                if (parentIndex > -1) {
                    parent.getParentItem().addChildItem(parentIndex, child);
                }
            } else if (listHasAConclusion(parent) && index == parent.getChildItemsView().size()) {
                addChildItem(parent, child);
            } else {
                parent.addChildItem(index, child);
            }
        }
    }

    public static void addChildItem(TableOfContentItemVO parent, TableOfContentItemVO child) {
        if (parent != null && child != null) {
            if (listHasAConclusion(parent) && Arrays.asList(NUMBERED_ITEMS).contains(getTagValueFromTocItemVo(child))) {
                parent.addChildItem(parent.getChildItemsView().size() - 1, child);
            } else if (listHasAConclusion(parent) && SUBPARAGRAPH.equals(getTagValueFromTocItemVo(child))) {
                int index = parent.getParentItem().getChildItemsView().indexOf(parent) + 1;
                if (index < parent.getParentItem().getChildItemsView().size()) {
                    parent.getParentItem().addChildItem(index, child);
                } else {
                    parent.getParentItem().addChildItem(child);
                }
            } else {
                parent.addChildItem(child);
            }
        }
    }

    public static boolean listHasAnIntro(TableOfContentItemVO item) {
        return getTagValueFromTocItemVo(item).equals(LIST)
                && !item.getChildItemsView().isEmpty()
                && getTagValueFromTocItemVo(item.getChildItemsView().get(0)).equals(SUBPARAGRAPH);
    }

    public static boolean listHasAConclusion(TableOfContentItemVO item) {
        return getTagValueFromTocItemVo(item).equals(LIST)
                && !item.getChildItemsView().isEmpty() && item.getChildItemsView().size() > 1
                && getTagValueFromTocItemVo(item.getChildItemsView().get(item.getChildItemsView().size()-1)).equals(SUBPARAGRAPH);
    }

    public static TableOfContentItemVO getFirstChildWithTagName(TableOfContentItemVO item, List<String> tagNames) {
        for (TableOfContentItemVO child: item.getChildItemsView()) {
            if (tagNames.contains(getTagValueFromTocItemVo(child))) {
                return child;
            }
        }
        return null;
    }
}
