/*
 * Copyright 2024 European Union
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 *     https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package integration.importoj;

import eu.europa.ec.leos.domain.repository.document.Bill;
import eu.europa.ec.leos.integration.ExternalDocumentProvider;
import eu.europa.ec.leos.services.importoj.ConversionHelper;
import eu.europa.ec.leos.services.importoj.ImportServiceImpl;
import eu.europa.ec.leos.services.numbering.NumberServiceProposalTest;
import eu.europa.ec.leos.services.processor.content.XmlContentProcessor;
import eu.europa.ec.leos.services.processor.content.XmlContentProcessorProposal;
import eu.europa.ec.leos.services.support.XPathCatalog;
import eu.europa.ec.leos.services.support.XercesUtils;
import eu.europa.ec.leos.services.util.TestUtils;
import eu.europa.ec.leos.vo.toc.TableOfContentItemVO;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.w3c.dom.Document;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;

import static eu.europa.ec.leos.services.support.XercesUtils.createXercesDocument;
import static eu.europa.ec.leos.services.util.TestUtils.squeezeXmlAndRemoveAllNS;
import static eu.europa.ec.leos.test.support.model.ModelHelper.createBillForBytes;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.spy;

public class ImportServiceProposalTest_IT extends NumberServiceProposalTest {

    @Mock
    protected ExternalDocumentProvider externalDocumentProvider;
    @Mock
    protected ConversionHelper conversionHelper;

    @InjectMocks
    protected XmlContentProcessor xmlContentProcessor = Mockito.spy(new XmlContentProcessorProposal());
    @InjectMocks
    protected XPathCatalog xPathCatalog = spy(new XPathCatalog());

    protected ImportServiceImpl importService;

    protected final static String PREFIX_FILE = "/importoj/";
    protected final static String PREFIX_FILE_EC = "/importoj/ec/";

    @Override
    protected void getStructureFile() {
        docTemplate = "BL-023";
        configFile = "/structure-test-bill-EC.xml";
    }

    @Before
    public void onSetUp() {
        super.setup();
        importService = new ImportServiceImpl(externalDocumentProvider, conversionHelper, xmlContentProcessor, numberService, xPathCatalog, documentLanguageContext);
    }

    @Test
    public void test_importElement() {
        final byte[] xmlInput = TestUtils.getFileContent(PREFIX_FILE, "test_importElement_154Articles.xml");
        final byte[] xmlStart = TestUtils.getFileContent(PREFIX_FILE_EC, "test_importElement_start.xml");
        final byte[] xmlExpected = TestUtils.getFileContent(PREFIX_FILE_EC, "test_importElement_expected.xml");

        final Bill originalDocument = createBillForBytes(xmlStart);
        List<String> elementsIds = new ArrayList<>();
        IntStream.range(1, 100).forEach(val -> elementsIds.add("art_" + val));  //total are 155, import only first 100
        List<TableOfContentItemVO> tocList = new ArrayList<TableOfContentItemVO>();

        // When
        long startTime = System.currentTimeMillis();
        byte[] xmlResult = importService.insertSelectedElements(originalDocument, xmlInput, elementsIds, tocList);
        long endTime = System.currentTimeMillis();

        // Then
        String result = new String(xmlResult);
        String expected = new String(xmlExpected);
        result = squeezeXmlAndRemoveAllNS(result);
        expected = squeezeXmlAndRemoveAllNS(expected);
        assertEquals(expected, result);
        assertTrue(endTime - startTime < 25_000);  // check how you are converting Node to String. The time shouldn't go exponential.
    }

    @Test
    public void test_removeINP() {
        final byte[] xmlInput = TestUtils.getFileContent(PREFIX_FILE, "test_importElement_154Articles_with_NO_IND_or_WRP.xml");
        final byte[] xmlStart = TestUtils.getFileContent(PREFIX_FILE_EC, "test_importElement_start_with_IND_and_WRP.xml");
        final byte[] xmlExpected = TestUtils.getFileContent(PREFIX_FILE_EC, "test_importElement_expected_without_IND_or_WRP.xml");

        final Bill originalDocument = createBillForBytes(xmlStart);
        List<String> elementsIds = new ArrayList<>();
        List<TableOfContentItemVO> tocList = new ArrayList<TableOfContentItemVO>();

        // When
        byte[] xmlResult = importService.insertSelectedElements(originalDocument, xmlInput, elementsIds, tocList);

        // Then
        String result = new String(xmlResult);
        String expected = new String(xmlExpected);
        result = squeezeXmlAndRemoveAllNS(result);
        expected = squeezeXmlAndRemoveAllNS(expected);
        assertEquals(expected, result);
    }

    @Test
    public void test_addMoreThanOneWRPParagraph() {
        final byte[] xmlInput = TestUtils.getFileContent(PREFIX_FILE, "test_addMoreThanOneWRPParagraph.xml");
        final byte[] xmlStart = TestUtils.getFileContent(PREFIX_FILE_EC, "test_importElement_start_with_NO_IND_or_WRP.xml");
        final byte[] xmlExpected = TestUtils.getFileContent(PREFIX_FILE_EC, "test_addMoreThanOneWRPParagraph_expected.xml");

        final Bill originalDocument = createBillForBytes(xmlStart);
        List<String> elementsIds = new ArrayList<>();
        elementsIds.add("art_2");
        List<TableOfContentItemVO> tocList = new ArrayList<TableOfContentItemVO>();

        // When
        byte[] xmlResult = importService.insertSelectedElements(originalDocument, xmlInput, elementsIds, tocList);

        // Then
        String result = new String(xmlResult);
        String expected = new String(xmlExpected);
        result = squeezeXmlAndRemoveAllNS(result);
        expected = squeezeXmlAndRemoveAllNS(expected);
        assertEquals(expected, result);
    }

    @Test
    public void test_importElement_single_checkCorrectNamespaces() {
        final byte[] xmlInput = TestUtils.getFileContent(PREFIX_FILE, "test_importElement_154Articles.xml");
        final byte[] xmlStart = TestUtils.getFileContent(PREFIX_FILE_EC, "test_importElement_start.xml");
        final byte[] xmlExpected = TestUtils.getFileContent(PREFIX_FILE_EC, "test_importElement_single_expected.xml");

        final Bill originalDocument = createBillForBytes(xmlStart);
        List<String> elementsIds = new ArrayList<>();
        IntStream.range(1, 2).forEach(val -> elementsIds.add("art_" + val));  //total are 155, import only 1
        List<TableOfContentItemVO> tocList = new ArrayList<TableOfContentItemVO>();

        // When
        byte[] xmlResult = importService.insertSelectedElements(originalDocument, xmlInput, elementsIds, tocList);

        // Then
        Document document = createXercesDocument(xmlResult);
        String actualResult = XercesUtils.nodeToString(document);
        assertFalse(actualResult.contains("xmlns:fmx"));
        assertFalse(actualResult.contains("xmlns:fn"));

        String result = new String(xmlResult);
        String expected = new String(xmlExpected);
        result = squeezeXmlAndRemoveAllNS(result);
        expected = squeezeXmlAndRemoveAllNS(expected);
        assertEquals(expected, result);
    }

    @Test
    public void test_importElement_wrongStructure() {
        final byte[] xmlInput = TestUtils.getFileContent(PREFIX_FILE_EC, "test_importElement_wrongStructure.xml");
        final byte[] xmlStart = TestUtils.getFileContent(PREFIX_FILE_EC, "test_importElement_start.xml");
        final byte[] xmlExpected = TestUtils.getFileContent(PREFIX_FILE_EC, "test_importElement_wrongStructure_expected.xml");

        final Bill originalDocument = createBillForBytes(xmlStart);
        List<String> elementsIds = Arrays.asList("art_1");
        List<TableOfContentItemVO> tocList = new ArrayList<TableOfContentItemVO>();

        // When
        byte[] xmlResult = importService.insertSelectedElements(originalDocument, xmlInput, elementsIds, tocList);

        // Then
        Document document = createXercesDocument(xmlResult);
        String actualResult = XercesUtils.nodeToString(document);
        assertFalse(actualResult.contains("xmlns:fmx"));
        assertFalse(actualResult.contains("xmlns:fn"));

        String result = new String(xmlResult);
        String expected = new String(xmlExpected);
        result = squeezeXmlAndRemoveAllNS(result);
        expected = squeezeXmlAndRemoveAllNS(expected);
        assertEquals(expected, result);
    }

}
