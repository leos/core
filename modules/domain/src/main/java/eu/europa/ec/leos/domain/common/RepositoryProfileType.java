package eu.europa.ec.leos.domain.common;

public enum RepositoryProfileType {
    DEFAULT,
    CMIS,
    REST
}
