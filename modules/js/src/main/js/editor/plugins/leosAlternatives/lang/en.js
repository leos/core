CKEDITOR.plugins.setLang( 'leosAlternatives', 'en', {
    warningMsg: "<span>If any changes were made to the default content, selecting an alternative will discard them. Are you sure to continue?</span>",
    warningTitle: "Confirm alternative change"
});
