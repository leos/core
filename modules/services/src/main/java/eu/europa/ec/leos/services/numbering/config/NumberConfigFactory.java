package eu.europa.ec.leos.services.numbering.config;

import eu.europa.ec.leos.services.structure.StructureContext;
import eu.europa.ec.leos.services.support.XercesUtils;
import eu.europa.ec.leos.services.utils.StructureConfigUtils;
import eu.europa.ec.leos.vo.structure.NumberingConfig;
import eu.europa.ec.leos.vo.structure.NumberingType;
import eu.europa.ec.leos.vo.structure.TocItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.w3c.dom.Node;

import javax.inject.Provider;
import java.util.List;

import static eu.europa.ec.leos.services.support.XmlHelper.LEOS_LIST_TYPE_ATTR;
import static eu.europa.ec.leos.services.support.XmlHelper.NUM;
import static eu.europa.ec.leos.services.utils.StructureConfigUtils.getNumberingByName;
import static eu.europa.ec.leos.services.utils.StructureConfigUtils.getNumberingTypeByDepth;
import static eu.europa.ec.leos.services.utils.StructureConfigUtils.getTocItemByNumValue;
import static eu.europa.ec.leos.services.utils.StructureConfigUtils.getTocItemsByName;

@Configuration
public class NumberConfigFactory {

    private static final Logger LOG = LoggerFactory.getLogger(NumberConfigFactory.class);

    @Autowired
    protected Provider<StructureContext> structureContextProvider;

    public NumberConfig getNumberConfig(String elementName, int depth, final Node firstElement, String language) {
        List<TocItem> tocItems = structureContextProvider.get().getTocItems();
        List<NumberingConfig> numberingConfigs = structureContextProvider.get().getNumberingConfigs();
        String listTypeAttributeValue = XercesUtils.getAttributeValue(firstElement.getParentNode(), LEOS_LIST_TYPE_ATTR);
        List<TocItem> foundTocItems = getTocItemsByName(tocItems, elementName);
        NumberingType numberingType ;
        if (listTypeAttributeValue != null) {
            numberingType = NumberingType.fromValue(listTypeAttributeValue.toUpperCase());
            XercesUtils.removeAttribute(firstElement.getParentNode(), LEOS_LIST_TYPE_ATTR);
        } else if (foundTocItems.size() > 1 && XercesUtils.getFirstChild(firstElement, NUM) != null) {
            String currentNum = XercesUtils.getNodeNum(firstElement);
            TocItem tocItem = getTocItemByNumValue(numberingConfigs, foundTocItems, currentNum, depth, language);
            if (tocItem != null) {
                numberingType = StructureConfigUtils.getNumberingTypeByLanguage(tocItem, language);
            } else {
                numberingType = StructureConfigUtils.getNumberingTypeByLanguage(foundTocItems.get(0), language);
            }
        } else {
            numberingType = StructureConfigUtils.getNumberingTypeByLanguage(foundTocItems.get(0), language);
        }

        NumberingConfig numberingConfig = getNumberingByName(numberingConfigs, numberingType);
        if (depth != 0) { // if is a POINT, different config depending on the depth
            numberingType = getNumberingTypeByDepth(numberingConfig, depth);
            numberingConfig = getNumberingByName(numberingConfigs, numberingType); // update config
        }

        NumberConfig numberConfig = getNumberConfig(numberingType, numberingConfig);
        return numberConfig;
    }

    public NumberConfig getNumberConfigByNumberingType(NumberingType numberingType) {
        return getNumberConfig(numberingType);
    }

    private NumberConfig getNumberConfig(NumberingType numberingType) {
        List<NumberingConfig> numberingConfigs = structureContextProvider.get().getNumberingConfigs();
        NumberingConfig numberingConfig = getNumberingByName(numberingConfigs, numberingType);
        return getNumberConfig(numberingType, numberingConfig);
    }

    public NumberConfig getSoleNumberingConfig(String elementName, String language) {
        List<TocItem> tocItems = structureContextProvider.get().getTocItems();
        TocItem tocItem = StructureConfigUtils.getTocItemByName(tocItems, elementName);
        if (tocItem.getSoleNumbering() != null) {
            NumberingType numberingType = StructureConfigUtils.getSoleNumberingTypeByLanguage(tocItem, language);
            return getNumberConfig(numberingType);
        }
        return null;
    }

    private NumberConfig getNumberConfig(NumberingType numberingType, NumberingConfig numberingConfig) {
        String prefix = numberingConfig.getPrefix();
        String suffix = numberingConfig.getSuffix();
        switch (numberingType) {
            case ARABIC:
            case ARABIC_POSTFIXDOT:
            case ARABIC_PARENTHESIS:
                return new NumberConfigArabic(prefix, suffix);
            case ROMAN_LOWER_PARENTHESIS:
                return new NumberConfigRoman(false, prefix, suffix);
            case ROMAN_UPPER:
            case ROMAN_UPPER_POSTFIXDOT:
            case ROMAN_UPPER_POSTFIXPARENTHESIS:
                return new NumberConfigRoman(true, prefix, suffix);
            case ALPHA_LOWER_PARENTHESIS:
                return new NumberConfigAlpha(false, prefix, suffix);
            case GREEK_ALPHA_LOWER_PARENTHESIS:
                return new NumberConfigGreekAlpha(false, prefix, suffix);
            case CYRILLIC_ALPHA_LOWER_PARENTHESIS:
                return new NumberConfigCyrillicAlpha(false, prefix, suffix);
            case ALPHA_UPPER_POSTFIXDOT:
            case ALPHA_UPPER_POSTFIXPARENTHESIS:
                return new NumberConfigAlpha(true, prefix, suffix);
            case BULLET_BLACK_CIRCLE:
            case BULLET_WHITE_CIRCLE:
            case BULLET_BLACK_SQUARE:
            case BULLET_WHITE_SQUARE:
            case INDENT:
                return new NumberConfigSymbol(numberingConfig.getSequence(), prefix, suffix);
            case HIGHER_ELEMENT_NUM:
                return new NumberConfigArabic("","");
            case SOLE_ARTICLE:
            case SOLE_ARTICLE_CYRILLIC:
            case SOLE_ARTICLE_GREEK:
            case SOLE_RECITAL:
            case SOLE_RECITAL_CYRILLIC:
            case SOLE_RECITAL_GREEK:
                return new SoleNumberConfig(numberingConfig.getLabel(), true);
            case NONE:
                return null;
            default:
                throw new IllegalStateException("No configuration found for numbering: " + numberingConfig.getType());
        }
    }

}
