<?xml version="1.0" encoding="UTF-8"?>
<!--

    Copyright 2024 European Union

    Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
    You may not use this work except in compliance with the Licence.
    You may obtain a copy of the Licence at:

        https://joinup.ec.europa.eu/software/page/eupl

    Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the Licence for the specific language governing permissions and limitations under the Licence.

-->
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <parent>
        <groupId>eu.europa.ec.leos</groupId>
        <artifactId>repository</artifactId>
        <version>5.2.3-SNAPSHOT</version>
    </parent>

    <groupId>eu.europa.ec.leos.repository</groupId>
    <artifactId>config</artifactId>
    <packaging>jar</packaging>

    <name>Config</name>
    <description>Repository config</description>

    <dependencies>
        <!--Logging frameworks -->
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-api</artifactId>
        </dependency>
        <!-- Security framework -->
        <dependency>
            <groupId>org.springframework.security</groupId>
            <artifactId>spring-security-config</artifactId>
        </dependency>
    </dependencies>

    <profiles>
        <profile>
            <id>env-default</id>
            <activation>
                <property>
                    <name>!env</name>
                </property>
            </activation>
            <properties>
                <build.env>local</build.env>
            </properties>
        </profile>
        <profile>
            <id>env</id>
            <activation>
                <property>
                    <name>env</name>
                </property>
            </activation>
            <properties>
                <build.env>${env}</build.env>
            </properties>
        </profile>
    </profiles>

    <build>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-clean-plugin</artifactId>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-resources-plugin</artifactId>
                <executions>
                    <execution>
                        <id>filter-resources-common</id>
                        <phase>process-resources</phase>
                        <goals>
                            <goal>copy-resources</goal>
                        </goals>
                        <configuration>
                            <outputDirectory>target/generated-config</outputDirectory>
                            <filters>
                                <filter>src/etc/config/filters/${build.env}.properties</filter>
                            </filters>
                            <resources>
                                <resource>
                                    <!-- Filtered resource files. -->
                                    <filtering>true</filtering>
                                    <directory>src/etc/config/resources</directory>
                                    <includes>
                                        <include>**/*.properties</include>
                                        <include>**/*.wsdl</include>
                                    </includes>
                                </resource>
                            </resources>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>

</project>
