package eu.europa.ec.leos.vo.response;

import lombok.Data;

@Data
public class LeosClientResponse {
    private String name;
    private String displayName;
}

