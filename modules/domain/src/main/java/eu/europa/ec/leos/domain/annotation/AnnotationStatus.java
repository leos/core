package eu.europa.ec.leos.domain.annotation;

public enum AnnotationStatus {
    NORMAL,
    ACCEPTED,
    DELETED,
    ALL
}
