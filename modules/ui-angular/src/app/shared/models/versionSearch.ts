export type VersionSearchParams = {
  type: string;
  author: string;
};
