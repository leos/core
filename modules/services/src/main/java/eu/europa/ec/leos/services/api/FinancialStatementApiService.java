package eu.europa.ec.leos.services.api;

import eu.europa.ec.leos.domain.repository.document.FinancialStatement;

public interface FinancialStatementApiService extends BaseDocumentService<FinancialStatement>{

}
