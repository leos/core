package eu.europa.ec.leos.repository.mapping;

public interface RepositoryPropertiesMapper {

    String getId(RepositoryProperties prop);
}
