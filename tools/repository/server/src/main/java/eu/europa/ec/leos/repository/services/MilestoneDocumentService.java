package eu.europa.ec.leos.repository.services;

import eu.europa.ec.leos.repository.controllers.requests.QueryFilter;
import eu.europa.ec.leos.repository.entities.Document;
import eu.europa.ec.leos.repository.exceptions.RepositoryException;
import eu.europa.ec.leos.repository.model.LeosDocument;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

public interface MilestoneDocumentService {
    List<LeosDocument> findMilestonesByStatus(String status);

    List<LeosDocument> findMilestoneByName(final String fileName);

    Optional<LeosDocument> findMilestoneByRef(final String Ref);

    Optional<LeosDocument> findMilestoneById(final BigDecimal id);

    List<LeosDocument> findMilestoneByPackageId(final String pkgId, final boolean fetchContent) throws RepositoryException;

    LeosDocument createMilestoneFromContent(final Document doc, Map<String, ?> metadata,
                                            byte[] contentBytes, final String userId) throws RepositoryException;

    LeosDocument updateMilestoneMetadata(final BigDecimal milestoneId, Map<String, ?> properties, String userId) throws RepositoryException;

    LeosDocument updateMilestone(final Document doc, byte[] content, Map<String, ?> properties, String userId) throws RepositoryException;

    List<LeosDocument> findMilestonesUsingFilter(final String packageName, final Set<String> categories, final QueryFilter queryFilter, final int startIndex,
                                                 final int maxResults, final boolean fetchContent);

    long countMilestonesUsingFilter(final String packageName, final Set<String> categories, final QueryFilter queryFilter);

    void deleteMilestoneByRef(Document doc);
}
