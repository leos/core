package eu.europa.ec.leos.vo.toc;

public enum TocItemPosition {
    BEFORE,
    AS_CHILDREN,
    AFTER
}
