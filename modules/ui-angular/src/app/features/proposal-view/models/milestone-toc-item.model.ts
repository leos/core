export interface MilestoneTocItem {
  name: string;
  href: string;
  children: MilestoneTocItem[];
}
