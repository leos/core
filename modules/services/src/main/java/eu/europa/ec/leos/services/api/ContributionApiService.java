package eu.europa.ec.leos.services.api;

import eu.europa.ec.leos.domain.common.Result;
import eu.europa.ec.leos.domain.repository.LeosCategory;
import eu.europa.ec.leos.model.action.ContributionVO;
import eu.europa.ec.leos.services.collection.CreateCollectionResult;
import eu.europa.ec.leos.services.dto.request.ApplyContributionsRequest;
import eu.europa.ec.leos.services.dto.response.DocumentViewResponse;
import eu.europa.ec.leos.services.response.MergeContributionResponse;

import java.io.IOException;
import java.util.List;

public interface ContributionApiService {

    CreateCollectionResult createCloneProposal(String userLogin, String legDocumentName, String legFileId);

    Result<?> updateClonedProposalRevisionStatus(String proposalRef, String legFileId);

    List<ContributionVO> listContributionsForDocument(String documentRef);

    DocumentViewResponse compareAndShowRevision(String contextPath, String documentRef, String contributionsVersionRef, String legFileName) throws Exception;

    void declineContribution(String contributionVersionRef);

    void markContributionAsProcessed(String contributionVersionRef);

    MergeContributionResponse mergeContribution(String documentRef, ApplyContributionsRequest request) throws Exception;

    void sendFeedback(String proposalRef, String documentRef, String legFileName);
    
    void updateFeedbackAnnotations(String cloneProposalRef, String cloneLegFileName, String contributionsVersionRef) throws Exception;

    int countFeedbackAnnotationsFromLeg(String legFileName, String versionedReference, String proposalRef);

    void handleMilestoneAccept(String proposalRef, String clonedLegFileName, Boolean isAddedElseDeleted, String annexRef, LeosCategory category) throws IOException;

    void handleMilestoneReject(String proposalRef, String clonedLegFileName, String originalLegFileId, String annexRef, boolean isAdded);
}
