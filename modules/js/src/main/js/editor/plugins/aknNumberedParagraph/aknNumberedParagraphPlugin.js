/*
 * Copyright 2024 European Union
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 *     https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
; // jshint ignore:line
define(function aknNumberedParagraphPluginModule(require) {
    "use strict";

    // load module dependencies
    var pluginTools = require("plugins/pluginTools");
    var CKEDITOR = require("promise!ckEditor");
    var $ = require('jquery');
    var leosHierarchicalElementTransformerStamp = require("plugins/leosHierarchicalElementTransformer/hierarchicalElementTransformer");
    var leosKeyHandler = require("plugins/leosKeyHandler/leosKeyHandler");
    var renumberModule = require("plugins/leosNumber/listItemNumberModule");
    var leosPluginUtils = require("plugins/leosPluginUtils");
    var leosCommandStateHandler = require("plugins/leosCommandStateHandler/leosCommandStateHandler");
    var leosTrackChanges = require("plugins/leosTrackChanges/leosTrackChanges");

    var DATA_INDENT_ORIGIN_LEVEL = "data-indent-origin-indent-level";
    var DATA_INDENT_ORIGIN_NUMBER = "data-indent-origin-num";
    var DATA_INDENT_ORIGIN_NUMBER_ID = "data-indent-origin-num-id";
    var DATA_INDENT_ORIGIN_NUMBER_ORIGIN = "data-indent-origin-num-origin";
    var DATA_INDENT_ORIGIN_TYPE = "data-indent-origin-type";
    var LEOS_INDENT_ORIGIN_LEVEL = "leos:indent-origin-indent-level";
    var LEOS_INDENT_ORIGIN_NUMBER = "leos:indent-origin-num";
    var LEOS_INDENT_ORIGIN_NUMBER_ID = "leos:indent-origin-num-id";
    var LEOS_INDENT_ORIGIN_NUMBER_ORIGIN = "leos:indent-origin-num-origin";
    var LEOS_INDENT_ORIGIN_TYPE = "leos:indent-origin-type";

    var LOG = require("logger");
    var ENTER_KEY = 13;
    var UNDERLINE = CKEDITOR.CTRL + 85;

    var pluginName = "aknNumberedParagraph";
    var LIST_FROM_MATCH = /^(ul|ol)$/;
    var HTML_SUB_PARAGRAPH = "p";
    var HTML_PARAGRAPH = "li";
    var config = { attributes: false, childList: true, subtree: true };

    var CMD_NAME = "aknNumberedParagraph";
    var NUMBERED = CKEDITOR.TRISTATE_ON;
    var UNNUMBERED = CKEDITOR.TRISTATE_OFF;
    var DISABLED = CKEDITOR.TRISTATE_DISABLED;
    var PARA_MODE = NUMBERED;
    var SWITCHED = false;
    var PARA_SELECTOR = "*[data-akn-name='aknNumberedParagraph']";

    var DELETED = "deletedX";
    var TRANSFORMED = "trans";
    var DATA_AKN_ATTR_SOFTACTION = "data-akn-attr-softaction";
    var DATA_AKN_ATTR_SOFTTRANSFROM = "data-akn-attr-softtrans_from";
    var DATA_REFERS_TO = "refersto";
    var INP = "~INP";

    function blockNumbering(editor, command) {
        if (leosPluginUtils.isDefinitionArticle(editor)) {
            command.setState(DISABLED);
        }
    }

    var pluginDefinition = {
        icons: pluginName.toLowerCase(),
        init : function init(editor) {
            renumberModule.init(editor);

            editor.ui.addButton(pluginName, {
                label: 'Paragraph mode',
                command: CMD_NAME,
                toolbar: 'paragraphmode'
            });

            var paraCommand = editor.addCommand(CMD_NAME, {
                exec: function(editor) {
                   _changeParaMode(this, editor);
                }
            });

            leosKeyHandler.on({
                editor : editor,
                eventType : 'key',
                key : ENTER_KEY,
                action : _onEnterKey
            });

            leosKeyHandler.on({
                editor : editor,
                eventType : 'key',
                key : UNDERLINE,
                action : _onCtrlUKey
            });

            function _onCtrlUKey(context) {
                context.event.cancel();
            }

            editor.on("change", function(event) {
                blockNumbering(editor, paraCommand);
                var jqEditor = $(event.editor.editable().$);
                var article = jqEditor.find("*[data-akn-name='article']");
                if (article.length !== 0) {
                    var olElementFromArticle = article.find(">ol");
                    if (olElementFromArticle.length === 0) {
                        //TODO
                        if(PARA_MODE === NUMBERED){
                            article.append("<ol><li data-akn-num='1.'><br></li></ol>");
                        }else {
                            article.append("<ol><li ><br></li></ol>");
                        }
                        event.editor.getSelection().selectElement(new CKEDITOR.dom.node(article.find(">ol>li>br")[0]));
                    }
                }
            });

            editor.on("change", resetDataAknNameForOrderedList, null, null, 0);
            editor.on("change", resetNumbering, null, null, 1);
            editor.on("beforeCommandExec", _transformSubparagraphs, null, null, 0);
            editor.on("afterCommandExec", _checkParagraphsStructureAfterInsertSubparagraph, null, null, 100);
            editor.on("change", _transformSubparagraphs, null, null, 100);
            editor.on("receiveData", _startObservingAllParagraphs);
            editor.on("focus", _setCurrentParaMode, null, paraCommand);
            editor.on("dataReady", _setCurrentParaMode, null, paraCommand);
            editor.on('afterCommandExec', _restoreParagraphStructure, null, null, 0);
            editor.on('selectionChange', _onSelectionChange, null, null, 11);
            editor.on('instanceReady', function(event) {
                blockNumbering(event.editor, paraCommand);
            });
        }
    };

    function _onEnterKey(context) {
        LOG.debug("ENTER event intercepted: ", context.event);
        var selection = context.event.editor.getSelection();
        var startElement = leosKeyHandler.getSelectedElement(selection);

        // If we are in the first level paragraph and content is empty, it should be stopped
        // If content is not empty but the cursor is at the first character, it should NOT be stopped. LEOS-2831.
        if (leosKeyHandler.isContentEmptyTextNode(startElement) && isFirstLevelLiSelected(context)) {
            context.event.cancel();
        }
    }

    function _convertToParagraph(element) {
        element.renameNode(HTML_PARAGRAPH);
        element.setAttribute(leosPluginUtils.DATA_AKN_ELEMENT, leosPluginUtils.PARAGRAPH);
        element.setAttribute(leosPluginUtils.DATA_AKN_NAME, leosPluginUtils.AKN_NUMBERED_PARAGRAPH);
    }

    //Fix ckeditor enterKey plugin's behaviour when enter is pressed at the end of a sub-paragraph and restore the paragraph structure
    function _restoreParagraphStructure(event) {
        if (event.data.name === 'enter') {
            event.editor.fire('lockSnapshot', {"forceUpdate": true});
            var selectedElement = leosKeyHandler.getSelectedElement(event.editor.getSelection());
            var isSelectedElementSubParagraph = leosPluginUtils.getElementName(selectedElement) === HTML_SUB_PARAGRAPH && isFirstLevelLi(getClosestLiElement(selectedElement));
            var isSelectedElementEmpty = leosKeyHandler.isContentEmptyTextNode(selectedElement);
            var isOnlyChild = !selectedElement.hasNext() && !selectedElement.hasPrevious();
            var hasTextNext = leosPluginUtils.hasTextOrBogusAsNextSibling(selectedElement);
            var parent = selectedElement.getParent();
            var isParentSubParagraph = leosPluginUtils.getElementName(parent) === HTML_SUB_PARAGRAPH;

            if(isSelectedElementSubParagraph && isSelectedElementEmpty && (hasTextNext || isParentSubParagraph)){
                event.editor.fire('unlockSnapshot');
                selectedElement.insertBefore(parent);
                leosPluginUtils.setFocus(selectedElement, event.editor);
                if(isOnlyChild){
                    parent.appendBogus();
                }
            } else {
                event.editor.fire('unlockSnapshot');
            }

        }
    }

    function _checkParagraphsStructureAfterInsertSubparagraph(event) {
        if ((event.data.name === 'leosHierarchicalElementSubparagraphAfterLastPoint') && PARA_MODE === UNNUMBERED) {
            var editor = event.editor;
            var selectedElement = leosKeyHandler.getSelectedElement(editor.getSelection());
            var parent = selectedElement.getParent();
            var previousNode = selectedElement.hasPrevious() && selectedElement.getPrevious() instanceof CKEDITOR.dom.element
                ? selectedElement.getPrevious() : null;
            // transforms subparagraphs to paragraphs
            if (_isSubParagraph(selectedElement) && parent.is(HTML_PARAGRAPH) && previousNode.is('ol')) {
                _convertToParagraph(selectedElement);
                selectedElement.insertAfter(parent);
                leosPluginUtils.setFocus(selectedElement, editor);
            }
        }
    }

    function _startObservingAllParagraphs(event){
        var editor = event.editor;
        if(editor.editable && editor.editable().getChildren && editor.editable().getChildren().count() > 0){
            _addMutationObserverToParagraphList(_getFirstLevelOlElement(editor).$)
        }
    }

    function _addMutationObserverToParagraphList(paragraphList){
        if (paragraphList && !paragraphList.paragraphMutationObserver){
            paragraphList.paragraphMutationObserver = new MutationObserver(_processMutations);
            paragraphList.paragraphMutationObserver.observe(paragraphList, config);
        }
    }

    function _processMutations(mutationsList) {
        var mutations = _getMutations(mutationsList);
        leosPluginUtils.popSingleSubElement(mutations.singleSubParagraphs);
    }

    function _getMutations(mutationsList){
        var singleSubParagraphs = [];
        var isSubParagraphPushed = {};
        for(var i = 0; i < mutationsList.length; i++){
            _pushMutations(mutationsList[i].target, singleSubParagraphs, isSubParagraphPushed);
        }
        return {singleSubParagraphs: singleSubParagraphs};
    }

    function _pushMutations(node, singleSubParagraphs, isSubParagraphPushed){
        for (var i = 0; i < node.childNodes.length; i++){
            var child = node.childNodes[i];
            if(child.childNodes.length > 0){
                _pushMutations(child, singleSubParagraphs, isSubParagraphPushed);
            }
            _pushSingleSubParagraphs(node, child, singleSubParagraphs, isSubParagraphPushed);
        }
    }

    function _pushSingleSubParagraphs(node, child, singleSubParagraphs, isSubParagraphPushed){
        var isSingleSubParagraph = leosPluginUtils.getElementName(node) === HTML_PARAGRAPH && leosPluginUtils.getElementName(child) === HTML_SUB_PARAGRAPH
            && !child.previousSibling && !child.nextSibling;
        if(isSingleSubParagraph){
            var subParagraph = new CKEDITOR.dom.element(child);
            if(isFirstLevelLi(subParagraph.getParent()) && isSubParagraphPushed[subParagraph] !== 1){
                isSubParagraphPushed[subParagraph] = 1;
                singleSubParagraphs.push(subParagraph);
            }
        }
    }

    //This method sets the current paragraph mode (Numbered/Un-numbered) to the command state.
    function _setCurrentParaMode(event) {
        var cmd = event.listenerData;
        var jqEditor = $(event.editor.editable().$);
        var paragraphs = jqEditor.find(PARA_SELECTOR);
        var rootElement = event.editor.element.getFirst();
        var isAlternative = rootElement && rootElement.getAttribute("leos:alternative");

        if (paragraphs.length > 0) {
            if(isAlternative && rootElement.getName() === leosTrackChanges.core.ARTICLE) {
                PARA_MODE = paragraphs[0].getAttribute(leosPluginUtils.DATA_AKN_NUM) ? NUMBERED : UNNUMBERED;
            } else {
                PARA_MODE = paragraphs[0].getAttribute(leosPluginUtils.DATA_AKN_NUM)
                && !(paragraphs[0].getAttribute(leosPluginUtils.DATA_AKN_NUM_ID) && paragraphs[0].getAttribute(leosPluginUtils.DATA_AKN_NUM_ID).startsWith(DELETED))
                && paragraphs[0].getAttribute(leosTrackChanges.core.DATA_AKN_ACTION_NUMBER) !== leosTrackChanges.core.DELETE_ACTION
                  ? NUMBERED : UNNUMBERED;
            }
        }
        cmd.setState(PARA_MODE);
        blockNumbering(event.editor, cmd);
    }

    //This method toggle the existing paragraph mode (Numbered -> Un-numbered & vice-versa) based on user input.
    function _changeParaMode(cmd, editor) {
        PARA_MODE = cmd.state === NUMBERED ? UNNUMBERED : NUMBERED;
        SWITCHED = true;
        cmd.setState(PARA_MODE);
        blockNumbering(editor, cmd);
        if (PARA_MODE === UNNUMBERED) {
            transformSubparagraphs(editor);
        }
        editor.fire("change");
    }

    /*
     * Resets the numbering of the points depending on nesting level
     */
    var resetNumbering = function resetNumbering(event) {
        var ckEditor = event.editor;
        ckEditor.fire('lockSnapshot');
        var jqEditor = $(ckEditor.editable().$);
        var paragraphs = jqEditor.find(PARA_SELECTOR);
        var rootElement = ckEditor.element.getFirst();
        var isAlternative = rootElement && rootElement.getAttribute("leos:alternative");
        if (paragraphs.length > 0) {
            if (PARA_MODE === NUMBERED) {
                var renumber = true;
                if (ckEditor.LEOS.isTrackChangesEnabled) {
                    for (var ii = 0; ii < paragraphs.length; ii++) {
                        var dataAknNum = paragraphs[ii].getAttribute(leosPluginUtils.DATA_AKN_NUM);
                        leosTrackChanges.core.setOriginalNumber(paragraphs[ii], dataAknNum);
                        if (SWITCHED && dataAknNum && paragraphs[ii].getAttribute(leosTrackChanges.core.DATA_AKN_TC_ORIGINAL_NUMBER) === leosTrackChanges.core.UNNUMBERED) {
                            paragraphs[ii].removeAttribute(leosPluginUtils.DATA_AKN_NUM);
                            paragraphs[ii].removeAttribute(leosPluginUtils.DATA_AKN_NUM_ID);
                            leosTrackChanges.core.removeTrackChangesAttributesForNumbering(paragraphs[ii]);
                            renumber = false;
                        }
                    }
                }
                if (renumber && !(isAlternative && rootElement.getName() === leosTrackChanges.core.ARTICLE)) {
                    renumberModule.updateNumbers([paragraphs[0].parentElement], renumberModule.getSequences('Paragraph'));
                }
            } else {
                for (var ii = 0; ii < paragraphs.length; ii++) {
                    if (ckEditor.LEOS.isTrackChangesEnabled) {
                        var dataAknNum = paragraphs[ii].getAttribute(leosPluginUtils.DATA_AKN_NUM);
                        leosTrackChanges.core.setOriginalNumber(paragraphs[ii], dataAknNum);
                        if (dataAknNum && paragraphs[ii].getAttribute(leosTrackChanges.core.DATA_AKN_TC_ORIGINAL_NUMBER) === leosTrackChanges.core.UNNUMBERED) {
                            paragraphs[ii].removeAttribute(leosPluginUtils.DATA_AKN_NUM);
                            paragraphs[ii].removeAttribute(leosPluginUtils.DATA_AKN_NUM_ID);
                            leosTrackChanges.core.removeTrackChangesAttributesForNumbering(paragraphs[ii]);
                        } else if (dataAknNum && paragraphs[ii].getAttribute(leosTrackChanges.core.DATA_AKN_TC_ORIGINAL_NUMBER) === leosTrackChanges.core.NEW) {
                            paragraphs[ii].removeAttribute(leosPluginUtils.DATA_AKN_NUM);
                            paragraphs[ii].removeAttribute(leosPluginUtils.DATA_AKN_NUM_ID);
                        } else if (SWITCHED) {
                            leosTrackChanges.core.addTrackChangesAttributesForNumbering(ckEditor, paragraphs[ii], leosTrackChanges.core.DELETE_ACTION);
                        } else if (!dataAknNum
                            && paragraphs[ii].getAttribute(leosTrackChanges.core.DATA_AKN_TC_ORIGINAL_NUMBER) !== leosTrackChanges.core.UNNUMBERED
                            && paragraphs[ii].getAttribute(leosTrackChanges.core.DATA_AKN_TC_ORIGINAL_NUMBER) !== leosTrackChanges.core.NEW) {
                            leosTrackChanges.core.addTrackChangesAttributesForNumbering(ckEditor, paragraphs[ii], leosTrackChanges.core.DELETE_ACTION);
                        } else if (dataAknNum
                            && paragraphs[ii].getAttribute(leosTrackChanges.core.DATA_AKN_TC_ORIGINAL_NUMBER) === leosTrackChanges.core.UNNUMBERED
                            && paragraphs[ii].getAttribute(leosTrackChanges.core.DATA_AKN_ACTION_NUMBER) === leosTrackChanges.core.INSERT_ACTION) {
                            leosTrackChanges.core.addTrackChangesAttributesForNumbering(ckEditor, paragraphs[ii], leosTrackChanges.core.DELETE_ACTION);
                        }
                    } else {
                        if (paragraphs[ii].getAttribute(leosPluginUtils.DATA_ORIGIN) === "ec") {
                            var numId = paragraphs[ii].getAttribute(leosPluginUtils.DATA_AKN_NUM_ID);
                            if (numId == null || !numId.startsWith(DELETED)) {
                                paragraphs[ii].setAttribute(leosPluginUtils.DATA_AKN_NUM_ID, DELETED + numId);
                            }
                        } else {
                            paragraphs[ii].removeAttribute(leosPluginUtils.DATA_AKN_NUM);
                            paragraphs[ii].removeAttribute(leosPluginUtils.DATA_AKN_NUM_ID);
                        }
                    }
                }
            }
        }
        SWITCHED = false;
        if (!!ckEditor.getCommand('indent') && !!ckEditor.elementPath()) {
            ckEditor.getCommand('indent').refresh(ckEditor, ckEditor.elementPath());
        }
        ckEditor.fire('unlockSnapshot');
    }

     var getClosestLiElement = function getClosestLiElement(element) {
        return element.getAscendant('li', true);
    };

    var isFirstLevelLi = function isFirstLevelLi(liElement) {
        return !liElement.getAscendant('li');
    };

    var isFirstLevelOl = function isFirstLevelOl(olElement) {
        return !olElement.getAscendant('ol');
    };

    var isFirstLevelLiSelected = function isFirstLevelLiSelected(context) {
        var liElement = getClosestLiElement(context.firstRange.startContainer);
        return liElement && isFirstLevelLi(liElement);
    };

    function getClosestOlAncestor(selection) {
        var commonElementAncestor = selection.getCommonAncestor();
        commonElementAncestor = commonElementAncestor && commonElementAncestor.getAscendant("ol", true);
        return commonElementAncestor;
    }

    function insertBeforeNode(parentNodeIndex, listNode, currentNode, childNodeIndex) {
        parentNodeIndex++;
        listNode.insertBefore(currentNode);
        childNodeIndex--;
        return {
            parentNodeIndex: parentNodeIndex,
            childNodeIndex: childNodeIndex
        };
    }

    function _isSubParagraph(element) {
        return !!element && (element instanceof CKEDITOR.dom.element) && ((element.is(HTML_SUB_PARAGRAPH) && !element.getAttribute(leosPluginUtils.DATA_AKN_ELEMENT)
            && !element.getAttribute(leosPluginUtils.DATA_AKN_NAME)) || leosPluginUtils.isSubparagraph(element));
    }

    function _transformSubparagraphs(event) {
        if (PARA_MODE === UNNUMBERED && !!event.data && !!event.data.name && (event.data.name.includes("save")
            || event.data.name == "change")) {
            transformSubparagraphs(event.editor);
        }
    }

    // This method transforms subparagraphs into paragraphs when included in unnumbered paragraphs: ol/li/p to ol/li
    var transformSubparagraphs = function transformSubparagraphs(editor) {
        // transforms subparagraphs to paragraphs
        editor.fire('lockSnapshot');
        var firstLevelOlElt = _getFirstLevelOlElement(editor);
        if (firstLevelOlElt && firstLevelOlElt instanceof CKEDITOR.dom.element) {
            var paragraphNodes = firstLevelOlElt.getChildren();
            for (var paragraphNodeIndex=0; paragraphNodeIndex < paragraphNodes.count(); paragraphNodeIndex++) {
                var paragraphNode = paragraphNodes.getItem(paragraphNodeIndex);
                var currentParagraphNodeToBeDeleted = false;
                if (leosPluginUtils.getElementName(paragraphNode) === HTML_PARAGRAPH) {
                    if(paragraphNode.getAttribute("contenteditable") === "false"){
                        continue;
                    }
                    var childNodes = paragraphNode.getChildren();
                    var isFirstOccurrence = true;
                    for (var childNodeIndex=0; childNodeIndex < childNodes.count(); childNodeIndex++) {
                        var currentNode = childNodes.getItem(childNodeIndex);
                        var nextNode = currentNode.hasNext() ? currentNode.getNext() : null;
                        // Empty text nodes should be removed from children
                        if ((currentNode.$.nodeType === Node.TEXT_NODE) && (currentNode.$.textContent === '')) {
                            currentNode.remove();childNodeIndex--;
                        }
                        // Default behavior: when this is a subparagraph converts it to a paragraph
                        else if ((leosPluginUtils.getElementName(currentNode) === HTML_SUB_PARAGRAPH) && (!LIST_FROM_MATCH.test(leosPluginUtils.getElementName(nextNode)))) {
                            _convertToParagraph(currentNode);

                            if(isFirstOccurrence) {
                                if (paragraphNode.getAttribute(leosPluginUtils.DATA_AKN_NUM)) {
                                    currentNode.setAttribute(leosPluginUtils.DATA_AKN_NUM, paragraphNode.getAttribute(leosPluginUtils.DATA_AKN_NUM));
                                    currentNode.setAttribute(leosPluginUtils.DATA_AKN_NUM_ID, paragraphNode.getAttribute(leosPluginUtils.DATA_AKN_NUM_ID));
                                    if (paragraphNode.getAttribute('data-akn-action-number')) {
                                        currentNode.setAttribute('data-akn-action-number', paragraphNode.getAttribute('data-akn-action-number'));
                                    }
                                    if (paragraphNode.getAttribute('data-akn-tc-original-number')) {
                                        currentNode.setAttribute('data-akn-tc-original-number', paragraphNode.getAttribute('data-akn-tc-original-number'));
                                    }
                                    if (paragraphNode.getAttribute('data-akn-uid-number')) {
                                        currentNode.setAttribute('data-akn-uid-number', paragraphNode.getAttribute('data-akn-uid-number'));
                                    }
                                    if (paragraphNode.getAttribute('title-number')) {
                                        currentNode.setAttribute('title-number', paragraphNode.getAttribute('title-number'));
                                    }
                                }

                                currentNode.setAttribute(DATA_AKN_ATTR_SOFTACTION, TRANSFORMED);
                                currentNode.setAttribute(DATA_AKN_ATTR_SOFTTRANSFROM, paragraphNode.getAttribute("id"));
                                isFirstOccurrence = false;
                            }

                            if (childNodeIndex>0) {
                                currentNode.insertAfter(paragraphNodes.getItem(paragraphNodeIndex));childNodeIndex--;paragraphNodeIndex++;
                            }
                            //If this is the first child it should be inserted before
                            else {
                                //if current paragraph contains only sub paragraphs, the paragraph should be removed afterwards
                                if (childNodes.count() === 1) {
                                    currentParagraphNodeToBeDeleted = true;
                                }
                                currentNode.insertBefore(paragraphNode);childNodeIndex--;paragraphNodeIndex++;
                            }
                        }
                        // When this is a list converts intro and conclusion to a paragraph
                        else if (LIST_FROM_MATCH.test(leosPluginUtils.getElementName(currentNode))) {
                            var grandChildNodes = currentNode.getChildren();
                            var foundPoint = false;
                            for (var grandChildNodeIndex=0; grandChildNodeIndex < grandChildNodes.count(); grandChildNodeIndex++) {
                                var listNode = grandChildNodes.getItem(grandChildNodeIndex);
                                var nextListNode = grandChildNodes.count()>grandChildNodeIndex+1 ? grandChildNodes.getItem(grandChildNodeIndex+1) : null;
                                var prevListNode = grandChildNodeIndex>0 ? grandChildNodes.getItem(grandChildNodeIndex-1) : null;
                                foundPoint = !_isSubParagraph(listNode);
                                if (!foundPoint && _isSubParagraph(listNode) && _isSubParagraph(nextListNode)) {
                                    _convertToParagraph(listNode);
                                    var result = insertBeforeNode(paragraphNodeIndex, listNode, paragraphNode, grandChildNodeIndex);
                                    paragraphNodeIndex = result.parentNodeIndex;
                                    grandChildNodeIndex = result.childNodeIndex;
                                }
                                if (foundPoint && _isSubParagraph(listNode) && !!nextListNode && !_isSubParagraph(prevListNode)) {
                                    _convertToParagraph(listNode);
                                    listNode.insertAfter(paragraphNode);
                                    grandChildNodeIndex--;
                                }
                            }
                        }
                        // All other cases (except empty texts)
                        else if (!leosKeyHandler.isContentEmptyTextNode(currentNode)) {
                            if (LIST_FROM_MATCH.test(leosPluginUtils.getElementName(nextNode))) {
                                nextNode.insertAfter(currentNode); // If there is a list after, this list is included in the same paragraph
                            }
                        }
                    }
                    if (currentParagraphNodeToBeDeleted) {
                        var sel = editor.getSelection(),
                            range = sel.getRanges()[ 0 ],
                            cursor = range.clone();
                        var startContainerParents = cursor.startContainer.getParents();
                        var endContainerParents = cursor.endContainer.getParents();
                        paragraphNode.remove();paragraphNodeIndex--;
                        // To avoid bug of ticket LEOS-2734: when removing a selectable node, move the cursor to avoid bad positioning of it. Move cursor to the beginning of the paragrpah
                        leosPluginUtils.keepCursorPosition(startContainerParents, endContainerParents, editor, cursor);
                    }
                }
            }
        }
        editor.fire('unlockSnapshot');
    };

    function _getFirstLevelOlElement(editor) {
        var jqEditor = $(editor.editable().$);
        var article = jqEditor.find("*[data-akn-name='article']");
        var baseElementList = article.find(">ol");
        if (baseElementList) {
            return new CKEDITOR.dom.node(baseElementList[0]);
        }
        return null;
    }

    // Checks if an element is a text element or an inline element
    function _isTextOrInlineElement(element) {
        var elementName = leosPluginUtils.getElementName(element);
        return ((elementName === 'text') || CKEDITOR.dtd.$inline.hasOwnProperty(elementName));
    }

    function resetDataAknNameForOrderedList(event) {
        event.editor.fire('lockSnapshot');
        var closestOlAncestor = getClosestOlAncestor(event.editor.getSelection());
        if (closestOlAncestor) {
            var firstLevelLi = true;
            closestOlAncestor.forEach && closestOlAncestor.forEach(function(currentNode) {
                if (firstLevelLi && currentNode.getAscendant("li")) {
                    firstLevelLi = false;
                }
                var currentNodeName = leosPluginUtils.getElementName(currentNode);
                if (currentNodeName === "ol" && firstLevelLi) {
                    currentNode.removeAttribute("data-akn-name");
                }
                if (currentNodeName === "li" && firstLevelLi) {
                    currentNode.setAttribute("data-akn-name", "aknNumberedParagraph");
                    currentNode.setAttribute("data-akn-element", "paragraph");
                    // returning false so the iterator won't go to its children
                    return false;
                }
            });
        }
        event.editor.fire('unlockSnapshot');
    }

    pluginTools.addPlugin(pluginName, pluginDefinition);

    var leosHierarchicalElementTransformer = leosHierarchicalElementTransformerStamp({
        firstLevelConfig: {
            akn: 'paragraph',
            html: 'li',
            attr: [{
                akn: 'leos:editable',
                html: 'contenteditable'
            }, {
                akn: 'xml:id',
                html: 'id'
            }, {
                akn: 'leos:origin',
                html: 'data-origin'
            }, {
                akn: 'leos:softaction',
                html: 'data-akn-attr-softaction'
            }, {
                akn: 'leos:softactionroot',
                html: 'data-akn-attr-softactionroot'
            }, {
                akn: 'leos:softuser',
                html: 'data-akn-attr-softuser'
            }, {
                akn: 'leos:softdate',
                html: 'data-akn-attr-softdate'
            }, {
                akn: 'leos:softmove_from',
                html: 'data-akn-attr-softmove_from'
            }, {
                akn: 'leos:softmove_to',
                html: 'data-akn-attr-softmove_to'
            }, {
                akn: 'leos:softmove_label',
                html: 'data-akn-attr-softmove_label'
            }, {
                akn: 'leos:softtrans_from',
                html: 'data-akn-attr-softtrans_from'
            }, {
                akn: LEOS_INDENT_ORIGIN_TYPE,
                html: DATA_INDENT_ORIGIN_TYPE
            }, {
                akn: LEOS_INDENT_ORIGIN_LEVEL,
                html: DATA_INDENT_ORIGIN_LEVEL
            }, {
                akn: LEOS_INDENT_ORIGIN_LEVEL,
                html: DATA_INDENT_ORIGIN_LEVEL
            }, {
                akn: LEOS_INDENT_ORIGIN_NUMBER,
                html: DATA_INDENT_ORIGIN_NUMBER
            }, {
                akn: LEOS_INDENT_ORIGIN_NUMBER_ID,
                html: DATA_INDENT_ORIGIN_NUMBER_ID
            }, {
                akn: LEOS_INDENT_ORIGIN_NUMBER_ORIGIN,
                html: DATA_INDENT_ORIGIN_NUMBER_ORIGIN
            }, {
                html: 'data-akn-name=aknNumberedParagraph'
            }, {
                html: 'data-akn-element=paragraph'
            }, {
                akn : "leos:renumbered",
                html : "data-akn-attr-renumbered"
            }, {
                akn: "leos:action-number",
                html: "data-akn-action-number"
            }, {
                akn: "leos:uid-number",
                html: "data-akn-uid-number"
            }, {
                akn: "leos:title-number",
                html: "title-number"
            }, {
                akn: "leos:tc-original-number",
                html: "data-akn-tc-original-number"
            }, {
                akn : "leos:action",
                html : "data-akn-action"
            }, {
                akn : "leos:uid",
                html : "data-akn-uid"
            }, {
                akn: "leos:action-enter",
                html: "data-akn-action-enter"
            }, {
                akn: "leos:uid-enter",
                html: "data-akn-uid-enter"
            }, {
                akn: "leos:title-enter",
                html: "title-enter"
            }]
        },
        rootElementsForFrom: ['paragraph'],
        contentWrapperForFrom: 'subparagraph',
        rootElementsForTo: ['li']
    });
    
    var transformationConfig = leosHierarchicalElementTransformer.getTransformationConfig();
    
    // return plugin module
    var pluginModule = {
        name : pluginName,
        transformSubparagraphs: transformSubparagraphs,
        resetNumbering: resetNumbering,
        transformationConfig : transformationConfig
    };

    function _onSelectionChange(event) {
        let editor = event.editor;
        if (leosPluginUtils.isDefinitionArticle(editor)) {
            editor.getCommand(CMD_NAME).setState(DISABLED);
        }else{
            leosCommandStateHandler.changeCommandState(editor, CMD_NAME, null, true);
        }
    }

    pluginTools.addTransformationConfigForPlugin(transformationConfig, pluginName);

    return pluginModule;
});