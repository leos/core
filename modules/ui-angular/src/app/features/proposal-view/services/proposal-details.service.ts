import { HttpClient, HttpContext } from '@angular/common/http';
import { Injectable, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { EuiGrowlService } from '@eui/core';
import {
  Collaborator,
  CollaboratorRequest,
  CreateDraftBody,
  CreateDraftResponse,
  Document,
  ExceptionResponseVO,
  ErrorCode,
  LeosAppConfig,
  Permission,
  User
} from '@leos/shared';
import { TranslateService } from '@ngx-translate/core';
import { parse as parseContentDisposition } from 'content-disposition-attachment';
import {
  BehaviorSubject,
  combineLatestWith,
  filter,
  finalize,
  map,
  Observable,
  Subject,
  switchMap,
  takeUntil,
  tap,
} from 'rxjs';
import { apiBaseUrl } from 'src/config';

import { AppConfigService } from '@/core/services/app-config.service';
import { IS_ERROR_INTERCEPTION_ENABLED } from '@/core/services/error-handler.interceptor';
import { MilestoneDescriptor } from '@/shared/components/proposal-milestone-view/proposal-milestone-view.component';
import { LoadingService } from '@/shared/services/loading.service';
import { downloadBlob } from '@/shared/utils';

import { ExportPackageVO } from '../models/export-package.model';
import { Milestone } from '../models/milestone.model';
import {EuiDialogService} from "@eui/components/eui-dialog";

@Injectable({ providedIn: 'root' })
export class ProposalDetailsService implements OnDestroy {
  collaborators$: Observable<Collaborator[]>;
  userAutocompleteData$: Observable<User[]>;
  proposalDetails$: Observable<Document>;
  userInputFieldChange$: Observable<string>;
  milestones$: Observable<Milestone[]>;
  exportedDocuments$: Observable<ExportPackageVO[]>;
  permissions$: Observable<Permission[]>;
  clonedProposalCount: number;
  exceptionResponseVO: ExceptionResponseVO = null;

  private collaboratorsBS = new BehaviorSubject<Collaborator[]>([]);
  private userInputFieldChangeBS = new BehaviorSubject('');
  private proposalRefBS = new BehaviorSubject<[string, boolean]>([null, false]);
  private milestonesBS = new BehaviorSubject<Milestone[]>([]);
  private proposalDetailsResponse$ = this.proposalRefBS.pipe(
    switchMap(([_proposalRef, loading]) => this.getProposalDetails(loading, this.proposalRef)),
  );
  private permissionsBS = new BehaviorSubject<Permission[]>([]);

  private userAutocompleteDataResponse$ = this.userInputFieldChangeBS.pipe(
    filter((name) => name.length > 1),
    switchMap((name) => this.searchUsers(name)),
  );
  private destroy$ = new Subject<void>();
  private translated : boolean = false;

  constructor(
    private appConfig: AppConfigService,
    private http: HttpClient,
    private router: Router,
    private loadingService: LoadingService,
    private growlService: EuiGrowlService,
    private translateService: TranslateService,
    private dialogService: EuiDialogService,
  ) {
    this.userInputFieldChange$ = this.userInputFieldChangeBS.asObservable();

    this.proposalDetails$ = this.proposalDetailsResponse$.pipe(
      tap((res) => this.fetchCollaborators()),
      tap((res) => this.loadingService.setLoading(false)),
      tap((res) => this.loadProposalMilestones()),
    );

    this.userAutocompleteData$ = this.userAutocompleteDataResponse$;

    this.exportedDocuments$ = this.proposalRefBS.pipe(
      switchMap(() => this.getAllExportDocuments()),
    );
    this.collaborators$ = this.collaboratorsBS.asObservable();
    this.milestones$ = this.milestonesBS.asObservable();

    this.permissions$ = this.permissionsBS.asObservable();
    this.collaborators$
      .pipe(takeUntil(this.destroy$), combineLatestWith(this.appConfig.config))
      .subscribe(([collaborators, config]) => {
        const permissions = this.resolvePermissions(collaborators, config);
        this.permissionsBS.next(permissions);
      });

    this.clonedProposalCount = 0;
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
    this.clonedProposalCount = 0;
  }

  setUserAutocompleteInputChange(name: string) {
    this.userInputFieldChangeBS.next(name);
  }

  setProposalRef(proposalRef: string, loading = true) {
    this.proposalRefBS.next([proposalRef, loading]);
  }

  get proposalRef(): string {
    return this.proposalRefBS.getValue()[0];
  }

  setTranslasted(translated: boolean) {
    this.translated = translated;
  }

  getTranslated(): boolean {
    return this.translated;
  }

  createAnnex() {
    this.loadingService.setLoading(true);
    this.http
      .post<any>(
        `${apiBaseUrl}/secured/proposals/${this.proposalRef}/createAnnex`,
        {},
      )
      .subscribe((val) => {
        this.setProposalRef(this.proposalRef);
        this.loadingService.setLoading(false);
      });
  }

  updateAnnexTitle(annexId: string, annexTitle: string) {
    this.loadingService.setLoading(true);
    this.http
      .put<any>(
        `${apiBaseUrl}/secured/proposals/${this.proposalRef}/update-annex-title/${annexId}`,
        {},
        { params: { title: annexTitle } },
      )
      .subscribe((val) => {
        this.setProposalRef(this.proposalRef);
        this.loadingService.setLoading(false);
      });
  }

  deleteAnnex(annexRef: string) {
    this.loadingService.setLoading(true);
    this.http
      .delete<any>(
        `${apiBaseUrl}/secured/proposals/${this.proposalRef}/deleteAnnex/${annexRef}`,
        {},
      )
      .subscribe({
        next: (res) => this.setProposalRef(this.proposalRef),
        error: (res) => {
          this.loadingService.setLoading(false);
          this.growlService.growl({
            severity: 'danger',
            summary: this.translateService.instant(
              'page.collection.drafts.annex.deletion.error',
            ),
            detail: res,
            life: 3000,
            isGrowlSticky: false,
            position: 'bottom-right',
          });
        },
      });
  }

  updateAnnexOrder(
    annexRef: string,
    moveDirection: string,
    timesToMove: number,
  ) {
    this.loadingService.setLoading(true);
    this.http
      .post<any>(
        `${apiBaseUrl}/secured/updateAnnexOrder/${this.proposalRef}/annex/${annexRef}?moveDirection=${moveDirection}&timesToMove=${timesToMove}`,
        {},
      )
      .subscribe(() => {
        this.loadingService.setLoading(false);
        this.setProposalRef(this.proposalRef);
      });
  }

  updateProposalMetadata(docPurpose: string, eeaRelevance: boolean) {
    this.http
      .put<any>(`${apiBaseUrl}/secured/proposal/${this.proposalRef}`, {
        docPurpose,
        eeaRelevance,
        title: '',
      })
      .subscribe((val) => {
        this.proposalRefBS.next([this.proposalRef, true]);
      });
  }

  downloadProposal() {
    this.loadingService.setLoading(true);
    this.http
      .get(`${apiBaseUrl}/secured/proposals/${this.proposalRef}/download`, {
        responseType: 'blob',
      })
      .subscribe({
        next: (blob) => downloadBlob(blob, `Proposal_${this.proposalRef}.zip`),
        complete: () => this.loadingService.setLoading(false),
      });
  }

  exportProposal(outputType: string) {
    this.http
      .get(
        `${apiBaseUrl}/secured/proposal/${this.proposalRef}/export?exportOutput=${outputType}`,
        { responseType: 'text' },
      )
      .subscribe({
        next: () => {
          this.appConfig.config.subscribe((c) => {
            const userEmail = c.user.email;
            const fileType = { PDF: 'Pdf', WORD: 'Legiswrite' }[outputType];
            this.translateService
              .get('page.editor.export-email-sent', { fileType, userEmail })
              .subscribe((message) => {
                this.growlService.growl({
                  severity: 'info',
                  summary: message,
                  life: 3000,
                  isGrowlSticky: false,
                  position: 'bottom-right',
                });
              });
          });
        },
        error: (err) => {
          // TODO : handle errors
          this.growlService.growlError(err.error);
        },
      });
  }

  public async validateProposal() {
    this.appConfig.config.subscribe((c) => {
      const userEmail = c.user.email;
      this.translateService
        .get('page.editor.validate-email-sent', { userEmail })
        .subscribe((message) => {
          this.growlService.growl({
            severity: 'info',
            summary: message,
            life: 5000,
            isGrowlSticky: false,
            position: 'bottom-right',
          });
        });
    });
    this.http
      .get(
        `${apiBaseUrl}/secured/proposal/${this.proposalRef}/validate`,
        { responseType: 'text' },
      )
      .subscribe({
        error: (err) => {
          this.growlService.growlError(err.error);
        }
      });
  }

  deleteProposal() {
    this.loadingService.setLoading(true);
    this.http
      .delete<string>(`${apiBaseUrl}/secured/proposal/${this.proposalRef}`)
      .subscribe(() => {
        this.loadingService.setLoading(false);
        this.router.navigate(['/workspace']);
      });
  }

  addCollaborators(
    collaborators: CollaboratorRequest[],
    options?: { skipError400Interception: boolean },
  ) {
    return this.http
      .post<null>(
        `${apiBaseUrl}/secured/proposal/${this.proposalRef}/bulkCollaborators`,
        { collaborators },
        {
          context: options?.skipError400Interception
            ? new HttpContext().set(
                IS_ERROR_INTERCEPTION_ENABLED,
                (err) => err.status !== 400,
              )
            : undefined,
        },
      )
      .pipe(tap(() => this.fetchCollaborators()));
  }

  setCollaboratorsRole(collaboratorToUpdate: CollaboratorRequest) {
    this.updateCollaboratorRole(collaboratorToUpdate);
  }

  deleteCollaborator(req: CollaboratorRequest) {
    this.deleteProposalCollaborators(req).subscribe(() =>
      this.fetchCollaborators(),
    );
  }

  loadProposalMilestones(loader = true) {
    if (loader) {
      this.loadingService.setLoading(true);
    }
    return this.http
      .get<Milestone[]>(
        `${apiBaseUrl}/secured/proposals/${this.proposalRef}/milestones`,
      )
      .pipe(
        tap({
          next: () => loader && this.loadingService.setLoading(false),
          error: () => loader && this.loadingService.setLoading(false),
        }),
      )
      .subscribe((miles) => {
        this.milestonesBS.next(miles);
        this.loadingService.setLoading(false);
      });
  }

  reloadMilestones() {
    this.loadProposalMilestones(false);
  }

  createMilestone(milestoneComment: string, isClonedProposal = false) {
    this.loadingService.setLoading(true);
    const successMessage = isClonedProposal
      ? this.translateService.instant(
          'page.collection.milestones.create-milestone-dialog.success-clone',
        )
      : this.translateService.instant(
          'page.collection.milestones.create-milestone-dialog.success',
        );
    return this.http
      .post(
        `${apiBaseUrl}/secured/proposals/${this.proposalRef}/milestones`,
        milestoneComment,
      )
      .subscribe({
        next: (res) => {
          this.growlService.growl({
            severity: 'success',
            summary: this.translateService.instant(
              'global.notifications.title.success',
            ),
            detail: successMessage,
            life: 3000,
            isGrowlSticky: false,
            position: 'bottom-right',
          });
          this.loadProposalMilestones();
        },
        error: (res) => {
          this.loadingService.setLoading(false);
          this.exceptionResponseVO = res.error;
          if (this.exceptionResponseVO.errorCode === ErrorCode.CM001) {
            this.dialogService.openDialog({
              title: this.translateService.instant(this.exceptionResponseVO.messageKey + '.title'),
              content: this.translateService.instant(this.exceptionResponseVO.messageKey + '.message'),
              hasDismissButton: false,
            });
            this.growlService.clearGrowl();
          } else {
            this.growlService.growl({
              severity: 'danger',
              summary: this.translateService.instant(
                'page.collection.milestones.create-milestone-dialog.error',
              ),
              detail: res,
              life: 3000,
              isGrowlSticky: false,
              position: 'bottom-right',
            });
          }
        },
      });
  }

  sendMilestoneForContribution(milestone: MilestoneDescriptor, login: string) {
    this.loadingService.setLoading(true);
    const body = {
      legDocumentName: milestone.legDocumentName,
      userLogin: login,
    };
    return this.http
      .post(
        `${apiBaseUrl}/secured/contribution/create-clone-proposal/${milestone.legFileId}`,
        body,
      )
      .subscribe({
        next: (res) => {
          this.growlService.growl({
            severity: 'success',
            summary: this.translateService.instant(
              'global.notifications.title.success',
            ),
            detail: this.translateService.instant(
              'page.collection.milestones.send-copy-for-contribution-dialog.contribution-success',
            ),
            life: 3000,
            isGrowlSticky: false,
            position: 'bottom-right',
          });
          this.loadProposalMilestones();
        },
        error: (res) => {
          this.loadingService.setLoading(false);
          this.growlService.growl({
            severity: 'danger',
            summary: this.translateService.instant(
              'page.collection.milestones.send-copy-for-contribution-dialog.contribution-error',
            ),
            detail: res,
            life: 3000,
            isGrowlSticky: false,
            position: 'bottom-right',
          });
        },
      });
  }

  sendRevisionForMerge(milestone: MilestoneDescriptor) {
    this.loadingService.setLoading(true);

    return this.http
      .post(
        `${apiBaseUrl}/secured/contribution/revision-done/${this.proposalRef}`,
        milestone.legFileId,
      )
      .subscribe({
        next: (res) => {
          this.growlService.growl({
            severity: 'success',
            summary: this.translateService.instant(
              'global.notifications.title.success',
            ),
            detail: this.translateService.instant(
              'page.collection.milestones.send-copy-for-contribution-send-revision-message-success',
            ),
            life: 3000,
            isGrowlSticky: false,
            position: 'bottom-right',
          });
          this.loadProposalMilestones();
        },
        error: (res) => {
          this.growlService.growl({
            severity: 'danger',
            summary: this.translateService.instant(
              'page.collection.milestones.send-copy-for-contribution-send-revision-message-error',
            ),
            detail: res,
            life: 3000,
            isGrowlSticky: false,
            position: 'bottom-right',
          });
        },
      });
  }

  updateExplanatoryTitle(docId: string, title: string) {
    this.loadingService.setLoading(true);
    this.http
      .put<any>(
        `${apiBaseUrl}/secured/proposals/${this.proposalRef}/update-explanatory-title/${docId}`,
        {},
        { params: { title } },
      )
      .subscribe((val) => {
        this.setProposalRef(this.proposalRef);
        this.loadingService.setLoading(false);
      });
  }

  deleteExplanatory(explanatoryRef: string) {
    return this.http.delete<any>(
      `${apiBaseUrl}/secured/proposal/${this.proposalRef}/deleteExplanatory/${explanatoryRef}`,
    );
  }

  createExplanatory(data: CreateDraftBody) {
    this.loadingService.setLoading(true);
    return this.http
      .post<CreateDraftResponse>(
        `${apiBaseUrl}/secured/proposal/createExplanatory`,
        data,
      )
      .pipe(finalize(() => this.loadingService.setLoading(false)));
  }

  deleteExportDocument(exportId: string) {
    return this.http.delete<ExportPackageVO[]>(
      `${apiBaseUrl}/secured/proposal/${this.proposalRef}/deleteExport/${exportId}`,
    );
  }

  previewExport(exportId: string) {
    return this.http
      .get(
        `${apiBaseUrl}/secured/proposal/${this.proposalRef}/previewExport/${exportId}`,
        {
          observe: 'response',
          responseType: 'blob',
        },
      )
      .subscribe((resp) => {
        const cd = parseContentDisposition(
          resp.headers.get('Content-Disposition'),
        );
        const filename = cd.attachment ? cd.filename : 'Preview.docx';
        downloadBlob(resp.body, filename);
      });
  }

  notifyExport(exportId: string) {
    return this.http.get(
      `${apiBaseUrl}/secured/proposal/${this.proposalRef}/notifyExport/${exportId}`,
    );
  }

  getAllExportDocuments() {
    return this.http.get<ExportPackageVO[]>(
      `${apiBaseUrl}/secured/proposal/${this.proposalRef}/getExports`,
    );
  }

  updateExportDocument(exportId: string, comments: string[]) {
    return this.http.put<ExportPackageVO[]>(
      `${apiBaseUrl}/secured/proposal/${this.proposalRef}/updateExport/${exportId}`,
      comments,
    );
  }

  fetchCollaborators() {
    this.loadingService.setLoading(true);
    return this.listCollaborators(this.proposalRef)
      .subscribe((col) => {
        this.loadingService.setLoading(false);
        this.collaboratorsBS.next(col);
      });
  }

  listCollaborators(proposalRef: string) {
    return this.http
      .get<Collaborator[]>(
        `${apiBaseUrl}/secured/proposal/${proposalRef}/collaborators`,
      )
  }

  isInternalUser (collaborator: Collaborator): boolean {
    return collaborator.clientSystem==null;
  }

  hasPermissions(required: Permission[], any = false) {
    return this.permissions$.pipe(
      map((permissions) =>
        any
          ? required.some((p) => permissions.includes(p))
          : !required.some((p) => !permissions.includes(p)),
      ),
    );
  }

  deleteFinancialStatement(financialStatementRef: string) {
    this.loadingService.setLoading(true);
    this.http
      .delete<any>(
        `${apiBaseUrl}/secured/proposal/${this.proposalRef}/delete-financial-statement/${financialStatementRef}`,
        {},
      )
      .subscribe({
        next: (res) => this.setProposalRef(this.proposalRef),
        error: (res) => {
          this.loadingService.setLoading(false);
          this.growlService.growl({
            severity: 'danger',
            summary: this.translateService.instant(
              'page.collection.drafts.financial-statement.delete.error',
            ),
            detail: res,
            life: 3000,
            isGrowlSticky: false,
            position: 'bottom-right',
          });
        },
      });
  }

  createFinancialStatement() {
    this.loadingService.setLoading(true);
    this.http
      .post<any>(
        `${apiBaseUrl}/secured/proposal/${this.proposalRef}/create-financial-statement`,
        {},
      )
      .subscribe({
        next: (res) => this.setProposalRef(this.proposalRef),
        error: (res) => {
          this.loadingService.setLoading(false);
          this.growlService.growl({
            severity: 'danger',
            summary: this.translateService.instant(
              'page.collection.drafts.financial-statement.create.error',
            ),
            detail: res,
            life: 3000,
            isGrowlSticky: false,
            position: 'bottom-right',
          });
        },
      });
  }

  public getProposalDetails(loading = true, ref : string): Observable<Document> {
    if (loading) this.loadingService.setLoading(true);
    return this.http.get<Document>(
      `${apiBaseUrl}/secured/proposals/${ref}`,
    );
  }

  private updateCollaboratorRole(collaborator: CollaboratorRequest) {
    return this.http
      .put<any>(
        `${apiBaseUrl}/secured/proposal/${this.proposalRef}/collaborators`,
        {
          userId: collaborator.userId,
          roleName: collaborator.roleName,
          connectedDG: collaborator.connectedDG,
        },
      )
      .subscribe(() => this.fetchCollaborators());
  }

  private deleteProposalCollaborators(
    collaborator: CollaboratorRequest,
  ): Observable<Collaborator[]> {
    return this.http.delete<any>(
      `${apiBaseUrl}/secured/proposal/${this.proposalRef}/collaborators`,
      {
        body: {
          userId: collaborator.userId,
          roleName: collaborator.roleName,
          connectedDG: collaborator.connectedDG,
        },
      },
    );
  }

  searchUsers(name: string): Observable<User[]> {
    return this.http.get<User[]>(`${apiBaseUrl}/secured/proposal/searchUser`, {
      params: { searchKey: name },
    });
  }

  private retrieveAuthority(collaborators: Collaborator[], config: LeosAppConfig) {
    /** Normal user collaborator - has preference over entity collaborators **/
    const userCollaborators = collaborators
      .filter((c) => c.login === config.user.login);
    for (const collaborator of userCollaborators) {
      const collaboratorRootEntity = collaborator.entity.name.split("\\.", 2);
      for (const entity of config.user.entities) {
        const userRootEntity = entity.name.split("\\.", 2);
        if (userRootEntity[0] === collaboratorRootEntity[0]) {
          return [collaborator.role];
        }
      }
    }
    /** Entity collaborators **/
    const entityCollaborators = collaborators
      .filter((c) => c.login === c.entity.name)
      .sort((c1, c2) => {
          const c1l = c1.entity.name.split(".").length - 1,
            c2l = c2.entity.name.split(".").length - 1;
          return (c2l < c1l) ? -1 : ((c1l === c2l) ? 0 : 1); // Reverse order
      });
    for (const collaborator of entityCollaborators) {
      for (const entity of config.user.entities) {
        if (entity.name.concat(".").startsWith(collaborator.entity.name.concat("."))) {
          return [collaborator.role];
        }
      }
    }
    return [];
  }

  private resolvePermissions(
    collaborators: Collaborator[],
    config: LeosAppConfig,
  ) {
    const docRoles = this.retrieveAuthority(collaborators, config);
    const roles = [...config.user.roles, ...docRoles, config.contextRole];
    const permissions = roles.flatMap((r) => config.permissionsMap[r]);
    return [...new Set(permissions)];
  }
}
