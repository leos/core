export type DocumentSearchParams = {
  searchText: string;
  completeWords: boolean;
  matchCase: boolean;
};
