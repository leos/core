## **SUMMARY**

\<Story summaries should be structured as follows: As \[**the actor**\], I want \[**the something**\] so I can \[**the goal**\].\
Example: As a user, I want a button to open the milestone explorer so I can consult the containing documents of a milestone\>

## **ACCEPTANCE CRITERIAS / GOALS**

\<Checklist to confirm the work done on a ticket meets the intended purpose and maps the deliverable into smaller outcomes. Also helps Q&A to better structure the validation tests.\
Example:

- A "Open Milestone" button is present on the UI
- Clicking on the button opens the milestone explorer
- Milestone explorer contains a tab for each document
- Each tab presents the HTML rendition for the selected document
- Each tab presents the annotations client on read mode for the selected document\>

## **OTHER DESCRIPTIONS / DOCUMENTATION**

\<Mockups, images and or documentation pages, if available\>

## **RELEASE AND BUILD VERSION**

**_OPTIONAL delete if not a bug_**

\<The version where the issue has been identified \>

## BROWSER USED

**_OPTIONAL delete if not a bug_**

\<The web browser where the issue has been identified \>

## **STEPS TO REPRODUCE**

**_OPTIONAL delete if not a bug_**

_\<List of exact steps needed to reproduce the issue - add screenshots if needed to help contextualize\>_

