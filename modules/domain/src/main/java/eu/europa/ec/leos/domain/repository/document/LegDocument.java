package eu.europa.ec.leos.domain.repository.document;

import eu.europa.ec.leos.domain.repository.Content;
import eu.europa.ec.leos.domain.repository.LeosCategory;
import eu.europa.ec.leos.domain.repository.LeosLegStatus;
import eu.europa.ec.leos.domain.repository.common.VersionType;
import io.atlassian.fugue.Option;

import java.time.Instant;
import java.util.List;

public final class LegDocument extends LeosDocument {
    private final List<String> milestoneComments;
    private final String initialCreatedBy;
    private final Instant initialCreationInstant;
    private final String jobId;
    private final Instant jobDate;
    private final LeosLegStatus status;
    private final List<String> containedDocuments;
    private final String milestoneRef;
    private final String packageId;

    public LegDocument(String id, String name, String createdBy, Instant creationInstant, String lastModifiedBy,
                       Instant lastModificationInstant, String versionSeriesId, String cmisVersionLabel, String versionLabel,
                       String versionComment, VersionType versionType, boolean isLatestVersion, List<String> milestoneComments,
                       Option<Content> content, String initialCreatedBy, Instant initialCreationInstant,
                       String jobId, Instant jobDate, LeosLegStatus status, List<String> containedDocuments, String milestoneRef,
                       String packageId) {

        super(LeosCategory.LEG, id, name, createdBy, creationInstant, lastModifiedBy, lastModificationInstant,
                versionSeriesId, cmisVersionLabel, versionLabel, versionComment, versionType, isLatestVersion, false, content);
        this.milestoneComments = milestoneComments;
        this.initialCreatedBy = initialCreatedBy;
        this.initialCreationInstant = initialCreationInstant;
        this.jobId = jobId;
        this.jobDate = jobDate;
        this.status = status;
        this.containedDocuments = containedDocuments;
        this.milestoneRef = milestoneRef;
        this.packageId = packageId;
    }

    public List<String> getMilestoneComments() {
        return milestoneComments;
    }

    public String getInitialCreatedBy() {
        return initialCreatedBy;
    }

    public Instant getInitialCreationInstant() {
        return initialCreationInstant;
    }

    public String getJobId() {
        return jobId;
    }

    public Instant getJobDate() {
        return jobDate;
    }

    public LeosLegStatus getStatus() {
        return status;
    }

    public List<String> getContainedDocuments() {
        return containedDocuments;
    }

    public String getMilestoneRef() {
        return milestoneRef;
    }

    public String getPackageId() {
        return packageId;
    }
}