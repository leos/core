/*
 * Copyright 2024 European Union
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 *     https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package eu.europa.ec.leos.vo.toc;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import eu.europa.ec.leos.model.action.SoftActionType;
import eu.europa.ec.leos.vo.coedition.CoEditionVO;
import eu.europa.ec.leos.vo.structure.NumberingType;
import eu.europa.ec.leos.vo.structure.TocItem;
import eu.europa.ec.leos.vo.structure.TocItemTypeName;
import eu.europa.ec.leos.vo.toc.indent.IndentedItemType;
import org.w3c.dom.Node;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.stream.Stream;

import static eu.europa.ec.leos.util.LeosDomainUtil.addDateIfNotNull;
import static eu.europa.ec.leos.util.LeosDomainUtil.addFieldIfNotNull;
import static eu.europa.ec.leos.util.LeosDomainUtil.addListFieldIfNotNull;
import static eu.europa.ec.leos.util.LeosDomainUtil.calculateLeftPadd;

@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id", scope = TableOfContentItemVO.class)
public class TableOfContentItemVO implements Serializable {

    public static final long serialVersionUID = -1;

    private TocItem tocItem;
    private String id;
    private String originAttr;
    private String number;
    private String initialNum;
    private String originNumAttr;
    private String heading;
    private String originalHeading;
    private TocItemTypeName originalTocItemType;
    private String originHeadingAttr;
    private String content;
    private Node node;
    private String list;
    private boolean movedOnEmptyParent;
    private boolean undeleted;
    private boolean isBlock;
    private boolean isCrossHeading;
    private boolean isCrossHeadingInList;
    private TocItemTypeName tocItemType = TocItemTypeName.REGULAR;
    private final List<TableOfContentItemVO> childItems = new ArrayList<>();
    private TableOfContentItemVO parentItem;
    private SoftActionType softActionAttr;
    private Boolean isSoftActionRoot;
    private String trackChangeAction = "";
    private String softMoveTo;
    private String softMoveFrom;
    private String softTransFrom;
    private String softUserAttr;
    private GregorianCalendar softDateAttr;
    private final List<CoEditionVO> coEditionVos = new ArrayList<>();
    private boolean isAffected;
    private Boolean isNumberingToggled;
    private SoftActionType numSoftActionAttr;
    private SoftActionType headingSoftActionAttr;
    private Boolean restored;
    private int itemDepth;
    private int originalIndentLevel;
    private int indentLevel;
    private String elementNumberId;

    private IndentedItemType indentOriginType = null;
    private int indentOriginIndentLevel = -1;
    private String indentOriginNumId = null;
    private String indentOriginNumValue = null;
    private String indentOriginNumOrigin = null;

    private String style;
    private Boolean isAutoNumOverwritten = false;
    private NumberingType numberingType;
    private boolean isSoleNumbered;

    public TableOfContentItemVO() {
        super();
    }

    public TableOfContentItemVO(TocItem tocItem, String id, String originAttr, String number, String originNumAttr,
                                String heading, String originalHeading, TocItemTypeName originalTocItemType,
                                Node node, String content) {
        this.tocItem = tocItem;
        this.id = id;
        this.originAttr = originAttr;
        this.number = number;
        this.originNumAttr = originNumAttr;
        this.heading = heading;
        this.originalHeading = originalHeading;
        this.originalTocItemType = originalTocItemType;
        this.node = node;
        this.content = content;
        this.isAffected = false;
        this.itemDepth = 0;
        this.originalIndentLevel = 0;
    }

    public TableOfContentItemVO(TocItem tocItem, String id, String originAttr, String number, String originNumAttr,
                                String heading, String originalHeading, TocItemTypeName originalTocItemType,
                                Node node, String list, String content, SoftActionType softActionAttr, Boolean isSoftActionRoot, String softUserAttr, GregorianCalendar softDateAttr) {
        this(tocItem, id, originAttr, number, originNumAttr, heading, originalHeading, originalTocItemType, node, content);
        this.list = list;
        this.softActionAttr = softActionAttr;
        this.isSoftActionRoot = isSoftActionRoot;
        this.softUserAttr = softUserAttr;
        this.softDateAttr = softDateAttr;
    }

    public TableOfContentItemVO(TocItem tocItem, String id, String originAttr, String number, String originNumAttr,
                                String heading, String originalHeading, TocItemTypeName originalTocItemType,
                                Node node, String list, String content, SoftActionType softActionAttr, Boolean isSoftActionRoot, String softUserAttr,
                                GregorianCalendar softDateAttr, String trackChangeAction) {
        this(tocItem, id, originAttr, number, originNumAttr, heading, originalHeading, originalTocItemType, node, list, content, softActionAttr, isSoftActionRoot, softUserAttr, softDateAttr);
        this.trackChangeAction = trackChangeAction;
    }

    public TableOfContentItemVO(TocItem tocItem, String id, String originAttr, String number, String originNumAttr, String heading, String originalHeading, TocItemTypeName originalTocItemType,
                                Node node, String list, String content, SoftActionType softActionAttr, Boolean isSoftActionRoot, String softUserAttr, GregorianCalendar softDateAttr,
                                String softMoveFrom, String softMoveTo, String softTransFrom, boolean undeleted, SoftActionType numSoftActionAttr) {
        this(tocItem, id, originAttr, number, originNumAttr, heading, originalHeading, originalTocItemType, node,
                list, content, softActionAttr, isSoftActionRoot, softUserAttr, softDateAttr);
        this.softMoveFrom = softMoveFrom;
        this.softMoveTo = softMoveTo;
        this.softTransFrom = softTransFrom;
        this.undeleted = undeleted;
        this.numSoftActionAttr = numSoftActionAttr;
    }

    public TableOfContentItemVO(TocItem tocItem, String id, String originAttr, String number, String originNumAttr, String heading, String originalHeading, TocItemTypeName originalTocItemType,
            Node node, String list, String content, SoftActionType softActionAttr, Boolean isSoftActionRoot, String softUserAttr, GregorianCalendar softDateAttr,
            String softMoveFrom, String softMoveTo, String softTransFrom, boolean undeleted, SoftActionType numSoftActionAttr, String trackChangeAction) {
        this(tocItem, id, originAttr, number, originNumAttr, heading, originalHeading, originalTocItemType, node,
                list, content, softActionAttr, isSoftActionRoot, softUserAttr,
                softDateAttr, softMoveFrom, softMoveTo, softTransFrom, undeleted,
                numSoftActionAttr);
        this.trackChangeAction = trackChangeAction;
    }

    public TableOfContentItemVO(TocItem tocItem, String id, String originAttr, String number, String originNumAttr, String heading, String originalHeading, TocItemTypeName originalTocItemType, String originHeadingAttr,
                                Node node, String list, String content, SoftActionType softActionAttr, Boolean isSoftActionRoot, String softUserAttr,
                                GregorianCalendar softDateAttr, String softMoveFrom, String softMoveTo, String softTransFrom, boolean undeleted, SoftActionType numSoftActionAttr, SoftActionType headingSoftActionAttr, int itemDepth,
                                int indentLevel, String elementNumberId, IndentedItemType indentOriginType, Integer indentOriginIndentLevel, String indentOriginNumId, String indentOriginNumValue, String indentOriginNumOrigin,
                                String style, Boolean isAutoNumOverwritten) {
        this(tocItem, id, originAttr, number, originNumAttr, heading, originalHeading, originalTocItemType, node,
                list, content, softActionAttr, isSoftActionRoot, softUserAttr, softDateAttr);
        this.softMoveFrom = softMoveFrom;
        this.softMoveTo = softMoveTo;
        this.softTransFrom = softTransFrom;
        this.undeleted = undeleted;
        this.numSoftActionAttr = numSoftActionAttr;
        this.itemDepth = itemDepth;
        this.originHeadingAttr = originHeadingAttr;
        this.headingSoftActionAttr = headingSoftActionAttr;
        this.elementNumberId = elementNumberId;
        this.indentLevel = indentLevel;
        this.indentOriginType = indentOriginType;
        this.indentOriginIndentLevel = indentOriginIndentLevel != null ? indentOriginIndentLevel : -1;
        this.indentOriginNumId = indentOriginNumId;
        this.indentOriginNumValue = indentOriginNumValue;
        this.indentOriginNumOrigin = indentOriginNumOrigin;
        this.style = style;
        this.isAutoNumOverwritten = isAutoNumOverwritten;
    }

    public void populateIndentInfo(IndentedItemType indentOriginType, int indentOriginIndentLevel, String indentOriginNumId
            , String indentOriginNumValue, String indentOriginNumOrigin) {
        if (!isIndented()) {
            this.indentOriginType = indentOriginType;
            this.indentOriginIndentLevel = indentOriginIndentLevel;
            this.indentOriginNumId = indentOriginNumId;
            this.indentOriginNumValue = indentOriginNumValue;
            this.indentOriginNumOrigin = indentOriginNumOrigin;
        }
    }

    public void resetIndentInfo() {
        this.indentOriginType = null;
        this.indentOriginIndentLevel = 0;
        this.indentOriginNumId = null;
        this.indentOriginNumValue = null;
        this.indentOriginNumOrigin = null;
    }

    public boolean isIndented() {
        return (this.indentOriginType != null && !this.indentOriginType.equals(IndentedItemType.RESTORED));
    }

    public boolean isIndentedOrRestored() {
        return (this.indentOriginType != null);
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getHeading() {
        return heading;
    }

    public void setHeading(String heading) {
        this.heading = heading;
    }

    public String getOriginalHeading() {
        return originalHeading;
    }

    public void setOriginalHeading(String originalHeading) {
        this.originalHeading = originalHeading;
    }

    public TocItemTypeName getOriginalTocItemType() {
        return originalTocItemType;
    }

    public void setOriginalTocItemType(TocItemTypeName originalTocItemType) {
        this.originalTocItemType = originalTocItemType;
    }

    public String getOriginHeadingAttr() {
        return originHeadingAttr;
    }

    public void setOriginHeadingAttr(String originHeadingAttr) {
        this.originHeadingAttr = originHeadingAttr;
    }

    public String getId() {
        return id;
    }

    public String getOriginAttr() {
        return originAttr;
    }

    public void setOriginAttr(String originAttr) {
        this.originAttr = originAttr;
    }

    public String getOriginNumAttr() {
        return originNumAttr;
    }

    public void setOriginNumAttr(String originNumAttr) {
        this.originNumAttr = originNumAttr;
    }

    public TocItem getTocItem() {
        return tocItem;
    }

    public void setTocItem(TocItem tocItem) {
        this.tocItem = tocItem;
    }

    public Node getNode() {
        return node;
    }

    public void setNode(Node node) {
        this.node = node;
    }

    public String getList() {
        return list;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public boolean isAffected() {
        return isAffected;
    }

    public void setAffected(boolean affected) {
        isAffected = affected;
    }

    /**
     * @return the softActionAttr
     */
    public SoftActionType getSoftActionAttr() {
        return softActionAttr;
    }

    /**
     * @param softActionAttr the softActionAttr to set
     */
    public void setSoftActionAttr(SoftActionType softActionAttr) {
        this.softActionAttr = softActionAttr;
    }

    public Boolean isSoftActionRoot() {
        return isSoftActionRoot;
    }

    public void setSoftActionRoot(Boolean softActionRoot) {
        isSoftActionRoot = softActionRoot;
    }

    /**
     * @return the softUserAttr
     */
    public String getSoftUserAttr() {
        return softUserAttr;
    }

    /**
     * @param softUserAttr the softUserAttr to set
     */
    public void setSoftUserAttr(String softUserAttr) {
        this.softUserAttr = softUserAttr;
    }

    /**
     * @return the softDateAttr
     */
    public GregorianCalendar getSoftDateAttr() {
        return softDateAttr;
    }

    /**
     * @param softDateAttr the softDateAttr to set
     */
    public void setSoftDateAttr(GregorianCalendar softDateAttr) {
        this.softDateAttr = softDateAttr;
    }

    /**
     * @return the softMoveTo
     */
    public String getSoftMoveTo() {
        return softMoveTo;
    }

    /**
     * @param softMoveTo the softMoveTo to set
     */
    public void setSoftMoveTo(String softMoveTo) {
        this.softMoveTo = softMoveTo;
    }

    /**
     * @return the softMoveFrom
     */
    public String getSoftMoveFrom() {
        return softMoveFrom;
    }

    /**
     * @param softMoveFrom the softMoveFrom to set
     */
    public void setSoftMoveFrom(String softMoveFrom) {
        this.softMoveFrom = softMoveFrom;
    }

    public TableOfContentItemVO getParentItem() {
        return parentItem;
    }

    public void setParentItem(TableOfContentItemVO parentItem) {
        this.parentItem = parentItem;
    }

    public List<TableOfContentItemVO> getChildItems() {
        return childItems;
    }

    public boolean isMovedOnEmptyParent() {
        return movedOnEmptyParent;
    }

    public void setMovedOnEmptyParent(boolean movedOnEmptyParent) {
        this.movedOnEmptyParent = movedOnEmptyParent;
    }

    public Boolean isNumberingToggled() {
        return isNumberingToggled;
    }

    public void setNumberingToggled(Boolean numberingToggled) {
        this.isNumberingToggled = numberingToggled;
    }

    public Boolean isRestored() {
        return restored;
    }

    public void setRestored(Boolean restored) {
        this.restored = restored;
    }

    public boolean isBlock() {
        return isBlock;
    }

    public void setBlock(boolean block) {
        isBlock = block;
    }

    public boolean isCrossHeading() {
        return isCrossHeading;
    }

    public void setCrossHeading(boolean crossHeading) {
        isCrossHeading = crossHeading;
    }

    public boolean isCrossHeadingInList() {
        return isCrossHeadingInList;
    }

    public void setCrossHeadingInList(boolean crossHeadingInList) {
        isCrossHeadingInList = crossHeadingInList;
    }

    public void addChildItem(TableOfContentItemVO tableOfContentItemVO) {
        if (tableOfContentItemVO.getTocItem().isRoot()) {
            throw new IllegalArgumentException("Cannot add a root item as a child!");
        }
        childItems.add(tableOfContentItemVO);
        tableOfContentItemVO.parentItem = this;
    }

    public void addChildItem(int index, TableOfContentItemVO tableOfContentItemVO) {
        if (tableOfContentItemVO.getTocItem().isRoot()) {
            throw new IllegalArgumentException("Cannot add a root item as a child!");
        }
        childItems.add(index, tableOfContentItemVO);
        tableOfContentItemVO.parentItem = this;
    }

    public void addAllChildItems(List<TableOfContentItemVO> tableOfContentItemVOList) {
        for (TableOfContentItemVO item : tableOfContentItemVOList) {
            addChildItem(item);
        }
    }

    public void removeChildItem(TableOfContentItemVO tableOfContentItemVO) {
        childItems.remove(tableOfContentItemVO);
    }

    public void removeAllChildItems() {
        childItems.clear();
    }

    public List<TableOfContentItemVO> getChildItemsView() {
        return Collections.unmodifiableList(new ArrayList<>(childItems));
    }

    public boolean containsItem(String aknTag) {
        List<TableOfContentItemVO> chldItms = this.childItems;
        for(TableOfContentItemVO child : chldItms) {
            if(child.getTocItem().getAknTag().value().equals(aknTag)) {
                return true;
            }
        }
        return false;
    }

    public List<CoEditionVO> getCoEditionVos() {
        return coEditionVos;
    }

    public void addUserCoEdition(CoEditionVO coEditionVO) {
        coEditionVos.add(coEditionVO);
    }

    public void removeAllUserCoEdition() {
        coEditionVos.clear();
    }

    public boolean isUndeleted() {
        return undeleted;
    }

    public void setUndeleted(boolean undeleted) {
        this.undeleted = undeleted;
    }

    public int getItemDepth() {
        return itemDepth;
    }

    public void setItemDepth(int depth) {
        this.itemDepth = depth;
    }

    public int getOriginalDepth() {
        return originalIndentLevel;
    }

    public void setOriginalDepth(int originalIndentLevel) {
        this.originalIndentLevel = originalIndentLevel;
    }

    public SoftActionType getNumSoftActionAttr() {
        return numSoftActionAttr;
    }

    public void setNumSoftActionAttr(SoftActionType numSoftActionAttr ) {
        this.numSoftActionAttr = numSoftActionAttr;
    }

    public SoftActionType getHeadingSoftActionAttr() {
        return headingSoftActionAttr;
    }

    public void setHeadingSoftActionAttr(SoftActionType headingSoftActionAttr) {
        this.headingSoftActionAttr = headingSoftActionAttr;
    }

    public String getElementNumberId() {
        return elementNumberId;
    }

    public void setElementNumberId(String elementNumberId) {
        this.elementNumberId = elementNumberId;
    }

    public String getSoftTransFrom() {
        return softTransFrom;
    }

    public void setSoftTransFrom(String softTransFrom) {
        this.softTransFrom= softTransFrom;
    }

    public IndentedItemType getIndentOriginType() { return indentOriginType; }

    public void setIndentOriginType(IndentedItemType indentedItemType) { this.indentOriginType = indentedItemType; }

    public int getIndentOriginIndentLevel() { return indentOriginIndentLevel; }

    public String getIndentOriginNumId() { return indentOriginNumId; }

    public String getIndentOriginNumValue() { return indentOriginNumValue; }

    public String getIndentOriginNumOrigin() { return indentOriginNumOrigin; }

    public void setList(String list) {
        this.list = list;
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }

    public Boolean isAutoNumOverwritten() {
        return isAutoNumOverwritten;
    }

    public void setAutoNumOverwritten(Boolean autoNumOverwritten) {
        isAutoNumOverwritten = autoNumOverwritten;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TableOfContentItemVO that = (TableOfContentItemVO) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        return  node != null ? node.equals(that.node) : that.node == null;

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (node != null ? node.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return printTocAsTree(this, 0);
    }

    public String printTocAsTree(TableOfContentItemVO item, int deep) {
        // change RIGHT_CHAR=", " and LEFT_CHAR="", in case you want all in one line. Or overload the method.
        final String RIGHT_CHAR = "\n";
        final String LEFT_CHAR = "\t";

        String leftPadClassname = calculateLeftPadd(deep, LEFT_CHAR);
        String leftPad = calculateLeftPadd(deep + 1, LEFT_CHAR);

        final StringBuilder sb = new StringBuilder(RIGHT_CHAR);
        sb.append(leftPadClassname).append( "TableOfContentItemVO[").append(RIGHT_CHAR);
        addFieldIfNotNull("id", item.id, leftPad, RIGHT_CHAR, sb);
        addFieldIfNotNull("tocItem", item.tocItem != null ? item.tocItem.getAknTag() : null, leftPad, RIGHT_CHAR, sb);
        addFieldIfNotNull("node", item.node, leftPad, RIGHT_CHAR, sb);
        addFieldIfNotNull("originAttr", item.originAttr, leftPad, RIGHT_CHAR, sb);
        addFieldIfNotNull("number", item.number, leftPad, RIGHT_CHAR, sb);
        addFieldIfNotNull("initialNum", item.initialNum, leftPad, RIGHT_CHAR, sb);
        addFieldIfNotNull("originNumAttr", item.originNumAttr, leftPad, RIGHT_CHAR, sb);
        addFieldIfNotNull("heading", item.heading, leftPad, RIGHT_CHAR, sb);
        addFieldIfNotNull("originHeadingAttr", item.originHeadingAttr, leftPad, RIGHT_CHAR, sb);
        addFieldIfNotNull("list", item.list, leftPad, RIGHT_CHAR, sb);
        addFieldIfNotNull("movedOnEmptyParent", item.movedOnEmptyParent, leftPad, RIGHT_CHAR, sb);
        addFieldIfNotNull("undeleted", item.undeleted, leftPad, RIGHT_CHAR, sb);
        sb.append(leftPad + "parentItem=").append(getParentString(item.parentItem)).append(RIGHT_CHAR);
        addFieldIfNotNull("softActionAttr", item.softActionAttr, leftPad, RIGHT_CHAR, sb);
        addFieldIfNotNull("isSoftActionRoot", item.isSoftActionRoot, leftPad, RIGHT_CHAR, sb);
        addFieldIfNotNull("softMoveTo", item.softMoveTo, leftPad, RIGHT_CHAR, sb);
        addFieldIfNotNull("softMoveFrom", item.softMoveFrom, leftPad, RIGHT_CHAR, sb);
        addFieldIfNotNull("softUserAttr", item.softUserAttr, leftPad, RIGHT_CHAR, sb);
        addDateIfNotNull("softDateAttr", item.softDateAttr, leftPad, RIGHT_CHAR, sb);
        addListFieldIfNotNull("coEditionVos", item.coEditionVos, leftPad, RIGHT_CHAR, sb);
        addFieldIfNotNull("isAffected", item.isAffected, leftPad, RIGHT_CHAR, sb);
        addFieldIfNotNull("isNumberingToggled", item.isNumberingToggled, leftPad, RIGHT_CHAR, sb);
        addFieldIfNotNull("numSoftActionAttr", item.numSoftActionAttr, leftPad, RIGHT_CHAR, sb);
        addFieldIfNotNull("headingSoftActionAttr", item.headingSoftActionAttr, leftPad, RIGHT_CHAR, sb);
        addFieldIfNotNull("restored", item.restored, leftPad, RIGHT_CHAR, sb);
        addFieldIfNotNull("itemDepth", item.itemDepth, leftPad, RIGHT_CHAR, sb);
        addFieldIfNotNull("originalIndentLevel", item.originalIndentLevel, leftPad, RIGHT_CHAR, sb);
        addFieldIfNotNull("elementNumberId", item.elementNumberId, leftPad, RIGHT_CHAR, sb);

        if(!item.childItems.isEmpty()) {
            final StringBuilder sbChildren = new StringBuilder();
            for (TableOfContentItemVO child : item.childItems){
                sbChildren.append(printTocAsTree(child, deep + 2));
            }
            sb.append(leftPad).append("childItems=[").append(sbChildren).append(RIGHT_CHAR);
        }

        sb.append(leftPadClassname).append("]");
        return sb.toString();
    }

    private String getParentString(TableOfContentItemVO parentItem) {
        StringBuilder sb = new StringBuilder();
        if(parentItem != null){
            sb.append("TableOfContentItemVO[");
            sb.append("id=").append(parentItem.getId());
            sb.append(", tocItem=").append(parentItem.tocItem != null ? parentItem.tocItem.getAknTag() : "null");
            sb.append("]");
        }
        return sb.toString();
    }

    public Stream<TableOfContentItemVO> flattened() {
        return Stream.concat(
                Stream.of(this),
                childItems.stream().flatMap(TableOfContentItemVO::flattened)
        );
    }

    public int getIndentLevel() {
        return indentLevel;
    }

    public void setIndentLevel(int indentLevel) {
        this.indentLevel = indentLevel;
    }

    public void setInitialNum(String initialNum) {
        this.initialNum = initialNum;
    }

    public String getInitialNum() {
        return initialNum;
    }

    public void setTocItemType(TocItemTypeName tocItemType) {
        this.tocItemType = tocItemType;
    }

    public TocItemTypeName getTocItemType() {
        return this.tocItemType;
    }

    public String getTrackChangeAction() {
        return trackChangeAction;
    }

    public void setTrackChangeAction(String trackChangeAction) {
        this.trackChangeAction = trackChangeAction;
    }

    public NumberingType getNumberingType() {
        return numberingType;
    }

    public void setNumberingType(NumberingType numberingType) {
        this.numberingType = numberingType;
    }

    public boolean isSoleNumbered() { return isSoleNumbered; }

    public void setSoleNumbered(boolean soleNumbered) { isSoleNumbered = soleNumbered; }
}
