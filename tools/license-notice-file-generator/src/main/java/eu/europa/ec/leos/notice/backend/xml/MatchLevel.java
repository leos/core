package eu.europa.ec.leos.notice.backend.xml;

public enum MatchLevel {
    FULL,
    GROUP_ID_AND_ARTIFACT_ID,
    GROUP_ID
}