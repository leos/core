package eu.europa.ec.leos.services.collection;

import eu.europa.ec.leos.domain.repository.LeosPackage;
import eu.europa.ec.leos.domain.repository.LinkedPackage;
import eu.europa.ec.leos.domain.repository.document.Proposal;
import eu.europa.ec.leos.domain.repository.document.XmlDocument;
import eu.europa.ec.leos.i18n.MessageHelper;
import eu.europa.ec.leos.model.notification.collaborators.AddCollaborator;
import eu.europa.ec.leos.model.notification.collaborators.CollaboratorEmailNotification;
import eu.europa.ec.leos.model.notification.collaborators.EditCollaborator;
import eu.europa.ec.leos.model.notification.collaborators.RemoveCollaborator;
import eu.europa.ec.leos.model.user.ClientSystem;
import eu.europa.ec.leos.model.user.Collaborator;
import eu.europa.ec.leos.model.user.Entity;
import eu.europa.ec.leos.model.user.User;
import eu.europa.ec.leos.permissions.Role;
import eu.europa.ec.leos.security.LeosPermissionAuthorityMap;
import eu.europa.ec.leos.security.LeosPermissionAuthorityMapHelper;
import eu.europa.ec.leos.services.document.SecurityService;
import eu.europa.ec.leos.services.dto.collaborator.CollaboratorDTO;
import eu.europa.ec.leos.services.exception.CollaboratorException;
import eu.europa.ec.leos.services.exception.SendNotificationException;
import eu.europa.ec.leos.services.notification.NotificationService;
import eu.europa.ec.leos.services.store.PackageService;
import eu.europa.ec.leos.services.user.UserService;
import eu.europa.ec.leos.vo.response.LeosClientResponse;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class CollaboratorServiceImpl implements CollaboratorService {
    private static final Logger LOG = LoggerFactory.getLogger(CollaboratorServiceImpl.class);

    private final NotificationService notificationService;
    private final MessageHelper messageHelper;
    private final PackageService packageService;
    private final SecurityService securityService;
    private final UserService userService;
    private final LeosPermissionAuthorityMap leosPermissionAuthorityMap;
    private final LeosPermissionAuthorityMapHelper authorityMapHelper;
    private final LeosClientService leosClientService;

    @Override
    public List<CollaboratorDTO> getCollaborators(Proposal proposal) {
        LOG.trace("Getting collaborators for proposal {}", proposal);
        return Collections.unmodifiableList(proposal.getCollaborators().stream()
                .map(collaborator -> createCollaboratorDTO(collaborator.getLogin(), collaborator.getRole(), this::getUser, collaborator.getEntity(), collaborator.getLeosClientId()))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toList()));
    }

    private Optional<CollaboratorDTO> createCollaboratorDTO(String login, String roleName, Function<String, User> converter, String entityName, String clientId) {
        try {
            User user = converter.apply(login);
            return Optional.of(new CollaboratorDTO(login, user.getName(), roleName, pickFromUserEntitiesByName(user, entityName), getLeosClient(clientId)));
        } catch (Exception e) {
            return Optional.empty();
        }
    }

    private Entity pickFromUserEntitiesByName(final User user, final String entityName) {
        return user != null ? user.getEntities().stream()
                .filter(entity -> entity.getName().equals(entityName))
                .findFirst()
                .orElse(null) : null;
    }

    @Override
    public String addCollaborator(Proposal proposal, String userId, String roleName, String selectedEntity, String proposalUrl, String systemClientId) {
        final User user = getUser(userId);
        final Role role = getRole(roleName);
        final String entity = getEntity(selectedEntity, user);

        List<XmlDocument> documents = getXmlDocumentsForProposal(proposal.getMetadata().get().getRef());
        if (isCollaboratorPresent(documents, user, role, entity)) {
            throw new CollaboratorException(messageHelper.getMessage("collaborator.message.user.present", user.getLogin(), role.getName(), entity));
        }

        documents.forEach(doc -> updateCollaborators(user, role, entity, systemClientId, doc, false));

        sendNotification(new AddCollaborator(user, entity, role.getName(), proposal.getId(), proposalUrl));
        LOG.info("Collaborator '{}', role '{}', entity '{}' inserted to proposal id {}", user.getLogin(), role.getName(), entity, proposal.getId());
        return entity;
    }

    @Override
    public String removeCollaborator(Proposal proposal, String userId, String roleName, String selectedEntity, String proposalUrl) {
        LOG.trace("Removing collaborator...{}, with authority {}", userId, roleName);
        final User user = getUser(userId);
        final Role role = getRole(roleName);
        final String entity = selectedEntity != null ? getEntity(selectedEntity, user) : null;

        List<XmlDocument> documents = getXmlDocumentsForProposal(proposal.getMetadata().get().getRef());
        if (!isCollaboratorPresent(documents, user, role, entity)) {
            throw new CollaboratorException(messageHelper.getMessage("collaborator.message.user.notPresent", user.getLogin(), role.getName(), entity));
        }
        if (!hasCollaboratorDifferentRole(documents, user, role)) {
            throw new CollaboratorException(messageHelper.getMessage("collaborator.message.role.different", user.getLogin(), role.getName()));
        }
        if (isCollaboratorLastOwner(documents, user, entity)) {
            throw new CollaboratorException(messageHelper.getMessage("collaborator.message.last.owner.removed", role.getName()));
        }

        documents.forEach(doc -> updateCollaborators(user, role, entity, null, doc, true));

        sendNotification(new RemoveCollaborator(user, entity, role.getName(), proposal.getId(), proposalUrl));
        LOG.info("Collaborator '{}', role '{}', entity '{}' removed from proposal id {}", user.getLogin(), role.getName(), entity, proposal.getId());
        return entity;
    }

    @Override
    public String editCollaborator(Proposal proposal, String userId, String newRoleName, String selectedEntity, String proposalUrl) {
        final User user = getUser(userId);
        final Role newRole = getRole(newRoleName);
        final String entity = getEntity(selectedEntity, user);

        List<XmlDocument> documents = getXmlDocumentsForProposal(proposal.getMetadata().get().getRef());
        String collaboratorRole = documents.get(0).getCollaborators().stream()
                .filter(c -> user.getLogin().equals(c.getLogin()) && c.getEntity().equals(entity))
                .map(Collaborator::getRole)
                .findFirst()
                .orElse(null);
        if (collaboratorRole == null) {
            LOG.warn("User '{}' does not have any role for Entity '{}'", userId, selectedEntity);
            throw new CollaboratorException(messageHelper.getMessage("collaborator.message.role.notFoundForEntity", messageHelper.getMessage(userId, selectedEntity)));
        }
        Role oldRole = authorityMapHelper.getRoleFromListOfRoles(collaboratorRole);
        LOG.trace("Updating collaborator {}, role {} with new role {}", userId, oldRole.getName(), newRoleName);

        if (isCollaboratorLastOwner(documents, user, entity)) {
            LOG.warn("Should be at least one user with role {}", oldRole);
            throw new CollaboratorException(messageHelper.getMessage("collaborator.message.last.owner.edited", messageHelper.getMessage(oldRole.getMessageKey())));
        }

        documents.forEach(doc -> updateCollaborators(user, newRole, entity, null, doc, false));

        sendNotification(new EditCollaborator(user, entity, newRole.getName(), proposal.getId(), proposalUrl));
        LOG.info("Collaborator '{}', oldRole '{}', entity '{}' updated new role to '{}' for proposal id {}", user.getLogin(), oldRole.getName(), entity, newRole.getName(), proposal.getId());
        return entity;
    }

    @Override
    public void synchCollaborators(Proposal proposal) {
        LOG.trace("Synch collaborators...");

        List<Collaborator> collaborators = proposal.getCollaborators();
        List<XmlDocument> documents = getXmlDocumentsForProposal(proposal.getMetadata().get().getRef());

        documents.forEach(doc -> {
            updateCollaborators(doc, collaborators);
        });
    }

    private User getUser(String userId) {
        if (StringUtils.isEmpty(userId)) {
            throw new CollaboratorException(messageHelper.getMessage("collaborator.message.user.noId"));
        }
        final String DOMAIN_SEPARATOR = "/";
        if (userId.contains(DOMAIN_SEPARATOR)) {
            userId = userId.substring(userId.lastIndexOf(DOMAIN_SEPARATOR) + 1);
        }

        User user = userService.getUser(userId);
        if (user == null) {
            LOG.warn("User '{}' not found!", userId);
            throw new CollaboratorException(messageHelper.getMessage("collaborator.message.user.notFound", userId));
        }
        return user;
    }

    private ClientSystem getLeosClient(String clientSystemId) {
        if (clientSystemId==null || StringUtils.isEmpty(clientSystemId)) {
            return null;
        }
        final Optional<LeosClientResponse> leosClient = leosClientService.getLeosClient(clientSystemId);
        if (leosClient.isPresent()) {
            final LeosClientResponse leosClientResponse = leosClient.get();
            return ClientSystem.builder().clientId(leosClientResponse.getName()).displayName(leosClientResponse.getDisplayName()).build();
        } else {
            return null;
        }
    }

    private String getEntity(String connectedDG, User user) {
        String entity;
        if (connectedDG == null) {
            Entity firstEntity = user.getEntities().get(0);
            if (firstEntity == null) {
                LOG.error("User '{}' has no Entity associated", user.getLogin());
                throw new CollaboratorException(messageHelper.getMessage("collaborator.message.user.noEntity", user.getLogin()));
            }
            entity = firstEntity.getName();
        } else {
            Entity userEntity = user.getEntities().stream()
                    .filter(e -> e.getOrganizationName().equalsIgnoreCase(connectedDG))
                    .findFirst()
                    .orElse(null);
            if (userEntity == null) {
                LOG.error("User '{}' has no Entity with name '{}'", user.getLogin(), connectedDG);
                throw new CollaboratorException(messageHelper.getMessage("collaborator.message.user.unknownEntity", user.getLogin(), connectedDG));
            }
            entity = userEntity.getName();
        }
        return entity;
    }

    private Role getRole(String roleName) {
        if (StringUtils.isEmpty(roleName)) {
            throw new CollaboratorException(messageHelper.getMessage("collaborator.message.role.null"));
        }
        Role role = leosPermissionAuthorityMap.getAllRoles()
                .stream()
                .filter(r -> roleName.equals(r.getName()))
                .findFirst()
                .orElse(null);
        if (role == null) {
            LOG.warn("Role '{}' not found", roleName);
            throw new CollaboratorException(messageHelper.getMessage("collaborator.message.role.notFound", roleName));
        }

        if (!role.isCollaborator()) {
            LOG.warn("Role {}, is not a contributor", roleName);
            throw new CollaboratorException(messageHelper.getMessage("collaborator.message.role.notContributor", roleName));
        }
        return role;
    }

    private boolean isCollaboratorPresent(List<XmlDocument> documents, User user, Role role, String selectedEntity) {
        List<Collaborator> collaborators = documents.get(0).getCollaborators();
        return collaborators.stream()
                .anyMatch(collaborator -> collaborator.getLogin().equals(user.getLogin())
                        && collaborator.getRole().equals(role.getName())
                        && (collaborator.getEntity().equals(selectedEntity) || selectedEntity == null)
                );
    }

    private boolean hasCollaboratorDifferentRole(List<XmlDocument> documents, User user, Role role) {
        List<Collaborator> collaborators = documents.get(0).getCollaborators();
        return collaborators.stream()
                .filter(collaborator -> collaborator.getLogin().equals(user.getLogin()))
                .anyMatch(collaborator -> role.getName().equals(collaborator.getRole()));
    }

    private boolean isCollaboratorLastOwner(List<XmlDocument> documents, User user, String entity) {
        boolean isLastOwner = false;
        List<Collaborator> collaborators = documents.get(0).getCollaborators();
        collaborators = collaborators.stream()
                .filter(c -> c.getRole().equals("OWNER"))
                .collect(Collectors.toList());
        if (collaborators.size() == 1
                && collaborators.get(0).getLogin().equals(user.getLogin())
                && (collaborators.get(0).getEntity().equals(entity) || entity == null)) {
            isLastOwner = true;
        }
        return isLastOwner;
    }

    private void sendNotification(CollaboratorEmailNotification collaboratorEmailNotification) {
        try {
            LOG.trace("Sending email to updated collaborator user {}", collaboratorEmailNotification.getRecipient().getLogin());
            notificationService.sendNotification(collaboratorEmailNotification);
        } catch (Exception e) {
            LOG.warn("Unexpected error occurred while sending notification to user {}", collaboratorEmailNotification.getRecipient().getLogin(), e);
            throw new SendNotificationException(
                    "Unexpected error occurred while sending notification to user " + collaboratorEmailNotification.getRecipient().getLogin(), e);
        }
    }

    private List<XmlDocument> getXmlDocumentsForProposal(String proposalRef) {
        List<XmlDocument> docsList = new ArrayList<>();
        LeosPackage leosPackage = packageService.findPackageByDocumentRef(proposalRef, Proposal.class);
        docsList.addAll(packageService.findDocumentsByPackagePath(leosPackage.getPath(), XmlDocument.class, false));
        List<LinkedPackage> linkedPackages = packageService.findLinkedPackagesByPackageId(leosPackage.getId());
        for (LinkedPackage pkg : linkedPackages) {
            LeosPackage linkedPackage = packageService.findPackageByPackageId(pkg.getLinkedPackageId());
            if (linkedPackage.getTranslated()) {
                docsList.addAll(packageService.findDocumentsByPackagePath(linkedPackage.getPath(), XmlDocument.class, false));
            }
        }
        return docsList;
    }

    //Update document based on action(add/edit/remove)
    private void updateCollaborators(User user, Role role, String selectedEntity, String systemClientId, XmlDocument doc, boolean isRemoveAction) {
        Validate.notNull(doc, "The document must not be null!");
        Validate.notNull(user, "The user must not be null!");
        List<Collaborator> collaborators = doc.getCollaborators();

        if (collaborators != null) {
            collaborators.removeIf(c -> c == null || c.getLogin() == null || (c.getLogin().equals(user.getLogin()) &&
                    (c.getEntity() == null || selectedEntity == null || c.getEntity().equals(selectedEntity))));
            if (!isRemoveAction) {
                //pick selectedEntity or first found entity if no selectedEntity defined
                String newEntity = selectedEntity;
                if (newEntity == null) {
                    newEntity = user.getEntities().get(0) != null ? user.getEntities().get(0).getName() : null;
                }
                collaborators.add(new Collaborator(user.getLogin(), role.getName(), newEntity, systemClientId));
            }
            securityService.updateCollaborators(doc.getMetadata().get().getRef(), doc.getId(), collaborators, doc.getClass());
        }
    }

    private void updateCollaborators(XmlDocument doc, List<Collaborator> collaborators) {
        Validate.notNull(doc, "The document must not be null!");
        Validate.notNull(collaborators, "The collaborators must not be null!");

        securityService.updateCollaborators(doc.getMetadata().get().getRef(), doc.getId(), collaborators, doc.getClass());
    }
}
