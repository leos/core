package eu.europa.ec.digit.leos.pilot.export.util;

import org.springframework.util.xml.SimpleNamespaceContext;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xml.sax.SAXException;
import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.ByteArrayOutputStream;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.ArrayList;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.europa.ec.digit.leos.pilot.export.exception.XmlUtilException;
import eu.europa.ec.digit.leos.pilot.export.exception.XmlValidationException;

public class XmlUtil {

    private static final Logger LOG = LoggerFactory.getLogger(XmlUtil.class);
    public static final String XML_DOC_EXT = ".xml";
    public static final String XML_NAME = "name";
    public static final String NAMESPACE_AKN_NAME = "akn";
    public static final String NAMESPACE_AKN_URI = "http://docs.oasis-open.org/legaldocml/ns/akn/3.0";
    public static final String NAMESPACE_AKN4EU_NAME = "akn4eu";
    public static final String NAMESPACE_AKN4EU_URI = "http://imfc.europa.eu/akn4eu";
    public static final String TAG_AKN4EU_NAME = "akn4eu:akn4euVersion";


    public static class XmlFile {
        private Document xmlDocument;
        private String name;

        public XmlFile() {
            this("");
        }

        public XmlFile(String name) {
            this.name = name;
            this.xmlDocument = null;
        }

        public static String parseNode(Node node) throws XmlUtilException {
            if (XmlUtil.isNodeEmpty(node)) {
                return null;
            }
            ByteArrayOutputStream buffer = new ByteArrayOutputStream();
            try {
                StreamResult result = new StreamResult(buffer);
                Node securedNode = createSecureDocumentFromNode(node);
                DOMSource source = new DOMSource(securedNode);
                Transformer transformer = getTransformer();
                transformer.setOutputProperty(OutputKeys.ENCODING,"UTF-8");
                transformer.setOutputProperty(OutputKeys.VERSION, "1.0");
                transformer.transform(source, result);
                String nodeContent = new String(buffer.toByteArray(), StandardCharsets.UTF_8).replaceAll("(<\\?xml.*?\\?>)", "");
                return nodeContent.replaceAll("xmlns(.*?)=(\".*?\")", "");
            } catch(TransformerException e) {
                closeOutputStream(buffer);
                throw new XmlUtilException("Error getting xml bytes", e);
            } catch (Exception e) {
                closeOutputStream(buffer);
                throw new XmlUtilException("Error getting xml bytes", e);
            }
        }

        public void createNewXmlDocument() throws XmlUtilException {
            try {
                DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
                builderFactory.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
                builderFactory.setFeature("http://xml.org/sax/features/external-general-entities", false);
                builderFactory.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
                builderFactory.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
                builderFactory.setNamespaceAware(true);

                this.xmlDocument = builderFactory.newDocumentBuilder().newDocument();
            } catch (ParserConfigurationException e) {
                throw new XmlUtilException("Error creating new xml document", e);
            }
        }

        public void parse(InputStream inputStream, String name) throws XmlUtilException {
            try {
                DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
                builderFactory.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
                builderFactory.setFeature("http://xml.org/sax/features/external-general-entities", false);
                builderFactory.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
                builderFactory.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
                builderFactory.setNamespaceAware(true);

                this.xmlDocument = builderFactory.newDocumentBuilder().parse(inputStream);
                this.name = name;
            } catch (ParserConfigurationException | SAXException | IOException e) {
                throw new XmlUtilException("Error parsing xml stream", e);
            }
        }

        public Element createRoot(final String rootName){
            Element rootElement = this.xmlDocument.createElement(rootName);
            this.xmlDocument.appendChild(rootElement);
            return rootElement;
        }

        public Element newElement(final String elementName){
            return this.xmlDocument.createElement(elementName);
        }

        public Node getRootNode(){
            return this.xmlDocument.getFirstChild();
        }

        public Element getElementById(final String id){
            return this.xmlDocument.getElementById(id);
        }

        public Node getElementByName(final String name){
            NodeList nodes = this.xmlDocument.getElementsByTagName(name);
            return (nodes.getLength() > 0) ? nodes.item(0) : null;
        }

        public NodeList getElementsByName(final String name){
            return this.xmlDocument.getElementsByTagName(name);
        }

        public Document getXmlDocument() {
            return this.xmlDocument;
        }

        public byte[] getBytes() throws XmlUtilException {
            if (this.xmlDocument == null){ return null; }

            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            try {
                Node secureNode = createSecureDocumentFromNode(this.xmlDocument);
                StreamResult xmlStreamResult = new StreamResult(outputStream);
                DOMSource xmlSource = new DOMSource(secureNode);
                Transformer transformer = getTransformer();
                transformer.setOutputProperty(OutputKeys.ENCODING,"UTF-8");
                transformer.setOutputProperty(OutputKeys.VERSION, "1.0");
                transformer.transform(xmlSource, xmlStreamResult);
                byte [] xmlBytes = outputStream.toByteArray();
                closeOutputStream(outputStream);
                return xmlBytes;
            } catch(TransformerException e){
                closeOutputStream(outputStream);
                throw new XmlUtilException("Error getting xml bytes", e);
            } catch (Exception e) {
                closeOutputStream(outputStream);
                throw new XmlUtilException("Error getting xml bytes", e);
            }
        }

        private static Transformer getTransformer() throws TransformerConfigurationException {
            final TransformerFactory transformerFactory = TransformerFactory.newInstance();
            // Secure the factory to prevent XXE attacks
            transformerFactory.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
            return transformerFactory.newTransformer();
        }

        private static Node createSecureDocumentFromNode(Node node) throws Exception {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            dbf.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
            dbf.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
            dbf.setFeature("http://xml.org/sax/features/external-general-entities", false);
            dbf.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
            dbf.setNamespaceAware(true);

            DocumentBuilder builder = dbf.newDocumentBuilder();
            Document secureDocument = builder.newDocument();
            if (node instanceof Document) {
                NodeList childNodes = node.getChildNodes();
                for (int i = 0; i < childNodes.getLength(); i++) {
                    Node child = secureDocument.importNode(childNodes.item(i), true);
                    secureDocument.appendChild(child);
                }
            } else {
                Node importedNode = secureDocument.importNode(node, true);
                secureDocument.appendChild(importedNode);
            }
            return secureDocument;
        }

        public Text createTextNode(String value) {
            return this.xmlDocument.createTextNode(value);
        }

        public Element addElementValue(Element element, String value) {
            element.appendChild(this.createTextNode(value));
            return element;
        }

        public String getName(){
            return this.name;
        }

        public void setName(String name) {
            this.name = name;
        }

        private static void closeOutputStream(OutputStream outputStream) throws XmlUtilException {
            try {
                outputStream.close();
            } catch(IOException e){
                throw new XmlUtilException("Unable to close byte stream", e);
            }
        }
    }

    static class XPathSanitizer {
        private static String XPATH_PATTERN_STRING = "^(/|(\\.\\./)|(\\./)|\\.\\.//)?.*";
        private static final Pattern VALID_XPATH_PATTERN = Pattern.compile(XPATH_PATTERN_STRING);

        public static String sanitizeXPath(String xPath) throws IllegalArgumentException {
            if (xPath == null) {
                throw new IllegalArgumentException("XPath cannot be null");
            }
            // Validate against the allowed pattern
            if (!VALID_XPATH_PATTERN.matcher(xPath).matches()) {
                throw new IllegalArgumentException("XPath contains invalid characters "+xPath);
            }
            return xPath;
        }
    }

    public static XmlFile newXmlFile() throws XmlUtilException {
        XmlFile xmlFile = new XmlFile();
        xmlFile.createNewXmlDocument();
        return xmlFile;
    }

    public static Validator getAknSchemaValidator() throws XmlValidationException {
        try {
            SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            URL resource = XmlUtil.class.getClassLoader().getResource("metadata/schema/akomantoso30.xsd");
            Schema schema = factory.newSchema(resource);
            return schema.newValidator();
        } catch (SAXException ex) {
            throw new XmlValidationException("Error creating schema validator", ex);
        }
    }

    public static XmlFile parseXml(byte[] xmlContent) throws XmlUtilException {
        InputStream inputStream = new ByteArrayInputStream(xmlContent);
        return parseXml(inputStream, "");
    }

    public static XmlFile parseXml(InputStream inputStream) throws XmlUtilException {
        return parseXml(inputStream, "");
    }

    public static XmlFile parseXml(InputStream inputStream, String name) throws XmlUtilException {
        XmlFile xmlFile = new XmlFile();
        xmlFile.parse(inputStream, name);
        return xmlFile;
    }

    public static Node evalXpath(String xPath, Document document) throws XPathExpressionException {
        XPathFactory xPathFactory = XPathFactory.newInstance();
        XPath xpath = xPathFactory.newXPath();
        xpath.setNamespaceContext(getSimpleNamespaceContext());
        XPathExpression expr = xpath.compile(xPath);
        Node node = (Node) expr.evaluate(document, XPathConstants.NODE);
        return node;
    }

    public static byte[] nodeToByteArray(Document document) throws Exception {
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        transformer.transform(new DOMSource(document), new StreamResult(outputStream));
        return outputStream.toByteArray();
    }

    public static NodeList evaluateXPath(Document document, String xPath) throws Exception {
        XPathFactory xPathFactory = XPathFactory.newInstance();
        XPath xpath = xPathFactory.newXPath();
        xpath.setNamespaceContext(getSimpleNamespaceContext());
        XPathExpression expr = xpath.compile(xPath);
        return (NodeList) expr.evaluate(document, XPathConstants.NODESET);
    }

    public static boolean nodeNameEquals(Node node, String nodeName){
        if (XmlUtil.isNodeEmpty(node)) {
            return false;
        }
        if (StringUtil.isEmpty(node.getNodeName())) {
            return StringUtil.isEmpty(nodeName);
        }
        return node.getNodeName().equals(nodeName);
    }

    public static boolean nodeHasAttribute(Node node, String attributeName){
        if (XmlUtil.isNodeEmpty(node)) {
            return false;
        }
        if (node.getAttributes() == null) {
            return false;
        }
        final Node attributeNode = node.getAttributes().getNamedItem(attributeName);
        return !XmlUtil.isNodeEmpty(attributeNode);
    }

    public static String getNodeAttributeValue(Node node, String attributeName){
        return XmlUtil.nodeHasAttribute(node, attributeName)
                ? node.getAttributes().getNamedItem(attributeName).getNodeValue() : null;
    }

    public static boolean nodeAttributeValueEquals(Node node, String attributeName, String attributeValue){
        String value = XmlUtil.getNodeAttributeValue(node, attributeName);
        return !StringUtil.isEmpty(value) && value.equals(attributeValue);
    }

    public static Node getChildNodeWithName(Node node, String childName){
        if (XmlUtil.isNodeEmpty(node)) {
            return null;
        }

        Node result = null;
        NodeList childNodes = node.getChildNodes();
        int index = 0;

        while(index < childNodes.getLength() && XmlUtil.isNodeEmpty(result)){
            if (nodeNameEquals(childNodes.item(index), childName)){
                result = childNodes.item(index);
            }
            index++;
        }
        return result;
    }

    public static List<Node> getChildNodesWithName(Node node, String childName){
        NodeList childNodes = node.getChildNodes();
        List<Node> result = new ArrayList<>();

        for (int i = 0; i < childNodes.getLength(); i++){
            if (nodeNameEquals(childNodes.item(i), childName)){
                result.add(childNodes.item(i));
            }
        }

        return result;
    }

    public static boolean parentNodeNameEquals(Node node, String name){
        return !XmlUtil.isNodeEmpty(node.getParentNode()) && nodeNameEquals(node.getParentNode(), name);
    }

    public static void setNodeAttributeValue(Node node, String attributeName, String attributeValue){
        ((Element)node).setAttribute(attributeName, attributeValue);
    }

    public static void removeNodeAttributeValue(Node node, String attributeName){
        ((Element)node).removeAttribute(attributeName);
    }

    public static Node getXmlChildNodeWithNameAttributeValue(Node xmlNode, String nameAttributeValue){
        return getXmlChildNodeWithAttributeValue(xmlNode, "name", nameAttributeValue);
    }

    public static Node getXmlChildNodeWithXmlIdAttributeValue(Node xmlNode, String xmlIdAttributeValue){
        return getXmlChildNodeWithAttributeValue(xmlNode, "xml:id", xmlIdAttributeValue);
    }

    public static Node getXmlChildNodeWithAttributeValue(Node xmlNode, String attributeName, String attributeValue){
        if (XmlUtil.isNodeEmpty(xmlNode)){
            return null;
        }

        Node xmlNodeResult = null;
        NodeList xmlChildNodes = xmlNode.getChildNodes();
        int index = 0;

        while(index < xmlChildNodes.getLength() && XmlUtil.isNodeEmpty(xmlNodeResult)) {
            Node xmlChildNode = xmlChildNodes.item(index);
            if (nodeAttributeValueEquals(xmlChildNode, attributeName, attributeValue)) {
                xmlNodeResult = xmlChildNode;
            }
            index++;
        }
        return xmlNodeResult;
    }

    public static boolean isNodeEmpty(final Node node) {
        return (node == null);
    }

    public static List<Node> deleteElementsByXPath(Node node, String xPath, boolean namespaceEnabled) {
        List<Node> deletedNodes = new ArrayList<>();
        NodeList nodeList = getElementsByXPath(node, xPath, namespaceEnabled);
        for (int i = 0; i < nodeList.getLength(); i++) {
            if (nodeList.item(i).getParentNode() != null) {
                deletedNodes.add(nodeList.item(i).getParentNode().removeChild(nodeList.item(i)));
            }
        }
        return deletedNodes;
    }

    public static NodeList getElementsByXPath(Node node, String xPathExpression, boolean namespaceEnabled) {
        try {
            xPathExpression = XPathSanitizer.sanitizeXPath(xPathExpression);
            XPath xPathParser = XPathFactory.newInstance().newXPath();
            if (namespaceEnabled) {
                xPathParser.setNamespaceContext(getSimpleNamespaceContext());
            }
            NodeList nodes = (NodeList) xPathParser.evaluate(xPathExpression, node, XPathConstants.NODESET);
            return nodes;
        } catch (XPathExpressionException e) {
            throw new IllegalArgumentException("Cannot find xpath " + xPathExpression);
        }
    }

    private static SimpleNamespaceContext getSimpleNamespaceContext() {
        SimpleNamespaceContext nsc = new SimpleNamespaceContext();
        nsc.bindNamespaceUri("xml", "http://www.w3.org/XML/1998/namespace");
        nsc.bindNamespaceUri("leos", "urn:eu:europa:ec:leos");
        nsc.bindNamespaceUri(NAMESPACE_AKN4EU_NAME, NAMESPACE_AKN4EU_URI);
        nsc.bindNamespaceUri(NAMESPACE_AKN_NAME, NAMESPACE_AKN_URI); //fake to trick the parser for the default ns
        return nsc;
    }
}