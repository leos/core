package eu.europa.ec.leos.services.dto.request;



public class InsertElementRequest {

    private Position position;

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }
}
