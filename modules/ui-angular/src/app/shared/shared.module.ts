import { DragDropModule } from '@angular/cdk/drag-drop';
import { NgModule } from '@angular/core';
import { MatTreeModule } from '@angular/material/tree';
import { EuiAllModule } from '@eui/components';
import { TranslateModule } from '@ngx-translate/core';
import { NgForTrackByPropertyModule } from 'ng-for-track-by-property';

import { ConfirmReloadDialogComponent } from '@/shared/components/confirm-reload-dialog/confirm-reload-dialog.component';
import { NotificationCardContainerComponent } from '@/shared/components/notification-card-container/notification-card-container.component';
import { ProposalCreateWizardComponent } from '@/shared/components/proposal-create-wizard/proposal-create-wizard.component';
import { ProposalUploadWizardComponent } from '@/shared/components/proposal-upload-wizard/proposal-upload-wizard.component';
import { ProposalService } from '@/shared/services/proposal.service';

import { AknDocumentComponent } from './components/akn-document/akn-document.component';
import { CoEditionDetectedDialogComponent } from './components/co-edition-detected-dialog/co-edition-detected-dialog.component';
import { CoEditionInfoComponent } from './components/co-edition-info/co-edition-info.component';
import { ConfirmDeleteDialogComponent } from './components/confirm-delete-dialog/confirm-delete-dialog.component';
import { ConfirmDialogComponent } from './components/confirm-dialog/confirm-dialog.component';
import { DocumentAnnotationsComponent } from './components/document-annotations/document-annotations.component';
import { MilestoneTocComponent } from './components/milestone-toc/milestone-toc.component';
import { NotificationCardComponent } from './components/notification-card/notification-card.component';
import { NotificationUploadComponent } from './components/notification-upload/notification-upload.component';
import { ProposalCreateDraftComponent } from './components/proposal-create-draft/proposal-create-draft.component';
import { ProposalCreateFormComponent } from './components/proposal-create-form/proposal-create-form.component';
import { ProposalCreateTemplateSelectorComponent } from './components/proposal-create-template-selector/proposal-create-template-selector.component';
import { ProposalMilestoneViewComponent } from './components/proposal-milestone-view/proposal-milestone-view.component';
import { ResizeHandleComponent } from './components/resize-handle/resize-handle.component';
import { ZoomScrollbarComponent } from './components/zoom-scrollbar/zoom-scrollbar.component';
import { HideOnClickOutsideDirective } from './directives/HideOnClickOutside.directive';
import { ShowOnInstanceDirective } from './directives/showOnInstance.directive';
import { UserHasPermissionDirective } from './directives/userHasPermission.directive';
import { EscapeHtmlPipe } from './pipes/escape-html.pipe';
import { HtmlToPlaintextPipe } from './pipes/html-to-plaintext.pipe';
import { UnescapeHtmlPipe } from './pipes/unescape-html.pipe';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EuiDropdownModule } from '@eui/components/eui-dropdown';
import { DynamicDropdownHoverComponent } from '@/shared/components/dynamic-dropdown-hover/dynamic-dropdown-hover.component';
import { DynamicDropdownClickComponent } from '@/shared/components/dynamic-dropdown-click/dynamic-dropdown-click.component';
import { MoveToLeftSideToolbarDirective } from './directives/MoveToLeftSideToolbar.directive';

@NgModule({
  imports: [
    EuiAllModule,
    EuiDropdownModule,
    NgForTrackByPropertyModule,
    TranslateModule,
    ReactiveFormsModule,
    FormsModule,
    DragDropModule,
    MatTreeModule,
    CommonModule,
  ],
  declarations: [
    DynamicDropdownClickComponent,
    DynamicDropdownHoverComponent,
    ProposalCreateWizardComponent,
    ProposalUploadWizardComponent,
    AknDocumentComponent,
    DocumentAnnotationsComponent,
    ShowOnInstanceDirective,
    UserHasPermissionDirective,
    HideOnClickOutsideDirective,
    MoveToLeftSideToolbarDirective,
    ProposalCreateDraftComponent,
    ProposalCreateFormComponent,
    ProposalCreateTemplateSelectorComponent,
    ConfirmDeleteDialogComponent,
    ConfirmDialogComponent,
    CoEditionInfoComponent,
    CoEditionDetectedDialogComponent,
    MilestoneTocComponent,
    ResizeHandleComponent,
    ZoomScrollbarComponent,
    ProposalMilestoneViewComponent,
    HtmlToPlaintextPipe,
    EscapeHtmlPipe,
    UnescapeHtmlPipe,
    ConfirmReloadDialogComponent,
    NotificationCardContainerComponent,
    NotificationCardComponent,
    NotificationUploadComponent,
  ],
  exports: [
    EuiAllModule,
    NgForTrackByPropertyModule,
    TranslateModule,
    FormsModule,
    DragDropModule,
    MatTreeModule,
    AknDocumentComponent,
    DocumentAnnotationsComponent,
    ShowOnInstanceDirective,
    UserHasPermissionDirective,
    HideOnClickOutsideDirective,
    MoveToLeftSideToolbarDirective,
    ProposalCreateTemplateSelectorComponent,
    ConfirmDeleteDialogComponent,
    ConfirmDialogComponent,
    CoEditionInfoComponent,
    CoEditionDetectedDialogComponent,
    ResizeHandleComponent,
    ProposalMilestoneViewComponent,
    HtmlToPlaintextPipe,
    EscapeHtmlPipe,
    UnescapeHtmlPipe,
    ConfirmReloadDialogComponent,
    ZoomScrollbarComponent,
    NotificationCardContainerComponent,
    NotificationCardComponent,
    NotificationUploadComponent,
    DynamicDropdownHoverComponent,
  ],
  providers: [ProposalService, CommonModule],
})
export class SharedModule {}
