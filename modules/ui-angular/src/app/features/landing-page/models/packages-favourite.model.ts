export interface PackagesFavourite {
  creationDate: string;
  documentId: number;
  packageId: number;
  ref: string;
  title: string;
  isFavourite: boolean;
}
