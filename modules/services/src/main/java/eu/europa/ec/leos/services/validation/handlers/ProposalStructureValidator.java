/*
 * Copyright 2024 European Union
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 *     https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package eu.europa.ec.leos.services.validation.handlers;

import eu.europa.ec.leos.domain.repository.LeosCategory;
import eu.europa.ec.leos.domain.common.ErrorCode;
import eu.europa.ec.leos.domain.vo.DocumentVO;
import eu.europa.ec.leos.domain.vo.ErrorVO;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Component
public class ProposalStructureValidator implements Validator {

    @Override
    public void validate(DocumentVO documentVO, final List<ErrorVO> result) throws Exception {
        validateProposalStructure(documentVO, result);
    }

    /**
     * A valid proposal should contain instances of this documentvo: memo, bill.
     *
     * @param documentVO, input value
     */
    private void validateProposalStructure(DocumentVO documentVO, final List<ErrorVO> result) {
        String docId = documentVO.getId();
        List<LeosCategory> mandatoryCategories = new ArrayList<>(Collections.singletonList(LeosCategory.PROPOSAL));
        if(!documentVO.getMetadata().isImported()) {
            mandatoryCategories.add(LeosCategory.BILL);
        }
        mandatoryCategories.remove(documentVO.getCategory());
        if (documentVO.getChildDocuments() != null) {
            for (DocumentVO childDoc : documentVO.getChildDocuments()) {
                mandatoryCategories.remove(childDoc.getCategory());
            }
        }
        for (LeosCategory category : mandatoryCategories) {
            result.add(new ErrorVO(ErrorCode.DOCUMENT_NOT_FOUND, docId, StringUtils.capitalize(category.name().toLowerCase())));
        }
    }
}
