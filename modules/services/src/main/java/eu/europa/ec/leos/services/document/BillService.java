/*
 * Copyright 2024 European Union
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 *     https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package eu.europa.ec.leos.services.document;

import com.sun.istack.NotNull;
import eu.europa.ec.leos.domain.repository.common.VersionType;
import eu.europa.ec.leos.domain.repository.document.Bill;
import eu.europa.ec.leos.domain.repository.metadata.BillMetadata;
import eu.europa.ec.leos.domain.common.TocMode;
import eu.europa.ec.leos.domain.vo.CloneDocumentMetadataVO;
import eu.europa.ec.leos.model.action.VersionVO;
import eu.europa.ec.leos.model.user.User;
import eu.europa.ec.leos.services.structure.StructureContext;
import eu.europa.ec.leos.vo.light.Profile;
import eu.europa.ec.leos.vo.structure.TocItem;
import eu.europa.ec.leos.vo.toc.TableOfContentItemVO;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface BillService {

    Bill createBill(String templateId, String path, BillMetadata metadata, String actionMsg, byte[] content);

    Bill createBillFromContent(String path, BillMetadata metadata, String actionMsg, byte[] content, String name);

    Bill createClonedBillFromContent(String path, BillMetadata metadata, CloneDocumentMetadataVO cloneDocumentMetadataVO, String actionMsg, byte[] content, String name);
    
    Bill findBill(String id, boolean latest);

    Bill findBillVersion(String id);

    // FIXME temporary workaround
    Bill findBillByPackagePath(String path);

    Bill updateBill(String id, byte[] updatedContent);

    Bill updateBill(Bill bill, BillMetadata metadata, VersionType versionType, String actionMsg);

    Bill updateBill(Bill bill, byte[] updatedBillContent, String comments);

    Bill updateBill(String ref, String id, Map<String, Object> properties, boolean latest);

    Bill updateBillWithMilestoneComments(Bill bill, List<String> milestoneComments, VersionType versionType, String comment);

    Bill updateBillWithMilestoneComments(String ref, String billId, List<String> milestoneComments);

    Bill addAttachment(Bill bill, String href, String showAs, String actionMsg);

    Bill removeAttachment(Bill bill, String href, String actionMsg);

    Bill updateAttachments(Bill bill, HashMap<String, String> attachmentsElements, String actionMsg);

    Bill createVersion(String id, VersionType versionType, String comment);

    List<Bill> findVersions(String id);
    
    List<VersionVO> getAllVersions(String documentId, String docRef, int pageIndex, int pageSize);

    List<TableOfContentItemVO> getTableOfContent(Bill bill, TocMode mode, List<TocItem> tocItems);

    Bill saveTableOfContent(Bill bill, List<TableOfContentItemVO> tocList, String actionMsg, User user);

    List<TocItem> fetchTocItems(@NotNull Bill bill, StructureContext structureContext, Profile profile);

    List<String> getAncestorsIdsForElementId(Bill bill, List<String> elementIds);

    Bill findBillByRef(String ref);

    Bill getBillByRef(String ref);

    List<Bill> findAllMinorsForIntermediate(String docRef, String currIntVersion, int startIndex, int maxResults);
    
    int findAllMinorsCountForIntermediate(String docRef, String currIntVersion);

    Integer findAllMajorsCount(String docRef);

    List<Bill> findAllMajors(String docRef, int startIndex, int maxResults);

    Integer findRecentMinorVersionsCount(String documentId, String documentRef);

    List<Bill> findRecentMinorVersions(String documentId, String documentRef, int startIndex, int maxResults);

    Bill findFirstVersion(String documentRef);

    String generateBillReference(byte[] content, String language);
}
