package eu.europa.ec.leos.notice.common.xml;

public class GenericConversionError extends RuntimeException {
    public GenericConversionError(String message) {
        super(message);
    }
}
