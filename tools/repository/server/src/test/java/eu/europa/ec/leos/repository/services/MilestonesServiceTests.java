/*
 * Copyright 2024 European Union
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 *     https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package eu.europa.ec.leos.repository.services;

import eu.europa.ec.leos.repository.TestUtils;
import eu.europa.ec.leos.repository.controllers.requests.QueryFilter;
import eu.europa.ec.leos.repository.entities.DocumentMilestone;
import eu.europa.ec.leos.repository.entities.DocumentMilestoneList;
import eu.europa.ec.leos.repository.exceptions.RepositoryException;
import eu.europa.ec.leos.repository.model.LeosDocument;
import eu.europa.ec.leos.repository.repositories.DocumentMilestoneListRepository;
import eu.europa.ec.leos.repository.repositories.DocumentMilestoneRepository;
import eu.europa.ec.leos.repository.utils.ConversionUtils;
import org.assertj.core.util.Sets;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertFalse;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class MilestonesServiceTests {
    @Autowired
    PackageService packageService;
    @Autowired
    DocumentService documentService;
    @Autowired
    DocumentMilestoneRepository documentMilestoneRepository;
    @Autowired
    DocumentMilestoneListRepository documentMilestoneListRepository;

    private LeosDocument milestone;
    private final String PKG_NAME = "testMilestone";
    private final String MILESTONE_NAME = "PROP_ACT-clilif9gy0000ro28zt8al0yh-en.leg";
    private final String CAT = "LEG";
    private eu.europa.ec.leos.repository.model.Package pkg;
    private byte[] content;

    @Before
    public void setup() throws RepositoryException {
        pkg = packageService.createPackage(PKG_NAME, null,null, "EN", false, "demo");
        Map<String, ?> properties = new HashMap() {{
            put("status", "IN_PREPARATION");
            put("containedDocuments", Arrays.asList("ANNEX-clfwd4ig3000h9256za2lfv6x-en.xml", "DIR-clfwc8tt900099256foj1l39z-en.xml",
                    "EXPL_MEMORANDUM-clfwc8sc60008925620y1bsfl-en.xml", "main-clfwc8rhf00079256x6j3x0t9-en.xml", "STAT_FINANC_LEGIS-clfwcle7j000a9256o4cvc2p6-en.xml"));
            put("category", CAT);
            put("milestoneComments", Arrays.asList("For Interservice Consultation"));
            put("jobDate", ConversionUtils.getLeosDateAsString(new Date(), ConversionUtils.LEOS_REPO_DATE_FORMAT));
            put("initialCreationDate", ConversionUtils.getLeosDateAsString(new Date(), ConversionUtils.LEOS_REPO_DATE_FORMAT));
            put("initialCreatedBy", "jane");
            put ("name", MILESTONE_NAME);
            put("jobId", "230607113102710TBXAVVHGGP");
        }};
        content = TestUtils.getFileContent("/milestone/PROP_ACT-TEST-EN.leg");
        milestone = documentService.createDocumentFromContent(PKG_NAME,
                MILESTONE_NAME, properties, "0.1.1", 3,
                content, "First version", "jane");
        assertNotNull(milestone);
        Optional<DocumentMilestone> docMilestone = documentMilestoneRepository.findById(milestone.getVersionId());
        assertTrue(docMilestone.isPresent());
        List<DocumentMilestoneList> milestonesDocuments =
                documentMilestoneListRepository.findDocumentMilestoneListsByMilestone(docMilestone.get());
        assertFalse(milestonesDocuments.isEmpty());
        assertEquals(milestonesDocuments.size(), 5);
    }

    @After
    public void after() throws RepositoryException {
        packageService.deletePackage(PKG_NAME);
    }

    @Test
    @Transactional
    public void test_searchMilestone() throws RepositoryException {
        Optional<LeosDocument> milestoneOpt = documentService.findDocumentByName(MILESTONE_NAME);
        assertNotNull(milestoneOpt);
        assertTrue(milestoneOpt.isPresent());
        LeosDocument milestone = milestoneOpt.get();
        assertEquals(milestone.getName(), MILESTONE_NAME);
        assertTrue(Arrays.equals(milestone.getSource(), content));
        assertEquals(milestone.getRef(), MILESTONE_NAME.substring(0, MILESTONE_NAME.lastIndexOf('.')));
        assertEquals(milestone.getMetadata().get("containedDocuments"), this.milestone.getMetadata().get("containedDocuments"));
        assertEquals(milestone.getMetadata().get("milestoneComments"), this.milestone.getMetadata().get("milestoneComments"));
        assertEquals(milestone.getCategory(), CAT);
        assertEquals(pkg.getId(), milestone.getPackageId());
        assertEquals(milestone.getMetadata().get("status"), this.milestone.getMetadata().get("status"));
    }

    @Test
    @Transactional
    public void test_findMilestoneByStatus() throws RepositoryException {
        List<LeosDocument> milestones = documentService.findDocumentsByStatus("IN_PREPARATION");
        assertEquals(milestones.size(), 1);
        assertEquals(milestones.get(0).getName(), MILESTONE_NAME);
        assertEquals(milestones.get(0).getRef(), MILESTONE_NAME.substring(0, MILESTONE_NAME.lastIndexOf('.')));
        assertEquals(milestones.get(0).getMetadata().get("containedDocuments"), milestone.getMetadata().get("containedDocuments"));
        assertEquals(milestones.get(0).getMetadata().get("milestoneComments"), milestone.getMetadata().get("milestoneComments"));
        assertEquals(milestones.get(0).getCategory(), CAT);
        assertEquals(pkg.getId(), milestones.get(0).getPackageId());
        assertEquals(milestones.get(0).getMetadata().get("status"), milestone.getMetadata().get("status"));
    }

    @Test
    public void test_updateMilestone() throws Exception {
        Map<String, ?> properties = new HashMap() {
            {
                put("status", "FILE_READY");
            }};
        LeosDocument updatedMilestone = documentService.updateDocument(milestone.getRef(), milestone.getVersionId(), properties, "demo", true);
        assertEquals(updatedMilestone.getName(), MILESTONE_NAME);
        assertTrue(Arrays.equals(updatedMilestone.getSource(), content));
        assertEquals(updatedMilestone.getRef(), MILESTONE_NAME.substring(0, MILESTONE_NAME.lastIndexOf('.')));
        assertEquals(updatedMilestone.getMetadata().get("containedDocuments"), milestone.getMetadata().get("containedDocuments"));
        assertEquals(updatedMilestone.getMetadata().get("milestoneComments"), milestone.getMetadata().get("milestoneComments"));
        assertEquals(updatedMilestone.getCategory(), CAT);
        assertEquals(pkg.getId(), updatedMilestone.getPackageId());
        assertEquals(updatedMilestone.getMetadata().get("status"), "FILE_READY");
    }

    @Test
    @Transactional(readOnly = true)
    public void test_searchMilestonesWithFilter() {
        QueryFilter filter = new QueryFilter();
        filter.addFilter(new QueryFilter.Filter("containedDocuments", "IN", false, "ANNEX-clfwd4ig3000h9256za2lfv6x-en.xml", "DIR-clfwc8tt900099256foj1l39z" +
                "-en.xml"));
        Set<String> categories = Sets.set("LEG");
        List<LeosDocument> docs = documentService.findDocumentsUsingFilter("%", categories, filter, 0, 5, false);
        assertEquals(docs.size(), 1);
        assertEquals(docs.get(0).getCategory(), "LEG");
    }

    @Test
    @Transactional(readOnly = true)
    public void test_searchMilestonesWithFilterInPackage() {
        QueryFilter filter = new QueryFilter();
        filter.addFilter(new QueryFilter.Filter("containedDocuments", "IN", false, "ANNEX-clfwd4ig3000h9256za2lfv6x-en.xml", "DIR-clfwc8tt900099256foj1l39z" +
                "-en.xml"));
        Set<String> categories = Sets.set("LEG");
        List<LeosDocument> docs = documentService.findDocumentsUsingFilter("package-test", categories, filter, 0, 5, false);
        assertEquals(docs.size(), 0);
    }

    @Test
    @Transactional(readOnly = true)
    public void test_countMilestonesWithFilterInPackage() {
        QueryFilter filter = new QueryFilter();
        filter.addFilter(new QueryFilter.Filter("containedDocuments", "IN", false, "ANNEX-clfwd4ig3000h9256za2lfv6x-en.xml", "DIR-clfwc8tt900099256foj1l39z" +
                "-en.xml"));
        Set<String> categories = Sets.set("LEG");
        long count = documentService.countDocumentsUsingFilter("package-test", categories, filter);
        assertEquals(count, 0L);
    }

    @Test
    @Transactional(readOnly = true)
    public void test_countMilestonesWithFilter() {
        QueryFilter filter = new QueryFilter();
        filter.addFilter(new QueryFilter.Filter("containedDocuments", "IN", false, "ANNEX-clfwd4ig3000h9256za2lfv6x-en.xml", "DIR-clfwc8tt900099256foj1l39z" +
                "-en.xml"));
        Set<String> categories = Sets.set("LEG");
        long count = documentService.countDocumentsUsingFilter("%", categories, filter);
        assertEquals(count, 1L);
    }

    @Test
    @Transactional(readOnly = true)
    public void test_documentsByPackageName() throws RepositoryException {
        List<LeosDocument> docs = packageService.findDocumentsByPackageName( "%",
                Sets.set("LEG"), true, false);
        assertEquals(1, docs.size());
    }
}
