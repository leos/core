
Lists of 110 third-party dependencies.
     (BSD License) AntLR Parser Generator (antlr:antlr:2.7.7 - http://www.antlr.org/)
     (Eclipse Public License - v 1.0) (GNU Lesser General Public License) Logback Classic Module (ch.qos.logback:logback-classic:1.2.11 - http://logback.qos.ch/logback-classic)
     (Eclipse Public License - v 1.0) (GNU Lesser General Public License) Logback Core Module (ch.qos.logback:logback-core:1.2.11 - http://logback.qos.ch/logback-core)
     (The MIT License (MIT)) java jwt (com.auth0:java-jwt:3.2.0 - http://www.jwt.io)
     (Apache License, Version 2.0) ClassMate (com.fasterxml:classmate:1.5.1 - https://github.com/FasterXML/java-classmate)
     (The Apache Software License, Version 2.0) Jackson-annotations (com.fasterxml.jackson.core:jackson-annotations:2.13.2 - http://github.com/FasterXML/jackson)
     (The Apache Software License, Version 2.0) Jackson-core (com.fasterxml.jackson.core:jackson-core:2.13.2 - https://github.com/FasterXML/jackson-core)
     (The Apache Software License, Version 2.0) jackson-databind (com.fasterxml.jackson.core:jackson-databind:2.13.2.1 - http://github.com/FasterXML/jackson)
     (The Apache Software License, Version 2.0) Jackson datatype: jdk8 (com.fasterxml.jackson.datatype:jackson-datatype-jdk8:2.13.2 - https://github.com/FasterXML/jackson-modules-java8/jackson-datatype-jdk8)
     (The Apache Software License, Version 2.0) Jackson datatype: JSR310 (com.fasterxml.jackson.datatype:jackson-datatype-jsr310:2.13.2 - https://github.com/FasterXML/jackson-modules-java8/jackson-datatype-jsr310)
     (The Apache Software License, Version 2.0) Jackson-module-parameter-names (com.fasterxml.jackson.module:jackson-module-parameter-names:2.13.2 - https://github.com/FasterXML/jackson-modules-java8/jackson-module-parameter-names)
     (GNU LESSER GENERAL PUBLIC LICENSE, Version 2.1) SpotBugs Annotations (com.github.spotbugs:spotbugs-annotations:3.1.0 - https://spotbugs.github.io/)
     (Apache 2) Spring Test DBUnit (com.github.springtestdbunit:spring-test-dbunit:1.3.0 - https://springtestdbunit.github.com/spring-test-dbunit)
     (The Apache Software License, Version 2.0) FindBugs-jsr305 (com.google.code.findbugs:jsr305:3.0.2 - http://findbugs.sourceforge.net/)
     (MPL 2.0 or EPL 1.0) H2 Database Engine (com.h2database:h2:1.4.200 - https://h2database.com)
     (The Apache Software License, Version 2.0) project ':json-path' (com.jayway.jsonpath:json-path:2.6.0 - https://github.com/jayway/JsonPath)
     (Oracle Free Use Terms and Conditions (FUTC)) ojdbc8 (com.oracle.database.jdbc:ojdbc8:21.3.0.0 - https://www.oracle.com/database/technologies/maven-central-guide.html)
     (EDL 1.0) Jakarta Activation (com.sun.activation:jakarta.activation:1.2.2 - https://github.com/eclipse-ee4j/jaf/jakarta.activation)
     (Eclipse Distribution License - v 1.0) istack common utility code runtime (com.sun.istack:istack-commons-runtime:3.0.12 - https://projects.eclipse.org/projects/ee4j/istack-commons/istack-commons-runtime)
     (Apache License 2.0) JSON library from Android SDK (com.vaadin.external.google:android-json:0.0.20131108.vaadin1 - http://developer.android.com/sdk)
     (The Apache Software License, Version 2.0) HikariCP (com.zaxxer:HikariCP:4.0.3 - https://github.com/brettwooldridge/HikariCP)
     (Apache License, Version 2.0) Apache Commons Codec (commons-codec:commons-codec:1.15 - https://commons.apache.org/proper/commons-codec/)
     (The Apache Software License, Version 2.0) Commons IO (commons-io:commons-io:1.3.2 - http://jakarta.apache.org/commons/io/)
     (The Apache Software License, Version 2.0) Apache Commons Logging (commons-logging:commons-logging:1.2 - http://commons.apache.org/proper/commons-logging/)
     (European Union Public Licence (EUPL)) annotate-client (eu.europa.ec.leos.annotate:annotate-client:4.1.0-SNAPSHOT - https://joinup.ec.europa.eu/software/leos/annotate-client)
     (European Union Public Licence (EUPL)) annotate-server (eu.europa.ec.leos.annotate:annotate-server:4.1.0-SNAPSHOT - https://joinup.ec.europa.eu/software/leos/annotate-server)
     (EDL 1.0) Jakarta Activation API jar (jakarta.activation:jakarta.activation-api:1.2.2 - https://github.com/eclipse-ee4j/jaf/jakarta.activation-api)
     (EPL 2.0) (GPL2 w/ CPE) Jakarta Annotations API (jakarta.annotation:jakarta.annotation-api:1.3.5 - https://projects.eclipse.org/projects/ee4j.ca)
     (Eclipse Distribution License v. 1.0) (Eclipse Public License v. 2.0) Jakarta Persistence API (jakarta.persistence:jakarta.persistence-api:2.2.3 - https://github.com/eclipse-ee4j/jpa-api)
     (EPL 2.0) (GPL2 w/ CPE) javax.transaction API (jakarta.transaction:jakarta.transaction-api:1.3.3 - https://projects.eclipse.org/projects/ee4j.jta)
     (Eclipse Distribution License - v 1.0) Jakarta XML Binding API (jakarta.xml.bind:jakarta.xml.bind-api:2.3.3 - https://github.com/eclipse-ee4j/jaxb-api/jakarta.xml.bind-api)
     (Eclipse Public License 1.0) JUnit (junit:junit:4.13.1 - http://junit.org)
     (Apache License, Version 2.0) Byte Buddy (without dependencies) (net.bytebuddy:byte-buddy:1.11.22 - https://bytebuddy.net/byte-buddy)
     (Apache License, Version 2.0) Byte Buddy agent (net.bytebuddy:byte-buddy-agent:1.11.22 - https://bytebuddy.net/byte-buddy-agent)
     (The Apache Software License, Version 2.0) ASM based accessors helper used by json-smart (net.minidev:accessors-smart:2.4.8 - https://urielch.github.io/)
     (The Apache Software License, Version 2.0) JSON Small and Fast Parser (net.minidev:json-smart:2.4.8 - https://urielch.github.io/)
     (Apache License, Version 2.0) Apache HttpClient (org.apache.httpcomponents:httpclient:4.5.3 - http://hc.apache.org/httpcomponents-client)
     (Apache License, Version 2.0) Apache HttpCore (org.apache.httpcomponents:httpcore:4.4.15 - http://hc.apache.org/httpcomponents-core-ga)
     (Apache License, Version 2.0) Apache Log4j API (org.apache.logging.log4j:log4j-api:2.17.2 - https://logging.apache.org/log4j/2.x/log4j-api/)
     (Apache License, Version 2.0) Apache Log4j to SLF4J Adapter (org.apache.logging.log4j:log4j-to-slf4j:2.17.2 - https://logging.apache.org/log4j/2.x/log4j-to-slf4j/)
     (Apache License, Version 2.0) tomcat-embed-core (org.apache.tomcat.embed:tomcat-embed-core:9.0.62 - https://tomcat.apache.org/)
     (Apache License, Version 2.0) tomcat-embed-el (org.apache.tomcat.embed:tomcat-embed-el:9.0.62 - https://tomcat.apache.org/)
     (Apache License, Version 2.0) tomcat-embed-websocket (org.apache.tomcat.embed:tomcat-embed-websocket:9.0.62 - https://tomcat.apache.org/)
     (The Apache License, Version 2.0) org.apiguardian:apiguardian-api (org.apiguardian:apiguardian-api:1.1.2 - https://github.com/apiguardian-team/apiguardian)
     (Eclipse Public License - v 1.0) AspectJ runtime (org.aspectj:aspectjrt:1.8.13 - http://www.aspectj.org)
     (Eclipse Public License - v 2.0) AspectJ Weaver (org.aspectj:aspectjweaver:1.9.7 - https://www.eclipse.org/aspectj/)
     (Apache License, Version 2.0) AssertJ fluent assertions (org.assertj:assertj-core:3.21.0 - https://assertj.github.io/doc/assertj-core/)
     (Bouncy Castle Licence) Bouncy Castle Provider (org.bouncycastle:bcprov-jdk15on:1.55 - http://www.bouncycastle.org/java.html)
     (The MIT License) Checker Qual (org.checkerframework:checker-qual:3.5.0 - https://checkerframework.org)
     (GNU Lesser General Public License, Version 2.1) dbUnit Extension (org.dbunit:dbunit:2.7.3 - http://dbunit.sourceforge.net)
     (Apache License, Version 2.0) Apache FreeMarker (org.freemarker:freemarker:2.3.31 - https://freemarker.apache.org/)
     (Eclipse Distribution License - v 1.0) JAXB Runtime (org.glassfish.jaxb:jaxb-runtime:2.3.6 - https://eclipse-ee4j.github.io/jaxb-ri/)
     (Eclipse Distribution License - v 1.0) TXW2 Runtime (org.glassfish.jaxb:txw2:2.3.6 - https://eclipse-ee4j.github.io/jaxb-ri/)
     (BSD License 3) Hamcrest (org.hamcrest:hamcrest:2.2 - http://hamcrest.org/JavaHamcrest/)
     (BSD License 3) Hamcrest Core (org.hamcrest:hamcrest-core:2.2 - http://hamcrest.org/JavaHamcrest/)
     (GNU Library General Public License v2.1 or later) Hibernate ORM - hibernate-core (org.hibernate:hibernate-core:5.6.8.Final - https://hibernate.org/orm)
     (GNU Library General Public License v2.1 or later) Hibernate Commons Annotations (org.hibernate.common:hibernate-commons-annotations:5.1.2.Final - http://hibernate.org)
     (The Apache Software License, Version 2.0) javatuples (org.javatuples:javatuples:1.2 - http://www.javatuples.org)
     (Apache License, Version 2.0) Java Annotation Indexer (org.jboss:jandex:2.4.2.Final - http://www.jboss.org/jandex)
     (Apache License, version 2.0) JBoss Logging 3 (org.jboss.logging:jboss-logging:3.4.3.Final - http://www.jboss.org)
     (Eclipse Public License v2.0) JUnit Jupiter (Aggregator) (org.junit.jupiter:junit-jupiter:5.8.2 - https://junit.org/junit5/)
     (Eclipse Public License v2.0) JUnit Jupiter API (org.junit.jupiter:junit-jupiter-api:5.8.2 - https://junit.org/junit5/)
     (Eclipse Public License v2.0) JUnit Jupiter Engine (org.junit.jupiter:junit-jupiter-engine:5.8.2 - https://junit.org/junit5/)
     (Eclipse Public License v2.0) JUnit Jupiter Params (org.junit.jupiter:junit-jupiter-params:5.8.2 - https://junit.org/junit5/)
     (Eclipse Public License v2.0) JUnit Platform Commons (org.junit.platform:junit-platform-commons:1.8.2 - https://junit.org/junit5/)
     (Eclipse Public License v2.0) JUnit Platform Engine API (org.junit.platform:junit-platform-engine:1.8.2 - https://junit.org/junit5/)
     (The MIT License) mockito-core (org.mockito:mockito-core:4.0.0 - https://github.com/mockito/mockito)
     (The MIT License) mockito-junit-jupiter (org.mockito:mockito-junit-jupiter:4.0.0 - https://github.com/mockito/mockito)
     (Apache License, Version 2.0) Objenesis (org.objenesis:objenesis:3.2 - http://objenesis.org/objenesis)
     (The Apache License, Version 2.0) org.opentest4j:opentest4j (org.opentest4j:opentest4j:1.2.0 - https://github.com/ota4j-team/opentest4j)
     (BSD-3-Clause) asm (org.ow2.asm:asm:9.1 - http://asm.ow2.io/)
     (BSD-2-Clause) PostgreSQL JDBC Driver (org.postgresql:postgresql:42.3.4 - https://jdbc.postgresql.org)
     (The Apache Software License, Version 2.0) JSONassert (org.skyscreamer:jsonassert:1.5.0 - https://github.com/skyscreamer/JSONassert)
     (MIT License) JUL to SLF4J bridge (org.slf4j:jul-to-slf4j:1.7.36 - http://www.slf4j.org)
     (MIT License) SLF4J API Module (org.slf4j:slf4j-api:1.7.36 - http://www.slf4j.org)
     (Apache License, Version 2.0) Spring AOP (org.springframework:spring-aop:5.3.19 - https://github.com/spring-projects/spring-framework)
     (Apache License, Version 2.0) Spring Aspects (org.springframework:spring-aspects:5.3.19 - https://github.com/spring-projects/spring-framework)
     (Apache License, Version 2.0) Spring Beans (org.springframework:spring-beans:5.3.19 - https://github.com/spring-projects/spring-framework)
     (Apache License, Version 2.0) Spring Context (org.springframework:spring-context:5.3.19 - https://github.com/spring-projects/spring-framework)
     (Apache License, Version 2.0) Spring Context Support (org.springframework:spring-context-support:5.3.19 - https://github.com/spring-projects/spring-framework)
     (Apache License, Version 2.0) Spring Core (org.springframework:spring-core:5.3.19 - https://github.com/spring-projects/spring-framework)
     (Apache License, Version 2.0) Spring Expression Language (SpEL) (org.springframework:spring-expression:5.3.19 - https://github.com/spring-projects/spring-framework)
     (Apache License, Version 2.0) Spring Commons Logging Bridge (org.springframework:spring-jcl:5.3.19 - https://github.com/spring-projects/spring-framework)
     (Apache License, Version 2.0) Spring JDBC (org.springframework:spring-jdbc:5.3.19 - https://github.com/spring-projects/spring-framework)
     (Apache License, Version 2.0) Spring Messaging (org.springframework:spring-messaging:5.3.19 - https://github.com/spring-projects/spring-framework)
     (Apache License, Version 2.0) Spring Object/Relational Mapping (org.springframework:spring-orm:5.3.19 - https://github.com/spring-projects/spring-framework)
     (Apache License, Version 2.0) Spring TestContext Framework (org.springframework:spring-test:5.3.19 - https://github.com/spring-projects/spring-framework)
     (Apache License, Version 2.0) Spring Transaction (org.springframework:spring-tx:5.3.19 - https://github.com/spring-projects/spring-framework)
     (Apache License, Version 2.0) Spring Web (org.springframework:spring-web:5.3.19 - https://github.com/spring-projects/spring-framework)
     (Apache License, Version 2.0) Spring Web MVC (org.springframework:spring-webmvc:5.3.19 - https://github.com/spring-projects/spring-framework)
     (Apache License, Version 2.0) Spring WebSocket (org.springframework:spring-websocket:5.3.19 - https://github.com/spring-projects/spring-framework)
     (Apache License, Version 2.0) spring-boot (org.springframework.boot:spring-boot:2.6.7 - https://spring.io/projects/spring-boot)
     (Apache License, Version 2.0) spring-boot-autoconfigure (org.springframework.boot:spring-boot-autoconfigure:2.6.7 - https://spring.io/projects/spring-boot)
     (Apache License, Version 2.0) spring-boot-starter (org.springframework.boot:spring-boot-starter:2.6.7 - https://spring.io/projects/spring-boot)
     (Apache License, Version 2.0) spring-boot-starter-aop (org.springframework.boot:spring-boot-starter-aop:2.6.7 - https://spring.io/projects/spring-boot)
     (Apache License, Version 2.0) spring-boot-starter-data-jpa (org.springframework.boot:spring-boot-starter-data-jpa:2.6.7 - https://spring.io/projects/spring-boot)
     (Apache License, Version 2.0) spring-boot-starter-freemarker (org.springframework.boot:spring-boot-starter-freemarker:2.6.7 - https://spring.io/projects/spring-boot)
     (Apache License, Version 2.0) spring-boot-starter-jdbc (org.springframework.boot:spring-boot-starter-jdbc:2.6.7 - https://spring.io/projects/spring-boot)
     (Apache License, Version 2.0) spring-boot-starter-json (org.springframework.boot:spring-boot-starter-json:2.6.7 - https://spring.io/projects/spring-boot)
     (Apache License, Version 2.0) spring-boot-starter-logging (org.springframework.boot:spring-boot-starter-logging:2.6.7 - https://spring.io/projects/spring-boot)
     (Apache License, Version 2.0) spring-boot-starter-test (org.springframework.boot:spring-boot-starter-test:2.6.7 - https://spring.io/projects/spring-boot)
     (Apache License, Version 2.0) spring-boot-starter-tomcat (org.springframework.boot:spring-boot-starter-tomcat:2.6.7 - https://spring.io/projects/spring-boot)
     (Apache License, Version 2.0) spring-boot-starter-web (org.springframework.boot:spring-boot-starter-web:2.6.7 - https://spring.io/projects/spring-boot)
     (Apache License, Version 2.0) spring-boot-starter-websocket (org.springframework.boot:spring-boot-starter-websocket:2.6.7 - https://spring.io/projects/spring-boot)
     (Apache License, Version 2.0) spring-boot-test (org.springframework.boot:spring-boot-test:2.6.7 - https://spring.io/projects/spring-boot)
     (Apache License, Version 2.0) spring-boot-test-autoconfigure (org.springframework.boot:spring-boot-test-autoconfigure:2.6.7 - https://spring.io/projects/spring-boot)
     (Apache License, Version 2.0) Spring Data Core (org.springframework.data:spring-data-commons:2.6.4 - https://www.spring.io/spring-data/spring-data-commons)
     (Apache License, Version 2.0) Spring Data JPA (org.springframework.data:spring-data-jpa:2.6.4 - https://projects.spring.io/spring-data-jpa)
     (The Apache Software License, Version 2.0) org.xmlunit:xmlunit-core (org.xmlunit:xmlunit-core:2.8.4 - https://www.xmlunit.org/)
     (Apache License, Version 2.0) SnakeYAML (org.yaml:snakeyaml:1.29 - http://www.snakeyaml.org)
