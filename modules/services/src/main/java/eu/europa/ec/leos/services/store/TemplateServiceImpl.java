/*
 * Copyright 2024 European Union
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 *     https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package eu.europa.ec.leos.services.store;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.StaxDriver;
import eu.europa.ec.leos.domain.repository.Content;
import eu.europa.ec.leos.domain.repository.document.ConfigDocument;
import eu.europa.ec.leos.domain.repository.document.XmlDocument;
import eu.europa.ec.leos.repository.store.ConfigurationRepository;
import eu.europa.ec.leos.services.support.converter.DescriptionMapConverter;
import eu.europa.ec.leos.services.support.converter.LanguageMapConverter;
import eu.europa.ec.leos.services.support.converter.NameMapConverter;
import eu.europa.ec.leos.services.template.TemplateConfigurationService;
import eu.europa.ec.leos.vo.catalog.Catalog;
import eu.europa.ec.leos.vo.catalog.CatalogItem;
import io.atlassian.fugue.Option;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.List;

@Service
class TemplateServiceImpl implements TemplateService {

    private static final Logger LOG = LoggerFactory.getLogger(TemplateServiceImpl.class);

    private final ConfigurationRepository configRepository;

    @Value("${leos.templates.path}")
    private String templatesPath;

    @Value("${leos.templates.catalog}")
    private String templatesCatalog;

    @Value("${leos.environment}")
    private String leosEnvironment;

    @Autowired
    TemplateConfigurationService templateConfigurationService;

    TemplateServiceImpl(ConfigurationRepository configRepository) {
        this.configRepository = configRepository;
    }

    @Override
    public List<CatalogItem> getTemplatesCatalog() throws IOException {
        List<CatalogItem> catalogList = getCatalogItems(templatesCatalog);
        removeNotAllowedProposals(catalogList);
        return catalogList;
    }

    private boolean removeNotAllowedProposals(List<CatalogItem> catalogList) throws JsonProcessingException {
        boolean hasTemplate = false;
        for (CatalogItem catalogItem : catalogList) {
            if (catalogItem.isHidden() == null || !catalogItem.isHidden()) {
                boolean removeThisItem = true;
                if (catalogItem.getType().name().equals(CatalogItem.ItemType.TEMPLATE.name()) && verifyIfItemShouldBeProcessed(catalogItem)) {
                    hasTemplate = true;
                    removeThisItem = false;
                }
                if (!catalogItem.getType().name().equals(CatalogItem.ItemType.TEMPLATE.name()) && catalogItem.getItems() != null) {
                    boolean childrenHasTemplate = removeNotAllowedProposals(catalogItem.getItems());
                    if (childrenHasTemplate) {
                        hasTemplate = true;
                        removeThisItem = false;
                    }
                }
                if (removeThisItem) {
                    catalogItem.setHidden(true);
                }
            }
        }
        return hasTemplate;
    }

    private boolean verifyIfItemShouldBeProcessed(CatalogItem catalogItem) throws JsonProcessingException {
        boolean itemShouldBeProcessed = true;
        ObjectMapper objectMapper = new ObjectMapper();
        String catalogConf = templateConfigurationService.getTemplateConfiguration("catalog");
        JsonNode catalogNode = objectMapper.readTree(catalogConf);
        if (catalogNode != null && catalogNode.get(catalogItem.getKey()) != null) {
            if (catalogNode.get(catalogItem.getKey()).get("environments") != null) {
                itemShouldBeProcessed = checkJsonNodeForCatalog(catalogItem, catalogNode.get(catalogItem.getKey()));
            }
            if (catalogNode.get(catalogItem.getKey()).get("list-of-environments") != null) {
                JsonNode listEnvironmentJsonNode = catalogNode.get(catalogItem.getKey()).get("list-of-environments");
                for (JsonNode itemEnvironmentJsonNode : listEnvironmentJsonNode) {
                    if (itemEnvironmentJsonNode.get("environments") != null) {
                        itemShouldBeProcessed = checkJsonNodeForCatalog(catalogItem, itemEnvironmentJsonNode);
                    }
                }
            }
        }
        return itemShouldBeProcessed;
    }

    private boolean checkJsonNodeForCatalog(CatalogItem catalogItem, JsonNode catalogNode) {
        boolean itemShouldBeProcessed = true;
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm");
        String environments = catalogNode.get("environments").asText();
        if (!environments.contains(leosEnvironment) && !environments.contains("all")) {
            itemShouldBeProcessed = false;
        }
        String startDateStr = "";
        if (catalogNode.get("start-date") != null) {
            startDateStr = catalogNode.get("start-date").asText();
            LocalDateTime startDate = LocalDateTime.parse(startDateStr, dateTimeFormatter);
            if (LocalDateTime.now().isBefore(startDate)) {
                itemShouldBeProcessed = false;
            }
        }
        String endDateStr = "";
        if (catalogNode.get("end-date") != null) {
            endDateStr = catalogNode.get("end-date").asText();
            LocalDateTime endDate = LocalDateTime.parse(endDateStr, dateTimeFormatter);
            if (LocalDateTime.now().isAfter(endDate)) {
                itemShouldBeProcessed = false;
            }
        }
        return itemShouldBeProcessed;
    }

    private List<CatalogItem> getCatalogItems(String templatesCatalog) throws IOException {
        LOG.trace("Getting templates catalog... [path={}, name={}]", templatesPath, templatesCatalog);
        ConfigDocument catalogDocument = configRepository.findConfiguration(templatesPath, templatesCatalog);
        Option<Content> optContent = catalogDocument.getContent();
        if (optContent.isDefined()) {
            // FIXME handle option functionally?
            LOG.trace("Parsing templates catalog XML content...");
            Catalog catalog = loadCatalog(optContent.get().getSource().getInputStream());
            // FIXME handle null catalog!!!
            return catalog != null ? catalog.getItems() : Collections.emptyList();
        } else {
            LOG.trace("Templates catalog XML content is empty!");
            return Collections.emptyList();
        }
    }

    private Catalog loadCatalog(InputStream xmlCatalog) throws IOException {
        XStream xstream = new XStream(new StaxDriver());
        xstream.alias("catalog", Catalog.class);
        xstream.useAttributeFor(Catalog.class, "defaultLanguage");
        xstream.aliasAttribute(Catalog.class, "defaultLanguage", "lang");
        xstream.addImplicitCollection(Catalog.class, "itemList", CatalogItem.class);
        xstream.alias("item", CatalogItem.class);
        xstream.useAttributeFor(CatalogItem.class, "type");
        xstream.useAttributeFor(CatalogItem.class, "id");
        xstream.useAttributeFor(CatalogItem.class, "enabled");
        xstream.useAttributeFor(CatalogItem.class, "hidden");
        xstream.useAttributeFor(CatalogItem.class, "key");
        xstream.useAttributeFor(CatalogItem.class, "mandatory");
        xstream.aliasAttribute(CatalogItem.class, "defaultDocument", "default");
        xstream.aliasField("names", CatalogItem.class, "nameMap");
        xstream.registerLocalConverter(CatalogItem.class, "nameMap", new NameMapConverter());
        xstream.aliasField("descriptions", CatalogItem.class, "descMap");
        xstream.registerLocalConverter(CatalogItem.class, "descMap", new DescriptionMapConverter());
        xstream.aliasField("languages", CatalogItem.class, "langMap");
        xstream.registerLocalConverter(CatalogItem.class, "langMap", new LanguageMapConverter());
        xstream.addImplicitCollection(CatalogItem.class, "itemList", CatalogItem.class);
        xstream.allowTypesByWildcard(getAllowedTypes());

        // parse XML
        Catalog catalog = (Catalog) xstream.fromXML(xmlCatalog);

        if (catalog == null) {
            LOG.warn("Unable to load catalog!");
        }
        return catalog;
    }

    private String[] getAllowedTypes() {
        return new String[] {"eu.europa.ec.leos.**"};
    }

    @Override
    public XmlDocument getTemplate(String name) {
        LOG.trace("Getting template... [path={}, name={}]", templatesPath, name);
        return configRepository.findTemplate(templatesPath, name);
    }

    @Override
    public String getTemplateName(List<CatalogItem> catalogItems, String name, String language) {
        String templateName = "";
        for (CatalogItem item : catalogItems) {
            if (item.getId() != null && item.getId().contains(name)) {
                return item.getName(language);
            } else {
                templateName = getTemplateName(item.getItems(), name, language);
                if (templateName != null && !templateName.isEmpty()) {
                    return templateName;
                }
            }
        }
        return templateName;
    }

    @Override
    public CatalogItem getTemplateItem(String name) throws IOException {
        return getTemplateItem(getTemplatesCatalog(), name);
    }

    private CatalogItem getTemplateItem(List<CatalogItem> catalogItems, String name) {
        CatalogItem templateItem = null;
        for (CatalogItem item : catalogItems) {
            if (item.getId() != null && item.getId().contains(name)) {
                return item;
            } else {
                templateItem = getTemplateItem(item.getItems(), name);
                if (templateItem != null) {
                    return templateItem;
                }
            }
        }
        return templateItem;
    }
}
