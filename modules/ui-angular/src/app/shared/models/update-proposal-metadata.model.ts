export interface UpdateProposalMetadataModel {
  docPurpose: string;
  eeaRelevance: boolean;
}
