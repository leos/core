/*
 * Copyright 2024 European Union
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 *     https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package eu.europa.ec.leos.services.collection;

import eu.europa.ec.leos.domain.repository.LeosPackage;
import eu.europa.ec.leos.domain.repository.common.VersionType;
import eu.europa.ec.leos.domain.repository.document.Explanatory;
import eu.europa.ec.leos.domain.repository.document.Proposal;
import eu.europa.ec.leos.domain.repository.metadata.ProposalMetadata;
import eu.europa.ec.leos.domain.common.InstanceType;
import eu.europa.ec.leos.i18n.MessageHelper;
import eu.europa.ec.leos.instance.Instance;
import eu.europa.ec.leos.security.SecurityContext;
import eu.europa.ec.leos.services.collection.document.AnnexContextService;
import eu.europa.ec.leos.services.collection.document.BillContextService;
import eu.europa.ec.leos.services.collection.document.ContextActionService;
import eu.europa.ec.leos.services.collection.document.ExplanatoryContextService;
import eu.europa.ec.leos.services.collection.document.FinancialStatementContextService;
import eu.europa.ec.leos.services.collection.document.MemorandumContextService;
import eu.europa.ec.leos.services.document.ExplanatoryService;
import eu.europa.ec.leos.services.document.ProposalService;
import eu.europa.ec.leos.services.store.PackageService;
import eu.europa.ec.leos.services.store.TemplateService;
import eu.europa.ec.leos.services.support.url.CollectionUrlBuilder;
import io.atlassian.fugue.Option;
import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.inject.Provider;

import java.util.List;
import java.util.Map;

import static eu.europa.ec.leos.domain.repository.LeosCategory.COUNCIL_EXPLANATORY;
import static eu.europa.ec.leos.domain.repository.LeosCategory.PROPOSAL;

@Service
@Instance(InstanceType.COUNCIL)
public class CollectionContextMandateService extends CollectionContextService {
    private static final Logger LOG = LoggerFactory.getLogger(CollectionContextMandateService.class);

    CollectionContextMandateService(TemplateService templateService, PackageService packageService, ProposalService proposalService,
                                    CollectionUrlBuilder urlBuilder, Provider<MemorandumContextService> memorandumContextProvider,
                                    Provider<BillContextService> billContextProvider, Provider<AnnexContextService> annexContextProvider, SecurityContext securityContext, Provider<ExplanatoryContextService> explanatoryContextProvider, Provider<FinancialStatementContextService> financialStatementContextProvider, ExplanatoryService explanatoryService, MessageHelper messageHelper) {
        super(templateService, packageService, proposalService, urlBuilder, memorandumContextProvider, billContextProvider,
                securityContext,
                explanatoryContextProvider, financialStatementContextProvider, annexContextProvider, explanatoryService, messageHelper);
    }

    @Override
    public Proposal executeCreateExplanatoryDocument() {
        LOG.trace("Executing 'Create Proposal' use case...");
        this.packageService.useLanguage(this.language);
        this.packageService.useTranslated(this.translated);
        LeosPackage leosPackage = packageService.createPackage();
        Proposal proposalTemplate = cast(categoryTemplateMap.get(PROPOSAL));
        proposal = proposal == null ? proposalTemplate : proposal;
        Validate.notNull(proposalTemplate, "Proposal template is required!");
        Option<ProposalMetadata> metadataOption = proposalTemplate.getMetadata();
        Validate.isTrue(metadataOption.isDefined(), "Proposal metadata is required!");
        Validate.notNull(purpose, "Proposal purpose is required!");
        ProposalMetadata metadata = metadataOption.get()
                .builder()
                .withPurpose(purpose)
                .withEeaRelevance(eeaRelevance)
                .build();

        String explanatoryTemplate = categoryTemplateMap.get(COUNCIL_EXPLANATORY).getName();
        ExplanatoryContextService explanatoryContext = explanatoryContextProvider.get();
        explanatoryContext.usePackage(leosPackage);
        explanatoryContext.useCollaborators(proposalTemplate.getCollaborators());
        Explanatory explanatory = getExplanatory(metadata, explanatoryContext, explanatoryTemplate, true);

        Proposal proposal = proposalService.createProposal(proposalTemplate.getId(), leosPackage.getPath(), metadata, null);
        proposalService.addComponentRef(proposal, explanatory.getName(), COUNCIL_EXPLANATORY);
        return proposalService.createVersion(proposal.getId(), VersionType.INTERMEDIATE, actionMsgMap.get(ContextActionService.DOCUMENT_CREATED));
    }

    private Explanatory getExplanatory(ProposalMetadata metadata, ExplanatoryContextService explanatoryContext, String template,
                                       boolean createProposal) {
        explanatoryContext.useTemplate(template);
        explanatoryContext.usePurpose(purpose);
        explanatoryContext.useType(metadata.getType());
        explanatoryContext.useTitle(messageHelper.getMessage("document.default.explanatory.title.default." + template));
        explanatoryContext.useActionMessageMap(actionMsgMap);
        explanatoryContext.useCollaborators(proposal.getCollaborators());
        explanatoryContext.usePackageRef(proposal.getMetadata().get().getRef());
        Explanatory explanatory = explanatoryContext.executeCreateExplanatory();
        return explanatory;
    }

    @Override
    protected void executeUpdateExplanatory(LeosPackage leosPackage, String purpose, Map<ContextActionService, String> actionMsgMap) {
        List<Explanatory> explanatories = explanatoryService.findCouncilExplanatoryByPackagePath(leosPackage.getPath());
        explanatories.forEach(explanatory -> {
            ExplanatoryContextService explanatoryContext = explanatoryContextProvider.get();
            explanatoryContext.useExplanatory(explanatory);
            explanatoryContext.usePurpose(purpose);
            explanatoryContext.useActionMessageMap(actionMsgMap);
            explanatoryContext.executeUpdateExplanatory();
        });
    }

}