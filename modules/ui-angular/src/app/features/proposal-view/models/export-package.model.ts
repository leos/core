export interface ExportPackageVO {
  id: string;
  versionId: string;
  versionLabel: string;
  comments: string[];
  date: string;
  status: string;
}
