/*
 * Copyright 2024 European Union
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 *     https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package eu.europa.ec.leos.repository.mapping;

public enum RepositoryProperties {
    DOCUMENT_CATEGORY,
    DOCUMENT_TITLE,
    DOCUMENT_TEMPLATE,
    DOCUMENT_LANGUAGE,
    METADATA_REF,
    MILESTONE_COMMENTS,
    INITIAL_CREATED_BY,
    INITIAL_CREATION_DATE,
    METADATA_PROCEDURE_TYPE,
    METADATA_ACT_TYPE,
    JOB_ID,
    JOB_DATE,
    STATUS,
    METADATA_STAGE,
    METADATA_TYPE,
    METADATA_PURPOSE,
    METADATA_DOCTEMPLATE,
    METADATA_EEA_RELEVANCE,
    ANNEX_INDEX,
    ANNEX_NUMBER,
    ANNEX_TITLE,
    ANNEX_CLONED_REF,
    COLLABORATORS,
    VERSION_LABEL,
    VERSION_TYPE,
    CONTAINED_DOCUMENTS,
    CLONED_PROPOSAL,
    ORIGIN_REF,
    CLONED_FROM,
    REVISION_STATUS,
    CONTRIBUTION_STATUS,
    COMMENTS,
    CLONED_MILESTONE_ID,
    BASE_REVISION_ID,
    LIVE_DIFFING_REQUIRED,
    TRACK_CHANGES_ENABLED,
    IS_VERSION_ARCHIVED,
    CALLBACK_ADDRESS,
    IMPORTED;
}
