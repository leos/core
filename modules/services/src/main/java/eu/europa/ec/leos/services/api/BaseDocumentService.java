/*
 * Copyright 2024 European Union
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 *     https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */

package eu.europa.ec.leos.services.api;

import com.sun.istack.NotNull;
import eu.europa.ec.leos.domain.repository.Content;
import eu.europa.ec.leos.domain.repository.common.VersionType;
import eu.europa.ec.leos.domain.repository.document.XmlDocument;
import eu.europa.ec.leos.domain.common.TocMode;
import eu.europa.ec.leos.domain.vo.SearchMatchVO;
import eu.europa.ec.leos.model.action.TrackChangeActionType;
import eu.europa.ec.leos.model.action.VersionVO;
import eu.europa.ec.leos.services.dto.request.Position;
import eu.europa.ec.leos.services.dto.response.DocumentViewResponse;
import eu.europa.ec.leos.services.dto.response.SaveElementResponse;
import eu.europa.ec.leos.services.request.ReplaceAllMatchRequest;
import eu.europa.ec.leos.services.request.ReplaceMatchRequest;
import eu.europa.ec.leos.services.request.SaveAfterReplaceRequest;
import eu.europa.ec.leos.services.response.DocumentConfigResponse;
import eu.europa.ec.leos.services.response.EditElementResponse;
import eu.europa.ec.leos.services.support.XmlHelper;
import eu.europa.ec.leos.vo.structure.Attribute;
import eu.europa.ec.leos.services.utils.StructureConfigUtils;
import eu.europa.ec.leos.vo.toc.TableOfContentItemVO;
import eu.europa.ec.leos.vo.structure.TocItem;
import eu.europa.ec.leos.vo.structure.TocItemType;

import org.apache.commons.lang3.StringUtils;

import static eu.europa.ec.leos.services.support.XmlHelper.BLOCK;
import static eu.europa.ec.leos.services.support.XmlHelper.INDENT;
import static eu.europa.ec.leos.services.support.XmlHelper.LEVEL;
import static eu.europa.ec.leos.services.support.XmlHelper.PARAGRAPH;
import static eu.europa.ec.leos.services.support.XmlHelper.POINT;
import static eu.europa.ec.leos.services.support.XmlHelper.SUBPARAGRAPH;
import static eu.europa.ec.leos.services.support.XmlHelper.SUBPOINT;

import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface BaseDocumentService<T extends XmlDocument> {
    String getElement(String documentRef, String elementName, String elementId);

    DocumentViewResponse deleteBlock(String documentRef, String elementName, String elementId) throws Exception;

    SaveElementResponse saveElement(String documentRef, String elementId, String elementName, String elementFragment, boolean isSplit, String alternateElementId)
            throws Exception;

    DocumentViewResponse insertGroup(String documentRef, String elementName, String elementId, Position position);

    DocumentViewResponse insertElement(String documentRef, String elementName, String elementId, Position position);

    DocumentViewResponse mergeElement(String documentRef, String elementContent, String elementTag, String elementId)
            throws Exception;

    List<TableOfContentItemVO> getToc(String documentRef, TocMode mode, String clientContextToken);

    List<TocItem> getTocItems(@NotNull String documentRef);

    DocumentViewResponse getDocument(@NotNull String documentRef);

    List<VersionVO> saveDocument(String documentRef, String checkInComment, VersionType versionType);

    List<TableOfContentItemVO> saveToC(String documentRef, List<TableOfContentItemVO> toc, TocMode tocMode, String clientContextToken);

    List<SearchMatchVO> searchTextInDocument(String documentRef, String searchText, boolean matchCase,
                                             boolean completeWords, String tempUpdatedContentXML) throws Exception;

    DocumentViewResponse showVersion(String versionId);

    String compare(String newVersionId, String oldVersionId);

    DocumentViewResponse restoreToVersion(String documentRef, String versionId);

    EditElementResponse editElement(String documentRef, String elementId, String elementTagName);

    byte[] downloadVersion(String documentRef, boolean isWithAnnotations) throws Exception;

    byte[] downloadCleanVersion(String documentRef);

    DocumentViewResponse showCleanVersion(String documentRef);

    byte[] downloadXmlVersionFiles(String documentRef, String versionId);

    byte[] replaceAllTextInDocument(ReplaceAllMatchRequest event) throws Exception;

    byte[] replaceOneTextInDocument(ReplaceMatchRequest event) throws Exception;

    DocumentViewResponse saveAfterReplace(SaveAfterReplaceRequest event);

    DocumentConfigResponse getDocumentConfig(String documentRef, String clientContextToken);

    String fetchUserGuidance(String documentRef);

    DocumentViewResponse acceptChange(String documentRef, String elementId, String elementTagName, TrackChangeActionType changeType, String presenterId) throws Exception;

    DocumentViewResponse rejectChange(String documentRef, String elementId, String elementTagName, TrackChangeActionType changeType, String presenterId) throws Exception;

    default Map<String, Attribute> getArticleTypesAttributes(List<TocItem> tocItems) {
        Map<String, Attribute> articleTypesAttributes = new HashMap<>();
        List<TocItemType> tocItemTypes = StructureConfigUtils.getTocItemTypesByTagName(tocItems, XmlHelper.ARTICLE);
        tocItemTypes.forEach(tocItemType -> {
            Attribute attribute = tocItemType.getAttribute();
            if (attribute == null) {
                attribute = new Attribute();
                attribute.setAttributeName("");
                attribute.setAttributeValue("");
            }
            articleTypesAttributes.put(tocItemType.getName().name(), attribute);
        });
        return articleTypesAttributes;
    }

    default byte[] getContentForReplaceProcess(String updatedContentXML, XmlDocument document) {
        if (updatedContentXML == null || updatedContentXML.isEmpty()) {
            return getContent(document);
        }
        return updatedContentXML.getBytes(StandardCharsets.UTF_8);
    }

    default byte[] getContent(XmlDocument document) {
        final Content content = document.getContent().getOrError(() -> "Document content is required!");
        return content.getSource().getBytes();
    }

    boolean toggleTrackChangeEnabled(boolean isTrackChangeEnabled, String documentRef);
    default  boolean checkIfCloseElementEditor(String elementTagName, String elementContent) {
        switch (elementTagName) {
            case SUBPARAGRAPH:
            case SUBPOINT:
                return elementContent.contains("<" + elementTagName + ">") || StringUtils.countMatches(elementContent, "<" + elementTagName) > 1;
            case PARAGRAPH:
                return elementContent.contains("<paragraph") || elementContent.contains("<subparagraph");
            case POINT:
            case INDENT:
                return elementContent.contains("<subparagraph>");
            case LEVEL:
                return elementContent.contains("<level") || elementContent.contains("<subparagraph");
            case BLOCK:
                return elementContent.contains("<block");
            default:
                return false;
        }
    }
}
