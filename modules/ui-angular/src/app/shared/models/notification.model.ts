export interface Notification {
  start: string;
  end: string;
  newsTimestamp: string;
  title: string;
  body: string;
}
