export interface Milestone {
  clone?: boolean;
  clonedMilestones?: Milestone[];
  contributionChanged: boolean;
  createdBy: string;
  createdDate: string;
  documentTitle: string;
  legDocumentName: string;
  versionedReference: string;
  legFileId: string;
  proposalRef: string;
  status: MilestoneStatus | string;
  title: string;
  updatedDate: number;
  opened?: boolean;
}

export interface MilestoneViewResponse {
  documents: MilestoneViewItem[];
  pdfRenditionsPresent: boolean;
  contributionChanged: boolean;
}

export interface MilestoneViewItem {
  /* proposalRef */
  contentFileName: string;
  coverPage: boolean;
  contentStatus: string; //Added, Modified, Deleted, Processed
  leosCategory: string;
  order: number | null;
  version: string;
  xmlContent: string;
  tocData: string;
}

export enum MilestoneStatus {
  Ready = 'FILE_READY',
  ContributionSent = 'CONTRIBUTION_SENT',
  InPreparation = 'IN_PREPARATION',
  Error = 'FILE_ERROR',
  ReadyToMerge = 'Ready to merge',
  RevisionSent = 'Sent for contribution',
}
