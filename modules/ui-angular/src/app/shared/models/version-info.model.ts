export interface VersionInfoVO {
  documentVersion: string;
  lastModifiedBy: string;
  entity: string;
  lastModificationInstant: string;
  versionType: string;
  revisedBaseVersion: string;
  baseVersionTitle: string;
}
