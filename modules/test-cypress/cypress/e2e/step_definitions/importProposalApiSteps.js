import {Given, Then } from "cypress-cucumber-preprocessor/steps";
import FormData from 'form-data';

Given('Send a POST request to import act', () => {
    let url, bearerToken;
    if(Cypress.env('CE_ENV').includes('@local')) {
        url = Cypress.env('localImportProposalApiUrl');
        bearerToken = Cypress.env('bearerTokenLocalUser1');
    }
    if(Cypress.env('CE_ENV').includes("@nonlocal")) {
        url = Cypress.env('devImportProposalApiUrl');
        bearerToken = Cypress.env('bearerTokenRemoteUser1');
    }
    cy.fixture('/legFiles/PROP_ACT-clxkd3t4k000uok58ltymp0m5-es.leg', 'binary')
        .then(Cypress.Blob.binaryStringToBlob)
        .then((fileContent) => {
            const formData = new FormData();
            formData.append('legFile', fileContent, '/legFiles/PROP_ACT-clxkd3t4k000uok58ltymp0m5-es.leg');
            cy.request({
                method: 'POST',
                url: url,
                headers: {
                    'Authorization': 'Bearer ' + bearerToken,
                    'Content-Type': 'multipart/form-data'
                },
                body: formData,
                failOnStatusCode: false
            }).as('apiResponse');
        });
});

Then('the response status code should be 200 or match failure conditions', () => {
    cy.get('@apiResponse').then((response) => {
        const decode = new TextDecoder('utf-8');
        const responseBody = decode.decode(response.body);

        if (response.status === 200) {
            expect(response.status).to.eq(200);
            expect(responseBody).to.include('proposalId');
        } else if(response.status === 202) {
            expect(response.status).to.eq(202);
            expect(responseBody).to.include('Document found and updated with major version');
        } else if(response.status === 404) {
            // Handle failure conditions
            expect(response.status).to.eq(404);
            expect(responseBody).to.include('Original act not found for the uploaded translated version');
        } else {
            // Handle failure conditions
            expect(response.status).to.eq(500);
            expect(responseBody).to.include('The uploaded document version already exists. Please upload another version');
        }
    });
});

Given('user {string} calls import document api with xml file {string} and language {string} and navigate to edit drafting using leos light url', function (user, xmlFile, lang) {
    let bearerToken, url, contextToken, documentUrl;
    bearerToken = Cypress.env('bearerTokenLocalUser1');
    url = Cypress.env('localContextTokenApiUrl');
    cy.request({
        method: 'GET',
        url: url,
        qs: {
            clientId: 'dgtClientId',
            user: Cypress.env('local' + user),
            role: 'OWNER',
            systemName: 'DGT_EDIT'
        }
    }).then((response) => {
        expect(response.status).to.eq(200);
        contextToken = response.body;
    });
    url = Cypress.env('localImportDocumentApiUrl');
    cy.fixture("/xmlFiles/"+xmlFile, 'binary')
        .then(Cypress.Blob.binaryStringToBlob)
        .then((fileContent) => {
            const formData = new FormData();
            formData.append('inputFile', fileContent, "/xmlFiles/"+xmlFile);
            formData.append('language', lang);
            formData.append('callbackAddress', '');
            cy.request({
                method: 'POST',
                url: url,
                headers: {
                    'Authorization': 'Bearer ' + bearerToken,
                    'Content-Type': 'multipart/form-data'
                },
                body: formData,
                failOnStatusCode: false
            }).as('importDocumentApiResponse');
        });
    cy.get('@importDocumentApiResponse').then((response) => {
        const decode = new TextDecoder('utf-8');
        const responseBody = decode.decode(response.body);
        expect(response.status).to.eq(200);
        //expect(responseBody).to.include('New document created');
        expect(responseBody).to.include('documentUrl');
        const jsonResponse = JSON.parse(responseBody);
        documentUrl = jsonResponse.documentUrl;
        documentUrl = documentUrl.replace("http://","");
        cy.log(contextToken);
        cy.log(documentUrl);
        cy.visit("http" + "://" + Cypress.env('local' + user) + ":" + Cypress.env('localPassword') + "@" + documentUrl + "?clientContext=" + contextToken);
        cy.wait(5000);
    });
});