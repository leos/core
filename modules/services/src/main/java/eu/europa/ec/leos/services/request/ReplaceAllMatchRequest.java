/*
 * Copyright 2024 European Union
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 *     https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package eu.europa.ec.leos.services.request;

public class ReplaceAllMatchRequest {
    private String documentRef;
    private String searchText;
    private String replaceText;
    private boolean caseSensitive;
    private boolean completeWords;
    private String tempUpdatedContentXML;

    public String getDocumentRef() {
        return documentRef;
    }

    public void setDocumentRef(String documentRef) {
        this.documentRef = documentRef;
    }

    public String getSearchText() {
        return searchText;
    }

    public void setSearchText(String searchText) {
        this.searchText = searchText;
    }

    public String getReplaceText() {
        return replaceText;
    }

    public void setReplaceText(String replaceText) {
        this.replaceText = replaceText;
    }

    public boolean isCaseSensitive() {
        return caseSensitive;
    }

    public void setCaseSensitive(boolean caseSensitive) {
        this.caseSensitive = caseSensitive;
    }

    public boolean isCompleteWords() {
        return completeWords;
    }

    public void setCompleteWords(boolean completeWords) {
        this.completeWords = completeWords;
    }

    public String getTempUpdatedContentXML() {
        return tempUpdatedContentXML;
    }

    public void setTempUpdatedContentXML(String tempUpdatedContentXML) {
        this.tempUpdatedContentXML = tempUpdatedContentXML;
    }
}
