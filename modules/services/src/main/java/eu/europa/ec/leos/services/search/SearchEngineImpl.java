package eu.europa.ec.leos.services.search;

import eu.europa.ec.leos.domain.vo.ElementMatchVO;
import eu.europa.ec.leos.domain.vo.SearchMatchVO;
import eu.europa.ec.leos.model.user.User;
import eu.europa.ec.leos.services.processor.content.XmlContentProcessorImpl;
import eu.europa.ec.leos.services.support.IdGenerator;
import eu.europa.ec.leos.services.support.XmlHelper;
import eu.europa.ec.leos.services.support.XercesUtils;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.parser.Tag;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.Consumer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static eu.europa.ec.leos.services.support.LeosXercesUtils.DATE_FORMAT;
import static eu.europa.ec.leos.services.support.XmlHelper.AKOMANTOSO;
import static eu.europa.ec.leos.services.support.XmlHelper.parseXml;
import static eu.europa.ec.leos.services.support.XmlHelper.XMLID;
import static eu.europa.ec.leos.services.support.XercesUtils.createXercesDocument;
import static eu.europa.ec.leos.services.support.XercesUtils.nodeToByteArray;

public class SearchEngineImpl implements SearchEngine {

    private static final String INSERT_TAG = "ins";
    private static final String DELETE_TAG = "del";
    private static final String AUTHORIAL_NOTE = "authorialNote";
    private static final String META = "meta";
    private static final String DELETED_START_ID_VALUE = "deleted" + IdGenerator.PREFIX_DELIMITER;
    private static List<String> tagsToExclude = Arrays.asList(META, AUTHORIAL_NOTE, DELETE_TAG);
    private static List<String> tagsToExcludeHighlight = Arrays.asList(META, AUTHORIAL_NOTE);
    private static List<String> tagsTrackChanges = Arrays.asList(DELETE_TAG);
    private static List<String> customInlineTags = Arrays.asList(AUTHORIAL_NOTE, "signature", "placeholder", "omissis", "date", "mref");

    private String searchableString;
    private List<Index> indexesForString;
    private Map<String, Element> elementsById;

    private List<Index> indexesForStringHighlight;
    private Map<String, Element> elementsByIdHighlight;

    private boolean isHighlight;
    public SearchEngineImpl(byte[] content) {
        Document document = createXercesDocument(content);
        indexContent(document);
    }
    public SearchEngineImpl(byte[] content, Boolean isHighlight) {
        this.isHighlight = isHighlight;
        Document document = createXercesDocument(content);
        indexContent(document);
    }
    public static SearchEngineImpl forContent(byte[] xmlContent) {
        return new SearchEngineImpl(xmlContent);
    }
    public static SearchEngineImpl forContent(byte[] xmlContent, Boolean isHighlight) {
        return new SearchEngineImpl(xmlContent, isHighlight);
    }
    static boolean lastCharIsWhitespace(StringBuilder sb) {
        return sb.length() != 0 && sb.charAt(sb.length() - 1) == ' ';
    }

    /**
     * Indexes the content. It does a depth first transversal of the xml content hierarchy.
     * During a visit to a node, it gathers the content text if the node is of known html type.
     */
    private void indexContent(Document document) {
        List<Element> elements = new ArrayList<>();

        Node root = XercesUtils.getFirstElementByName(document, AKOMANTOSO);
        XercesUtils.sanitize(root);
        visitNode(root, elements);

        createSearchableString(elements);
    }

    private int visitNode(Node node, List<Element> elements) {
        String tagName = node.getNodeName();
        String nodeId = XercesUtils.getId(node);
        boolean isNodeIdStartsWithDeleted = (nodeId != null && nodeId.startsWith(DELETED_START_ID_VALUE));
        if((isHighlight ? tagsToExcludeHighlight : tagsToExclude).contains(tagName)
                ||  isNodeIdStartsWithDeleted){
            return 0;
        }

        boolean hasText = XercesUtils.hasChildTextNode(node);
        if (hasText) {
            return visitNodeWithText(node, elements);
        } else {
            return visitElementNode(node, elements);
        }
    }

    private int visitNodeWithText(Node node, List<Element> elements) {
        int textStartIndex = 0;

        NodeList nodeList = node.getChildNodes();
        for (int i = 0; i < nodeList.getLength(); i++) {
            int contentLength;
            Node childNode = nodeList.item(i);
            if (childNode.getNodeType() == Node.TEXT_NODE) {
                String content = childNode.getTextContent();
                addElementNode(node, elements, content, textStartIndex);
                contentLength = content.length();
            } else {
                contentLength = visitNode(childNode, elements);
            }
            textStartIndex += contentLength;
        }
        return textStartIndex;
    }

    private int visitElementNode(Node node, List<Element> elements) {
        addElementNode(node, elements, "", 0);
        int textStartIndex = 0;

        List<Node> nodeList = XercesUtils.getChildren(node);
        for (int i = 0; i < nodeList.size(); i++) {
            textStartIndex += visitNode(nodeList.get(i), elements);
        }

        return textStartIndex;
    }

    private void addElementNode(Node node, List<Element> elements, String content, int textStartIndex) {
        String tag = node.getNodeName();
        String xmlIdAttribute = XercesUtils.getAttributeValue(node, XMLID);
        String elementId = xmlIdAttribute != null ? xmlIdAttribute : tag + "_generated";

        Element element = new Element(
                elementId,
                content,
                XmlContentProcessorImpl.isEditableElement(node),
                tag,
                textStartIndex);
        elements.add(element);
    }

    private void createSearchableString(List<Element> elements) {
        indexesForString = new ArrayList<>();
        indexesForStringHighlight = new ArrayList<>();
        StringBuilder sb = new StringBuilder();
        elementsById = new HashMap<>();
        elementsByIdHighlight = new HashMap<>();

        // Loop to extract the text content of all the elements.
        // They will be concatenated so that later a regular string search can be done.
        elements.forEach(el -> {
            if (((!Tag.isKnownTag(el.tag) && !customInlineTags.contains(el.tag)) // unknown tags are considered block tags and space is inserted after them
            || (Tag.valueOf(el.tag).isBlock() && el.startIndexOfText <= 0) // if block tag is containing any text
            || el.tag.equals("br"))) {
                if (!lastCharIsWhitespace(sb) && !el.content.startsWith(" ")) {
                    sb.append(' ');
                    // for the empty character
                    (isHighlight ? indexesForStringHighlight : indexesForString).add(new Index(null, -1));
                }
            }

            sb.append(el.content);

            // For each character inside the content, make Index indicating the index value and position
            // of the character
            // Ex: a text of 4 chars will add 4 to the list for 0,1,2,3 positions
            for (int i = 0; i < el.content.length(); i++) {
                (isHighlight ? indexesForStringHighlight : indexesForString).add(new Index(el.elementId, i + el.startIndexOfText));
            }

            // Preserve the element to make a reference to future if it repeats
            Map<String, Element> elemById = isHighlight ? elementsByIdHighlight : elementsById;
            if (elemById.containsKey(el.elementId)) {
                elemById.get(el.elementId).content += el.content;
            } else {
                elemById.put(el.elementId, el);
            }
        });
        searchableString = sb.toString();
    }

    /**
     * Searches the text in the content xml.
     * It first finds the matching text and the positional indicies. Then using the indicies,
     * it looks up in the index map build during indexing step
     *
     * @param searchText   search term
     * @param isMatchCase  if search is case sensitive
     * @param isWholeWords if search is for whole word(s)
     * @return List of matching objects
     */
    @Override
    public List<SearchMatchVO> searchText(String searchText, boolean isMatchCase, boolean isWholeWords) {
        List<SearchMatchVO> searchMatchedElements = new ArrayList<>();
        StringBuilder patternText = new StringBuilder();
        String quotedText = Pattern.quote(searchText);
        if (!isMatchCase) {
            patternText.append("(?i)");
        }
        if (isWholeWords) {
            patternText.append("\\b").append(quotedText).append("\\b");
        } else {
            patternText.append(quotedText);
        }

        Matcher matcher = Pattern.compile(patternText.toString()).matcher(searchableString);
        while (matcher.find()) {
            List<ElementMatchVO> matchedElements = new ArrayList<>();
            for (int i = matcher.start(); i < matcher.end(); i++) {
                Index idx = isHighlight ? indexesForStringHighlight.get(i) : indexesForString.get(i);
                Element element = null;
                boolean doContinue = false;
                if (StringUtils.isEmpty(idx.elementId)) {
                    // do not include manually added spaces in the result
                    doContinue = true;
                }else {
                    element = isHighlight ? elementsByIdHighlight.get(idx.elementId) : elementsById.get(idx.elementId);
                    if (element.content.trim().length() == 0// do not include blanks in the result
                            || (isHighlight && tagsTrackChanges.contains(element.tag.trim())) // isHighlight. Ignore the del and ins tags
                    ) {
                        doContinue = true;
                    }
                }
                if(doContinue){
                    continue;
                }
                ElementMatchVO elementMatchVO;
                if (!matchedElements.isEmpty()) {
                    ElementMatchVO lastElement = matchedElements.get(matchedElements.size() - 1);
                    if (lastElement.getElementId().equals(element.elementId)) {
                        elementMatchVO = lastElement;
                    } else {
                        elementMatchVO = new ElementMatchVO(element.elementId, idx.indexInTag, element.isEditable);

                        matchedElements.add(elementMatchVO);
                    }
                } else {
                    elementMatchVO = new ElementMatchVO(element.elementId, idx.indexInTag, element.isEditable);
                    matchedElements.add(elementMatchVO);
                }

                elementMatchVO.setMatchEndIndex(idx.indexInTag + 1);
            }

            // calculate the isReplaceable based on the attributes of the matched elements and if the matched elements are cross-tags
            if (!matchedElements.isEmpty()) {
                searchMatchedElements.add(new SearchMatchVO(matchedElements, calculateReplaceble(matchedElements)));
            }
        }
        return searchMatchedElements;
    }
    private boolean calculateReplaceble(List<ElementMatchVO> matchedElements) {
        boolean replaceable = true;
        for (ElementMatchVO elementMatchVO : matchedElements) {
            if (!elementMatchVO.isEditable()) {
                replaceable = false;
                break;
            }
        }
        return replaceable;
    }

    @Override
    public byte[] replace(byte[] docContent, List<SearchMatchVO> searchMatchVOs, String searchText, String replaceText, boolean removeEmptyTags, User user, boolean isTrackChangesEnabled) {
        Document document = createXercesDocument(docContent);
        XercesUtils.sanitize(XercesUtils.getFirstElementByName(document, AKOMANTOSO));
        int replacedContentDiffLength = replaceText.length() - searchText.length();
        for (int i = 0; i < searchMatchVOs.size(); i++) {
            SearchMatchVO smVO = searchMatchVOs.get(i);
            if (!smVO.isReplaceable()) {
            	continue;
            }
            replace(document, smVO, replaceText, true, user, isTrackChangesEnabled);
            // update positions of elements that have replaced texts
            List<String> elementIdsReplaced = smVO.getMatchedElements().stream().map(ElementMatchVO::getElementId).collect(Collectors.toList());
            for (int j = i + 1; j < searchMatchVOs.size(); j++) {
                SearchMatchVO matchesSVO = searchMatchVOs.get(j);
                Stream<ElementMatchVO> elementMatchVOStream = matchesSVO.getMatchedElements().stream()
                        .filter(matchedElement -> elementIdsReplaced.contains(matchedElement.getElementId())
                                && (matchedElement.getMatchStartIndex() > 0));
                Consumer<ElementMatchVO> elementMatchVOConsumer = matchedElement -> {
                    matchedElement.setMatchStartIndex(matchedElement.getMatchStartIndex() + replacedContentDiffLength);
                    matchedElement.setMatchEndIndex(matchedElement.getMatchEndIndex() + replacedContentDiffLength);
                };
                if(isTrackChangesEnabled){
                    elementMatchVOStream.findFirst().ifPresent(elementMatchVOConsumer);
                }else {
                    elementMatchVOStream.forEach(elementMatchVOConsumer);
                }
            }
        }
        return nodeToByteArray(document);
    }
    /**
     * replaces the given text
     * 1. if the entire text to replace is inside an inline tag, then the replaced text will be inside the inline tag
     * 2. if only part of the text to replace is inside an inline tag, then that part will be replaced with empty string.
     *        If that inline tag becomes empty then it should be removed ideally
     * 3.  authorialNote tags will be preserved
     *
     * @param document
     * @param smVO
     * @param replaceText
     * @param removeEmptyTags
     *
     */
    private void replace(Document document, SearchMatchVO smVO, String replaceText, boolean removeEmptyTags, User user, boolean isTrackChangesEnabled) {
            if (StringUtils.isEmpty(replaceText)) {
                replaceText = "";
            }
            int startIndex = 0;
            int replaceLength = replaceText.length();
            List<String> replaceTextSegments = new ArrayList<>();
            // split the search text into as many segments as elementmatchVOs.
            // At the same time, also make corresponding replace segments for a search segment (criteria is length)
            for (ElementMatchVO eVO : smVO.getMatchedElements()) {
                // length of the search text segment present in the element
                int lenMatchInsideElement = eVO.getMatchEndIndex() - eVO.getMatchStartIndex();
                // add the length of the matched search segment to the new start index to get end index
                int endIndexLocal = startIndex + lenMatchInsideElement;
                String replaceSubText;
                // calculate corresponding replace segment
                if (startIndex < replaceLength) {
                    if (endIndexLocal < replaceLength) {
                        replaceSubText = replaceText.substring(startIndex, endIndexLocal);
                    } else {
                        replaceSubText = replaceText.substring(startIndex);
                    }
                } else {
                    replaceSubText = "";
                }
                replaceTextSegments.add(replaceSubText);
                startIndex = endIndexLocal;
            }
            // when search string segments < replace string
            if (startIndex < replaceLength) {
                replaceTextSegments.set(replaceTextSegments.size() - 1,
                        replaceTextSegments.get(replaceTextSegments.size() - 1).concat(replaceText.substring(startIndex)));
            }

            Map<String, List<Integer>> matchedElementsChildContentLength = new HashMap<>();
            for (ElementMatchVO eVO : smVO.getMatchedElements()) {
                if (!matchedElementsChildContentLength.containsKey(eVO.getElementId())) {
                    matchedElementsChildContentLength.put(eVO.getElementId(),
                            getChildElementsContentLength(document, eVO.getElementId()));
                }
            }

            Set<String> emptyElementSet = new HashSet<>();
            for (int i = 0; i < smVO.getMatchedElements().size(); i++) {
                ElementMatchVO eVO = smVO.getMatchedElements().get(i);
                boolean emptyTag = replaceContent(document, eVO, replaceTextSegments.get(i), removeEmptyTags,
                        matchedElementsChildContentLength.get(eVO.getElementId()), user, isTrackChangesEnabled);
                if (emptyTag) {
                    emptyElementSet.add(eVO.getElementId());
                }
            }

            removeEmptyElementsAndParents(document, emptyElementSet);
        }

    private List<Integer> getChildElementsContentLength(Document document, String elementId) {
        List<Integer> childNodesContentLength = new ArrayList<>();
        Node node = XercesUtils.getElementById(document, elementId);
        if (node != null) {
            NodeList nodeList = node.getChildNodes();
            Node nodeTemp;
            for (int i = 0; i < nodeList.getLength(); i++) {
                nodeTemp = nodeList.item(i);
                childNodesContentLength.add(nodeTemp.getTextContent().length());
            }
        }
        return childNodesContentLength;
    }

    private boolean replaceContent(Document document, ElementMatchVO eVO, String replaceSegmentGlobal, boolean removeEmptyTags,
            List<Integer> matchedElementChildContentLength, User user, boolean isTrackChangesEnabled) {
        if(isTrackChangesEnabled){
            return this.replaceContentIsTrackChangesEnabled( document,  eVO,  replaceSegmentGlobal,  removeEmptyTags,
             matchedElementChildContentLength,  user);
        }else{
            return this.replaceContentNoTrackChanges(document,  eVO,  replaceSegmentGlobal,  removeEmptyTags,
                    matchedElementChildContentLength);
        }
    }

    private boolean replaceContentNoTrackChanges(Document document, ElementMatchVO eVO, String replaceSegmentGlobal, boolean removeEmptyTags,
                                   List<Integer> matchedElementChildContentLength) {
        boolean containsNonEmptyElement = false;
        boolean containsEmptyTextElement = false;

        Node node = XercesUtils.getElementById(document, eVO.getElementId());
        if (node != null) {
            int index = 0;
            int startEVO = eVO.getMatchStartIndex();
            int endEVO = eVO.getMatchEndIndex();
            String replaceSegment = replaceSegmentGlobal;
            Node lastUpdatedNode = null;
            NodeList nodeList = node.getChildNodes();
            for (int i = 0; i < nodeList.getLength(); i++) {
                node = nodeList.item(i);
                int length;
                if(tagsToExclude.contains(node.getNodeName())){
                    length = 0;
                } else {
                    length = matchedElementChildContentLength.get(i);
                }
                int indexAtEndOfContent = index + length;
                if ((node.getNodeType() == Node.TEXT_NODE) && (index < endEVO && indexAtEndOfContent > startEVO)) {
                    String content = node.getTextContent();
                    int minReplaceSegmentLength = Math.min(replaceSegment.length(), indexAtEndOfContent - startEVO);
                    // replace from startEVO till length
                    String updatedContent = content.substring(0, startEVO - index) + replaceSegment.substring(0, minReplaceSegmentLength);
                    // tail part: if the search segment part falls within the content, then simply append the remaing content
                    if (indexAtEndOfContent > endEVO && endEVO > startEVO) {
                        // when there exist search segment that has been replaced somewhere in the middle i.e, startEVO>0
                        // but there is still some content left towards the end i.e, length > endEVO
                        updatedContent += content.substring(endEVO - Math.min(startEVO, index));
                    }
                    // record an update to be executed later
                    // in case the content is empty and as a result the tag as well, then it can also be deleted
                    if (removeEmptyTags) {
                        if ((startEVO == 0) && StringUtils.isEmpty(updatedContent)) {
                            containsEmptyTextElement = true;
                        } else if (!StringUtils.isEmpty(updatedContent)) {
                            containsNonEmptyElement = true;
                        }
                    }
                    node.setTextContent(parseXml(updatedContent));

                    lastUpdatedNode = node;
                    replaceSegment = replaceSegment.substring(minReplaceSegmentLength);
                    startEVO = indexAtEndOfContent;
                } else {
                    containsNonEmptyElement = true;
                }

                index += length;
            }
            // In case if the replace segment is too large and still some part was remaining,
            // simple append at the end.
            if (lastUpdatedNode != null && replaceSegment.length() > 0) {
                lastUpdatedNode.setTextContent(lastUpdatedNode.getTextContent() + parseXml(replaceSegment));
            }
        }
        return containsEmptyTextElement && !containsNonEmptyElement;
    }

    private boolean replaceContentIsTrackChangesEnabled(Document document, ElementMatchVO eVO, String replaceSegmentGlobal, boolean removeEmptyTags,
                                   List<Integer> matchedElementChildContentLength, User user) {
        boolean containsNonEmptyElement = false;
        boolean containsEmptyTextElement = false;
        // a map containing the modified text node as key and, as value, the list of newly added nodes (del, ins, and
        // remaining text node after the replaced fragment)
        Map<Node, List<Node>> nodeMap = new HashMap<>();

        Node node = XercesUtils.getElementById(document, eVO.getElementId());
        if (node != null) {
            int index = 0;
            int startEVO = eVO.getMatchStartIndex();
            int endEVO = eVO.getMatchEndIndex();
            String replaceSegment = replaceSegmentGlobal;
            NodeList nodeList = node.getChildNodes();
            for (int i = 0; i < nodeList.getLength(); i++) {
                node = nodeList.item(i);
                int length;
                if(tagsToExclude.contains(node.getNodeName())){
                    length = 0;
                } else {
                    length = matchedElementChildContentLength.get(i);
                }
                int indexAtEndOfContent = index + length;
                String deletedContent = "";
                if ((node.getNodeType() == Node.TEXT_NODE) && (index < endEVO && indexAtEndOfContent > startEVO)) {
                    String content = node.getTextContent();
                    int minReplaceSegmentLength = Math.min(replaceSegment.length(), indexAtEndOfContent - startEVO);
                    // replace from startEVO till length
                    String substring = content.substring(0, Math.max(startEVO - index, 0));
                    String updatedContent = substring + replaceSegment.substring(0, minReplaceSegmentLength);
                    String textBeforeSearchedFragment = substring;
                    String updatedContentSecondPart = null;
                    String updatedContentForEmptyTextValidation = updatedContent;
                    // tail part: if the search segment part falls within the content, then simply append the remaing content
                    if (indexAtEndOfContent > endEVO && endEVO > startEVO) {
                        // when there exist search segment that has been replaced somewhere in the middle i.e, startEVO>0
                        // but there is still some content left towards the end i.e, length > endEVO
                        updatedContentSecondPart = content.substring(endEVO - Math.min(startEVO, index));
                        updatedContentForEmptyTextValidation += updatedContentSecondPart;
                    }
                    // record an update to be executed later
                    // in case the content is empty and as a result the tag as well, then it can also be deleted
                    if (removeEmptyTags) {
                        if ((startEVO == 0) && StringUtils.isEmpty(updatedContentForEmptyTextValidation)) {
                            containsEmptyTextElement = true;
                        } else if (!StringUtils.isEmpty(updatedContentForEmptyTextValidation)) {
                            containsNonEmptyElement = true;
                        }
                    }
                    if(node.getParentNode().getNodeName().equalsIgnoreCase(INSERT_TAG)){
                        node.setTextContent(parseXml(updatedContentForEmptyTextValidation));
                    }else{
                        try {
                            deletedContent = content.substring(startEVO - index, endEVO - index);
                        }catch(Exception e){
                            //do nothing
                        }
                        this.generateTrackChangesNodes(document, deletedContent, replaceSegmentGlobal, user, nodeMap, node, updatedContentSecondPart);
                        node.setTextContent(parseXml(textBeforeSearchedFragment));
                    }
                    replaceSegment = replaceSegment.substring(minReplaceSegmentLength);
                    startEVO = indexAtEndOfContent;
                } else {
                    containsNonEmptyElement = true;
                }

                index += length;
            }
            // insert in correct order the newly created nodes
            nodeMap.entrySet().forEach(entry -> {
                Node parentNode = entry.getKey().getParentNode();
                final Node nodeRefChild = entry.getKey().getNextSibling();
                entry.getValue().forEach(child -> {
                    if (nodeRefChild != null) {
                        parentNode.insertBefore(child, nodeRefChild);
                    } else {
                        parentNode.appendChild(child);
                    }
                });
            });
            nodeMap.clear();
        }
        return containsEmptyTextElement && !containsNonEmptyElement;
    }

    private void generateTrackChangesNodes(Document document, String deletedContent, String replaceSegmentGlobal,
                      User user, Map<Node, List<Node>> nodeMap,
                      Node node, String updatedContentSecondPart) {
        String userLogin = null;
        String userName = null;
        if (user != null){
            userLogin = user.getLogin();
            userName = user.getName();
        }
        String prefixId = IdGenerator.getPrefixId(XercesUtils.getId(node.getParentNode()));

// del element
        org.w3c.dom.Element deleteNode =XercesUtils.createElement(document, DELETE_TAG, IdGenerator.generateId(prefixId) ,  deletedContent);
// ins element
        org.w3c.dom.Element  insertElement = XercesUtils.createElement(document, INSERT_TAG, IdGenerator.generateId(prefixId), replaceSegmentGlobal);
        if(userLogin != null && userName!= null) {
            XercesUtils.addAttribute(deleteNode, "leos:uid", userLogin);
            XercesUtils.addAttribute(deleteNode, "leos:title",
                    new StringBuilder(userName).append(" : ").append(ZonedDateTime.now().format(DATE_FORMAT)).toString());
            XercesUtils.addAttribute(insertElement, "leos:uid", userLogin);
            XercesUtils.addAttribute(insertElement, "leos:title",
                    new StringBuilder(userName).append(" : ").append(ZonedDateTime.now().format(DATE_FORMAT)).toString());
        }

        List<Node> addedElementList = new ArrayList<>();
        addedElementList.add(deleteNode);
        addedElementList.add(insertElement);
// second part of text node
        Node textNodeSecondPart = null;
        if(updatedContentSecondPart != null){
            textNodeSecondPart = document.createTextNode(updatedContentSecondPart);
            addedElementList.add(textNodeSecondPart);
        }
        nodeMap.put(node, addedElementList);
    }

    private void removeEmptyElementsAndParents(Document document, Set<String> elementIds) {
        for (String elementId : elementIds) {
            Node node = XercesUtils.getElementById(document, elementId);
            if (node != null) {
                while ((node.getParentNode() != null) && XmlHelper.INLINE_ELEMENTS.contains(node.getParentNode().getNodeName()) &&
                        StringUtils.isEmpty(node.getParentNode().getTextContent())) {
                    node = node.getParentNode();
                }
                XercesUtils.deleteElement(node);
            }
        }
    }

    private static class Element {
        String elementId;
        String content;
        boolean isEditable;
        String tag;
        int startIndexOfText;

        Element(String elementId, String content, boolean isEditable, String tag, int startIndexOfText) {
            this.elementId = elementId;
            this.content = content;
            this.isEditable = isEditable;
            this.tag = tag;
            this.startIndexOfText = startIndexOfText;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Element that = (Element) o;
            return Objects.equals(elementId, that.elementId) && Objects.equals(tag, that.tag);
        }

        @Override
        public int hashCode() {
            return Objects.hash(elementId, tag);
        }

        @Override
        public String toString() {
            return "Element{" +
                    "elementId='" + elementId + '\'' +
                    ", content='" + content + '\'' +
                    ", isEditable=" + isEditable +
                    ", tag='" + tag + '\'' +
                    ", startIndexOfText='" + startIndexOfText + '\'' +
                    '}';
        }
    }

    private static class Index {
        String elementId;
        int indexInTag;

        public Index(String elementId, int indexInTag) {
            this.elementId = elementId;
            this.indexInTag = indexInTag;
        }

        @Override
        public String toString() {
            return "Index{" +
                    "elementId='" + elementId + '\'' +
                    ", indexInTag=" + indexInTag +
                    '}';
        }
    }
}
