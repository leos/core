/*
 * Copyright 2024 European Union
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 *     https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package eu.europa.ec.leos.services.dto.response;

import eu.europa.ec.leos.model.user.User;
import eu.europa.ec.leos.security.LeosPermission;
import eu.europa.ec.leos.vo.light.Profile;

import java.util.Map;
import java.util.Set;

public class AppConfigResponse {

    private String mappingUrl;
    private boolean implicitSaveAndClose;
    private String spellCheckerName;
    private String spellCheckerServiceUrl;
    private String spellCheckerSourceUrl;
    private boolean searchAndReplaceEnabled;
    private boolean sendForRevisionEnabled;
    private boolean coverPageSeparated;
    private String supportDocumentCatalogKey;
    private boolean supportDocumentEnabled;
    private Map<String, Set<LeosPermission>> permissionsMap;
    private Profile profile;
    private User user;
    private String contextRole;
    private String headerTitle;
    private String headerPath;
    private String annotateAuthority;
    private String annotateClientUrl;
    private String annotateHostUrl;
    private String annotateJwtIssuerClientId;
    private String annotatePopupDefaultStatus;
    private boolean collectionCloseButtonEnabled;

    private boolean showRevisionEnabled;
    private boolean leosSwitchLevelArticle;
    private int searchOnMinimumCharacter;

    public AppConfigResponse() {
    }

    //**** Getter & Setters ****//

    public String getMappingUrl() {
        return mappingUrl;
    }

    public void setMappingUrl(String mappingUrl) {
        this.mappingUrl = mappingUrl;
    }

    public boolean isImplicitSaveAndClose() {
        return implicitSaveAndClose;
    }

    public void setImplicitSaveAndClose(boolean implicitSaveAndClose) {
        this.implicitSaveAndClose = implicitSaveAndClose;
    }

    public String getSpellCheckerName() {
        return spellCheckerName;
    }

    public void setSpellCheckerName(String spellCheckerName) {
        this.spellCheckerName = spellCheckerName;
    }

    public String getSpellCheckerServiceUrl() {
        return spellCheckerServiceUrl;
    }

    public void setSpellCheckerServiceUrl(String spellCheckerServiceUrl) {
        this.spellCheckerServiceUrl = spellCheckerServiceUrl;
    }

    public String getSpellCheckerSourceUrl() {
        return spellCheckerSourceUrl;
    }

    public void setSpellCheckerSourceUrl(String spellCheckerSourceUrl) {
        this.spellCheckerSourceUrl = spellCheckerSourceUrl;
    }

    public boolean isSearchAndReplaceEnabled() {
        return searchAndReplaceEnabled;
    }

    public void setSearchAndReplaceEnabled(boolean searchAndReplaceEnabled) {
        this.searchAndReplaceEnabled = searchAndReplaceEnabled;
    }

    public boolean isSendForRevisionEnabled() {
        return sendForRevisionEnabled;
    }

    public void setSendForRevisionEnabled(boolean sendForRevisionEnabled) {
        this.sendForRevisionEnabled = sendForRevisionEnabled;
    }

    public boolean isCoverPageSeparated() {
        return coverPageSeparated;
    }

    public void setCoverPageSeparated(boolean coverPageSeparated) {
        this.coverPageSeparated = coverPageSeparated;
    }

    public String getSupportDocumentCatalogKey() {
        return supportDocumentCatalogKey;
    }

    public void setSupportDocumentCatalogKey(String supportDocumentCatalogKey) {
        this.supportDocumentCatalogKey = supportDocumentCatalogKey;
    }

    public boolean isSupportDocumentEnabled() {
        return supportDocumentEnabled;
    }

    public void setSupportDocumentEnabled(boolean supportDocumentEnabled) {
        this.supportDocumentEnabled = supportDocumentEnabled;
    }

    public Map<String, Set<LeosPermission>> getPermissionsMap() {
        return permissionsMap;
    }

    public void setPermissionsMap(Map<String, Set<LeosPermission>> permissionsMap) {
        this.permissionsMap = permissionsMap;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getHeaderTitle() {
        return headerTitle;
    }

    public void setHeaderTitle(String headerTitle) {
        this.headerTitle = headerTitle;
    }

    public String getHeaderPath() {
        return headerPath;
    }

    public void setHeaderPath(String headerPath) {
        this.headerPath = headerPath;
    }

    public String getAnnotateAuthority() {
        return annotateAuthority;
    }

    public void setAnnotateAuthority(String annotateAuthority) {
        this.annotateAuthority = annotateAuthority;
    }

    public String getAnnotateClientUrl() {
        return annotateClientUrl;
    }

    public void setAnnotateClientUrl(String annotateClientUrl) {
        this.annotateClientUrl = annotateClientUrl;
    }

    public String getAnnotateHostUrl() {
        return annotateHostUrl;
    }

    public void setAnnotateHostUrl(String annotateHostUrl) {
        this.annotateHostUrl = annotateHostUrl;
    }

    public String getAnnotateJwtIssuerClientId() {
        return annotateJwtIssuerClientId;
    }

    public void setAnnotateJwtIssuerClientId(String annotateJwtIssuerClientId) {
        this.annotateJwtIssuerClientId = annotateJwtIssuerClientId;
    }

    public String getAnnotatePopupDefaultStatus() {
        return annotatePopupDefaultStatus;
    }

    public void setAnnotatePopupDefaultStatus(String annotatePopupDefaultStatus) {
        this.annotatePopupDefaultStatus = annotatePopupDefaultStatus;
    }

    public boolean isCollectionCloseButtonEnabled() {
        return collectionCloseButtonEnabled;
    }

    public void setCollectionCloseButtonEnabled(boolean collectionCloseButtonEnabled) {
        this.collectionCloseButtonEnabled = collectionCloseButtonEnabled;
    }
    
    public boolean isShowRevisionEnabled() {
        return showRevisionEnabled;
    }

    public void setShowRevisionEnabled(boolean showRevisionEnabled) {
        this.showRevisionEnabled = showRevisionEnabled;
    }

    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    public String getContextRole() {
        return contextRole;
    }

    public void setContextRole(String contextRole) {
        this.contextRole = contextRole;
    }

    public boolean getLeosSwitchLevelArticle() {
        return leosSwitchLevelArticle;
    }
    public void setLeosSwitchLevelArticle(boolean leosSwitchLevelArticle)  { this.leosSwitchLevelArticle = leosSwitchLevelArticle; }

    public int getSearchOnMinimumCharacter() {
        return searchOnMinimumCharacter;
    }

    public void setSearchOnMinimumCharacter(int searchOnMinimumCharacter) {
        this.searchOnMinimumCharacter = searchOnMinimumCharacter;
    }
}
