import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { apiBaseUrl } from 'src/config';

import { DocumentConfig } from '@/shared';
import { CROSSHEADING } from '@/shared/constants';
import {
  NodeMoveAction,
  NodeValidation,
  NodeValidationResponse,
} from '@/shared/models/drop-response.model';
import { TableOfContentItemVO, TocItem } from '@/shared/models/toc.model';
import { DocumentService } from '@/shared/services/document.service';
import {
  getActualTargetItem, getNumberingTypeByLanguage,
  isCrossheading,
  isDroppedOnPointOrIndent,
  isSourceDivision,
} from '@/shared/utils/toc.utils';
import { isTocItemsEqual } from '@/shared/utils/tocRules.utils';
import {cloneDeep} from "lodash-es";

Injectable();

export abstract class ValidateTocService {
  documentConfig: DocumentConfig;
  dropValidationResult$: Observable<NodeValidation>;

  private dropValidationResultBS: BehaviorSubject<NodeValidation> =
    new BehaviorSubject(null);

  protected constructor(
    protected http: HttpClient,
    protected documentService: DocumentService,
  ) {
    this.dropValidationResult$ = this.dropValidationResultBS.asObservable();
    this.documentService.documentConfig$.subscribe(
      (documentConfig) => (this.documentConfig = documentConfig),
    );
  }

  public validateNodeDrop(
    treeData: TableOfContentItemVO[],
    parentNode: TableOfContentItemVO,
    nodeTarget: TableOfContentItemVO,
    nodeDragged: TableOfContentItemVO,
    draggedNodeId: string[],
    draggedNodeTagName: string,
    targetNodeId: string,
    targetNodeTagName: string,
    parentNodeId: string,
    parentNodeTagName: string,
    position: string,
    documentType: string,
    documentRef: string,
    isAdd: boolean = false,
  ) {
    this.requestNodeDropValidation(
      treeData,
      draggedNodeId,
      draggedNodeTagName,
      targetNodeId,
      targetNodeTagName,
      parentNodeId,
      parentNodeTagName,
      position,
      documentType,
      documentRef,
    ).subscribe((response) => {
      const moveAction: NodeMoveAction = {
        isAdd,
        position,
      };
      let validationResult: NodeValidation = {
        ...response.result,
        sourceItem: nodeDragged,
        targetItem: nodeTarget,
        action: moveAction,
      };
      if (response.result.success && !response.result.warning) {
        if (position === 'AS_CHILDREN') {
          validationResult = {
            success: true,
            warning: false,
            targetItem: nodeTarget,
            sourceItem: nodeDragged,
            messageKey: 'toc.edit.window.drop.success.message',
            warningMessageKeys: [],
            action: moveAction,
          };
          const resultOfValidation = this.validateAddingItemAsChildOrSibling(
            validationResult,
            nodeDragged,
            nodeTarget,
            treeData,
            parentNode,
            position,
          );
          this.dropValidationResultBS.next(validationResult);
          return;
        }
        this.dropValidationResultBS.next(validationResult);
      } else this.dropValidationResultBS.next(validationResult);
    });
  }

  public validateAddingItemAsChildOrSibling(
    validationResult: NodeValidation,
    sourceItem: TableOfContentItemVO,
    targetItem: TableOfContentItemVO,
    tocTree: TableOfContentItemVO[],
    parentItem: TableOfContentItemVO,
    position: string,
  ): boolean {
    const targetTocItem = targetItem.tocItem;
    const targetRules = [
      targetTocItem.aknTag.toUpperCase(),
      getNumberingTypeByLanguage(targetTocItem, this.documentConfig.langGroup).toUpperCase(),
    ].join('_');
    const targetTocItems: TocItem[] = this.documentConfig.tocRules[targetRules];

    if (
      isSourceDivision(sourceItem) ||
      isCrossheading(sourceItem) ||
      isDroppedOnPointOrIndent(sourceItem, targetItem) ||
      sourceItem.tocItem.aknTag === targetItem.tocItem.aknTag
    ) {
      const actualTargetItem = getActualTargetItem(
        sourceItem,
        targetItem,
        parentItem,
        position,
        true,
      );
      return this.validateAddingToActualTargetItem(
        validationResult,
        sourceItem,
        targetItem,
        tocTree,
        actualTargetItem,
        position,
      );
    }
    //TODO : Add toc rules current problem rules are of type -> Map<TocItem,List<TocItem>> this cant't be parsed as json , and because some values have the same toc item key (aknTag) we can't map them by this identifier
    else if (
      targetTocItems?.length > 0 &&
      targetTocItems.some((item) => isTocItemsEqual(item, sourceItem.tocItem, this.documentConfig.langGroup))
    ) {
      //If target item type is root, source item will be added as child, else validate dropping item at dragged location
      const actualTargetItem = getActualTargetItem(
        sourceItem,
        targetItem,
        parentItem,
        position,
        false,
      );
      return (
        // isRootElement(targetItem) ||
        this.validateAddingToActualTargetItem(
          validationResult,
          sourceItem,
          targetItem,
          tocTree,
          actualTargetItem,
          position,
        )
      );
    } else {
      // If child elements not allowed in target validate adding it to its parent
      return this.validateAddingItemAsSibling(
        validationResult,
        sourceItem,
        targetItem,
        tocTree,
        parentItem,
        position,
      );
    }
  }

  private validateAddingToActualTargetItem = (
    validationResult: NodeValidation,
    sourceItem: TableOfContentItemVO,
    targetItem: TableOfContentItemVO,
    tocTree: TableOfContentItemVO[],
    actualTargetItem: TableOfContentItemVO,
    position: string,
  ): boolean => {
    const validAddingToItem = this.validateAddingToItem(
      validationResult,
      sourceItem,
      targetItem,
      tocTree,
      actualTargetItem,
      position,
    );
    const maxDepthReached = this.validateMaxDepth(
      validationResult,
      sourceItem,
      targetItem,
    );
    return validAddingToItem && !maxDepthReached;
  };

  private validateAddingItemAsSibling(
    validationResult: NodeValidation,
    sourceItem: TableOfContentItemVO,
    targetItem: TableOfContentItemVO,
    tocTree: TableOfContentItemVO[],
    parentItem: TableOfContentItemVO,
    position: string,
  ) {
    const actualTargetItem = getActualTargetItem(
      sourceItem,
      targetItem,
      parentItem,
      position,
      true,
    );
    return this.validateAddingToActualTargetItem(
      validationResult,
      sourceItem,
      targetItem,
      tocTree,
      actualTargetItem,
      position,
    );
  }

  private validateMaxDepth(
    validationResult: NodeValidation,
    sourceItem: TableOfContentItemVO,
    targetItem: TableOfContentItemVO,
  ) {
    if (targetItem.tocItem.maxDepth != null) {
      const maxDepthRule = parseInt(targetItem.tocItem.maxDepth, 10);
      if (maxDepthRule > 0 && targetItem.itemDepth >= maxDepthRule) {
        validationResult.success = false;
        validationResult.messageKey =
          'toc.edit.window.drop.error.depth.message';
        validationResult.sourceItem = sourceItem;
        validationResult.targetItem = targetItem;
      }
    }
    return true;
  }

  abstract validateAddingToItem(
    validationResult: NodeValidation,
    sourceItem: TableOfContentItemVO,
    targetItem: TableOfContentItemVO,
    tocTree: TableOfContentItemVO[],
    actualTargetItem: TableOfContentItemVO,
    position: string,
  );

  private prepareTocForSave(node: TableOfContentItemVO[]) {
    for (const n of node) {
      n['childItemsView'] = [];
      if (n.childItems && n.childItems.length > 0) {
        this.prepareTocForSave(n.childItems);
      }
    }
  }

  private requestNodeDropValidation(
    treeData: TableOfContentItemVO[],
    draggedNodeId: string[],
    draggedNodeTagName: string,
    targetNodeId: string,
    targetNodeTagName: string,
    parentNodeId: string,
    parentNodeTagName: string,
    position: string,
    documentType: string,
    documentRef: string,
  ) {
    if (draggedNodeTagName === CROSSHEADING) {
      draggedNodeTagName = 'crossheading';
    }
    if (targetNodeTagName === CROSSHEADING) {
      targetNodeTagName = 'crossheading';
    }
    const toc = cloneDeep(treeData);
    this.prepareTocForSave(toc);
    return this.http.post<NodeValidationResponse>(
      `${apiBaseUrl}/secured/toc/validate-node-drop`,
      {
        tableOfContentItemVOs: toc,
        draggedNodeId,
        draggedNodeTagName,
        targetNodeId,
        targetNodeTagName,
        parentNodeId,
        parentNodeTagName,
        position: position.toUpperCase(),
        documentRef,
        documentType: documentType.toUpperCase(),
      },
    );
  }
}
