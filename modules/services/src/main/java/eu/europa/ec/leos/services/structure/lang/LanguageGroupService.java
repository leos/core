package eu.europa.ec.leos.services.structure.lang;
/*
 * Copyright 2024 European Union
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence")
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 *     https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */

import eu.europa.ec.leos.domain.repository.document.ConfigDocument;
import eu.europa.ec.leos.repository.store.ConfigurationRepository;
import eu.europa.ec.leos.services.support.XmlHelper;
import eu.europa.ec.leos.vo.lang.LanguageGroup;
import eu.europa.ec.leos.vo.lang.ObjectFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class LanguageGroupService {

    private static final Logger LOG = LoggerFactory.getLogger(LanguageGroupService.class);

    @Value("${leos.light.lang.group.schema.path}")
    private String langGroupSchema;

    @Value("${leos.templates.path}")
    private String langGroupPath;

    @Value("${leos.light.lang.group.name}")
    private String langGroupName;

    ConfigurationRepository configurationRepository;
    LanguageMapHolder languageMapHolder;

    @Autowired
    public LanguageGroupService(ConfigurationRepository configurationRepository, LanguageMapHolder languageMapHolder) {
        this.configurationRepository = configurationRepository;
        this.languageMapHolder = languageMapHolder;
    }

    public void getLanguageMap() {
        byte[] languageDocument = getLangGroupDocument();
        final LanguageGroup languageGroup = loadLanguageGroupFromFile(languageDocument);
        Map<String, List<String>> languageMap = new HashMap<>();
        languageGroup.getGroups().getGroups().forEach(group -> {
            languageMap.put(group.getName(), group.getLangs());
        });
        languageMapHolder.loadLanguageMap(languageMap);
    }

    public byte[] getLangGroupDocument() {
        ConfigDocument langDocument = configurationRepository.findConfiguration(langGroupPath, langGroupName);
        return langDocument.getContent().get().getSource().getBytes();
    }

    private LanguageGroup loadLanguageGroupFromFile(byte[] fileBytes) {
        try {
            return XmlHelper.loadFromFile(fileBytes, LanguageGroup.class, ObjectFactory.class, langGroupSchema);
        } catch (Exception e) {
            LOG.debug("Error in loadLanguageGroupFromFile", e);
            throw new IllegalStateException("Error loading language group configurations", e);
        }
    }
}
