import { DOCUMENT } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { EuiDialogService } from '@eui/components/eui-dialog';
import { TranslateService } from '@ngx-translate/core';
import { cloneDeep, isEqual } from 'lodash-es';
import { combineLatest, distinctUntilChanged, map, Subject, take } from 'rxjs';

import { AppConfigService } from '@/core/services/app-config.service';
import { ActionManagerConnector } from '@/features/akn-document/services/action-manager-connector';
import { ChangeDetailsConnector } from '@/features/akn-document/services/change-details-connector';
import { LeosEditorConnector } from '@/features/akn-document/services/leos-editor-connector';
import { MathJaxConnector } from '@/features/akn-document/services/math-jax-connector';
import { MergeContributionsService } from '@/features/akn-document/services/merge-contributions.service';
import { RefToLinkConnector } from '@/features/akn-document/services/ref-to-link-connector';
import { SoftActionsConnector } from '@/features/akn-document/services/soft-actions-connector';
import { TrackChangesConnector } from '@/features/akn-document/services/track-changes-connector';
import { UserGuidanceConnector } from '@/features/akn-document/services/user-guidance-connector';
import { Require } from '@/features/leos-legacy/models/requirejs';
import { LeosLegacyService } from '@/features/leos-legacy/services/leos-legacy.service';
import { DocumentConfig } from '@/shared/models';
import { LeosAppConfig } from '@/shared/models/leos.model';
import { MergeActionVO } from '@/shared/models/merge-action-vo.model';
import { CoEditionServiceWS } from '@/shared/services/coEdition.websocket.service';
import { DocumentService } from '@/shared/services/document.service';
import { EnvironmentService } from '@/shared/services/enviroment.service';
import { LoadingService } from '@/shared/services/loading.service';

import { TocItem } from '../models/toc.model';
import { BlockDocumentEditorService } from './block-document-editor.service';
import { CheckBoxesConnector } from './check-boxes-connector';
import { DatePickerConnector } from './date-picker-connector';
import { MergeContributionConnector } from './merge-contribution-connector';
import { TableOfContentService } from './table-of-content.service';

export type EditorOpenState = 'OPEN' | 'CLOSE';

@Injectable()
export class CKEditorService {
  private actionManagerConnector?: ActionManagerConnector;
  private leosEditorConnector?: LeosEditorConnector;
  private userGuidanceConnector?: UserGuidanceConnector;
  private changeDetailsConnector?: ChangeDetailsConnector;
  private refToLinkConnector?: RefToLinkConnector;
  private softActionsConnector?: SoftActionsConnector;
  private mathJaxConnector?: MathJaxConnector;
  private trackChangesConnector?: TrackChangesConnector;
  private mergeContributionConnector?: MergeContributionConnector;
  private datePickerConnector?: DatePickerConnector;
  private checkBoxesConnector?: CheckBoxesConnector;

  private openStateSubj = new Subject<EditorOpenState>();

  constructor(
    private leosLegacyService: LeosLegacyService,
    private http: HttpClient,
    private documentService: DocumentService,
    private appConfig: AppConfigService,
    private coEditionService: CoEditionServiceWS,
    private dialogService: EuiDialogService,
    private translateService: TranslateService,
    private tableOfContentService: TableOfContentService,
    private loadingService: LoadingService,
    private environmentService: EnvironmentService,
    @Inject(DOCUMENT) private domDocument: Document,
    private blockDocumentEditorService: BlockDocumentEditorService,
    private mergeContributionService: MergeContributionsService,
  ) {}

  destroyDocumentEditor() {
    this.destroyEditorInstance();
    this.leosEditorConnector?.destroy();
    this.actionManagerConnector?.destroy();
    this.userGuidanceConnector?.destroy();
    this.softActionsConnector?.destroy();
    this.changeDetailsConnector?.destroy();
    this.refToLinkConnector?.destroy();
    this.mathJaxConnector?.destroy();
    this.trackChangesConnector?.destroy();
    this.mergeContributionConnector?.destroy();
    this.datePickerConnector?.destroy();
    this.checkBoxesConnector?.destroy();
  }

  get openState$() {
    return this.openStateSubj.asObservable();
  }

  // called from document-actions-dropdown.component.html
  toggleUserGuidance() {
    this.documentService.userGuidanceVisible$.subscribe((seeUserGuidance: boolean) => {
      this.userGuidanceConnector.toggleUserGuidance(seeUserGuidance);
    });
    this.documentService.toggleUserGuidance();
  }

  // called from document-editor.component
  closeElementEditor() {
    this.leosEditorConnector?.closeElement();
  }

  initUserGuidanceStandalone() {
    const rootElement = this.domDocument.getElementById('docContainer');
    combineLatest([this.leosLegacyService.require$, this.getLeosState()])
      .pipe(take(1))
      .subscribe(([require, leosState]) => {
        require(['js/leosModulesBootstrap']);
        this.initUserGuidance(require, leosState, rootElement);
      });
  }

  init() {
    // TODO: this should not be hardcoded
    const rootElement = this.domDocument.getElementById('docContainer');
    combineLatest([this.leosLegacyService.require$, this.getLeosState()])
      .pipe(take(1))
      .subscribe(([require, leosState]) => {
        require(['js/leosModulesBootstrap']);
        this.initActionManager(require, leosState, rootElement);
        this.initLeosEditor(require, leosState, rootElement);
        this.initUserGuidance(require, leosState, rootElement);
        this.initChangeDetails(require, leosState, rootElement);
        this.initRefToLink(require, leosState, rootElement);
        this.initSoftActions(require, leosState, rootElement);
        this.initMathJax(require, leosState, rootElement);
        this.initTrackChanges(require, leosState, rootElement);
        this.initMergeContribution(require, leosState, rootElement);
        if (this.documentService.documentType === 'stat_digit_financ_legis') {
          this.initDatePicker(require, leosState, rootElement);
          this.initCheckBoxes(require, leosState, rootElement);
        }
      });

    this.documentService.documentView$.subscribe(() => {
      this.refreshStateAllAvailableConnectors();
    });
  }

  refreshStateAllAvailableConnectors() {
    this.leosEditorConnector?.$triggerStateChange();
    this.actionManagerConnector?.$triggerStateChange();
    this.userGuidanceConnector?.$triggerStateChange();
    this.softActionsConnector?.$triggerStateChange();
    this.changeDetailsConnector?.$triggerStateChange();
    this.refToLinkConnector?.$triggerStateChange();
    this.mathJaxConnector?.$triggerStateChange();
    this.trackChangesConnector?.$triggerStateChange();
    this.mergeContributionConnector?.$triggerStateChange();
    if (this.datePickerConnector) {
      const rootElement = this.domDocument.getElementById('docContainer');
      combineLatest([
        this.leosLegacyService.require$,
        this.getLeosState(),
      ]).subscribe(([require, leosState]) => {
        require(['js/leosModulesBootstrap']);
        this.initDatePicker(require, leosState, rootElement);
      });
    }
    this.datePickerConnector?.$triggerStateChange();
    this.checkBoxesConnector?.$triggerStateChange();
  }

  refreshStateSpecificConnectors() {
    this.softActionsConnector?.$triggerStateChange();
    this.refToLinkConnector?.$triggerStateChange();
    this.mathJaxConnector?.$triggerStateChange();
    this.trackChangesConnector?.$triggerStateChange();
  }

  refreshStateSoftActionsConnector() {
    this.softActionsConnector?.$triggerStateChange();
  }

  triggerMergeContributionConnectorStateChange() {
    this.mergeContributionConnector.$triggerStateChange();
  }

  changeSeeTrackChangesState() {
    this.trackChangesConnector.getState().isTrackChangesShowed =
      !this.trackChangesConnector.getState().isTrackChangesShowed;
    this.leosEditorConnector.getState().isTrackChangesShowed =
      !this.leosEditorConnector.getState().isTrackChangesShowed;
    this.softActionsConnector.getState().isTrackChangesShowed =
      !this.softActionsConnector.getState().isTrackChangesShowed;
    this.trackChangesConnector.$triggerStateChange();
    this.leosEditorConnector.$triggerStateChange();
    this.softActionsConnector.$triggerStateChange();
  }

  changeEnableTrackChangesState(isTrackChangesEnabled) {
    this.leosEditorConnector.getState().isTrackChangesEnabled =
      isTrackChangesEnabled;
    this.leosEditorConnector.$triggerStateChange();
    this.trackChangesConnector.$triggerStateChange();
  }

  getSeeTrackChangesState() {
    return this.trackChangesConnector.getState().isTrackChangesShowed;
  }

  handleMergeContributionsActions(withTrackChanges: boolean, all: boolean) {
    if (all) {
      this.mergeContributionConnector?.acceptAndMergeAllChanges(
        withTrackChanges,
      );
    } else {
      this.mergeContributionConnector?.handleMergeAction();
    }
  }

  addMergeActionList(action: MergeActionVO) {
    this.mergeContributionService.addMergeActionList(action);
    this.mergeContributionConnector?.doUpdateMergeActionList({
      action,
      select: true,
    });
  }

  /*
        This function destroys the instance of the active ckeditor instance on each document page.
       */
  private destroyEditorInstance() {
    this.leosEditorConnector?.onUnregister(
      this.leosEditorConnector,
      'onUnregister',
    );
  }

  private initActionManager(
    require: Require,
    leosState: any,
    rootElement: HTMLElement,
  ) {
    this.actionManagerConnector = new ActionManagerConnector(
      {
        instanceType: process.env.NG_APP_LEOS_INSTANCE,
        tocItemsJsonArray: leosState.tocItemsJsonArray,
        tocEdition: leosState.tocEdition,
        hasUpdatePermission: leosState.permissions && leosState.permissions.includes('CAN_UPDATE'),
      },
      { rootElement },
    );

    require(['extension/actionManagerExtension'], (actionManager) => {
      actionManager.init(this.actionManagerConnector);
      this.actionManagerConnector.jsDepsInited();
    });
  }

  private initLeosEditor(
    require: Require,
    leosState: any,
    rootElement: HTMLElement,
  ) {
    this.leosEditorConnector = new LeosEditorConnector(
      //TODO pass only required state
      leosState,
      {
        rootElement,
      },
      this.http,
      this.documentService,
      this.coEditionService,
      this.dialogService,
      this.translateService,
      this.tableOfContentService,
      this.loadingService,
      this.environmentService,
      (state: EditorOpenState) => {
        this.openStateSubj.next(state);
      },
      this.actionManagerConnector,
    );
    require(['js/editor/leosEditorExtension'], (leosEditor) => {
      leosEditor.init(this.leosEditorConnector);
      this.leosEditorConnector.jsDepsInited();
      window['EditorConnector'] = this.leosEditorConnector;
    });
  }

  private initChangeDetails(
    require: Require,
    leosState: any,
    rootElement: HTMLElement,
  ) {
    this.changeDetailsConnector = new ChangeDetailsConnector(
      //TODO pass only required state
      leosState,
      {
        rootElement,
      },
    );
    require(['extension/changeDetailsExtension'], (changeDetails) => {
      changeDetails.init(this.changeDetailsConnector);
      this.changeDetailsConnector.jsDepsInited();
    });
  }

  private initUserGuidance(
    require: Require,
    leosState: any,
    rootElement: HTMLElement,
  ) {
    this.userGuidanceConnector = new UserGuidanceConnector(
      //TODO pass only required state
      leosState,
      {
        rootElement,
      },
      this.documentService,
    );
    require(['extension/userGuidanceExtension'], (userGuideance) => {
      userGuideance.init(this.userGuidanceConnector);
      this.userGuidanceConnector.jsDepsInited();
      this.userGuidanceConnector.requestUserGuidance();
    });
  }

  private initRefToLink(
    require: Require,
    leosState: any,
    rootElement: HTMLElement,
  ) {
    const otherTargets = ['contributionViewContainer','versionComparisonContainer'];
    this.refToLinkConnector = new RefToLinkConnector(
      //TODO pass only required state
      leosState, otherTargets,
      {
        rootElement,
      },
    );
    require(['extension/refToLinkExtension'], (refToLink) => {
      refToLink.init(this.refToLinkConnector);
      this.refToLinkConnector.jsDepsInited();
    });
  }

  private initSoftActions(
    require: Require,
    leosState: any,
    rootElement: HTMLElement,
  ) {
    const otherTargets = ['contributionViewContainer'];
    this.softActionsConnector = new SoftActionsConnector(
      //TODO pass only required state
      leosState,
      otherTargets,
      {
        rootElement,
      },
    );
    require(['extension/softActionsExtension'], (softActions) => {
      softActions.init(this.softActionsConnector);
      this.softActionsConnector.jsDepsInited();
    });
  }

  private initMathJax(
    require: Require,
    leosState: any,
    rootElement: HTMLElement,
  ) {
    const otherTargets = ['contributionViewContainer','versionComparisonContainer','treeContainer'];
    this.mathJaxConnector = new MathJaxConnector(leosState, otherTargets,
      {
      rootElement,
    });

    require(['extension/mathJaxExtension'], (mathJax) => {
      mathJax.init(this.mathJaxConnector);
      this.mathJaxConnector.jsDepsInited();
    });
  }

  private initTrackChanges(
    require: Require,
    leosState: any,
    rootElement: HTMLElement,
  ) {
    this.trackChangesConnector = new TrackChangesConnector(leosState, {
      rootElement,
    });

    require(['extension/trackChangesExtension'], (trackChanges) => {
      trackChanges.init(this.trackChangesConnector);
      this.trackChangesConnector.jsDepsInited();
    });
  }

  private initMergeContribution(
    require: Require,
    leosState: any,
    rootElement: HTMLElement,
  ) {
    this.mergeContributionService.contributions$.subscribe((contributions) => {
      if (contributions.length > 0) {
        this.mergeContributionConnector = new MergeContributionConnector(
          leosState,
          this.documentService,
          {
            rootElement,
          },
          this.mergeContributionService,
          this.domDocument,
        );

        require(['extension/mergeContributionExtension'], (
          mergeContribution,
        ) => {
          mergeContribution.init(this.mergeContributionConnector);
          this.mergeContributionConnector.jsDepsInited();
        });
      }
    });
  }

  private initDatePicker(
    require: Require,
    leosState: any,
    rootElement: HTMLElement,
  ) {
    this.datePickerConnector = new DatePickerConnector(
      { ...{ hasUpdatePermission: leosState.permissions && leosState.permissions.includes('CAN_UPDATE'),}, ...leosState },
      {
        rootElement,
      },
      this.http,
      this.documentService,
      this.tableOfContentService,
      this.coEditionService,
      this.blockDocumentEditorService,
    );

    require(['extension/datePickerExtension'], (datePicker) => {
      datePicker.init(this.datePickerConnector);
      this.datePickerConnector.jsDepsInited();
    });
  }

  private initCheckBoxes(require: Require, leosState: any, rootElement: HTMLElement) {
    // see modules/ui/src/main/java/eu/europa/ec/leos/ui/view/financialstatement/FinancialStatementScreenImpl.java
    this.checkBoxesConnector = new CheckBoxesConnector(
      {
        checkBoxTagName: 'indent',
        checkedBoxValue: '&#x2611;',
        uncheckedBoxValue: '&#x2610;',
        checkBoxAttributeName: 'name',
        checkedBoxAttribute: 'checked',
        uncheckedBoxAttribute: 'unchecked',
        hasUpdatePermission: leosState.permissions && leosState.permissions.includes('CAN_UPDATE'),
      },
      {
        rootElement,
      },
      this.http,
      this.documentService,
      this.tableOfContentService,
      this.coEditionService,
      this.blockDocumentEditorService,
    );

    require(['extension/checkBoxesExtension'], (checkBoxes) => {
      checkBoxes.init(this.checkBoxesConnector);
      this.checkBoxesConnector.jsDepsInited();
    });
  }

  private getLeosState() {
    return combineLatest([
      this.appConfig.config,
      this.documentService.documentConfig$,
    ]).pipe(
      map(([config, extraConfig]) =>
        this.renameConfigKeysForEditor({ ...config, ...extraConfig }),
      ),
    );
  }

  // called from constructor
  private renameConfigKeysForEditor(config: any) {
    const oldConfig: LeosAppConfig & DocumentConfig = cloneDeep(config);
    const tocItems: any = cloneDeep(oldConfig.tocItems);

    tocItems.forEach((i: TocItem) => {
      i.aknTag = i.aknTag.toLowerCase() as any;
      if (this.documentService.documentType.toLowerCase() === 'coverpage') {
        if (i.aknTag.toLowerCase() === 'doc_purpose') {
          i.aknTag = 'docpurpose';
        }
      }
      if (this.documentService.documentType === 'memorandum') {
        if (i.aknTag === 'main_body') {
          i.aknTag = 'mainBody';
        } else if (i.aknTag === 'block_container') {
          i.aknTag = 'blockContainer';
        }
      }
    });

    //toc-items
    Object.defineProperty(
      config,
      'tocItemsJsonArray',
      Object.getOwnPropertyDescriptor(config, 'tocItems'),
    );

    config.tocItemsJsonArray = JSON.stringify(tocItems);
    delete config['tocItems'];

    //numberingConfigsJsonArray
    Object.defineProperty(
      config,
      'numberingConfigsJsonArray',
      Object.getOwnPropertyDescriptor(config, 'numberingConfig'),
    );
    config.numberingConfigsJsonArray = JSON.stringify(
      oldConfig.numberingConfig,
    );
    delete config['numberingConfig'];

    config.listNumberConfigJsonArray = JSON.stringify(
      oldConfig.listNumberConfigJsonArray,
    );
    config.alternateConfigsJsonArray = JSON.stringify(
      oldConfig.alternateConfigs,
    );

    //articleTypesConfig
    Object.defineProperty(
      config,
      'articleTypesConfigJsonArray',
      Object.getOwnPropertyDescriptor(config, 'articleTypesConfig'),
    );
    config.articleTypesConfigJsonArray = JSON.stringify(
      oldConfig.articleTypesConfig,
    );
    delete config['articleTypesConfig'];

    //documentsMetadata
    Object.defineProperty(
      config,
      'documentsMetadataJsonArray',
      Object.getOwnPropertyDescriptor(config, 'documentsMetadata'),
    );
    config.documentsMetadataJsonArray = JSON.stringify(
      oldConfig.documentsMetadata,
    );
    delete config['documentsMetadata'];

    //implicitSaveEnabled

    Object.defineProperty(
      config,
      'isImplicitSaveEnabled',
      Object.getOwnPropertyDescriptor(config, 'implicitSaveAndClose'),
    );
    config.isImplicitSaveEnabled = JSON.stringify(
      oldConfig.implicitSaveAndClose,
    );
    delete config['implicitSaveAndClose'];

    config['spellCheckerName'] = oldConfig.spellCheckerName;

    config['refConfigs'] = oldConfig.refConfigs;

    config['langGroup'] = oldConfig.langGroup;

    config['tocEdition'] = !oldConfig.profile || oldConfig.profile.tocEdition;

    const profileTCEnabled =
      !oldConfig.profile || oldConfig.profile.trackChangesEnabled;
    config['isTrackChangesShowed'] =
      profileTCEnabled && oldConfig.trackChangesShowed;
    config['isTrackChangesEnabled'] =
      profileTCEnabled && oldConfig.trackChangesEnabled;
    config['permissions'] = this.documentService.getUserPermissions();

    return config;
  }
}
