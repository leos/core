/*
 * Copyright 2024 European Union
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 *     https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package eu.europa.ec.leos.services.compare;

import com.google.common.base.Stopwatch;
import eu.europa.ec.leos.i18n.MessageHelper;
import eu.europa.ec.leos.model.action.SoftActionType;
import eu.europa.ec.leos.security.SecurityContext;
import eu.europa.ec.leos.services.clone.CloneContext;
import eu.europa.ec.leos.services.compare.vo.Element;
import eu.europa.ec.leos.services.processor.content.XmlContentProcessor;
import eu.europa.ec.leos.services.support.XercesUtils;
import eu.europa.ec.leos.services.support.XmlHelper;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

import static eu.europa.ec.leos.services.compare.ComparisonHelper.buildElement;
import static eu.europa.ec.leos.services.compare.ComparisonHelper.getListWrapper;
import static eu.europa.ec.leos.services.compare.ComparisonHelper.is;
import static eu.europa.ec.leos.services.compare.ComparisonHelper.isElementContentEqual;
import static eu.europa.ec.leos.services.compare.ComparisonHelper.isElementTransformedFrom;
import static eu.europa.ec.leos.services.compare.ComparisonHelper.isListIntro;
import static eu.europa.ec.leos.services.compare.ComparisonHelper.isListIntroAndFirstSubpoint;
import static eu.europa.ec.leos.services.compare.ComparisonHelper.isSoftAction;
import static eu.europa.ec.leos.services.compare.ComparisonHelper.isSoftDeletedOrSoftMovedTo;
import static eu.europa.ec.leos.services.compare.ComparisonHelper.withPlaceholderPrefix;
import static eu.europa.ec.leos.services.compare.IndentContentComparatorHelper.containsNotDeletedElementsInOtherContext;
import static eu.europa.ec.leos.services.compare.IndentContentComparatorHelper.elementImpactedByIndentation;
import static eu.europa.ec.leos.services.compare.IndentContentComparatorHelper.hasIndentedChild;
import static eu.europa.ec.leos.services.compare.IndentContentComparatorHelper.isElementIndented;
import static eu.europa.ec.leos.services.compare.IndentContentComparatorHelper.isElementIndentedInOtherContext;
import static eu.europa.ec.leos.services.compare.IndentContentComparatorHelper.isElementRemovedInOtherContext;
import static eu.europa.ec.leos.services.compare.IndentContentComparatorHelper.isFirstSubElement;
import static eu.europa.ec.leos.services.compare.IndentContentComparatorHelper.isIndentedRenumbering;
import static eu.europa.ec.leos.services.compare.IndentContentComparatorHelper.isNewElementOutdentedFromOld;
import static eu.europa.ec.leos.services.compare.IndentContentComparatorHelper.isRemovedElementIndentedInNewContext;
import static eu.europa.ec.leos.services.compare.IndentContentComparatorHelper.shouldBeMarkedAsSoftAdded;
import static eu.europa.ec.leos.services.compare.IndentContentComparatorHelper.wasChildOfPreviousSibling;
import static eu.europa.ec.leos.services.support.XercesUtils.createXercesDocument;
import static eu.europa.ec.leos.services.support.XercesUtils.insertAttributeIfNotPresent;
import static eu.europa.ec.leos.services.support.XercesUtils.updateXMLIDAttribute;
import static eu.europa.ec.leos.services.support.XmlHelper.EMPTY_STRING;
import static eu.europa.ec.leos.services.support.XmlHelper.LEOS_DELETABLE_ATTR;
import static eu.europa.ec.leos.services.support.XmlHelper.LEOS_EDITABLE_ATTR;
import static eu.europa.ec.leos.services.support.XmlHelper.LEOS_INITIAL_NUM;
import static eu.europa.ec.leos.services.support.XmlHelper.LEOS_SOFT_ACTION_ATTR;
import static eu.europa.ec.leos.services.support.XmlHelper.LEOS_SOFT_ACTION_ROOT_ATTR;
import static eu.europa.ec.leos.services.support.XmlHelper.LEOS_SOFT_DATE_ATTR;
import static eu.europa.ec.leos.services.support.XmlHelper.LEOS_SOFT_MOVE_FROM;
import static eu.europa.ec.leos.services.support.XmlHelper.LEOS_SOFT_MOVE_TO;
import static eu.europa.ec.leos.services.support.XmlHelper.LEOS_SOFT_TRANS_FROM;
import static eu.europa.ec.leos.services.support.XmlHelper.LEOS_SOFT_USER_ATTR;
import static eu.europa.ec.leos.services.support.XmlHelper.LIST;
import static eu.europa.ec.leos.services.support.XmlHelper.NUM;
import static eu.europa.ec.leos.services.support.XmlHelper.SOFT_DELETE_PLACEHOLDER_ID_PREFIX;
import static eu.europa.ec.leos.services.support.XmlHelper.SOFT_MOVE_PLACEHOLDER_ID_PREFIX;
import static eu.europa.ec.leos.services.support.XmlHelper.SOFT_TRANSFORM_PLACEHOLDER_ID_PREFIX;
import static eu.europa.ec.leos.services.support.XmlHelper.SUBPARAGRAPH;
import static eu.europa.ec.leos.services.support.XmlHelper.SUBPOINT;
import static eu.europa.ec.leos.services.support.XmlHelper.UTF_8;
import static eu.europa.ec.leos.services.support.XmlHelper.XMLID;
import static eu.europa.ec.leos.services.support.XmlHelper.getDateAsXml;
import static eu.europa.ec.leos.services.support.XmlHelper.getSoftUserAttribute;

@Service
public class XMLContentComparatorServiceImpl implements ContentComparatorService {

    private static final Logger LOG = LoggerFactory.getLogger(XMLContentComparatorServiceImpl.class);
    private static final String FAKE = "</fake>";
    private static final String NAME_MATH_TEX = "name=\"mathTex\"";

    protected MessageHelper messageHelper;
    protected TextComparator textComparator;
    protected SecurityContext securityContext;
    protected XmlContentProcessor xmlContentProcessor;
    protected CloneContext cloneContext;

    @Autowired
    public XMLContentComparatorServiceImpl(MessageHelper messageHelper, TextComparator textComparator,
                                           SecurityContext securityContext, XmlContentProcessor xmlContentProcessor, CloneContext cloneContext) {
        this.messageHelper = messageHelper;
        this.textComparator = textComparator;
        this.securityContext = securityContext;
        this.xmlContentProcessor = xmlContentProcessor;
        this.cloneContext = cloneContext;
    }

    @Override
    public String compareContents(ContentComparatorContext context) {

        Stopwatch stopwatch = Stopwatch.createStarted();

        setupCompareElements(context, false);

        computeDifferencesAtNodeLevel(context);
        
        // LEOS-5819: re-generate soft action label attributes
        byte[] xmlContent = XercesUtils.nodeToByteArray(context.getResultNode());
        Document document = createXercesDocument(xmlContent);
        Node result = document.getFirstChild();
        xmlContentProcessor.updateSoftMoveLabelAttribute(result, LEOS_SOFT_MOVE_TO);
        xmlContentProcessor.updateSoftMoveLabelAttribute(result, LEOS_SOFT_MOVE_FROM);

        LOG.debug("Comparison finished!  ({} milliseconds)", stopwatch.elapsed(TimeUnit.MILLISECONDS));
        return XercesUtils.nodeToString(result);
    }

    private void resetResultNode(ContentComparatorContext context, Node node) {
        node = XercesUtils.importNodeInDocument(node.getOwnerDocument(), node);
        node.setTextContent(EMPTY_STRING);
        context.setResultNode(node);
    }

    protected void addToResultNode(ContentComparatorContext context, Node node) {
        if (node.getOwnerDocument() != context.getResultNode().getOwnerDocument()) {
            node = XercesUtils.importNodeInDocument(context.getResultNode().getOwnerDocument(), node);
        }
        context.getResultNode().appendChild(node);
    }

    protected void appendDeletedNumberElement(ContentComparatorContext context) {
        Element softDeletedNewElement = context.getNewElement();
        Node node = softDeletedNewElement.getNode();
        if(context.getOldContentElements().containsKey(softDeletedNewElement.getTagId().replace(
                SOFT_DELETE_PLACEHOLDER_ID_PREFIX, EMPTY_STRING))) {
            Element originalOldElement = context.getOldContentElements().get(softDeletedNewElement.getTagId().replace(
                    SOFT_DELETE_PLACEHOLDER_ID_PREFIX, EMPTY_STRING));
            node.setTextContent(originalOldElement.getTagContent());
        }
        String attrName = context.getAttrName();
        String attrValue = getStartTagValueForRemovedElement(softDeletedNewElement, context);
        if (attrName != null && attrValue != null) {
            XercesUtils.addAttribute(node, attrName, attrValue);
        }
        addToResultNode(context, node);
    }

    protected void appendMovedToOrDeletedElement(ContentComparatorContext context) {
        Element softMovedToOrDeletedNewElement = context.getNewElement();
        Node node = softMovedToOrDeletedNewElement.getNode();
        String attrName = context.getAttrName();
        String attrValue = getStartTagValueForRemovedElement(softMovedToOrDeletedNewElement, context);
        if (attrName != null && attrValue != null) {
            XercesUtils.addAttribute(node, attrName, attrValue);
        }
        addToResultNode(context, node);
    }

    private void computeDifferencesAtNodeLevel(ContentComparatorContext context) {

        if (shouldIgnoreElement(context.getOldContentRoot())) {
            return;
        }

        int oldContentChildIndex = 0; // current index in oldContentRoot children list
        int newContentChildIndex = 0; // current index in newContentRoot children list
        int intermediateContentChildIndex = 0; // current index in intermediateContentRoot children list

        while (context.getOldContentRoot() != null && oldContentChildIndex < context.getOldContentRoot().getChildren().size()
                && newContentChildIndex < context.getNewContentRoot().getChildren().size()) {

            context.setOldElement(context.getOldContentRoot().getChildren().get(oldContentChildIndex))
                    .setNewElement(context.getNewContentRoot().getChildren().get(newContentChildIndex))
                    .setIndexOfOldElementInNewContent(getBestMatchInList(context.getNewContentRoot().getChildren(), context.getOldElement()))
                    .setIndexOfNewElementInOldContent(getBestMatchInList(context.getOldContentRoot().getChildren(), context.getNewElement()));

            if (isElementIndexLessThanRootChildren(context.getIntermediateContentRoot(), intermediateContentChildIndex)) {
                context.setIntermediateElement(context.getIntermediateContentRoot().getChildren().get(intermediateContentChildIndex));
            }

            // at each step, check for a particular structural change in this order
            if (shouldIgnoreElement(context.getNewElement())) {
                // Case when removed element has been indented, it's position in the hierarchy has changed:
                // that's the best way to display removed element at it's original position
                if (isElementIndented(context.getNewElement())
                        && (isSoftAction(context.getNewElement().getNode(), SoftActionType.MOVE_TO)
                        || isSoftAction(context.getNewElement().getNode(), SoftActionType.DELETE)
                        || isSoftAction(context.getNewElement().getNode(), SoftActionType.DELETE_TRANSFORM))
                        && (!shouldIgnoreElement(context.getOldElement()))) {
                    appendMovedToOrDeletedElement(context);
                } else if(isTransformedNumElement(context.getNewElement(), context.getNewContentRoot())) {
                    appendDeletedNumberElement(context);
                }
                newContentChildIndex++;
                if (Boolean.TRUE.equals(context.getThreeWayDiff()) && shouldIgnoreElement(context.getIntermediateElement())) {
                    intermediateContentChildIndex++;
                }
            } else if (shouldIgnoreElement(context.getOldElement())) {
                oldContentChildIndex++;
                if (Boolean.TRUE.equals(context.getThreeWayDiff()) && shouldIgnoreElement(context.getIntermediateElement())) {
                    intermediateContentChildIndex++;
                }
            }
            // Check if indented element removed from new context
            else if (context.getIndexOfOldElementInNewContent() == -1 && isElementImpactedByIndention(context.getNewContentElements(), context.getOldElement())) {
                // means that old element is not anymore child in new context and has been removed from new content root
                oldContentChildIndex++;
                if (Boolean.TRUE.equals(context.getThreeWayDiff())) {
                    int indexOfIntermediateElementInNewContent = getBestMatchInList(context.getNewContentRoot().getChildren(), context.getIntermediateElement());
                    if (indexOfIntermediateElementInNewContent == -1
                            && isElementImpactedByIndention(context.getNewContentElements(), context.getIntermediateElement())) {
                        // means that intermediate element is not anymore child in new context and has been removed from new content root
                        intermediateContentChildIndex++;
                    }
                } else if (isElementRemovedInOtherContext(context.getNewContentElements(), context.getOldElement())
                        && !containsNotDeletedElementsInOtherContext(context.getNewContentElements(), context.getOldElement())) {
                    appendRemovedContent(context);
                }
            } else if (Boolean.TRUE.equals(context.getThreeWayDiff()) && context.getIntermediateContentRoot() != null && getBestMatchInList(context.getIntermediateContentRoot().getChildren(), context.getOldElement()) == -1
                    && isElementImpactedByIndention(context.getIntermediateContentElements(), context.getOldElement())) {
                // means that old element is not anymore child in intermediate context and has been removed from intermediate content root
                oldContentChildIndex++;
            } else if (Boolean.TRUE.equals(context.getThreeWayDiff()) && context.getIntermediateElement() != null && getBestMatchInList(context.getNewContentRoot().getChildren(), context.getIntermediateElement()) == -1 && context.getIndexOfNewElementInOldContent() != -1 && context.getIndexOfOldElementInNewContent() != -1
                    && isElementImpactedByIndention(context.getNewContentElements(), context.getIntermediateElement())) {
                // means that intermediate element is not anymore child in new context and has been removed from new content root
                intermediateContentChildIndex++;
            }
            // Check if indented element added in new context
            else if (context.getIndexOfNewElementInOldContent() == -1
                    && isElementImpactedByIndention(context.getOldContentElements(), context.getNewElement())) {
                // means that new element is added as child in new context
                if ((!isSoftAction(context.getOldElement().getNode(), SoftActionType.MOVE_FROM)
                        && isElementRemovedInOtherContext(context.getNewContentElements(), context.getOldElement()))
                        && (!isElementIndented(context.getNewElement()) ||
                        !wasChildOfPreviousSibling(context.getNewElement(), context, newContentChildIndex)) &&
                        !isNewElementOutdentedFromOld(context)) {
                    appendRemovedElementContentIfRequired(context);
                    oldContentChildIndex++;
                }
                appendAddedElementContent(context);
                newContentChildIndex++;
                if (Boolean.TRUE.equals(context.getThreeWayDiff())) {
                    int indexOfIntermediateElementInOldContent = getBestMatchInList(context.getOldContentRoot().getChildren(), context.getIntermediateElement());
                    if (indexOfIntermediateElementInOldContent == -1 && context.getIntermediateElement() != null
                            && isElementImpactedByIndention(context.getOldContentElements(), context.getIntermediateElement())) {
                        // means that intermediate element is added as child in intermediate context
                        if (!isSoftAction(context.getOldElement().getNode(), SoftActionType.MOVE_FROM) && isElementRemovedInOtherContext(context.getIntermediateContentElements(), context.getOldElement())) {
                            appendRemovedElementContentIfRequired(context);
                            oldContentChildIndex++;
                        }
                        intermediateContentChildIndex++;
                    }
                }
            } else if (Boolean.TRUE.equals(context.getThreeWayDiff()) && context.getIntermediateContentRoot() != null && getBestMatchInList(context.getIntermediateContentRoot().getChildren(), context.getNewElement()) == -1
                    && isElementImpactedByIndention(context.getIntermediateContentElements(), context.getNewElement())) {
                // means that new element is added as child in new context
                if (!isSoftAction(context.getIntermediateElement().getNode(), SoftActionType.MOVE_FROM) && isElementRemovedInOtherContext(context.getNewContentElements(), context.getIntermediateElement())) {
                    appendIntermediateRemovedElement(context, intermediateContentChildIndex);
                    intermediateContentChildIndex++;
                } else if (isElementIndented(context.getIntermediateElement()) && context.getIntermediateElement().getTagId().contains(context.getNewElement().getTagId())) {
                    intermediateContentChildIndex++;
                }
                appendAddedElementContent(context);
                newContentChildIndex++;
            }
            // Case when subparagraph has been moved out of a list, find subparagraph in new context and compare
            else if (wasIntroOfOtherElement(context.getNewElement(), context.getOldElement()) != null
                    && !isElementIndented(context.getNewElement())
                    && !isSoftAction(context.getNewElement().getNode(), SoftActionType.MOVE_FROM)) {
                        context.setOldElement(wasIntroOfOtherElement(context.getNewElement(), context.getOldElement()));
                        compareElementContents(context);
                        newContentChildIndex++;
            } else if (newContentChildIndex == context.getIndexOfOldElementInNewContent()
                    && (!context.getDisplayRemovedContentAsReadOnly()
                    || (shouldCompareElements(context.getOldElement(), context.getNewElement())
                    && shouldCompareElements(context.getNewElement(), context.getOldElement())))) {

                if (Boolean.TRUE.equals(context.getThreeWayDiff()) && isIntermediateElementRemovedInNewContent(context)) {
                    if (isElementIndexLessThanRootChildren(context.getIntermediateContentRoot(), intermediateContentChildIndex)) {
                        // LEOS-4392 If old and new contents are same check if structure elements are added/moved/transformed in
                        // intermediate version but reverted in new version for three way diff.
                        if (!isCurrentElementNonIgnored(context.getIntermediateElement().getNode())) {
                            compareRevertedChanges(context);
                        }
                        intermediateContentChildIndex++;
                    } else { //No more children in intermediate check for remaining child elements in new/old versions
                        oldContentChildIndex++;
                        newContentChildIndex++;
                    }
                } else {
                    if (isThreeWayDiffEnabled(context) && isAddedNonIgnoredElement(context.getIntermediateElement().getNode()) &&
                            !context.getNewElement().getTagId().equalsIgnoreCase(context.getIntermediateElement().getTagId()) &&
                            isElementIndexLessThanRootChildren(context.getIntermediateContentRoot(), intermediateContentChildIndex)) {
                        // Ignore soft added or move_from structured action in intermediate as it is already reverted in new version
                        intermediateContentChildIndex++;
                    } else {
                        // element did not changed relative position so check if it's content is changed and should be compared
                        if (!isElementIndexLessThanRootChildren(context.getIntermediateContentRoot(), intermediateContentChildIndex)) {
                            context.setIntermediateElement(null);
                        }
                        compareElementContents(context);
                        oldContentChildIndex++;
                        newContentChildIndex++;
                        if (isElementIndexLessThanRootChildren(context.getIntermediateContentRoot(), intermediateContentChildIndex)) {
                            intermediateContentChildIndex++;
                        }
                    }
                }
            } else if (context.getIndexOfNewElementInOldContent() < 0 && context.getIndexOfOldElementInNewContent() < 0) {
                // oldElement was completely replaced with newElement
                boolean newContentIncremented = false;
                int ignoredElementIndex = getIndexOfIgnoredElementInNewContent(context);
                if (shouldAppendAddedElement(context, oldContentChildIndex, newContentChildIndex, ignoredElementIndex)) {
                    appendAddedElementContent(context);
                    newContentChildIndex++;
                    newContentIncremented = true;
                } else {
                    if (ignoredElementIndex != -1 && (!isConvertedSubparagraphToIntro(context.getNewElement(), context.getOldContentElements())
                            || (newContentChildIndex != oldContentChildIndex))) {
                        appendRemovedElementContentIfRequired(context);
                    } else if ((isElementUnWrapped(context) || isElementWrapped(context))
                            && !elementImpactedByIndentation(context)
                            && !hasIndentedChild(context.getNewContentElements(), context.getOldElement().getParent())) {
                        appendAddedElementContent(context);
                        newContentChildIndex++;
                    } else if (isElementUnWrapped(context)) {
                        if (isThreeWayDiffEnabled(context) && isIntermediateElementUnWrapped(context)) {
                            compareElementContents(new ContentComparatorContext.Builder(context)
                                    .withIntermediateElement(context.getIntermediateElement().getChildren().get(0))
                                    .withOldElement(context.getOldElement().getChildren().get(0))
                                    .build());
                            intermediateContentChildIndex++;
                        } else {
                            compareElementContents(new ContentComparatorContext.Builder(context)
                                    .withOldElement(context.getOldElement().getChildren().get(0))
                                    .withStartTagAttrName(context.getAttrName())
                                    .withStartTagAttrValue(context.getAddedValue())
                                    .build());
                        }
                        oldContentChildIndex++;
                        newContentChildIndex++;
                    } else if (oldContentChildIndex == newContentChildIndex) {
                        if (getIntroFromFirstList(context.getNewElement()) != null
                                && context.getOldElement().getTagName().equals(XmlHelper.CONTENT)) {
                            appendRemovedElementContentIfRequired(context);
                            appendAddedElementContentIfRequired(context);
                        } else if (isListWrapperSoftDeletedInPreviousList(context.getNewElement())) {
                            appendRemovedListWrapperOfPreviousList(context);
                            appendAddedElementContent(context);
                        } else {
                            appendAddedElementContentIfRequired(context);
                            appendRemovedElementContentIfRequired(context);
                        }
                        newContentChildIndex++;
                    }
                    oldContentChildIndex++;
                }
                if ((shouldIncrementIntermediateIndex(context) && newContentIncremented)) {
                    intermediateContentChildIndex++;
                }
            } else if (context.getIndexOfNewElementInOldContent() >= oldContentChildIndex && context.getIndexOfOldElementInNewContent() > newContentChildIndex) {
                // newElement appears to be moved backward to newContentChildIndex and oldElement appears to be moved forward from oldContentChildIndex
                // at the same time
                // so display the element that was moved more positions because it's more likely to be the action the user actually made
                if ((context.getIndexOfNewElementInOldContent() - oldContentChildIndex > context.getIndexOfOldElementInNewContent() - newContentChildIndex)
                        || Boolean.TRUE.equals(context.getDisplayRemovedContentAsReadOnly()) && !shouldCompareElements(context.getOldElement(), context.getNewElement())) {
                    // newElement was moved backward to newContentChildIndex more positions than oldElement was moved forward from oldContentChildIndex
                    // or the newElement should not be compared with the oldElement
                    // so display the added newElement in the new location for now
                    boolean shouldIncrementIntermediateIndex = shouldIncrementIntermediateIndex(context);
                    appendAddedElementContent(context);
                    newContentChildIndex++;
                    if (shouldIncrementIntermediateIndex) {
                        intermediateContentChildIndex = incrementIntermediateIndexIfRequired(context, context.getNewElement(), intermediateContentChildIndex);
                    }
                } else {
                    // oldElement was moved forward from oldContentChildIndex more or just as many positions as newElement was moved backward to newContentChildIndex
                    // so display the removed oldElement in the original location for now
                    appendRemovedElementContentIfRequired(context);
                    oldContentChildIndex++;
                    intermediateContentChildIndex = incrementIntermediateIndexIfRequired(context, context.getOldElement(), intermediateContentChildIndex);
                }
            } else if (context.getIndexOfNewElementInOldContent() >= 0 && context.getIndexOfNewElementInOldContent() < oldContentChildIndex) {
                // newElement was moved forward to newContentChildIndex and the removed oldElement is already displayed
                // in the original location so display the added newElement in the new location also
                boolean shouldIncrementIntermediateIndex = shouldIncrementIntermediateIndex(context);
                appendAddedElementContent(context);
                newContentChildIndex++;
                if (shouldIncrementIntermediateIndex) {
                    intermediateContentChildIndex = incrementIntermediateIndexIfRequired(context, context.getNewElement(), intermediateContentChildIndex);
                }
            } else if (context.getIndexOfOldElementInNewContent() >= 0 &&
                    context.getIndexOfOldElementInNewContent() < newContentChildIndex &&
                    context.getIndexOfNewElementInOldContent() >= 0) {
                // oldElement was moved backward from oldContentChildIndex and the added newElement is already displayed
                // in the new location so display the removed oldElement in the original location also provided there is
                // no new element added in the new content before it.
                // Is Moved To element has been indented, then skip it ?
                appendRemovedElementContentIfRequired(context);
                oldContentChildIndex++;
                intermediateContentChildIndex = incrementIntermediateIndexIfRequired(context, context.getOldElement(), intermediateContentChildIndex);
            } else if ((context.getIndexOfNewElementInOldContent() < 0 && getIndexOfIgnoredElementInNewContent(context) < 0)
                    || shouldAppendAddedElement(context, oldContentChildIndex, newContentChildIndex, getIndexOfIgnoredElementInNewContent(context))) {
                // newElement is simply added or added before deleted or moved element so display the added element
                boolean shouldIncrementIntermediateIndex = shouldIncrementIntermediateIndex(context);
                appendAddedElementContent(context);
                newContentChildIndex++;
                if (shouldIncrementIntermediateIndex) {
                    intermediateContentChildIndex = incrementIntermediateIndexIfRequired(context, context.getNewElement(), intermediateContentChildIndex);
                }
            } else if (isElementRemovedInOtherContext(context.getNewContentElements(), context.getOldElement())
                    && isSoftAction(context.getNewElement().getNode(), SoftActionType.MOVE_FROM)
                    && hasIndentedChild(context.getNewElement())) {
                // Particular case where Move To and Move From element are together at the same place
                // that could happen when elements are indented to the move from element
                if (oldContentChildIndex > newContentChildIndex) {
                    appendAddedElementContentIfRequired(context);
                    appendRemovedElementContentIfRequired(context);
                } else {
                    appendRemovedElementContentIfRequired(context);
                    appendAddedElementContentIfRequired(context);
                }
                intermediateContentChildIndex++;
                oldContentChildIndex++;
                newContentChildIndex++;
            } else {
                // oldElement was deleted or moved so only display the removed element
                if (!isElementIndentedInOtherContext(context.getNewContentElements(), context.getOldElement())) {
                    appendRemovedElementContentIfRequired(context);
                }
                oldContentChildIndex++;
                intermediateContentChildIndex = incrementIntermediateIndexIfRequired(context, context.getOldElement(), intermediateContentChildIndex);
            }
        }

        if (context.getOldContentRoot() != null && oldContentChildIndex < context.getOldContentRoot().getChildren().size()) {
            // there are still children in the old root that have not been processed
            // it means they were all moved backward or under a different parent or deleted
            // so display the removed children
            for (int i = oldContentChildIndex; i < context.getOldContentRoot().getChildren().size(); i++) {
                Element oldElementChild = context.getOldContentRoot().getChildren().get(i);
                context.setOldElement(oldElementChild);
                ContentComparatorContext newContext = context;

                if (Boolean.TRUE.equals(context.getThreeWayDiff())
                    &&isElementIndexLessThanRootChildren(context.getIntermediateContentRoot(), i)) {
                        Element intermediateElementChild = context.getIntermediateContentRoot().getChildren().get(i);
                        newContext = new ContentComparatorContext.Builder(context)
                                .withIntermediateElement(intermediateElementChild)
                                .build();
                }

                // In case of out/indentation the indented children should not be added as removed elements
                if (!shouldIgnoreElement(oldElementChild)
                        && !containsNotDeletedElementsInOtherContext(context.getNewContentElements(), oldElementChild)
                        && isElementRemovedInOtherContext(context.getNewContentElements(), oldElementChild)
                        && !isElementUnWrapped(context)) {
                    appendRemovedElementContentIfRequired(newContext);
                }
            }
        } else if (newContentChildIndex < context.getNewContentRoot().getChildren().size()) {
            // there are still children in the new root that have not been processed
            // it means they were all moved forward or from a different parent or added
            // so display the added children
            int newContentIndexForChildren = newContentChildIndex;
            int intermediateContentIndexForChildren = intermediateContentChildIndex;
            while ((isElementIndexLessThanRootChildren(context.getNewContentRoot(), newContentIndexForChildren))) {
                Element newElementChild = context.getNewContentRoot().getChildren().get(newContentIndexForChildren);

                if (Boolean.TRUE.equals(context.getThreeWayDiff()) && isElementIndexLessThanRootChildren(context.getIntermediateContentRoot(), intermediateContentIndexForChildren)) {
                    Element intermediateElementChild = context.getIntermediateContentRoot().getChildren().get(intermediateContentIndexForChildren);
                    context.setIntermediateElement(intermediateElementChild);
                    //there are children added in intermediate and kept in new content as well.If any children
                    //deleted in the new content then display the removed content in three way diff as deleted.
                    if (intermediateElementChild != null && !newElementChild.getTagId().equalsIgnoreCase(intermediateElementChild.getTagId())
                            && !context.getNewContentElements().containsKey(intermediateElementChild.getTagId())
                            && !isElementIndentedInOtherContext(context.getNewContentElements(), intermediateElementChild)) {
                        appendRemovedContent(new ContentComparatorContext.Builder(context)
                                .withOldElement(intermediateElementChild)
                                .withOldContentNode(context.getIntermediateContentNode())
                                .build());
                        intermediateContentIndexForChildren++;
                        continue; //skip to next iteration till deleted content is fully displayed
                    }
                }
                if (!shouldIgnoreElement(newElementChild)) {
                    appendAddedElementContent(context.setIndexOfOldElementInNewContent(newContentIndexForChildren).setNewElement(newElementChild));
                }
                newContentIndexForChildren++;
                intermediateContentIndexForChildren = incrementIntermediateIndexIfRequired(context, newElementChild, intermediateContentIndexForChildren);

            }
            if (Boolean.TRUE.equals(context.getThreeWayDiff()) && isElementIndexLessThanRootChildren(context.getIntermediateContentRoot(), intermediateContentIndexForChildren)) {
                appendIntermediateRemovedElement(context, intermediateContentIndexForChildren);
            }
        } else if (Boolean.TRUE.equals(context.getThreeWayDiff()) && isElementIndexLessThanRootChildren(context.getIntermediateContentRoot(), intermediateContentChildIndex)) {
            appendIntermediateRemovedElement(context, intermediateContentChildIndex);
        }
    }

    private boolean isTransformedNumElement(Element element, Element parent) {
        return (element != null && element.getTagName().equalsIgnoreCase(NUM) &&
                withPlaceholderPrefix(element.getNode(), SOFT_DELETE_PLACEHOLDER_ID_PREFIX) &&
                isSoftAction(parent.getNode(), SoftActionType.TRANSFORM));
    }

    // There are still children remaining in the intermediate but not present in old and new content
    // it means that new element is added in intermediate and removed in current so it should be shown as deleted in comparison
    private void appendIntermediateRemovedElement(ContentComparatorContext context, int intermediateContentIndexForChildren) {
        for (int i = intermediateContentIndexForChildren; i < context.getIntermediateContentRoot().getChildren().size(); i++) {
            Element intermediateElementChild = context.getIntermediateContentRoot().getChildren().get(i);
            if (!isCurrentElementNonIgnored(intermediateElementChild.getNode())
                    && !containsNotDeletedElementsInOtherContext(context.getNewContentElements(), intermediateElementChild)
                    && isElementRemovedInOtherContext(context.getNewContentElements(), intermediateElementChild)) {
                appendRemovedContent(new ContentComparatorContext.Builder(context)
                        .withOldElement(intermediateElementChild)
                        .withOldContentNode(context.getIntermediateContentNode())
                        .build());
            }
        }
    }

    private boolean isNewAddedElement(ContentComparatorContext context, int oldContentChildIndex, int newContentChildIndex, int ignoredElementIndex) {
        return ((isAddedElement(context.getNewElement().getNode()) || isAddedNonIgnoredElement(context.getNewElement().getNode()))
                && ignoredElementIndex > oldContentChildIndex && ignoredElementIndex > newContentChildIndex);
    }

    private boolean shouldAppendAddedElement(ContentComparatorContext context, int oldContentChildIndex, int newContentChildIndex, int ignoredElementIndex) {
        return (isAddedNonIgnoredElement(context.getNewElement().getNode())
                && ignoredElementIndex > oldContentChildIndex && ignoredElementIndex > newContentChildIndex);
    }

    protected final void compareElementContents(ContentComparatorContext context) {
        Node node = getNodeFromElement(context.getNewElement());
        if (!isActionRoot(node) && !containsAddedNonIgnoredElements(node)
                && ((isElementContentEqual(context) && !containsIgnoredElements(node)) || (context.getIgnoreRenumbering() && !isIndentedRenumbering(context.getOldElement(), context.getNewElement()) && shouldIgnoreRenumbering(context.getNewElement())))) {
            if (Boolean.TRUE.equals(context.getThreeWayDiff())) {
                node = buildNodeForAddedElement(context.getNewElement(), context.getIntermediateElement(), context);
            } else if (!(isElementContentEqual(context) && !containsIgnoredElements(node)) && context.getStartTagAttrName() != null && shouldBeMarkedAsAdded(context)) {
                XercesUtils.insertOrUpdateAttributeValue(node, context.getStartTagAttrName(), context.getStartTagAttrValue());
            }
            addToResultNode(context, node);
        } else if (!shouldIgnoreElement(context.getOldElement()) && (!context.getIgnoreElements() || !shouldIgnoreElement(context.getNewElement()))) {
            //add the start tag
            if (isThreeWayDiffEnabled(context)) {
                if (isElementContentEqual(context) && !containsIgnoredElements(node)) {
                    node = buildNode(context.getNewElement());
                } else if (!shouldIgnoreElement(context.getNewElement()) && ((isAddedNonIgnoredElement(context.getIntermediateElement().getNode()) &&
                        !shouldIgnoreElement(context.getIntermediateElement())) ||
                        shouldIgnoreElement(context.getIntermediateElement()))) { // build start tag for moved/added element with added styles
                    node = buildNodeForAddedElement(context);
                } else if (shouldIgnoreElement(context.getNewElement())) {
                    node = buildNodeForRemovedElement(context.getNewElement());
                } else if (isActionRoot(node) || (isListIntroAndFirstSubpoint(context.getNewElement()) && isActionRoot(node.getParentNode().getParentNode()))) {
                    node = buildNodeForAddedElement(context);
                } else if (context.getOldElement() == null && !shouldIgnoreElement(context.getNewElement()) && !shouldIgnoreElement(context.getIntermediateElement())) { //build start tag for added element in intermediate
                    node = buildNodeForAddedElement(context);
                } else if (context.getNewElement() != null && context.getIntermediateElement() == null && context.getOldElement() == null) {
                    node = buildNode(context.getNewElement(), context.getAttrName(), context.getAddedIntermediateValue());
                } else {
                    node = buildNode(context.getNewElement()); //build tag for children without styles
                }
            } else {
                // Indented elements should not be marked as added
                if (shouldBeMarkedAsAdded(context)) {
                    node = buildNode(context.getNewElement(), context.getStartTagAttrName(), context.getStartTagAttrValue());
                } else {
                    node = buildNode(context.getNewElement()); //build tag for children without styles
                }
            }

            context.resetStartTagAttribute();

            if (context.getNewElement().getTagName().equalsIgnoreCase(NUM) && shouldIgnoreElement(context.getNewElement())) {
                node.setTextContent(getRemovedNumContent(context));
                addToResultNode(context, node);
            } else if ((context.getNewElement() != null && context.getNewElement().hasTextChild()) || (context.getOldElement() != null && context.getOldElement().hasTextChild())) {
                String oldContent = XercesUtils.getContentNodeAsXmlFragment(getNodeFromElement(context.getOldElement()));
                String newContent = XercesUtils.getContentNodeAsXmlFragment(getNodeFromElement(context.getNewElement()));
                String intermediateContent = null;
                if (isThreeWayDiffEnabled(context) && context.getIntermediateElement().hasTextChild()) {
                    intermediateContent = XercesUtils.getContentNodeAsXmlFragment(context.getIntermediateElement().getNode());
                }
                String result;
                try {
                    result = textComparator.compareTextNodeContents(oldContent, newContent, intermediateContent, context);
                    result = correctFormulasEscaping(result, context);
                } catch (Exception e) {
                    LOG.error("Failure during text comparison. Exception thrown from text diffing library ", e);
                    result = messageHelper.getMessage("leos.version.compare.error.message");
                }
                result = "<fake xmlns:leos=\"urn:eu:europa:ec:leos\">" + result + FAKE;
                Node comparedContentNode = XercesUtils.createNodeFromXmlFragment(node.getOwnerDocument(), result.getBytes(UTF_8));
                node.setTextContent(EMPTY_STRING);
                node = XercesUtils.copyContent(comparedContentNode, node);
                addToResultNode(context, node);
            } else {
                node = XercesUtils.importNodeInDocument(context.getResultNode().getOwnerDocument(), node);
                node.setTextContent(EMPTY_STRING);
                ContentComparatorContext newContext = new ContentComparatorContext.Builder(context)
                        .withOldContentRoot(context.getOldElement())
                        .withNewContentRoot(context.getNewElement())
                        .withIntermediateContentRoot(context.getIntermediateElement())
                        .withResultNode(node)
                        .build();
                addToResultNode(context, node);
                computeDifferencesAtNodeLevel(newContext);
            }
        } else if (shouldDisplayRemovedContent(context.getIndexOfOldElementInNewContent())) {
            //element removed in the new version
            appendRemovedElementContent(context);
        }
    }

    private String correctFormulasEscaping(String result, ContentComparatorContext context) {
        // Detect when the diff result contains a formula to avoid escaping of special characters - LEOS-6058
        int inlineFormulaIndex = result.indexOf(NAME_MATH_TEX);
        boolean resultIsFormula = (context.getNewElement() != null && context.getNewElement().getTagName().equalsIgnoreCase("inline") &&
                context.getNewElement().getNode().getAttributes() != null &&
                context.getNewElement().getNode().getAttributes().getNamedItem("name") != null &&
                context.getNewElement().getNode().getAttributes().getNamedItem("name").getNodeValue().equalsIgnoreCase("mathTex"));
        if (resultIsFormula) {
            result = result.replace("\\\\\\\\", "\\\\");
        } else if (inlineFormulaIndex > 0) {
            while (inlineFormulaIndex > 0) {
                int inlineFormulaStart = result.indexOf(">", inlineFormulaIndex) + 1;
                int inlineFormulaEnd = result.indexOf("</inline", result.indexOf(NAME_MATH_TEX));
                String formula = result.substring(inlineFormulaStart, inlineFormulaEnd);
                formula = formula.replace("\\\\\\\\", "\\\\");
                result = result.substring(0, inlineFormulaStart) + formula + result.substring(inlineFormulaEnd);
                inlineFormulaIndex = result.indexOf(NAME_MATH_TEX, inlineFormulaEnd);
            }
        }
        return result;
    }

    private Node getNodeFromElement(Element element) {
        Node node = null;
        if (element != null) {
            node = element.getNode();
        }
        return node;
    }

    private boolean isThreeWayDiffEnabled(ContentComparatorContext context) {
        return context.getThreeWayDiff() && context.getIntermediateElement() != null;
    }

    private boolean shouldIncrementIntermediateIndex(ContentComparatorContext context) {
        boolean shouldIncrementIndex;
        if (isThreeWayDiffEnabled(context)) {
            if (context.getOldElement().getTagId().equalsIgnoreCase(context.getIntermediateElement().getTagId()) ||
                    !context.getNewElement().getTagId().equalsIgnoreCase(context.getIntermediateElement().getTagId())) {
                shouldIncrementIndex = false;
            } else {
                shouldIncrementIndex = true;
            }
        } else {
            shouldIncrementIndex = false;
        }
        return shouldIncrementIndex;
    }

    private int incrementIntermediateIndexIfRequired(ContentComparatorContext context, Element element, int intermediateContentChildIndex) {
        if (isThreeWayDiffEnabled(context) &&
                element.getTagId().equalsIgnoreCase(context.getIntermediateElement().getTagId()) || shouldIgnoreElement(context.getIntermediateElement())) {
            intermediateContentChildIndex++;
        }
        return intermediateContentChildIndex;
    }

    private void compareRevertedChanges(ContentComparatorContext context) {
        //LEOS-4392 If structure is different in intermediate, update context by setting old = intermediate
        //and compare with new version like two version diff
        ContentComparatorContext newContext = new ContentComparatorContext.Builder(context)
                .withOldElement(context.getIntermediateElement())
                .withOldContentRoot(context.getIntermediateContentRoot())
                .withOldContentElements(context.getIntermediateContentElements())
                .withOldContentNode(context.getIntermediateContentNode())
                .build();
        appendRemovedElementContentIfRequired(newContext);
    }

    protected Node buildNode(Element element, String attrName, String attrValue) {
        Node node = buildNode(element);
        // Indented should be considered as added
        if ((attrName != null) && shouldBeMarkedAsSoftAdded(element, attrValue)) {
            XercesUtils.insertOrUpdateAttributeValue(node, attrName, attrValue);
        }
        return node;
    }

    protected Node buildNode(Element element) {
        Node node = element.getNode();
        if (shouldIgnoreElement(element)) {
            // add read-only attributes
            addReadOnlyAttributes(node);
        }
        return node;
    }

    protected void addReadOnlyAttributes(Node node) {
        XercesUtils.insertOrUpdateAttributeValue(node, LEOS_DELETABLE_ATTR, Boolean.FALSE.toString());
        XercesUtils.insertOrUpdateAttributeValue(node, LEOS_EDITABLE_ATTR, Boolean.FALSE.toString());
    }

    protected final void appendRemovedElementContentIfRequired(ContentComparatorContext context) {
        if (!isRemovedElementIndentedInNewContext(context, context.getOldElement()) && (shouldIgnoreElement(context.getNewContentRoot()) || isElementInItsOriginalPosition(context.getNewContentRoot()))) {
            appendRemovedElementContent(context);
        } else if (!isElementMovedOrTransformed(context.getNewElement()) || isCurrentElementNonIgnored(getNodeFromElement(context.getOldElement()))) {
            appendRemovedContent(context);
        }
    }

    protected final void appendAddedElementContentIfRequired(ContentComparatorContext context) {
        appendAddedElementContent(context);
    }

    protected final boolean isElementRemovedFromContent(int indexOfOldElementInNewContent) {
        return indexOfOldElementInNewContent == -1;
    }

    protected boolean containsSoftDeleteElement(Element element, Map<String, Element> contentElements) {
        return element != null && contentElements.containsKey(SOFT_DELETE_PLACEHOLDER_ID_PREFIX + element.getTagId());
    }

    protected boolean containsSoftMoveToTransformedElement(Map<String, Element> contentElements, Element element) {
        return element != null && (contentElements.containsKey(SOFT_MOVE_PLACEHOLDER_ID_PREFIX + SOFT_TRANSFORM_PLACEHOLDER_ID_PREFIX + element.getParent().getTagId())
                && contentElements.containsKey(SOFT_TRANSFORM_PLACEHOLDER_ID_PREFIX + element.getParent().getTagId())
                && !shouldIgnoreElement(contentElements.get(SOFT_TRANSFORM_PLACEHOLDER_ID_PREFIX + element.getParent().getTagId())));
    }

    protected boolean containsSoftMoveToElement(Map<String, Element> contentElements, Element element) {
        return element != null && (contentElements.containsKey(SOFT_MOVE_PLACEHOLDER_ID_PREFIX + element.getTagId())
                && contentElements.containsKey(element.getTagId()) && !shouldIgnoreElement(contentElements.get(element.getTagId())));
    }

    protected boolean isAddedNonIgnoredElement(Node node) {
        return isSoftAction(node, SoftActionType.MOVE_FROM) || isSoftAction(node, SoftActionType.ADD);
    }

    protected boolean isAddedElement(Node node) {
        if (node != null) {
            String attrValue = XercesUtils.getAttributeValue(node, XMLID);
            return attrValue == null;
        }
        return false;
    }

    protected final int getBestMatchInList(List<Element> childElements, Element element) {
        if (shouldIgnoreElement(element)) {
            return -2;
        } else if (element == null) {
            return -1;
        }
        int foundPosition = -1;
        int[] rank = new int[childElements.size()];
        for (int iCount = 0; iCount < childElements.size(); iCount++) {
            Element listElement = childElements.get(iCount);

            if (listElement.getTagId() != null && element.getTagId() != null
                    && listElement.getTagId().equals(element.getTagId())) {
                rank[iCount] = 1000;
                break;
            } else if ((listElement.getTagId() == null && element.getTagId() == null)
                    && listElement.getTagName().equals(element.getTagName())) {//only try to find match if tagID is not present

                // compute node distance
                int maxDistance = 100;
                int distanceWeight = maxDistance / 5; //after distance of 5 nodes it is discarded
                int nodeDistance = Math.abs(listElement.getNodeIndex() - element.getNodeIndex());
                nodeDistance = Math.min(nodeDistance * distanceWeight, maxDistance); // 0...maxDistance

                // compute node similarity
                int similarityWeight = 2;
                int similarity = (int) (100 * listElement.contentSimilarity(element)); //0...100
                similarity = similarity * similarityWeight;

                // compute node rank
                rank[iCount] = (maxDistance - nodeDistance)  //distance 0=100, 1=80,2=60,..5=0
                        + similarity;
            } else {
                rank[iCount] = 0;
            }
        }

        int bestRank = 0;
        for (int iCount = 0; iCount < rank.length; iCount++) {
            if (bestRank < rank[iCount]) {
                foundPosition = iCount;
                bestRank = rank[iCount];
            }
        }
        return bestRank > 0 ? foundPosition : -1;
    }

    protected boolean containsIgnoredElement(Node node) {
        return isSoftAction(node, SoftActionType.DELETE) || withPlaceholderPrefix(node, SOFT_DELETE_PLACEHOLDER_ID_PREFIX) || isSoftAction(node, SoftActionType.MOVE_TO);
    }

    protected Node getNonIgnoredChangedElementContent(Node contentNode, Element element, String attrName, String attrValue) {
        Node node = XercesUtils.getElementById(contentNode, element.getTagId());
        if (!containsIgnoredElements(node)) {
            node = getChangedElementContent(contentNode, element, attrName, attrValue);
        } else if (!shouldIgnoreElement(element)) {
            XercesUtils.insertOrUpdateAttributeValue(node, attrName, attrValue);
            for (Element child : element.getChildren()) {
                if (!shouldIgnoreElement(child)) {
                    // add child without changing the start tag
                    node.appendChild(getNonIgnoredChangedElementContent(contentNode, child, null, null));
                }
            }
        }
        return node;
    }

    @Override
    public String compareDeletedElements(ContentComparatorContext context) {
        Stopwatch stopwatch = Stopwatch.createStarted();

        boolean usesFakeParentNode = setupCompareElements(context, true);
        computeDeletedNodesAtEachLevel(context);

        LOG.debug("Comparison finished!  ({} milliseconds)", stopwatch.elapsed(TimeUnit.MILLISECONDS));
        String result = XercesUtils.nodeToString(context.getResultNode());
        return usesFakeParentNode ? result.substring(result.indexOf("<", result.indexOf("<fake") + 1), result.indexOf("</fake")) : result;
    }

    private void computeDeletedNodesAtEachLevel(ContentComparatorContext context) {
        int oldContentChildIndex = 0; // current index in oldContentRoot children list
        int newContentChildIndex = 0; // current index in newContentRoot children list

        if (context.getOldContentRoot() != null && context.getOldContentRoot().getChildren().isEmpty()
                && context.getNewContentRoot().getChildren().isEmpty()) {
            context.setOldElement(context.getOldContentRoot());
            context.setNewElement(context.getNewContentRoot());
            context.setIndexOfNewElementInOldContent(0);
            context.setIndexOfOldElementInNewContent(0);
            context.setResultNode(context.getNewElement().getNode());
        }

        while (context.getOldContentRoot() != null && oldContentChildIndex < context.getOldContentRoot().getChildren().size()
                && newContentChildIndex < context.getNewContentRoot().getChildren().size()) {

            context.setOldElement(context.getOldContentRoot().getChildren().get(oldContentChildIndex));
            context.setNewElement(context.getNewContentRoot().getChildren().get(newContentChildIndex));
            context.setIndexOfNewElementInOldContent(getBestMatchInList(context.getOldContentRoot().getChildren(), context.getNewElement()));
            context.setIndexOfOldElementInNewContent(getBestMatchInList(context.getNewContentRoot().getChildren(), context.getOldElement()));

            if (!shouldIgnoreElement(context.getOldElement()) && context.getIndexOfOldElementInNewContent() < 0
                    && context.getIndexOfNewElementInOldContent() > 0 &&
                    !containsSoftDeleteElement(context.getOldElement(), context.getNewContentElements())) {
                //If oldElement still exists in newElement but at different position so skip it
                if (!context.getNewContentElements().containsKey(context.getOldElement().getTagId())) {
                    // oldElement was deleted so only display the removed element with soft attributes
                    Node node = context.getOldElement().getNode();
                    insertSoftDeleteAttributes(node);
                    addReadOnlyAttributes(node);
                    addToResultNode(context, node);
                }
                oldContentChildIndex++;
            } else if (context.getIndexOfNewElementInOldContent() < 0 && context.getIndexOfOldElementInNewContent() < 0
                    && !isElementMovedOrTransformed(context.getNewElement())) {
                // oldElement was completely replaced with newElement
                int ignoredElementIndex = getIndexOfIgnoredElementInNewContent(context);
                if (isNewAddedElement(context, oldContentChildIndex, newContentChildIndex, ignoredElementIndex)) {
                    Node newNode = context.getNewElement().getNode();
                    addToResultNode(context, newNode);
                    newContentChildIndex++;
                } else {
                    Node newNode = context.getNewElement().getNode();
                    Node oldNode = context.getOldElement().getNode();
                    if (ignoredElementIndex != -1) {
                        if (isSoftAction(newNode, SoftActionType.MOVE_TO)) {
                            addToResultNode(context, newNode);
                            newContentChildIndex++;
                        } else {
                            addReadOnlyAttributes(oldNode);
                            insertSoftDeleteAttributes(oldNode);
                            addToResultNode(context, oldNode);
                            newContentChildIndex++;
                        }
                    } else if (isElementWrapped(context) || isElementUnWrapped(context)) {
                        addToResultNode(context, newNode);
                        newContentChildIndex++;
                    } else if (isElementIndented(context.getNewElement())) {
                        addToResultNode(context, newNode);
                        newContentChildIndex++;
                    } else if (isList(context.getOldElement()) && isList(context.getNewElement())
                            && hasIndentedChild(context.getNewElement())
                            && context.getNewElement().getParent().getTagId().equals(context.getOldElement().getParent().getTagId())) {
                        addToResultNode(context, newNode);
                        newContentChildIndex++;
                    } else if (isFirstSubElement(context.getNewElement()) && isFirstSubElement(context.getOldElement())
                            && context.getNewElement().getParent().getTagId().equals(context.getOldElement().getParent().getTagId())) {
                        addToResultNode(context, newNode);
                    } else if (oldContentChildIndex == newContentChildIndex &&
                            !isElementIndentedInOtherContext(context.getNewContentElements(), context.getOldElement())
                            && !isElementTransformedFrom(context.getNewContentRoot().getNode(), LEOS_SOFT_TRANS_FROM, context.getOldElement().getTagId())
                            && !context.getNewContentElements().containsKey(context.getOldElement().getTagId())) {
                        addReadOnlyAttributes(oldNode);
                        insertSoftDeleteAttributes(oldNode);
                        addToResultNode(context, oldNode);
                        newContentChildIndex++;
                        if(!shouldIgnoreElement(context.getNewElement())) {
                            addToResultNode(context, newNode);
                        }
                    }
                    oldContentChildIndex++;
                }
            } else if (context.getIndexOfNewElementInOldContent() < 0 && context.getIndexOfOldElementInNewContent() > 0) {
                // newElement is added before the existing old element so show the newly added one
                Node newNode = context.getNewElement().getNode();
                addToResultNode(context, newNode);
                newContentChildIndex++;
                if (isSoftAction(newNode, SoftActionType.MOVE_TO) && context.getNewElement().getTagId().equalsIgnoreCase(
                        SOFT_MOVE_PLACEHOLDER_ID_PREFIX + context.getOldElement().getTagId())) {
                    oldContentChildIndex++;
                }
            } else if (context.getIndexOfOldElementInNewContent() > 0 && context.getIndexOfNewElementInOldContent() < 0 &&
                    isAddedNonIgnoredElement(context.getNewElement().getNode())) {
                //New element is moved from its original position to show the moved element
                Node newNode = context.getNewElement().getNode();
                addToResultNode(context, newNode);
                newContentChildIndex++;
            } else if (context.getIndexOfNewElementInOldContent() > 0 && context.getIndexOfOldElementInNewContent() < 0
                    && containsSoftDeleteElement(context.getOldElement(), context.getNewContentElements())) {
                //old element is deleted after the existing one so show the deleted one
                Element deletedElement = context.getNewContentElements().get(SOFT_DELETE_PLACEHOLDER_ID_PREFIX +
                        context.getOldElement().getTagId());
                insertSoftDeleteAttributes(deletedElement.getNode());
                addToResultNode(context, deletedElement.getNode());
                oldContentChildIndex++;
            } else {
                compareChildNodes(context);
                oldContentChildIndex++;
                newContentChildIndex++;
            }
        }
        if (oldContentChildIndex < context.getOldContentRoot().getChildren().size()) {
            int oldContentIndexForChildren = oldContentChildIndex;
            while ((isElementIndexLessThanRootChildren(context.getOldContentRoot(), oldContentIndexForChildren))) {
                Element oldElementChild = context.getOldContentRoot().getChildren().get(oldContentIndexForChildren);
                Node node = oldElementChild.getNode();
                if (!shouldIgnoreElement(oldElementChild)
                        && !containsNotDeletedElementsInOtherContext(context.getNewContentElements(), oldElementChild)
                        && isElementRemovedInOtherContext(context.getNewContentElements(), oldElementChild)
                        && !isElementUnWrapped(context)) {
                    insertSoftDeleteAttributes(node);
                    addReadOnlyAttributes(node);
                    addToResultNode(context, node);
                }
                oldContentIndexForChildren++;
            }
        }
        if (newContentChildIndex < context.getNewContentRoot().getChildren().size()) {
            int newContentIndexForChildren = newContentChildIndex;
            while ((isElementIndexLessThanRootChildren(context.getNewContentRoot(), newContentIndexForChildren))) {
                Element newElementChild = context.getNewContentRoot().getChildren().get(newContentIndexForChildren);
                Node node = newElementChild.getNode();
                addToResultNode(context, node);
                newContentIndexForChildren++;
            }
        }
    }

    private void insertSoftDeleteAttributes(Node node) {
        insertAttributeIfNotPresent(node, LEOS_SOFT_ACTION_ATTR, SoftActionType.DELETE.getSoftAction());
        insertAttributeIfNotPresent(node, LEOS_SOFT_ACTION_ROOT_ATTR, "true");
        insertAttributeIfNotPresent(node, LEOS_SOFT_USER_ATTR, getSoftUserAttribute(securityContext.getUser()));
        insertAttributeIfNotPresent(node, LEOS_SOFT_DATE_ATTR, getDateAsXml());
        updateXMLIDAttribute(node, SOFT_DELETE_PLACEHOLDER_ID_PREFIX, false);
    }

    private void compareChildNodes(ContentComparatorContext context) {
        Node node = context.getNewElement().getNode();
        if ((context.getNewElement() != null && context.getNewElement().hasTextChild())) {
            addToResultNode(context, node);
        } else {
            node = XercesUtils.importNodeInDocument(context.getResultNode().getOwnerDocument(), node);
            node.setTextContent(EMPTY_STRING);
            ContentComparatorContext newContext = new ContentComparatorContext.Builder(context)
                    .withOldContentRoot(context.getOldElement())
                    .withNewContentRoot(context.getNewElement())
                    .withIntermediateContentRoot(context.getIntermediateElement())
                    .withResultNode(node)
                    .build();
            addToResultNode(context, node);
            computeDeletedNodesAtEachLevel(newContext);
        }
    }

    protected boolean isElementWrapped(ContentComparatorContext context) {
        //e.g. to check if the <content> is wrapped by <subparagraph> or <alinea> in case of outdent/indent
        if (CollectionUtils.isNotEmpty(context.getNewElement().getChildren())) {
            return context.getOldElement() != null && context.getOldElement().equals(context.getNewElement().getChildren().get(0));
        }
        return false;
    }

    protected boolean isElementUnWrapped(ContentComparatorContext context) {
        //e.g. to check if the <alinea> or <subparagraph> is removed in case of outdent/indent
        if (context.getOldElement().getChildren() != null && !context.getOldElement().getChildren().isEmpty()) {
            return context.getNewElement().equals(context.getOldElement().getChildren().get(0));
        }
        return false;
    }

    private boolean isIntermediateElementUnWrapped(ContentComparatorContext context) {
        //e.g. to check if the <alinea> or <subparagraph> is removed in case of outdent/indent
        if (CollectionUtils.isNotEmpty(context.getIntermediateElement().getChildren())) {
            return context.getNewElement().equals(context.getIntermediateElement().getChildren().get(0));
        }
        return false;
    }

    private boolean setupCompareElements(ContentComparatorContext context, boolean enableFakeEncapsulation) {
        // If enabled, encapsulate the xml content in a fake parent tag so Xerces can process the content
        final String oldXml = enableFakeEncapsulation ? "<fake>" + context.getComparedVersions()[0] + FAKE : context.getComparedVersions()[0];
        final String newXml = enableFakeEncapsulation ? "<fake>" + context.getComparedVersions()[1] + FAKE : context.getComparedVersions()[1];
        boolean usesFakeEncapsulation = false;
        Node oldNode = XercesUtils.createXercesDocument(oldXml.getBytes(UTF_8), false).getDocumentElement();
        Node newNode = XercesUtils.createXercesDocument(newXml.getBytes(UTF_8), false).getDocumentElement();

        XercesUtils.addLeosNamespace(oldNode);
        XercesUtils.addLeosNamespace(newNode);

        if (enableFakeEncapsulation && XercesUtils.countChildren(oldNode, Collections.emptyList()) == 1 && XercesUtils.countChildren(newNode, Collections.emptyList()) == 1) {
            oldNode = XercesUtils.createXercesDocument(context.getComparedVersions()[0].getBytes(UTF_8), false).getDocumentElement();
            newNode = XercesUtils.createXercesDocument(context.getComparedVersions()[1].getBytes(UTF_8), false).getDocumentElement();
            XercesUtils.addLeosNamespace(oldNode);
            XercesUtils.addLeosNamespace(newNode);
        } else {
            usesFakeEncapsulation = enableFakeEncapsulation;
        }

        resetResultNode(context, newNode);
        context.setOldContentElements(new HashMap<>());
        context.setNewContentElements(new HashMap<>());

        final Element oldElement = buildElement(oldNode, new HashMap<>(), context.getOldContentElements());
        final Element newElement = buildElement(newNode, new HashMap<>(), context.getNewContentElements());

        context.setOldContentNode(oldNode)
                .setNewContentNode(newNode)
                .setOldContentRoot(oldElement)
                .setNewContentRoot(newElement);

        if (Boolean.TRUE.equals(context.getThreeWayDiff())) {
            final String intermediateXml = context.getComparedVersions()[2];
            final Node intermediateNode = XercesUtils.createXercesDocument(intermediateXml.getBytes(UTF_8), false).getDocumentElement();

            context.setIntermediateContentElements(new HashMap<>());
            final Element intermediateElement = buildElement(intermediateNode, new HashMap<>(), context.getIntermediateContentElements());

            context.setIntermediateContentNode(intermediateNode)
                    .setIntermediateContentRoot(intermediateElement);
        }
        return usesFakeEncapsulation;
    }

    private boolean isList(Element element) {
        return element != null && element.getTagName().equals(LIST);
    }

    @Override
    public String[] twoColumnsCompareContents(ContentComparatorContext context) {
        return new String[]{context.getLeftResultBuilder().toString(), context.getRightResultBuilder().toString()};
    }

    public Element getIntroFromFirstList(Element element) {
        if (element.getTagName().equals(XmlHelper.LIST) && !element.getChildren().isEmpty() && isIntroFromFirstList(element.getChildren().get(0))) {
            return element.getChildren().get(0);
        }
        return null;
    }

    public boolean isIntroFromFirstList(Element element) {
        if (element != null && element.getTagName().equals(SUBPARAGRAPH)) {
            boolean isFirstElement = false;
            int index = element.getParent().getParent().getChildren().indexOf(element.getParent());
            isFirstElement = (index == 0) || (index == 1 && element.getParent().getParent().getChildren().get(0).getTagName().equals(NUM));
            return (isFirstElement
                    && !element.getParent().getParent().getTagName().equals(XmlHelper.LEVEL)
                    && isListIntro(element));
        }
        return false;
    }

    public Element wasIntroOfOtherElement(Element element, Element otherElement) {
        if (element.getTagName().equalsIgnoreCase(XmlHelper.SUBPARAGRAPH)
            && otherElement.getTagName().equalsIgnoreCase(XmlHelper.LIST)) {
                List<Element> children = otherElement.getChildren();
                for (Element child : children) {
                    if (!child.getTagName().equals(SUBPARAGRAPH)) {
                        return null;
                    }
                    if (child.getTagId().equals(element.getTagId())) {
                        return child;
                    }
                }
        }
        return null;
    }

    protected boolean isConvertedAlineaToIntro(Element oldElement, Element newElement) {
        if (oldElement.getTagName().equals(SUBPOINT)) {
            Optional<Element> convertedElement = newElement.getChildren().stream()
                    .filter(e -> e.getTagName().equals(SUBPARAGRAPH))
                    .findFirst();
            if (convertedElement.isPresent()) {
                return convertedElement.get().getTagId().equals(oldElement.getTagId());
            }
        }
        return false;
    }

    protected boolean isConvertedSubparagraphToIntro(Element newElement, Map<String, Element> otherContextElements) {
        if (newElement.getTagName().equals(SUBPARAGRAPH) && newElement.getParent().getTagName().equals(LIST)) {
            Element originalElement = otherContextElements.get(newElement.getTagId());
            if (originalElement != null) {
                return !originalElement.getParent().getTagName().equals(LIST);
            }
        }
        return false;
    }

    protected boolean isConvertedSubparagraphToIntro(Element oldElement, Element newElement) {
        if (oldElement.getTagName().equals(SUBPARAGRAPH) && newElement.getTagName().equals(LIST)) {
            Optional<Element> convertedElement = newElement.getChildren().stream()
                    .filter(e -> e.getTagName().equals(SUBPARAGRAPH))
                    .findFirst();
            if (convertedElement.isPresent()) {
                return convertedElement.get().getTagId().equals(oldElement.getTagId());
            }
        }
        return false;
    }

    protected Element getPreviousListFromSubParagraph(Element element) {
        if (is(element, SUBPARAGRAPH) && element.getParent() != null) {
            int index = element.getParent().getChildren().indexOf(element);
            Element previousSibling = null;
            if (isListIntro(element) && element.getParent().getParent() != null) {
                index = element.getParent().getParent().getChildren().indexOf(element.getParent());
                previousSibling = index > 0
                        ? element.getParent().getParent().getChildren().get(index - 1) : null;
            } else if (index > 0) {
                previousSibling = index > 0
                        ? element.getParent().getChildren().get(index - 1) : null;
            }
            return previousSibling;
        } else if (is(element, LIST)) {
            int index = element.getParent().getChildren().indexOf(element);
            return index > 0
                    ? element.getParent().getChildren().get(index - 1) : null;
        }
        return null;
    }

    protected boolean isListWrapperSoftDeletedInPreviousList(Element element) {
       Element previousSibling = getPreviousListFromSubParagraph(element);
       Element listWrapper = getListWrapper(previousSibling);
       return (isSoftDeletedOrSoftMovedTo(listWrapper));
    }

    protected void appendRemovedListWrapperOfPreviousList(ContentComparatorContext context) {
        Element element = context.getNewElement();
        Element previousSibling = getPreviousListFromSubParagraph(element);
        Element listWrapper = getListWrapper(previousSibling);
        if (isSoftDeletedOrSoftMovedTo(listWrapper)) {
            String attrName = context.getAttrName();
            String attrValue = getStartTagValueForRemovedElement(listWrapper, context);
            if (attrName != null && attrValue != null) {
                XercesUtils.addAttribute(listWrapper.getNode(), attrName, attrValue);
            }
            addToResultNode(context, listWrapper.getNode());
        }
    }

    private boolean isClonedProposalOrContribution() {
        return (cloneContext != null && (cloneContext.isClonedProposal() || cloneContext.isContribution()));
    }

    protected boolean shouldDisplayRemovedContent(int indexOfOldElementInNewContent) {
        return isElementRemovedFromContent(indexOfOldElementInNewContent);
    }

    protected boolean containsIgnoredElements(Node node) {
        if (isClonedProposalOrContribution()) {
            boolean containsIgnoredElement = false;
            if (containsIgnoredElement(node)) {
                containsIgnoredElement = true;
            } else {
                NodeList nodeList = node.getChildNodes();
                for (int i = 0; i < nodeList.getLength(); i++) {
                    containsIgnoredElement = containsIgnoredElements(nodeList.item(i));
                    if (containsIgnoredElement) {
                        break;
                    }
                }
            }
            return containsIgnoredElement;
        }
        return false;
    }

    protected boolean containsAddedNonIgnoredElements(Node node) {
        if (isClonedProposalOrContribution()) {
            boolean containsAddedNonIgnoredElement = false;
            if (isAddedNonIgnoredElement(node)) {
                containsAddedNonIgnoredElement = true;
            } else {
                NodeList nodeList = node.getChildNodes();
                for (int i = 0; i < nodeList.getLength(); i++) {
                    containsAddedNonIgnoredElement = containsAddedNonIgnoredElements(nodeList.item(i));
                    if (containsAddedNonIgnoredElement) {
                        break;
                    }
                }
            }
            return containsAddedNonIgnoredElement;
        }
        return false;
    }

    protected boolean isElementInItsOriginalPosition(Element element) {
        if (isClonedProposalOrContribution()) {
            if (element != null &&
                    (isSoftAction(element.getNode(), SoftActionType.ADD)
                            || isSoftAction(element.getNode(), SoftActionType.TRANSFORM)
                            || isSoftAction(element.getNode(), SoftActionType.MOVE_FROM))) {
                return false;
            } else if (element != null && isListIntroAndFirstSubpoint(element)) {
                return (!isSoftAction(element.getParent().getParent().getNode(), SoftActionType.ADD)
                        && !isSoftAction(element.getParent().getParent().getNode(), SoftActionType.TRANSFORM)
                        && !isSoftAction(element.getParent().getParent().getNode(), SoftActionType.MOVE_FROM));
            }
            return true;
        }
        return true;
    }

    protected boolean shouldIgnoreElement(Element element) {
        return element != null
                && (isSoftAction(element.getNode(), SoftActionType.DELETE)
                || isSoftAction(element.getNode(), SoftActionType.MOVE_TO)
                || withPlaceholderPrefix(element.getNode(), SOFT_DELETE_PLACEHOLDER_ID_PREFIX))
                && !isSoftAction(element.getNode(), SoftActionType.DELETE_TRANSFORM);
    }

    protected boolean isElementMovedOrTransformed(Element element) {
        if (isClonedProposalOrContribution()) {
            return isSoftAction(element.getNode(), SoftActionType.MOVE_FROM) || isSoftAction(element.getNode(), SoftActionType.TRANSFORM);
        }
        return false;
    }

    protected boolean shouldCompareElements(Element oldElement, Element newElement) {
        if (isClonedProposalOrContribution()) {
            return newElement == null || oldElement == null
                    || !(isSoftAction(newElement.getNode(), SoftActionType.MOVE_FROM) && !isSoftAction(oldElement.getNode(), SoftActionType.MOVE_FROM))
                    && !(isSoftAction(newElement.getNode(), SoftActionType.TRANSFORM) && !isSoftAction(oldElement.getNode(), SoftActionType.TRANSFORM));
        }
        return true;
    }

    protected boolean shouldIgnoreRenumbering(Element element) {
        if (isClonedProposalOrContribution()) {
            return NUM.equals(element.getTagName())
                    && (isSoftAction(element.getParent().getNode(), SoftActionType.MOVE_FROM)
                    || isSoftAction(element.getParent().getNode(), SoftActionType.ADD));
        }
        return false;
    }

    protected boolean isActionRoot(Node node) {
        if (isClonedProposalOrContribution()) {
            return XercesUtils.containsAttributeWithValue(node, LEOS_SOFT_ACTION_ROOT_ATTR, Boolean.TRUE.toString());
        }
        return false;
    }

    protected Node buildNodeForAddedElement(Element newElement, Element oldElement, ContentComparatorContext context) {
        return buildNode(context.getNewElement(), context.getStartTagAttrName(), context.getStartTagAttrValue());
    }

    protected Node getChangedElementContent(Node contentNode, Element element, String attrName, String attrValue) {
        Node node = null;
        if (isClonedProposalOrContribution()) {
            if (!shouldIgnoreElement(element)) {
                node = XercesUtils.getElementById(contentNode, element.getTagId());
                if (node == null) {
                    //LEOS-5691:If getElementById returns null, look for element by Name (Only case with metadata elements)
                    node = XercesUtils.getFirstElementByName(contentNode, element.getTagName());
                }
                // Lists should not be marked as added
                if (attrName != null && attrValue != null && !isList(element)) {
                    XercesUtils.insertOrUpdateAttributeValue(node, attrName, attrValue);
                }
            }
        } else {
            node = element.getNode();
            XercesUtils.insertOrUpdateAttributeValue(node, attrName, attrValue);
        }
        return node;
    }

    protected void appendAddedElementContent(ContentComparatorContext context) {
        if (isClonedProposalOrContribution()) {
            appendAddedElement(context);
        } else {
            Node node = getChangedElementContent(context.getNewElement().getNode(), context.getNewElement(), context.getAttrName(), context.getAddedValue());
            addToResultNode(context, node);
        }
    }

    private void appendAddedElement(ContentComparatorContext context) {
        String newElementTagId = context.getNewElement().getTagId();
        if (Boolean.TRUE.equals(context.getDisplayRemovedContentAsReadOnly()) && !shouldIgnoreElement(context.getNewElement())) {
            if (newElementTagId != null) {
                if (context.getOldContentElements().containsKey(newElementTagId.replace(
                        SOFT_TRANSFORM_PLACEHOLDER_ID_PREFIX, EMPTY_STRING))) {
                    //append the soft movedFrom element content compared to the original content and ignore its renumbering
                    Element transformedElementInOldContent = context.getOldContentElements().get(context.getNewElement().
                            getTagId().replace(SOFT_TRANSFORM_PLACEHOLDER_ID_PREFIX, EMPTY_STRING));
                    compareElementContents(new ContentComparatorContext.Builder(context)
                            .withOldElement(transformedElementInOldContent)
                            .withDisplayRemovedContentAsReadOnly(Boolean.TRUE)
                            .withIgnoreElements(Boolean.TRUE)
                            .withIgnoreRenumbering(Boolean.TRUE)
                            .withStartTagAttrName(context.getAttrName())
                            .withStartTagAttrValue(context.getAddedValue())
                            .build());
                } else if (!newElementTagId.startsWith(SOFT_MOVE_PLACEHOLDER_ID_PREFIX)
                        && !newElementTagId.startsWith(SOFT_DELETE_PLACEHOLDER_ID_PREFIX)
                        && !newElementTagId.startsWith(SOFT_TRANSFORM_PLACEHOLDER_ID_PREFIX)) {
                    compareElementContents(new ContentComparatorContext.Builder(context)
                            .withIndexOfOldElementInNewContent(-1)
                            .withOldElement(null)
                            .withDisplayRemovedContentAsReadOnly(Boolean.TRUE)
                            .withIgnoreElements(Boolean.TRUE)
                            .withIgnoreRenumbering(Boolean.TRUE)
                            .withStartTagAttrName(context.getAttrName())
                            .withStartTagAttrValue(context.getAddedValue())
                            .build());
                }
            } else {
                Node node = getChangedElementContent(context.getNewContentNode(), context.getNewElement(), context.getAttrName(), context.getAddedValue());
                addToResultNode(context, node);
            }
        } else {
            Node node = getNonIgnoredChangedElementContent(context.getNewContentNode(), context.getNewElement(), context.getAttrName(), context.getAddedValue());
            addToResultNode(context, node);
        }
    }

    protected void appendRemovedElementContent(ContentComparatorContext context) {
        if (isClonedProposalOrContribution()) {
            appendSoftRemovedElementContent(context);
        } else if (!isConvertedAlineaToIntro(context.getOldElement(), context.getNewElement())
                && !isConvertedSubparagraphToIntro(context.getOldElement(), context.getNewElement())) {
            Node node = getChangedElementContent(context.getOldElement().getNode(), context.getOldElement(), context.getAttrName(), context.getRemovedValue());
            addToResultNode(context, node);
        }
    }

    protected void appendRemovedContent(ContentComparatorContext context) {
        if (isClonedProposalOrContribution()) {
            if ((isSoftAction(context.getOldElement().getNode(), SoftActionType.ADD)) ||
                    (isSoftAction(context.getOldElement().getNode(), SoftActionType.MOVE_FROM) && !context.getNewContentElements().containsKey(context.getOldElement().getTagId()))) {
                //If element is added in old content but deleted in the new one look for leos:softAction="add" in old element OR
                //If element contains soft action move_from in old content but deleted in new content display the move_from element as deleted
                Node node = getChangedElementContent(context.getOldContentNode(), context.getOldElement(), context.getAttrName(), context.getRemovedValue());
                addReadOnlyAttributes(node);
                addToResultNode(context, node);
            } else if (containsSoftMoveToElement(context.getNewContentElements(), context.getOldElement()) &&
                    containsSoftDeleteElement(context.getOldElement(), context.getNewContentElements())) {
                //If element is soft deleted in new content then print the deleted element from new content
                appendSoftActionPrefix(context, SOFT_MOVE_PLACEHOLDER_ID_PREFIX);
            } else if (containsSoftDeleteElement(context.getOldElement(), context.getNewContentElements())) {
                //If element is soft deleted in new content then print the deleted element from new content
                appendSoftActionPrefix(context, SOFT_DELETE_PLACEHOLDER_ID_PREFIX);
            } else if (context.getOldElement().getTagName().equals(NUM)
                    && context.getOldElement().getParent().getTagId().equals(context.getNewElement().getParent().getTagId())
                    && isElementIndentedInOtherContext(context.getNewContentElements(), context.getOldElement().getParent())) {
                // Removed num on indentation should be marked as "removed"
                String attrValue = getStartTagValueForRemovedElementFromAncestor(context);
                Node node = getChangedElementContent(context.getOldContentNode(), context.getOldElement(), context.getAttrName(), attrValue);
                addToResultNode(context, node);
            }
        } else {
            appendRemovedElementContent(context);
        }
    }

    private void appendSoftActionPrefix(ContentComparatorContext context, String softActionPrefix) {
        Element softDeletedNewElement = context.getNewContentElements().get(softActionPrefix + context.getOldElement().getTagId());
        Node node = softDeletedNewElement.getNode();
        XercesUtils.insertOrUpdateAttributeValue(node, context.getAttrName(), context.getRemovedValue());
        addReadOnlyAttributes(node);
        addToResultNode(context, node);
    }

    private String getStartTagValueForRemovedElementFromAncestor(ContentComparatorContext context) {
        return context.getRemovedValue();
    }

    protected boolean isCurrentElementNonIgnored(Node node) {
        if (isClonedProposalOrContribution()) {
            return isSoftAction(node, SoftActionType.MOVE_FROM);
        }
        return false;
    }

    protected int getIndexOfIgnoredElementInNewContent(ContentComparatorContext context) {
        if (isClonedProposalOrContribution()) {
            Element element = null;
            if (context.getNewContentElements().containsKey(SOFT_DELETE_PLACEHOLDER_ID_PREFIX + context.getOldElement().getTagId())) {
                element = context.getNewContentElements().get(SOFT_DELETE_PLACEHOLDER_ID_PREFIX + context.getOldElement().getTagId());
            } else if (context.getNewContentElements().containsKey(SOFT_MOVE_PLACEHOLDER_ID_PREFIX + context.getOldElement().getTagId())) {
                element = context.getNewContentElements().get(SOFT_MOVE_PLACEHOLDER_ID_PREFIX + context.getOldElement().getTagId());
            }
            return element != null ? context.getNewContentRoot().getChildren().indexOf(element) : -1;
        }
        return -1;
    }

    private void appendSoftRemovedElementContent(ContentComparatorContext context) {
        if (context.getOldElement() == null) {
            return;
        }

        if (Boolean.TRUE.equals(context.getDisplayRemovedContentAsReadOnly()) && !shouldIgnoreElement(context.getOldElement())) {
            if (context.getOldElement().getTagId() != null) {
                if (containsSoftMoveToTransformedElement(context.getNewContentElements(), context.getOldElement())) {
                    //append the soft movedTo element content
                    Element softMovedToTansformedElement = context.getNewContentElements().get(SOFT_MOVE_PLACEHOLDER_ID_PREFIX + SOFT_TRANSFORM_PLACEHOLDER_ID_PREFIX +
                            context.getOldElement().getParent().getTagId());
                    appendMovedOrTransformedContent(context, softMovedToTansformedElement);
                } else if (context.getNewContentElements().containsKey(SOFT_DELETE_PLACEHOLDER_ID_PREFIX + SOFT_TRANSFORM_PLACEHOLDER_ID_PREFIX +
                        context.getOldElement().getParent().getTagId())) {
                    //element was soft deleted, and it's ID was prepended with SOFT_DELETE_PLACEHOLDER_ID_PREFIX + SOFT_TRANSFORM_PLACEHOLDER_ID_PREFIX
                    Element softDeletedTransformedElement = context.getNewContentElements().get(SOFT_DELETE_PLACEHOLDER_ID_PREFIX + SOFT_TRANSFORM_PLACEHOLDER_ID_PREFIX +
                            context.getOldElement().getParent().getTagId());
                    int indexOfSoftDeletedElementInNewContent = getBestMatchInList(softDeletedTransformedElement.getParent().getChildren(),
                            softDeletedTransformedElement);

                    compareElementContents(new ContentComparatorContext.Builder(context)
                            .withIndexOfOldElementInNewContent(indexOfSoftDeletedElementInNewContent)
                            .withNewElement(softDeletedTransformedElement)
                            .withDisplayRemovedContentAsReadOnly(Boolean.TRUE)
                            .withIgnoreElements(Boolean.FALSE)
                            .withIgnoreRenumbering(Boolean.FALSE)
                            .withStartTagAttrName(context.getAttrName())
                            .withStartTagAttrValue(getStartTagValueForRemovedElement(softDeletedTransformedElement, context))
                            .build());
                } else if (containsSoftMoveToElement(context.getNewContentElements(), context.getOldElement())) {
                    //append the soft movedTo element content
                    appendMovedToElementWithoutContent(context);
                } else if (containsSoftDeleteElement(context.getOldElement(), context.getNewContentElements())) {
                    //element was soft deleted, and it's ID was prepended with SOFT_DELETE_PLACEHOLDER_ID_PREFIX
                    Element softDeletedNewElement = context.getNewContentElements().get(SOFT_DELETE_PLACEHOLDER_ID_PREFIX + context.getOldElement().getTagId());
                    int indexOfSoftDeletedElementInNewContent = getBestMatchInList(softDeletedNewElement.getParent().getChildren(), softDeletedNewElement);

                    String oldNum = XercesUtils.getAttributeValue(softDeletedNewElement.getNode(), LEOS_INITIAL_NUM);
                    if (oldNum != null) {
                        Node newNumNode = XercesUtils.getFirstChild(softDeletedNewElement.getNode(), NUM);
                        newNumNode.setTextContent(oldNum);
                    }

                    compareElementContents(new ContentComparatorContext.Builder(context)
                            .withIndexOfOldElementInNewContent(indexOfSoftDeletedElementInNewContent)
                            .withNewElement(softDeletedNewElement)
                            .withDisplayRemovedContentAsReadOnly(Boolean.TRUE)
                            .withIgnoreElements(Boolean.FALSE)
                            .withIgnoreRenumbering(Boolean.FALSE)
                            .withStartTagAttrName(context.getAttrName())
                            .withStartTagAttrValue(getStartTagValueForRemovedElement(softDeletedNewElement, context))
                            .build());
                } else if (!isSoftAction(context.getNewElement().getNode(), SoftActionType.TRANSFORM) && !isSoftAction(context.getOldElement().getNode(), SoftActionType.TRANSFORM)
                        && !context.getNewContentElements().containsKey(context.getOldElement().getTagId()) &&
                        !isElementTransformedFrom(context.getNewContentRoot().getNode(), LEOS_SOFT_TRANS_FROM, context.getOldElement().getTagId())) {
                    //Element is added/present in old content but deleted from new content, so just display the deleted content
                    String attrValue = getStartTagValueForRemovedElementFromAncestor(context);
                    Node node = getChangedElementContent(context.getOldContentNode(), context.getOldElement(), context.getAttrName(), attrValue);
                    addReadOnlyAttributes(node);
                    addToResultNode(context, node);
                }
            } else {
                appendRemovedContent(context);
            }
        } else if (!context.getNewContentElements().containsKey(SOFT_TRANSFORM_PLACEHOLDER_ID_PREFIX + context.getOldElement().getParent().getTagId())
                && !context.getOldElement().getTagId().equals(SOFT_TRANSFORM_PLACEHOLDER_ID_PREFIX + context.getOldElement().getParent().getTagId())
                && (context.getNewContentElements().containsKey(SOFT_DELETE_PLACEHOLDER_ID_PREFIX + SOFT_TRANSFORM_PLACEHOLDER_ID_PREFIX + context.getOldElement().getParent().getTagId())
                || (context.getNewContentElements().containsKey(SOFT_MOVE_PLACEHOLDER_ID_PREFIX + SOFT_TRANSFORM_PLACEHOLDER_ID_PREFIX + context.getOldElement().getParent().getTagId())))) {
            //append the soft movedTo element
            appendMovedToElementWithoutContent(context);
        } else if (context.getOldElement().getTagId().equals(SOFT_TRANSFORM_PLACEHOLDER_ID_PREFIX + context.getOldElement().getParent().getTagId())
                && (containsSoftDeleteElement(context.getOldElement(), context.getNewContentElements())
                || context.getNewContentElements().containsKey(SOFT_MOVE_PLACEHOLDER_ID_PREFIX + context.getOldElement().getTagId()))) {

            Element movedOrDeletedTransformedElement = context.getNewContentElements().get((context.getNewContentElements().containsKey(SOFT_MOVE_PLACEHOLDER_ID_PREFIX + context.getOldElement().getTagId())
                    ? SOFT_MOVE_PLACEHOLDER_ID_PREFIX : SOFT_DELETE_PLACEHOLDER_ID_PREFIX) + context.getOldElement().getTagId());
            String attrValue = getStartTagValueForRemovedElement(movedOrDeletedTransformedElement, context);
            Node node = getNonIgnoredChangedElementContent(context.getNewContentNode(), movedOrDeletedTransformedElement, context.getAttrName(), attrValue);
            addToResultNode(context, node);
        }
    }

    private void appendMovedOrTransformedContent(ContentComparatorContext context, Element element) {
        Node node = XercesUtils.getElementById(context.getNewContentNode(), element.getTagId());
        XercesUtils.insertOrUpdateAttributeValue(node, context.getAttrName(), context.getRemovedValue());
        addReadOnlyAttributes(node);
        addToResultNode(context, node);
    }

    private void appendMovedToElementWithoutContent(ContentComparatorContext context) {
        Element softMovedToElement = context.getNewContentElements().get(SOFT_MOVE_PLACEHOLDER_ID_PREFIX + context.getOldElement().getTagId());
        if (softMovedToElement != null) {
            appendMovedOrTransformedContent(context, softMovedToElement);
        }
    }

    protected String getStartTagValueForRemovedElement(Element newElement, ContentComparatorContext context) {
        if(isClonedProposalOrContribution()) {
            String attrValue;
            if (Boolean.TRUE.equals(context.getThreeWayDiff())) {
                if (context.getIntermediateContentElements() != null && newElement!= null && context.getIntermediateContentElements().get(newElement.getTagId()) == null) {
                    attrValue = context.getRemovedIntermediateValue();
                } else {
                    attrValue = context.getRemovedOriginalValue();
                }
            } else {
                attrValue = context.getRemovedValue();
            }
            return attrValue;
        }
        return context.getRemovedValue();
    }

    protected Node buildNodeForAddedElement(ContentComparatorContext context) {
        return buildNode(context.getNewElement());
    }

    protected Node buildNodeForRemovedElement(Element element) {
        return buildNode(element);
    }

    protected String getRemovedNumContent(ContentComparatorContext context) {
        if (isClonedProposalOrContribution()) {
            return context.getNewElement().getNode().getTextContent();
        } else {
            return (context.getOldElement().getNode()).getTextContent();
        }
    }

    protected boolean shouldBeMarkedAsAdded(ContentComparatorContext context) {
        return !isClonedProposalOrContribution() || (!elementImpactedByIndentation(context)
                && (context.getOldElement() == null
                || isSoftAction(context.getNewElement().getNode(), SoftActionType.MOVE_FROM)
                || isSoftAction(context.getNewElement().getNode(), SoftActionType.ADD)
                || !(context.getOldElement().getTagId().equals(context.getNewElement().getTagId()))));
    }

    protected boolean isElementImpactedByIndention(Map<String, Element> otherContextElements, Element element) {
        return (isElementIndentedInOtherContext(otherContextElements, element)
                || IndentContentComparatorHelper.hasIndentedParent(otherContextElements, element)
                || IndentContentComparatorHelper.hasIndentedChild(otherContextElements, element)
                || IndentContentComparatorHelper.hasIndentedChild(otherContextElements, element.getParent()));
    }
}
