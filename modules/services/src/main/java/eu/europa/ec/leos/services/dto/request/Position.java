package eu.europa.ec.leos.services.dto.request;

public enum Position {
    BEFORE,
    AFTER
}
