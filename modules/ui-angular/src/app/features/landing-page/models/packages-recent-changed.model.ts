export interface PackagesRecentlyChanged {
  ref: string;
  title: string;
  documentId: number;
  packageId: number;
  rank: string;
  greatestLastDate: string;
}
