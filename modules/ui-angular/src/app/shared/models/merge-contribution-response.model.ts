export interface MergeContributionResponse {
  mergeStatus: boolean;
  mergedContent: string;
}

