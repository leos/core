/*
 * Copyright 2024 European Union
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 *     https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package eu.europa.ec.leos.repository.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Collection;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlTransient;

@Entity
@Table(name = "DOCUMENT_PROPERTIES")
@NamedQueries({
    @NamedQuery(name = "DocumentProperties.findAll", query = "SELECT d FROM DocumentProperties d"),
    @NamedQuery(name = "DocumentProperties.findById", query = "SELECT d FROM DocumentProperties d WHERE d.id = :id"),
    @NamedQuery(name = "DocumentProperties.findByPropertyName", query = "SELECT d FROM DocumentProperties d WHERE d.propertyName = :propertyName"),
    @NamedQuery(name = "DocumentProperties.findByAuditCBy", query = "SELECT d FROM DocumentProperties d WHERE d.auditCBy = :auditCBy"),
    @NamedQuery(name = "DocumentProperties.findByAuditCDate", query = "SELECT d FROM DocumentProperties d WHERE d.auditCDate = :auditCDate"),
    @NamedQuery(name = "DocumentProperties.findByAuditLastMBy", query = "SELECT d FROM DocumentProperties d WHERE d.auditLastMBy = :auditLastMBy"),
    @NamedQuery(name = "DocumentProperties.findByAuditLastMDate", query = "SELECT d FROM DocumentProperties d WHERE d.auditLastMDate = :auditLastMDate")})
public class DocumentProperties implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "ID", nullable = false, updatable = false, precision = 22, scale = 0)
    @GeneratedValue(
            strategy = GenerationType.IDENTITY
    )
    private BigDecimal id;
    @Column(name = "PROPERTY_NAME", nullable = false)
    private String propertyName;
    @Column(name = "AUDIT_C_BY", nullable = false, length = 30)
    private String auditCBy;
    @Column(name = "AUDIT_C_DATE", nullable = false)
    private LocalDateTime auditCDate;
    @Column(name = "AUDIT_LAST_M_BY", length = 30)
    private String auditLastMBy;
    @Column(name = "AUDIT_LAST_M_DATE")
    private LocalDateTime auditLastMDate;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "propertyId")
    private Collection<DocumentPropertyValues> documentPropertyValuesCollection;
    @JoinColumn(name = "DOC_CATEGORY_ID", referencedColumnName = "ID")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private DocumentCategories docCategoryId;

    public DocumentProperties() {
    }

    public DocumentProperties(BigDecimal id) {
        this.id = id;
    }

    public DocumentProperties(BigDecimal id, String propertyName, String auditCBy, LocalDateTime auditCDate) {
        this.id = id;
        this.propertyName = propertyName;
        this.auditCBy = auditCBy;
        this.auditCDate = auditCDate;
    }

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public String getPropertyName() {
        return propertyName;
    }

    public void setPropertyName(String propertyName) {
        this.propertyName = propertyName;
    }

    public String getAuditCBy() {
        return auditCBy;
    }

    public void setAuditCBy(String auditCBy) {
        this.auditCBy = auditCBy;
    }

    public LocalDateTime getAuditCDate() {
        return auditCDate;
    }

    public void setAuditCDate(LocalDateTime auditCDate) {
        this.auditCDate = auditCDate;
    }

    public String getAuditLastMBy() {
        return auditLastMBy;
    }

    public void setAuditLastMBy(String auditLastMBy) {
        this.auditLastMBy = auditLastMBy;
    }

    public LocalDateTime getAuditLastMDate() {
        return auditLastMDate;
    }

    public void setAuditLastMDate(LocalDateTime auditLastMDate) {
        this.auditLastMDate = auditLastMDate;
    }

    @XmlTransient
    public Collection<DocumentPropertyValues> getDocumentPropertyValuesCollection() {
        return documentPropertyValuesCollection;
    }

    public void setDocumentPropertyValuesCollection(Collection<DocumentPropertyValues> documentPropertyValuesCollection) {
        this.documentPropertyValuesCollection = documentPropertyValuesCollection;
    }

    public DocumentCategories getDocCategoryId() {
        return docCategoryId;
    }

    public void setDocCategoryId(DocumentCategories docCategoryId) {
        this.docCategoryId = docCategoryId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DocumentProperties)) {
            return false;
        }
        DocumentProperties other = (DocumentProperties) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "eu.europa.ec.leos.repository.DocumentProperties[ id=" + id + " ]";
    }
    
}
