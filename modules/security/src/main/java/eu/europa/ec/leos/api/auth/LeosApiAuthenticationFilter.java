package eu.europa.ec.leos.api.auth;

import com.auth0.jwt.JWT;
import eu.europa.ec.leos.model.user.User;
import eu.europa.ec.leos.security.SecurityUser;
import eu.europa.ec.leos.security.SecurityUserProvider;
import eu.europa.ec.leos.security.TokenService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class LeosApiAuthenticationFilter extends AbstractAuthenticationProcessingFilter {
    
    private static final Logger LOG = LoggerFactory.getLogger(LeosApiAuthenticationFilter.class);
    private static final String AUTHORIZATION = "Authorization";
    private static final String LIGHT_BEARER_PARAMETER = "Client-Context";
    private static final String JSESSIONID = "JSESSIONID";
    private TokenService tokenService;
    private SecurityUserProvider securityUserProvider;
    
    public LeosApiAuthenticationFilter(String filterProcessorUrl, TokenService tokenService,
                                       SecurityUserProvider securityUserProvider) {
        super(filterProcessorUrl);
        this.tokenService = tokenService;
        this.securityUserProvider = securityUserProvider;
    }
    
    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {
        PreAuthenticatedAuthenticationToken preAuthRequest;
        JwtAuthenticationToken authRequest;
        String token;
        String userLogin;
        User user;

        //fetch token from header
        token =  request.getHeader(AUTHORIZATION).substring(7);

        //validate auth headers of request
        validateAuthorizationHeaders(request);

        //authorize request with session id
        authorizeWithSessionId(request, token);

        //validate token
        validateToken(request, token);

        //extract user
        userLogin = tokenService.extractUserFromToken(token);

        //authenticate with loggedin user
        Authentication authentication = validateTokenWithUser(token, userLogin);
        if(authentication != null) {
            return authentication;
        }


        String contextToken = request.getHeader(LIGHT_BEARER_PARAMETER);
        String contextRole = null;
        if(StringUtils.isNotBlank(contextToken)) {
            if (!tokenService.validateClientContextToken(contextToken)) {
                LOG.warn("Authorization failed! Wrong context token");
                throw new LeosApiAuthenticationException("Authorization failed! Wrong context token");
            }
            if(!tokenService.validateUserFromClientContext(contextToken, token)) {
                LOG.warn("Authorization failed! User mismatch in access token & context token");
                throw new LeosApiAuthenticationException("Authorization failed! User mismatch in access token & context token");
            }
            contextRole = tokenService.extractUserRoleFromToken(contextToken);
        }

        user = securityUserProvider.getUserByLogin(userLogin);

        if(user == null) {
            throw new LeosApiAuthenticationException("The provided user login cannot be validated: user login not found");
        }

        preAuthRequest = new PreAuthenticatedAuthenticationToken(user,"", getAuthorities(user, contextRole));
        preAuthRequest.setAuthenticated(true);
        return getAuthenticationManager().authenticate(preAuthRequest);
    }

    private Authentication validateTokenWithUser(String token, String userLogin) {
        JwtAuthenticationToken authRequest;
        if (StringUtils.isEmpty(userLogin)) {
            List<String> listAccessTokens = tokenService.getAccessTokenList();
            if (!listAccessTokens.isEmpty() && listAccessTokens.contains(token)) {
                throw new LeosApiAuthenticationException("Unauthorized token!");
            } else {
                tokenService.setAccessTokenInList(token);
            }
            authRequest = new JwtAuthenticationToken();
            authRequest.setAuthenticated(true);
            return getAuthenticationManager().authenticate(authRequest);
        }
        return null;
    }

    private void validateToken(HttpServletRequest request, String token) {
        if (!tokenService.validateAccessToken(token)) {
            LOG.warn("Authorization failed! Wrong access token");
            try {
                String claims = JWT.decode(token).getClaims().entrySet().stream()
                        .map(e -> e.getKey() + ": " + e.getValue().asString())
                        .collect(Collectors.joining(","));
                LOG.warn("Token details:\n" +
                                "Method: {}\n" +
                                "Path: {}\n" +
                                "Claims: {}",
                        request.getMethod(),
                        request.getContextPath() + request.getServletPath() + request.getPathInfo(),
                        claims
                );
            } catch (Exception e) {
            }
            throw new LeosApiAuthenticationException("Authorization failed! Wrong access token");
        }
    }

    private void authorizeWithSessionId(HttpServletRequest request, String token) {
        Map<String, String> accessTokenMap = tokenService.getAccessTokenSessionMap();
        String sessionId = accessTokenMap != null ? accessTokenMap.get(token) : null;
        if (sessionId != null) {
            Cookie[] cookieArray = request.getCookies();
            Cookie sessionIdCookie = cookieArray != null ? Arrays.stream(request.getCookies())
                    .filter(cookie -> JSESSIONID.equals(cookie.getName()))
                    .findAny().orElse(null) : null;
            if (sessionIdCookie == null || !sessionId.equals(sessionIdCookie.getValue())) {
                throw new LeosApiAuthenticationException("Unauthorized token!");
            }
        }
    }

    private void validateAuthorizationHeaders(HttpServletRequest request) {
        if (request.getHeader(AUTHORIZATION) == null || !request.getHeader(AUTHORIZATION).startsWith("Bearer ")
                || request.getHeader(AUTHORIZATION).startsWith("Bearer undefined")) {
            LOG.warn("Authorization failed! Wrong headers: '{}' is missing or contains a wrong value", AUTHORIZATION);
            throw new LeosApiAuthenticationException("Authorization failed! Wrong headers: " +
                    "Authorization is missing or contains a wrong value");
        }
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authResult)
            throws IOException, ServletException {
        super.successfulAuthentication(request, response, chain, authResult);
        chain.doFilter(request, response);
    }

    private Collection<GrantedAuthority> getAuthorities(User user, String role) {
        List<GrantedAuthority> allRoles = new ArrayList<>();
        if(StringUtils.isNotBlank(role)) {
            allRoles.add(new SimpleGrantedAuthority(role));
        } else if(user instanceof SecurityUser) {
            List<String> leosRoles = ((SecurityUser) user).getRoles();
            leosRoles.forEach(auth -> allRoles.add(new SimpleGrantedAuthority(auth)));
        }
        return allRoles;
    }
}
