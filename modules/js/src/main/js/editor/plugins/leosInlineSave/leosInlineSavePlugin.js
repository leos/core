/*
 * Copyright 2024 European Union
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 *     https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
; // jshint ignore:line
define(function leosInlineSavePluginModule(require) {
    "use strict";

    // load module dependencies
    var pluginTools = require("plugins/pluginTools");

    var pluginName = "leosInlineSave";
    var TRISTATE_DISABLED = CKEDITOR.TRISTATE_DISABLED, TRISTATE_OFF = CKEDITOR.TRISTATE_OFF;
    var SAVE_CMD_NAME = "inlinesave";
    var SAVE_CLOSE_CMD_NAME = "inlinesaveclose";
    var iconSaveClose =  'icons/leosinlinesaveclose.png';
    var iconSave = 'icons/leosinlinesave.png';

    function blockSaveWithoutClose(event, saveCommand) {
        if (event.editor.LEOS.profile.name === 'Inline AKN Paragraph'
            && event.editor.element.find('p[data-akn-element="paragraph"]').count() > 1) {
            saveCommand.setState(TRISTATE_DISABLED);
        }
    }

    var pluginDefinition = {
        icons: pluginName.toLowerCase(),

        init: function(editor) {
            var indented = false;

            editor.ui.addButton('leosInlineSave', {
                label: 'Save',
                command: SAVE_CMD_NAME,
                toolbar: 'save',
                icon: this.path + iconSave,
            });

            editor.ui.addButton('leosInlineSaveClose', {
                label: 'Save Close',
                command: SAVE_CLOSE_CMD_NAME,
                toolbar: 'save',
                icon: this.path + iconSaveClose,
            });

            var saveCommand = editor.addCommand(SAVE_CMD_NAME, {
                exec: function(editor) {
                    if (this.state != TRISTATE_DISABLED) {
                        if (editor.fire("canBeSaved")) {
                            editor.fire("save", {
                                data: editor.getData(),
                            });
                        }
                    }
                }
            });

            var saveCloseCommand = editor.addCommand(SAVE_CLOSE_CMD_NAME, {
                exec: function(editor) {
                    if (this.state != TRISTATE_DISABLED) {
                        if (editor.fire("canBeSaved")) {
                            editor.fire("save", {
                                data: editor.getData(),
                                isSaveAndClose: true
                            });
                            editor.once("receiveData", _doClose());
                        }
                    }
                }
            });

            function _doClose() {
                editor.fire("close");
            }

            editor.on('change', function(event) {
                if (event.editor.checkDirty()) {
                    saveCommand.setState(indented ? TRISTATE_DISABLED : TRISTATE_OFF);
                    saveCloseCommand.setState(TRISTATE_OFF);
                    blockSaveWithoutClose(event, saveCommand);
                }
            });

            editor.on('indent', function(event) {
                indented = true;
            });

            editor.on('resetIndent', function(event) {
                indented = false;
            });

            editor.on('focus', function(event) {
                saveCommand.setState(editor.checkDirty() && !indented ? TRISTATE_OFF : TRISTATE_DISABLED);
                saveCloseCommand.setState(editor.checkDirty() ? TRISTATE_OFF : TRISTATE_DISABLED);
                blockSaveWithoutClose(event, saveCommand);
            }, null, null, 100); //listen to the event as late as possible
            
            // dataReady is fired after setData is called.
            editor.on('dataReady', function(event) {
                saveCommand.setState(editor.checkDirty() ? TRISTATE_OFF : TRISTATE_DISABLED);
                saveCloseCommand.setState(editor.checkDirty() ? TRISTATE_OFF : TRISTATE_DISABLED);
                blockSaveWithoutClose(event, saveCommand);
            }, null, null, 100); //listen to the event as late as possible

            // instanceReady is fired after everything is ready.
            // This is a workaround to detect actual changes because after
            // initialization, the last handled event above happens while for
            // some reason checkDirty() returns true. The selection plugin fixes
            // this by overriding getSnapshot() in the beginning of
            // `instanceReady`.
            editor.once('instanceReady', function(event) {
                saveCommand.setState(editor.checkDirty() ? TRISTATE_OFF : TRISTATE_DISABLED);
                saveCloseCommand.setState(editor.checkDirty() ? TRISTATE_OFF : TRISTATE_DISABLED);
                blockSaveWithoutClose(event, saveCommand);
            }, null, null, 100); //listen to the event as late as possible
        }
    };
    
    pluginTools.addPlugin(pluginName, pluginDefinition);

    // return plugin module
    var pluginModule = {
        name: pluginName
    };

    return pluginModule;
});