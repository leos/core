/*
 * Copyright 2024 European Union
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 *     https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */

package eu.europa.ec.leos.services.controllers;

import eu.europa.ec.leos.services.api.TocApiService;
import eu.europa.ec.leos.services.dto.request.NodeDropValidationRequest;
import eu.europa.ec.leos.services.dto.request.TocValidationRequest;
import eu.europa.ec.leos.services.dto.response.NodeValidationResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/secured/toc/")
public class TableOfContentController {
    private static final Logger LOG = LoggerFactory.getLogger(TableOfContentController.class);

    @Autowired
    TocApiService tableOfContentService;

    @PostMapping(value = "/validate-node-drop", produces = MediaType.APPLICATION_JSON_VALUE )
    @ResponseBody
    public ResponseEntity<Object> validateNodeDrop(@RequestBody() NodeDropValidationRequest nodeValidationRequest) {
        try {
            NodeValidationResponse response = this.tableOfContentService.nodeValidationDrop(nodeValidationRequest);
            return ResponseEntity.ok().body(response);
        } catch (Exception e) {
            LOG.error("Error occurred while getting document toc validation - {}", e);
            return new ResponseEntity<>("Unexpected error occurred while getting document toc validation", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(value = "/validate-toc", produces = MediaType.APPLICATION_JSON_VALUE )
    @ResponseBody
    public ResponseEntity<Object> validateToc(@RequestBody() TocValidationRequest tocValidationRequest) {
        try {
            NodeValidationResponse response = this.tableOfContentService.validateToc(tocValidationRequest);
            return ResponseEntity.ok().body(response);
        } catch (Exception e) {
            LOG.error("Error occurred while getting document toc validation - {}", e);
            return new ResponseEntity<>("Unexpected error occurred while getting document toc validation", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
