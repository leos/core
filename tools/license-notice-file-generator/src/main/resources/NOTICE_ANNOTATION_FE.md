Copyright 2012, 2024 European Union

Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");<br/>
You may not use this work except in compliance with the Licence.<br/>
You may obtain a copy of the Licence at:<br/>

&ensp;&ensp;&ensp;&ensp;[https://joinup.ec.europa.eu/software/page/eupl](https://joinup.ec.europa.eu/software/page/eupl)

Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,<br/>WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.<br/>See the Licence for the specific language governing permissions and limitations under the Licence.

This product includes dynamically linked software developed by third parties which is provided under their respective licences:

__@socket.io/component-emitter 3.1.0__
 * https://github.com/socketio/emitter
 * License: MIT
 * Copyright:
   * Copyright (c) 2014 Component

__@types/cookie 0.4.1__
 * https://github.com/DefinitelyTyped/DefinitelyTyped
 * License: MIT
 * Copyright:
   * Copyright (c) Microsoft Corporation

__@types/cors 2.8.13__
 * https://github.com/DefinitelyTyped/DefinitelyTyped
 * License: MIT
 * Copyright:
   * Copyright (c) Microsoft Corporation

__@types/node 18.14.2__
 * https://github.com/DefinitelyTyped/DefinitelyTyped
 * License: MIT
 * Copyright:
   * Copyright (c) Microsoft Corporation.

__abbrev 1.1.1__
 * https://github.com/isaacs/abbrev-js
 * License: ISC
 * Copyright:
   * Copyright (c) Isaac Z. Schlueter and Contributors

__accepts 1.3.8__
 * https://github.com/jshttp/accepts
 * License: MIT
 * Copyright:
   * Copyright (c) 2015 Douglas Christopher Wilson <doug@somethingdoug.com>
   * Copyright (c) 2014 Jonathan Ong <me@jongleberry.com>

__accord 0.26.4__
 * https://github.com/jenius/accord
 * License: MIT
 * Copyright:
   * Copyright (c) 2013 Jeff Escalante

__align-text 0.1.4__
 * https://github.com/jonschlinkert/align-text
 * License: MIT
 * Copyright:
   * Copyright (c) 2015, Jon Schlinkert.
   * Copyright (c) 2015 Jon Schlinkert (https://github.com/jonschlinkert)

__amdefine 1.0.1__
 * https://github.com/jrburke/amdefine
 * License: MIT
 * Copyright:
   * Copyright (c) 2011-2016, The Dojo Foundation

__angular-multiple-select 1.1.3__
 * https://github.com/jagdeep-singh/angularMultipleSelect
 * License: MIT
 * Copyright:
   * Copyright (c) 2015 Jagdeep Singh

__annotate-client 1.84.0__
 * https://github.com/hypothesis/client
 * License: BSD-2-Clause
 * Copyright:
   * For the annotator subcomponent: Copyright 2012 Aron Carroll, Rufus Pollock, and Nick Stenning.
   * Copyright (c) 2013-2019 Hypothes.is Project and contributors

__ansi-cyan 0.1.1__
 * https://github.com/jonschlinkert/ansi-cyan
 * License: MIT
 * Copyright:
   * Copyright (c) 2015 Jon Schlinkert

__ansi-gray 0.1.1__
 * https://github.com/jonschlinkert/ansi-gray
 * License: MIT
 * Copyright:
   * Copyright (c) 2015 Jon Schlinkert

__ansi-red 0.1.1__
 * https://github.com/jonschlinkert/ansi-red
 * License: MIT
 * Copyright:
   * Copyright (c) 2015 Jon Schlinkert

__ansi-regex 5.0.1__
 * https://github.com/chalk/ansi-regex
 * License: MIT
 * Copyright:
   * Copyright (c) Sindre Sorhus <sindresorhus@gmail.com> (sindresorhus.com)

__ansi-styles 4.3.0__
 * https://github.com/chalk/ansi-styles
 * License: MIT
 * Copyright:
   * Copyright (c) Sindre Sorhus <sindresorhus@gmail.com> (sindresorhus.com)

__ansi-wrap 0.1.0__
 * https://github.com/jonschlinkert/ansi-wrap
 * License: MIT
 * Copyright:
   * Copyright (c) 2015 Jon Schlinkert

__anymatch 3.1.3__
 * https://github.com/micromatch/anymatch
 * License: ISC
 * Copyright:
   * Copyright (c) 2019 Elan Shanker, Paul Miller (https://paulmillr.com)

__append-buffer 1.0.2__
 * https://github.com/doowb/append-buffer
 * License: MIT
 * Copyright:
   * Copyright (c) 2017, Brian Woodward.
   * Copyright (c) 2017, Brian Woodward (https://doowb.com).

__archy 1.0.0__
 * https://github.com/substack/node-archy
 * License: MIT
 * Copyright:
   * James Halliday (mail@substack.net)

__arr-diff 4.0.0__
 * https://github.com/jonschlinkert/arr-diff
 * License: MIT
 * Copyright:
   * Copyright (c) 2014-2017, Jon Schlinkert
   * Copyright (c) 2017, Jon Schlinkert (https://github.com/jonschlinkert)

__arr-flatten 1.1.0__
 * https://github.com/jonschlinkert/arr-flatten
 * License: MIT
 * Copyright:
   * Copyright (c) 2014-2017, Jon Schlinkert
   * Copyright (c) 2017, Jon Schlinkert (https://github.com/jonschlinkert)

__arr-union 3.1.0__
 * https://github.com/jonschlinkert/arr-union
 * License: MIT
 * Copyright:
   * Copyright (c) 2014-2016, Jon Schlinkert
   * Copyright (c) 2016 Jon Schlinkert (https://github.com/jonschlinkert)

__array-differ 1.0.0__
 * https://github.com/sindresorhus/array-differ
 * License: MIT
 * Copyright:
   * Copyright (c) Sindre Sorhus (http://sindresorhus.com)

__array-each 1.0.1__
 * https://github.com/jonschlinkert/array-each
 * License: MIT
 * Copyright:
   * Copyright (c) 2017, Jon Schlinkert (https://github.com/jonschlinkert).
   * Copyright (c) 2015, 2017, Jon Schlinkert.

__array-find-index 1.0.2__
 * https://github.com/sindresorhus/array-find-index
 * License: MIT
 * Copyright:
   * Copyright (c) Sindre Sorhus <sindresorhus@gmail.com> (sindresorhus.com)

__array-slice 1.1.0__
 * https://github.com/jonschlinkert/array-slice
 * License: MIT
 * Copyright:
   * Copyright (c) 2014-2017, Jon Schlinkert

__array-union 1.0.2__
 * https://github.com/sindresorhus/array-union
 * License: MIT
 * Copyright:
   * Copyright (c) Sindre Sorhus <sindresorhus@gmail.com> (sindresorhus.com)

__array-uniq 1.0.3__
 * https://github.com/sindresorhus/array-uniq
 * License: MIT
 * Copyright:
   * Copyright (c) Sindre Sorhus <sindresorhus@gmail.com> (sindresorhus.com)

__array-unique 0.3.2__
 * https://github.com/jonschlinkert/array-unique
 * License: MIT
 * Copyright:
   * Copyright (c) 2014-2016, Jon Schlinkert

__arrify 1.0.1__
 * https://github.com/sindresorhus/arrify
 * License: MIT
 * Copyright:
   * Copyright (c) Sindre Sorhus <sindresorhus@gmail.com> (sindresorhus.com)

__asap 2.0.6__
 * https://github.com/kriskowal/asap
 * License: MIT
 * Copyright:
   * Copyright 2009-2014 Contributors

__assign-symbols 1.0.0__
 * https://github.com/jonschlinkert/assign-symbols
 * License: MIT
 * Copyright:
   * Copyright (c) 2015 Jon Schlinkert

__async-each-series 0.1.1__
 * https://github.com/jb55/async-each-series
 * License: MIT
 * Copyright:
   * Copyright (c) 2014 William Casarin

__async 2.6.4__
 * https://github.com/caolan/async
 * License: MIT
 * Copyright:
   * Copyright (c) 2010-2018 Caolan McMahon

__atob 2.1.2__
 * git://git.coolaj86.com/coolaj86/atob.js
 * License: Apache-2.0
 * Copyright:
   * Copyright 2015 AJ ONeal
   * copyright 2012-2018 AJ ONeal

__axios 0.21.4__
 * https://github.com/axios/axios
 * License: MIT
 * Copyright:
   * Copyright (c) 2014-present Matt Zabriskie

__balanced-match 1.0.2__
 * https://github.com/juliangruber/balanced-match
 * License: MIT
 * Copyright:
   * Copyright (c) 2013 Julian Gruber <julian@juliangruber.com>

__base64id 2.0.0__
 * https://github.com/faeldt/base64id
 * License: MIT
 * Copyright:
   * Copyright (c) 2012-2016 Kristian Faeldt <faeldt_kristian@cyberagent.co.jp>

__base 0.11.2__
 * https://github.com/node-base/base
 * License: MIT
 * Copyright:
   * Copyright (c) 2015-2017, Jon Schlinkert
   * Copyright (c) 2017, Jon Schlinkert (https://github.com/jonschlinkert)

__batch 0.6.1__
 * https://github.com/visionmedia/batch
 * License: MIT
 * Copyright:
   * Copyright (c) 2013 TJ Holowaychuk <tj@vision-media.ca>

__beeper 1.1.1__
 * https://github.com/sindresorhus/beeper
 * License: MIT
 * Copyright:
   * (c) Sindre Sorhus (http://sindresorhus.com)
   * Copyright (c) Sindre Sorhus <sindresorhus@gmail.com> (sindresorhus.com)

__binary-extensions 2.2.0__
 * https://github.com/sindresorhus/binary-extensions
 * License: MIT
 * Copyright:
   * Copyright (c) 2019 Sindre Sorhus <sindresorhus@gmail.com> (https://sindresorhus.com), Paul
            Miller (https://paulmillr.com)

__body-parser 1.14.2__
 * https://github.com/expressjs/body-parser
 * License: MIT
 * Copyright:
   * Copyright (c) 2014-2015 Douglas Christopher Wilson <doug@somethingdoug.com>
   * Copyright (c) 2014 Jonathan Ong <me@jongleberry.com>

__bower 1.8.14__
 * https://github.com/bower/bower
 * License: MIT
 * Copyright:
   * Copyright (c) 2013-2016 Kurento
   * Copyright (c) Kevin Martensson <kevinmartensson@gmail.com>
   * (c) 2013 Mikola Lysenko
   * Copyright (c) 2015 Rod Vagg rvagg (https://twitter.com/rvagg)
   * Copyright (c) 2011 Dominic Tarr
   * Copyright (c) 2012-present Twitter and other contributors (https://github.com/bower/bower/graphs/contributors)
   * Copyright (c) 2013-2018, Viacheslav Lotsmanov
   * Copyright (c) 2012, 2013 moutjs team and contributors (http://moutjs.com)
   * Copyright (c) 2007 Kris Zyp SitePen (www.sitepen.com)
   * Copyright (c) 2015 Rod Vagg
   * Copyright (c) 2012 Michael Hart (michael.hart.au@gmail.com)
   * Copyright 2016, Joyent Inc
   * Copyright 2016 Joyent, Inc.
   * copyright 2009-2015 Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors <http://underscorejs.org/>
   * Copyright (c) 2014 Mathias Buus
   * Copyright (c) 2013-2018 Viacheslav Lotsmanov
   * Copyright (c) 2005 Tom Wu
   * Copyright (c) Isaac Z. Schlueter
   * Copyright (c) 2014 Simon Boudrias
   * Copyright 2009-2011 Mozilla Foundation and contributors
   * Copyright 2009-2010, Rex Dieter <rdieter@fedoraproject.org>
   * Copyright (c) 2010-2012 Robert Kieffer
   * (c) Copyright 2013-2015 Kurento (http://kurento.org/)
   * Copyright 2012 Thorsten Lorenz
   * Copyright (c) 2016, Joyent, Inc.
   * Copyright (c) 2011 Arpad Borsos <arpad.borsos@googlemail.com>
   * Copyright Mathias Bynens <https://mathiasbynens.be/>
   * Copyright (c) 2012, Mark Cavage
   * (c) Kevin Martensson (https://github.com/kevva)
   * Copyright (c) 2015, Ilya Radchenko <ilya@burstcreations.com>
   * Copyright (c) 2011, Yahoo Inc.
   * Copyright (c) 2011-2014, Walmart and other contributors
   * Copyright (c) 2014 Nathan LaFreniere and other contributors
   * Copyright 2013 Tomas Aparicio
   * Copyright 2010 LearnBoost <dev@learnboost.com>
   * copyright (c) 2013 Jake Luer, jake@alogicalparadox.com
   * Copyright 2011 Mark Cavage <mcavage@gmail.com>
   * Copyright (c) 2015, Salesforce.com, Inc.
   * (c) Javier Blanco (http://jbgutierrez.info)
   * Copyright (c) 2012 Kris Kowal <kris.kowal@cixar.com>
   * Copyright 2012-2019 (c) Mihai Bazon <mihai.bazon@gmail.com>
   * Copyright 2009-2015 Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors
   * Copyright 2009, 2010, 2011 Isaac Z. Schlueter
   * Copyright 2009-2012 Kris Kowal
   * Copyright (c) 2012-2014, Walmart and other contributors
   * Copyright (c) 2016 Espen Hovlandsdal
   * Copyright (c) Isaac Z. Schlueter and Contributors
   * Copyright (c) 2011-2015 Jan Lehnardt <jan@apache.org> & Marc Bachmann <https://github.com/marcbachmann>
   * Copyright 2016 Trent Mick
   * Copyright (c) 2014 Jeremie Miller
   * Copyright (c) 2012-present Twitter and other contributors
   * Copyright 2011 Mozilla Foundation and contributors
   * Copyright (c) 2013 James Halliday (mail@substack.net)
   * Copyright 1997 Niels Provos <provos@physnet.uni-hamburg.de>
   * Copyright (c) 2011 Debuggable Limited <felix@debuggable.com>
   * Copyright (c) 2009-2013, Jeff Mott
   * Copyright (c) 2013, Deoxxa Development
   * Copyright 2009-2017 Kristopher Michael Kowal and contributors
   * Copyright (c) 2003-2005 Tom Wu
   * Copyright 2009-2017 Kristopher Michael Kowal
   * Copyright (c) 2014 Jon Schlinkert
   * Copyright (c) 2012, 2011 Ariya Hidayat (http://ariya.ofilabs.com/about) and other contributors
   * Copyright 2017 Joyent, Inc.
   * Copyright (c) 2016 Alex Indigo
   * Copyright (c) 2011 Yusuke Suzuki <utatane.tea@gmail.com>
   * Copyright (c) 2014 Jonathan Ong <me@jongleberry.com>
   * Copyright (c) 2013-2014 bl contributors
   * Copyright (c) 2005-2009 Tom Wu
   * Copyright (c) 2010 Benjamin Thomas, Robert Kieffer
   * Copyright (c) 2013 Gary Court, Jens Taylor
   * Copyright 2010 James Halliday (mail@substack.net)
   * Copyright (c) 2012 Jonathan Rajavuori
   * Copyright 2009-2013 Kristopher Michael Kowal
   * Copyright (c) 2011 TJ Holowaychuk <tj@vision-media.ca>
   * Copyright 2009-2012 Kristopher Michael Kowal
   * Copyright (c) 2012 Ariya Hidayat <ariya.hidayat@gmail.com>
   * Copyright (c) 2014 Jonathan Ong me@jongleberry.com
   * Copyright 2013 Michael Hart (michael.hart.au@gmail.com)
   * Copyright (c) 2015 Douglas Christopher Wilson <doug@somethingdoug.com>
   * Copyright 2015 Joyent, Inc.
   * Copyright 2009-2017 Kris Kowal
   * (c) Ahmad Nassri (https://www.ahmadnassri.com/)
   * Copyright (c) 2013 Julian Gruber <julian@juliangruber.com>
   * Copyright (c) Sindre Sorhus <sindresorhus@gmail.com> (sindresorhus.com)
   * Copyright (c) 2012 Felix Geisendorfer (felix@debuggable.com) and contributors
   * Copyright (c) 2011 Google Inc.
   * Copyright (c) 2014 Matt Lavin <matt.lavin@gmail.com>
   * Copyright (c) 2015, Ahmad Nassri <ahmad@ahmadnassri.com>
   * Copyright (c) 2015 JD Ballard
   * Copyright (c) Isaac Z. Schlueter, Ben Noordhuis, and Contributors
   * Copyright (c) 2014 Trent Millar
   * Copyright 2012 Cloud9 IDE, Inc.
   * Copyright (c) 2014 KARASZI Istvan
   * Copyright (c) Kevin Martensson
   * Copyright (c) 2011-2015 KARASZI Istvan <github@spam.raszi.hu>
   * Copyright (c) 2013-2016 bl contributors
   * Copyright (c) 2013, Dominic Tarr
   * Copyright (c) Sindre Sorhus <sindresorhus@gmail.com>
   * Copyright (c) 2012, Joyent, Inc.
   * Copyright (c) 2013 Trent Mick
   * Copyright (c) 2012 Yusuke Suzuki <utatane.tea@gmail.com>
   * Copyright 2018 Joyent, Inc.
   * (c) Sindre Sorhus (https://sindresorhus.com)
   * (c) Sindre Sorhus (http://sindresorhus.com)
   * Copyright (c) 2012 Arpad Borsos <arpad.borsos@googlemail.com>
   * Copyright (c) 2012 Twitter and other contributors
   * Copyright (c) 2015, Rebecca Turner
   * (c) Vsevolod Strukchinsky (floatdrop@gmail.com)
   * Copyright (c) 2014 Nadav Ivgi
   * Copyright (c) 2014 Nathan Rajlich <nathan@tootallnate.net>
   * Copyright (c) 2012 Joost-Wim Boekesteijn <joost-wim@boekesteijn.nl>
   * Copyright (c) 2015 Evgeny Poberezkin
   * Copyright 2010-2012 Mikeal Rogers
   * Copyright (c) 2013 Mikola Lysenko
   * Copyright (c) 2015 Douglas Christopher Wilson
   * Copyright (c) 2015 Calvin Metcalf
   * Copyright (c) 2013 Cowboy Ben Alman
   * (c) Chris Veness 2002-2016
   * Copyright (c) 2011 Joyent, Inc. and the persons identified as document authors
   * Copyright (c) 2014 Stefan Thomas
   * Copyright (c) Microsoft Open Technologies, Inc.
   * Copyright Joyent, Inc.
   * Copyright (c) 2011 Mark Cavage
   * Copyright (c) 2013-present Twitter and other contributors
   * Copyright (c) 2012 Simon Boudrias
   * Copyright (c) 2012 Mark Cavage
   * (c) 2011-2015 Jan Lehnardt <jan@apache.org> & Marc Bachmann <https://github.com/marcbachmann>
   * Copyright 2011 The Closure Compiler Authors
   * copyright ahmadnassri.com (https://www.ahmadnassri.com/)
   * Copyright (c) 2013 Ted Unangst <tedu@openbsd.org>
   * Copyright (c) 2013 Meryn Stol
   * Copyright (c) 2012-2014 Raynos
   * Copyright (c) 2014 Jonathan Ong
   * Copyright (c) 2010-2016 Robert Kieffer and other contributors
   * Copyright (c) 2014-2018 Suguru Motegi
   * Copyright (c) 2012-2014, Eran Hammer and other contributors
   * Copyright 2012-2015 The Dojo Foundation <http://dojofoundation.org/>
   * Copyright (c) 2012 Simon Boudrias (twitter vaxilart (https://twitter.com/Vaxilart))
   * Copyright (c) 2011 Ariya Hidayat <ariya.hidayat@gmail.com>
   * Copyright (c) 2011-2017 by Yehuda Katz
   * Copyright Caolan McMahon
   * Copyright (c) 2012 Cloud9 IDE, Inc.
   * (c) Copyright 2014 Kurento (http://kurento.org/)
   * Copyright (c) Feross Aboukhadijeh
   * Copyright Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors
   * Copyright (c) 2009-2011, Mozilla Foundation and contributors
   * Copyright 2014 Mozilla Foundation and contributors
   * copyright Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors <http://underscorejs.org/>
   * Copyright (c) 2018 Nikita Skovoroda <chalkerx@gmail.com>
   * Copyright (c) 1986-1993, 1998, 2004, 2007-2010 Thomas Williams, Colin Kelley
   * Copyright 2012 (c) Mihai Bazon <mihai.bazon@gmail.com>
   * Copyright (c) 2012 IndigoUnited
   * Copyright (c) 2010-2017 Caolan McMahon
   * Copyright (c) 2012-2014, Eran Hammer <eran@hammer.io>
   * Copyright JS Foundation and other contributors <https://js.foundation/>
   * (c) Bower team
   * Copyright (c) 2013 Joyent Inc.
   * Copyright 2006, Jeremy White <jwhite@codeweavers.com>
   * Copyright 2009-2010, Fathi Boudra <fabo@freedesktop.org>
   * Copyright Joyent, Inc. and other Node contributors
   * Copyright 2006, Kevin Krammer <kevin.krammer@gmx.at>
   * Copyright (c) Feross Aboukhadijeh (http://feross.org)
   * Copyright (c) 2014 bl contributors
   * (c) Vsevolod Strukchinsky (http://github.com/floatdrop)
   * Copyright (c) 2011 Tim Koschutzki (tim@debuggable.com) Felix Geisendorfer (felix@debuggable.com)
   * Copyright (c) 2010 Caolan McMahon
   * (c) Sindre Sorhus
   * Copyright 2013 Thorsten Lorenz
   * Copyright Isaac Z. Schlueter
   * Copyright 2007-2009 Tyler Close
   * Copyright (c) 2014 TJ Holowaychuk <tj@vision-media.ca>
   * Copyright (c) 2014, Eran Hammer and other contributors
   * Copyright (c) Vsevolod Strukchinsky <floatdrop@gmail.com>
   * Copyright (c) Isaac Z. Schlueter 'Author
   * Copyright (c) 2012 Mathias Bynens <mathias@qiwi.be>
   * Copyright 2012 Joyent, Inc.
   * Copyright 2016, Joyent, Inc.

__brace-expansion 1.1.11__
 * https://github.com/juliangruber/brace-expansion
 * License: MIT
 * Copyright:
   * Copyright (c) 2013 Julian Gruber <julian@juliangruber.com>

__braces 3.0.2__
 * https://github.com/micromatch/braces
 * License: MIT
 * Copyright:
   * Copyright (c) 2014-2018, Jon Schlinkert
   * Copyright (c) 2019, Jon Schlinkert (https://github.com/jonschlinkert)

__browser-sync-client 2.28.1__
 * https://github.com/shakyshane/browser-sync-client
 * License: ISC
 * Copyright:
   * Copyright 2015 Shane Osbourne
   * Copyright (c) 2014 Shane Osbourne

__browser-sync-ui 2.28.1__
 * https://github.com/BrowserSync/UI
 * License: Apache-2.0
 * Copyright:
   * Copyright (c) 2016 Shane Osbourne
   * (c) 2021 - https://browsersync.io' Browsersync.io
   * Copyright 2015 Shane Osbourne
   * (c) 2010-2020 Google LLC. http://angularjs.org

__browser-sync 2.28.1__
 * https://github.com/BrowserSync/browser-sync
 * License: Apache-2.0
 * Copyright:
   * Copyright JS Foundation and other contributors <https://js.foundation/>
   * Copyright Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors
   * Copyright 2015 Shane Osbourne
   * Copyright (c) 2021 Shane Osbourne

__bs-recipes 1.3.4__
 * https://github.com/BrowserSync/recipes
 * License: ISC
 * Copyright:
   * (c) 2016 Jade Gu

__bs-snippet-injector 2.0.1__
 * https://github.com/shakyShane/bs-snippet-injector
 * License: MIT
 * Copyright:
   * Shane Osbourn

__buffer-equal 1.0.1__
 * https://github.com/inspect-js/buffer-equal
 * License: MIT
 * Copyright:
   * Copyright (c) 2012 James Halliday and contributors

__bufferutil 4.0.7__
 * https://github.com/websockets/bufferutil
 * License: MIT
 * Copyright:
   * Copyright (c) 2011 Einar Otto Stangvik <einaros@gmail.com> (http://2x.io)

__bytes 3.1.2__
 * https://github.com/visionmedia/bytes.js
 * License: MIT
 * Copyright:
   * Copyright (c) 2015 Jed Watson <jed.watson@me.com>
   * Copyright (c) 2012-2014 TJ Holowaychuk <tj@vision-media.ca>

__cache-base 1.0.1__
 * https://github.com/jonschlinkert/cache-base
 * License: MIT
 * Copyright:
   * Copyright (c) 2014-2017, Jon Schlinkert
   * Copyright (c) 2017, Jon Schlinkert (https://github.com/jonschlinkert)

__call-bind 1.0.2__
 * https://github.com/ljharb/call-bind
 * License: MIT
 * Copyright:
   * Copyright (c) 2020 Jordan Harband

__camelcase-keys 2.1.0__
 * https://github.com/sindresorhus/camelcase-keys
 * License: MIT
 * Copyright:
   * Copyright (c) Sindre Sorhus <sindresorhus@gmail.com> (sindresorhus.com)

__camelcase 2.1.1__
 * https://github.com/sindresorhus/camelcase
 * License: MIT
 * Copyright:
   * Copyright (c) Sindre Sorhus <sindresorhus@gmail.com> (sindresorhus.com)

__center-align 0.1.3__
 * https://github.com/jonschlinkert/center-align
 * License: MIT
 * Copyright:
   * Copyright (c) 2015 Jon Schlinkert
   * (c) 2015, Jon Schlinkert
   * Copyright (c) 2015, Jon Schlinkert

__chalk 4.1.2__
 * https://github.com/chalk/chalk
 * License: MIT
 * Copyright:
   * Copyright (c) Sindre Sorhus <sindresorhus@gmail.com> (sindresorhus.com)

__char-props 0.1.5__
 * https://github.com/twolfson/char-props
 * License: MIT
 * Copyright:
   * Copyright (c) 2012 Todd Wolfson

__chokidar 3.5.3__
 * https://github.com/paulmillr/chokidar
 * License: MIT
 * Copyright:
   * Copyright (c) 2012-2019 Paul Miller (https://paulmillr.com), Elan Shanker

__ckeditor4 4.21.0__
 * https://github.com/ckeditor/ckeditor4-releases
 * Licenses:
   * LGPL-2.1
   * MPL-1.1
   * GPL-2.0
 * Copyright:
   * Copyright (c) 2006, Ivan Sagalaev
   * Copyright (c) 2012, CKSource (http://cksource.com) - Frederico Knabben
   * (c) Visoft, Inc.
   * Copyright (c) 2006, Ivan Sagalaev. YUI
   * (c) Vladimir Epifanov <voldmar@voldmar.ru>
   * Copyright (c) 2014 by Marijn Haverbeke <marijnh@gmail.com> and others
   * Copyright (c) 2014 by original authors fontello.com
   * Copyright (c) 1989, 1991 Free Software Foundation, Inc.
   * Copyright (c) 2003-2023, CKSource Holding sp. z o.o.
   * Copyright (c) $1
   * (c) Zaripov Yura <yur4ik7@ukr.net>
   * Copyright (c) 2014 by original authors
   * (c) Angel Garcia <angelgarcia.mail@gmail.com>
   * copyrighted by the Free Software Foundation
   * Copyright (c) 1991, 1999 Free Software Foundation, Inc.
   * Copyright (c) 2009, Yahoo! Inc.
   * Copyright (c) 2014-2023, CKSource Holding
   * Copyright (c) 2003-2023
   * (c) Jeremy Hull <sourdrums@gmail.com>
   * Copyright (c) 2003-2018, CKSource - Frederico Knabben
   * Copyright (c) $1. Alle Rechte
   * (c) Aahan Krish <geekpanth3r@gmail.com>
   * (c) Vasily Mikhailitchenko <vaskas@programica.ru>
   * Copyright (c) 2012 James Frasca. CodeMirror
   * (c) Ivan Sagalaev <Maniac@SoftwareManiacs.Org>
   * (c) Vasily Polovnyov <vast@whiteants.net>
   * Copyright (c) 2012 by Dave Gandy
   * Copyright (c) 2011 John Resig, https://jquery.com
   * Copyright (c) 2003-2022, CKSource (http://cksource.com) Holding sp. z o.o.
   * Copyright (c) 2014 Yehuda Katz, Tom Dale, Stefan Penner and contributors

__class-utils 0.3.6__
 * https://github.com/jonschlinkert/class-utils
 * License: MIT
 * Copyright:
   * Copyright (c) 2015, 2017-2018, Jon Schlinkert
   * Copyright (c) 2018, Jon Schlinkert (https://github.com/jonschlinkert)

__clean-css 3.4.28__
 * https://github.com/jakubpawlowicz/clean-css
 * License: MIT
 * Copyright:
   * Copyright (c) 2017 JakubPawlowicz.com

__cliui 8.0.1__
 * https://github.com/yargs/cliui
 * License: ISC
 * Copyright:
   * Copyright (c) 2015, Contributors

__clone-buffer 1.0.0__
 * https://github.com/gulpjs/clone-buffer
 * License: MIT
 * Copyright:
   * Copyright (c) 2016 Blaine Bublitz <blaine.bublitz@gmail.com> , Eric Schoffstall <yo@contra.io> and other contributors

__clone-stats 1.0.0__
 * https://github.com/hughsk/clone-stats
 * License: MIT
 * Copyright:
   * Copyright (c) 2014 Hugh Kennedy

__clone 2.1.2__
 * https://github.com/pvorb/node-clone
 * License: MIT
 * Copyright:
   * Copyright (c) 2011-2016 Paul Vorbach (https://paul.vorba.ch/) and contributors
            (https://github.com/pvorb/clone/graphs/contributors).

__cloneable-readable 1.1.3__
 * https://github.com/mcollina/cloneable-readable
 * License: MIT
 * Copyright:
   * Copyright (c) 2016 Matteo Collina

__collection-visit 1.0.0__
 * https://github.com/jonschlinkert/collection-visit
 * License: MIT
 * Copyright:
   * Copyright (c) 2015, 2017, Jon Schlinkert
   * Copyright (c) 2017, Jon Schlinkert (https://github.com/jonschlinkert)

__color-convert 2.0.1__
 * https://github.com/Qix-/color-convert
 * License: MIT
 * Copyright:
   * Copyright (c) 2011-2016 Heather Arthur <fayearthur@gmail.com>.
   * Copyright (c) 2016-2021 Josh Junon <josh@junon.me>.

__color-name 1.1.4__
 * https://github.com/colorjs/color-name
 * License: MIT
 * Copyright:
   * Copyright (c) 2015 Dmitry Ivanov

__color-support 1.1.3__
 * https://github.com/isaacs/color-support
 * License: ISC
 * Copyright:
   * Copyright (c) Isaac Z. Schlueter and Contributors

__commander 2.8.1__
 * https://github.com/tj/commander.js
 * License: MIT
 * Copyright:
   * Copyright (c) 2011 TJ Holowaychuk <tj@vision-media.ca>

__component-emitter 1.3.0__
 * https://github.com/component/emitter
 * License: MIT
 * Copyright:
   * Copyright (c) 2014 Component contributors <dev@component.io>

__concat-map 0.0.1__
 * https://github.com/substack/node-concat-map
 * License: MIT
 * Copyright:
   * James Halliday (mail@substack.net)

__concat-with-sourcemaps 1.1.0__
 * https://github.com/floridoo/concat-with-sourcemaps
 * License: ISC
 * Copyright:
   * Copyright (c) 2014, Florian Reiterer <me@florianreiterer.com>
   * (c) John Doe

__connect-history-api-fallback 1.6.0__
 * https://github.com/bripkens/connect-history-api-fallback
 * License: MIT
 * Copyright:
   * Copyright (c) 2012 Ben Ripkens http://bripkens.de

__connect 3.6.6__
 * https://github.com/senchalabs/connect
 * License: MIT
 * Copyright:
   * Copyright (c) 2010 Sencha Inc.
   * Copyright (c) 2011 TJ Holowaychuk
   * Copyright (c) 2015 Douglas Christopher Wilson
   * Copyright (c) 2011-2014 TJ Holowaychuk
   * Copyright (c) 2011 LearnBoost

__content-type 1.0.5__
 * https://github.com/jshttp/content-type
 * License: MIT
 * Copyright:
   * Copyright (c) 2015 Douglas Christopher Wilson

__convert-source-map 1.9.0__
 * https://github.com/thlorenz/convert-source-map
 * License: MIT
 * Copyright:
   * Copyright 2013 Thorsten Lorenz

__cookie 0.4.2__
 * https://github.com/jshttp/cookie
 * License: MIT
 * Copyright:
   * Copyright (c) 2012-2014 Roman Shtylman <shtylman@gmail.com>
   * Copyright (c) 2015 Douglas Christopher Wilson <doug@somethingdoug.com>

__copy-descriptor 0.1.1__
 * https://github.com/jonschlinkert/copy-descriptor
 * License: MIT
 * Copyright:
   * Copyright (c) 2015-2016, Jon Schlinkert

__core-util-is 1.0.3__
 * https://github.com/isaacs/core-util-is
 * License: MIT
 * Copyright:
   * Copyright Joyent, Inc. and other Node contributors

__cors 2.8.5__
 * https://github.com/expressjs/cors
 * License: MIT
 * Copyright:
   * Copyright (c) 2013 Troy Goode <troygoode@gmail.com>

__css-parse 2.0.0__
 * https://github.com/reworkcss/css-parse
 * License: MIT
 * Copyright:
   * Copyright (c) 2013 TJ Holowaychuk <tj@vision-media.ca>

__css 2.2.4__
 * https://github.com/reworkcss/css
 * License: MIT
 * Copyright:
   * Copyright (c) 2012 TJ Holowaychuk <tj@vision-media.ca>

__csv-parser 3.0.0__
 * https://github.com/mafintosh/csv-parser
 * License: MIT
 * Copyright:
   * Copyright (c) 2014 Mathias Buus

__currently-unhandled 0.4.1__
 * https://github.com/jamestalmage/currently-unhandled
 * License: MIT
 * Copyright:
   * Copyright (c) James Talmage <james@talmage.io> (github.com/jamestalmage)

__dateformat 2.2.0__
 * https://github.com/felixge/node-dateformat
 * License: MIT
 * Copyright:
   * Copyright (c) 2014 Charlike Mike Reagent, contributors.
   * (c) 2007-2009 Steven Levithan stevenlevithan.com

__debug 4.3.4__
 * https://github.com/debug-js/debug
 * License: MIT
 * Copyright:
   * Copyright (c) 2014-2017 TJ Holowaychuk <tj@vision-media.ca>

__debuglog 1.0.1__
 * https://github.com/sam-github/node-debuglog
 * License: MIT
 * Copyright:
   * Copyright Joyent, Inc. and other Node contributors

__decamelize 1.2.0__
 * https://github.com/sindresorhus/decamelize
 * License: MIT
 * Copyright:
   * Copyright (c) Sindre Sorhus <sindresorhus@gmail.com> (sindresorhus.com)

__decode-uri-component 0.2.2__
 * https://github.com/SamVerschueren/decode-uri-component
 * License: MIT
 * Copyright:
   * Copyright (c) Sindre Sorhus <sindresorhus@gmail.com> (sindresorhus.com)

__defaults 1.0.4__
 * https://github.com/sindresorhus/node-defaults
 * License: MIT
 * Copyright:
   * Copyright (c) 2015 Elijah Insua
   * Copyright (c) 2022 Sindre Sorhus

__define-properties 1.2.0__
 * https://github.com/ljharb/define-properties
 * License: MIT
 * Copyright:
   * Copyright (c) 2015 Jordan Harband

__define-property 2.0.2__
 * https://github.com/jonschlinkert/define-property
 * License: MIT
 * Copyright:
   * Copyright (c) 2018, Jon Schlinkert (https://github.com/jonschlinkert)
   * Copyright (c) 2015-2018, Jon Schlinkert

__depd 2.0.0__
 * https://github.com/dougwilson/nodejs-depd
 * License: MIT
 * Copyright:
   * Copyright (c) 2014-2018 Douglas Christopher Wilson

__deprecated 0.0.1__
 * https://github.com/wearefractal/deprecated
 * License: MIT
 * Copyright:
   * Copyright (c) 2014 Fractal <contact@wearefractal.com>

__destroy 1.0.4__
 * https://github.com/stream-utils/destroy
 * License: MIT
 * Copyright:
   * Copyright (c) 2014 Jonathan Ong me@jongleberry.com
   * Copyright (c) 2015-2022 Douglas Christopher Wilson doug@somethingdoug.com

__detect-file 1.0.0__
 * https://github.com/doowb/detect-file
 * License: MIT
 * Copyright:
   * Copyright (c) 2016-2017, Brian Woodward
   * Copyright (c) 2017, Brian Woodward (https://github.com/doowb)

__dev-ip 1.0.1__
 * https://github.com/shakyshane/dev-ip
 * License: MIT
 * Copyright:
   * Copyright (c) 2013 Shane Osbourne

__dezalgo 1.0.4__
 * https://github.com/npm/dezalgo
 * License: ISC
 * Copyright:
   * Copyright (c) Isaac Z. Schlueter and Contributors

__duplexer2 0.0.2__
 * https://github.com/deoxxa/duplexer2
 * License: BSD-3-Clause
 * Copyright:
   * Copyright (c) 2013, Deoxxa Development

__duplexer 0.1.2__
 * https://github.com/Raynos/duplexer
 * License: MIT
 * Copyright:
   * Copyright (c) 2012 Raynos.

__duplexify 3.7.1__
 * https://github.com/mafintosh/duplexify
 * License: MIT
 * Copyright:
   * Copyright (c) 2014 Mathias Buus

__easy-extender 2.3.4__
 * https://github.com/shakyshane/easy-extender
 * License: Apache-2.0
 * Copyright:
   * Copyright 2015 Shane Osbourne

__eazy-logger 4.0.0__
 * https://github.com/shakyshane/easy-logger
 * License: Apache 2.0
 * Copyright:
   * Copyright Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors
   * Copyright jQuery Foundation and other contributors <https://jquery.org/>
   * Copyright 2015 Shane Osbourne

__ee-first 1.1.1__
 * https://github.com/jonathanong/ee-first
 * License: MIT
 * Copyright:
   * Copyright (c) 2014 Jonathan Ong me@jongleberry.com

__emoji-regex 8.0.0__
 * https://github.com/mathiasbynens/emoji-regex
 * License: MIT
 * Copyright:
   * Copyright Mathias Bynens <https://mathiasbynens.be/>

__encodeurl 1.0.2__
 * https://github.com/pillarjs/encodeurl
 * License: MIT
 * Copyright:
   * Copyright (c) 2016 Douglas Christopher Wilson

__end-of-stream 1.4.4__
 * https://github.com/mafintosh/end-of-stream
 * License: MIT
 * Copyright:
   * Copyright (c) 2014 Mathias Buus

__engine.io-client 6.4.0__
 * https://github.com/socketio/engine.io-client
 * License: MIT
 * Copyright:
   * Copyright (c) 2014 Automattic, Inc.
   * Copyright (c) 2014-2015 Automattic <dev@cloudup.com>
   * Copyright (c) 2012 Niklas von Hertzen

__engine.io-parser 5.0.6__
 * https://github.com/socketio/engine.io-parser
 * License: MIT
 * Copyright:
   * Copyright (c) 2016 Guillermo Rauch

__engine.io 6.4.1__
 * https://github.com/socketio/engine.io
 * License: MIT
 * Copyright:
   * Copyright (c) 2014 Guillermo Rauch <guillermo@learnboost.com>

__error-ex 1.3.2__
 * https://github.com/qix-/node-error-ex
 * License: MIT
 * Copyright:
   * Copyright (c) 2015 JD Ballard

__escalade 3.1.1__
 * https://github.com/lukeed/escalade
 * License: MIT
 * Copyright:
   * Copyright (c) Luke Edwards <luke.edwards05@gmail.com> (lukeed.com)

__escape-html 1.0.3__
 * https://github.com/component/escape-html
 * License: MIT
 * Copyright:
   * Copyright (c) 2012-2013 TJ Holowaychuk
   * Copyright (c) 2015 Tiancheng "Timothy" Gu
   * Copyright (c) 2015 Andreas Lubbe

__escape-string-regexp 1.0.5__
 * https://github.com/sindresorhus/escape-string-regexp
 * License: MIT
 * Copyright:
   * Copyright (c) Sindre Sorhus <sindresorhus@gmail.com> (sindresorhus.com)

__etag 1.8.1__
 * https://github.com/jshttp/etag
 * License: MIT
 * Copyright:
   * Copyright (c) 2014-2016 Douglas Christopher Wilson

__event-stream 4.0.1__
 * https://github.com/dominictarr/event-stream
 * License: MIT
 * Copyright:
   * Copyright (c) 2011 Dominic Tarr

__eventemitter3 4.0.7__
 * https://github.com/primus/eventemitter3
 * License: MIT
 * Copyright:
   * Copyright (c) 2014 Arnout Kazemier

__expand-brackets 2.1.4__
 * https://github.com/jonschlinkert/expand-brackets
 * License: MIT
 * Copyright:
   * Copyright (c) 2015-2018, Jon Schlinkert

__expand-tilde 2.0.2__
 * https://github.com/jonschlinkert/expand-tilde
 * License: MIT
 * Copyright:
   * Copyright (c) 2015 Jon Schlinkert
   * Copyright (c) 2016, Jon Schlinkert (https://github.com/jonschlinkert)
   * Copyright (c) 2015-2016, Jon Schlinkert

__extend-shallow 3.0.2__
 * https://github.com/jonschlinkert/extend-shallow
 * License: MIT
 * Copyright:
   * Copyright (c) 2014-2015, 2017, Jon Schlinkert.

__extend 3.0.2__
 * https://github.com/justmoon/node-extend
 * License: MIT
 * Copyright:
   * Copyright (c) 2014 Stefan Thomas

__extglob 2.0.4__
 * https://github.com/micromatch/extglob
 * License: MIT
 * Copyright:
   * Copyright (c) 2015-2018, Jon Schlinkert.

__fancy-log 1.3.3__
 * https://github.com/gulpjs/fancy-log
 * License: MIT
 * Copyright:
   * Copyright (c) 2014, 2015, 2018 Blaine Bublitz <blaine.bublitz@gmail.com> and Eric Schoffstall
            <yo@contra.io>

__faye-websocket 0.7.3__
 * https://github.com/faye/faye-websocket-node
 * License: Apache-2.0
 * Copyright:
   * Copyright 2010-2019 James Coglan

__filesize 2.0.4__
 * https://github.com/avoidwork/filesize.js
 * License: BSD-3
 * Copyright:
   * Copyright (c) 2013 Jason Mulligan
   * copyright 2014 Jason Mulligan
   * Copyright (c) 2013, Jason Mulligan

__fill-range 7.0.1__
 * https://github.com/jonschlinkert/fill-range
 * License: MIT
 * Copyright:
   * Copyright (c) 2014-present, Jon Schlinkert.

__finalhandler 1.1.0__
 * https://github.com/pillarjs/finalhandler
 * License: MIT
 * Copyright:
   * Copyright (c) 2014-2017 Douglas Christopher Wilson <doug@somethingdoug.com>

__find-index 0.1.1__
 * https://github.com/jsdf/find-index
 * License: MIT
 * Copyright:
   * Copyright (c) 2019 James Friend

__find-up 1.1.2__
 * https://github.com/sindresorhus/find-up
 * License: MIT
 * Copyright:
   * Copyright (c) Sindre Sorhus <sindresorhus@gmail.com> (sindresorhus.com)

__findup-sync 2.0.0__
 * https://github.com/js-cli/node-findup-sync
 * License: MIT
 * Copyright:
   * Copyright (c) 2012-2016 Cowboy Ben Alman

__fined 1.2.0__
 * https://github.com/gulpjs/fined
 * License: MIT
 * Copyright:
   * Copyright (c) 2016, 2017, 2018 Blaine Bublitz <blaine.bublitz@gmail.com> and Eric Schoffstall <yo@contra.io>

__first-chunk-stream 1.0.0__
 * https://github.com/sindresorhus/first-chunk-stream
 * License: MIT
 * Copyright:
   * (c) Sindre Sorhus (http://sindresorhus.com)

__flagged-respawn 1.0.1__
 * https://github.com/gulpjs/flagged-respawn
 * License: MIT
 * Copyright:
   * Copyright (c) 2014-2018 Tyler Kellen <tyler@sleekcode.net> , Blaine Bublitz <blaine.bublitz@gmail.com> , and Eric Schoffstall <yo@contra.io>

__flush-write-stream 1.1.1__
 * https://github.com/mafintosh/flush-write-stream
 * License: MIT
 * Copyright:
   * Copyright (c) 2015 Mathias Buus

__follow-redirects 1.15.2__
 * https://github.com/follow-redirects/follow-redirects
 * License: MIT
 * Copyright:
   * Copyright 2014-present Olivier Lalonde <olalonde@gmail.com> , James Talmage <james@talmage.io>
            , Ruben Verborgh

__for-in 1.0.2__
 * https://github.com/jonschlinkert/for-in
 * License: MIT
 * Copyright:
   * Copyright (c) 2014-2017, Jon Schlinkert

__for-own 1.0.0__
 * https://github.com/jonschlinkert/for-own
 * License: MIT
 * Copyright:
   * Copyright (c) 2014-2017, Jon Schlinkert.

__fragment-cache 0.2.1__
 * https://github.com/jonschlinkert/fragment-cache
 * License: MIT
 * Copyright:
   * Copyright (c) 2016-2017, Jon Schlinkert

__fresh 0.5.2__
 * https://github.com/jshttp/fresh
 * License: MIT
 * Copyright:
   * Copyright (c) 2016-2017 Douglas Christopher Wilson <doug@somethingdoug.com>
   * Copyright (c) 2012 TJ Holowaychuk <tj@vision-media.ca>

__from 0.1.7__
 * https://github.com/dominictarr/from
 * License: MIT
 * Copyright:
   * Copyright (c) 2011 Dominic Tarr

__fs-extra 3.0.1__
 * https://github.com/jprichardson/node-fs-extra
 * License: MIT
 * Copyright:
   * Copyright (c) 2011-2017 JP Richardson

__fs-mkdirp-stream 1.0.0__
 * https://github.com/gulpjs/fs-mkdirp-stream
 * License: MIT
 * Copyright:
   * Copyright 2010 James Halliday
   * Copyright (c) 2017 Blaine Bublitz <blaine.bublitz@gmail.com> , Eric Schoffstall <yo@contra.io> and other contributors

__fs.realpath 1.0.0__
 * https://github.com/isaacs/fs.realpath
 * License: ISC
 * Copyright:
   * Copyright (c) 2016-2022 Isaac Z. Schlueter and Contributors

__function-bind 1.1.1__
 * https://github.com/Raynos/function-bind
 * License: MIT
 * Copyright:
   * Copyright (c) 2013 Raynos

__gaze 0.5.2__
 * https://github.com/shama/gaze
 * License: MIT
 * Copyright:
   * Copyright (c) 2018 Kyle Robinson Young

__get-caller-file 2.0.5__
 * https://github.com/stefanpenner/get-caller-file
 * License: ISC
 * Copyright:
   * Copyright 2018 Stefan Penner

__get-intrinsic 1.2.0__
 * https://github.com/ljharb/get-intrinsic
 * License: MIT
 * Copyright:
   * Copyright (c) 2020 Jordan Harband

__get-stdin 4.0.1__
 * https://github.com/sindresorhus/get-stdin
 * License: MIT
 * Copyright:
   * Copyright (c) Sindre Sorhus <sindresorhus@gmail.com> (sindresorhus.com)

__get-value 2.0.6__
 * https://github.com/jonschlinkert/get-value
 * License: MIT
 * Copyright:
   * Copyright (c) 2014-2016, Jon Schlinkert

__glob-parent 5.1.2__
 * https://github.com/gulpjs/glob-parent
 * License: ISC
 * Copyright:
   * Copyright (c) 2015, 2019 Elan Shanker

__glob-stream 6.1.0__
 * https://github.com/gulpjs/glob-stream
 * License: MIT
 * Copyright:
   * Copyright (c) 2015-2017 Blaine Bublitz <blaine.bublitz@gmail.com> , Eric Schoffstall <yo@contra.io> and other contributors

__glob-watcher 0.0.6__
 * https://github.com/wearefractal/glob-watcher
 * License: MIT
 * Copyright:
   * Copyright (c) 2013 Fractal <contact@wearefractal.com>

__glob2base 0.0.12__
 * https://github.com/wearefractal/glob2base
 * License: MIT
 * Copyright:
   * Copyright (c) 2014 Fractal <contact@wearefractal.com>

__glob 7.2.3__
 * https://github.com/isaacs/node-glob
 * License: ISC
 * Copyright:
   * Copyright (c) Isaac Z. Schlueter and Contributors

__global-modules 1.0.0__
 * https://github.com/jonschlinkert/global-modules
 * License: MIT
 * Copyright:
   * Copyright (c) 2015-present, Jon Schlinkert.

__global-prefix 1.0.2__
 * https://github.com/jonschlinkert/global-prefix
 * License: MIT
 * Copyright:
   * Copyright (c) 2015-present Jon Schlinkert

__globby 2.1.0__
 * https://github.com/sindresorhus/globby
 * License: MIT
 * Copyright:
   * Copyright (c) Sindre Sorhus <sindresorhus@gmail.com> (sindresorhus.com)

__globule 0.1.0__
 * https://github.com/cowboy/node-globule
 * License: MIT
 * Copyright:
   * Copyright (c) 2018 Cowboy Ben Alman

__glogg 1.0.2__
 * https://github.com/gulpjs/glogg
 * License: MIT
 * Copyright:
   * Copyright (c) 2014, 2015, 2018 Blaine Bublitz <blaine.bublitz@gmail.com> and Eric Schoffstall <yo@contra.io>

__glyphicons-only-bootstrap 1.0.1__
 * https://github.com/ohpyupi/glyphicons-only-bootstrap
 * License: MIT
 * Copyright:
   * ohpyupi (ohpyupi@gmail.com)

__graceful-fs 4.2.10__
 * https://github.com/isaacs/node-graceful-fs
 * License: ISC
 * Copyright:
   * Copyright (c) Isaac Z. Schlueter, Ben Noordhuis, and Contributors

__graceful-readlink 1.0.1__
 * https://github.com/zhiyelee/graceful-readlink
 * License: MIT
 * Copyright:
   * Copyright (c) 2015 Zhiye Li

__gulp-angular-templatecache 1.9.1__
 * https://github.com/miickel/gulp-angular-templatecache
 * License: MIT
 * Copyright:
   * Copyright (c) 2014 Mickel
   * Copyright (c) 2014 Mickel (http://mickel.me)

__gulp-concat 2.6.1__
 * https://github.com/contra/gulp-concat
 * License: MIT
 * Copyright:
   * Copyright (c) 2016 Contra <yo@contra.io>

__gulp-cssmin 0.1.7__
 * https://github.com/chilijung/gulp-cssmin
 * License: MIT
 * Copyright:
   * chilijung

__gulp-footer 1.0.5__
 * https://github.com/tracker1/gulp-footer
 * License: MIT
 * Copyright:
   * Copyright (c) 2013 GoDaddy.com

__gulp-header 1.8.2__
 * https://github.com/tracker1/gulp-header
 * License: MIT
 * Copyright:
   * Copyright (c) 2013-2015 Michael J. Ryan and GoDaddy.com

__gulp-inject 3.0.0__
 * https://github.com/klei/gulp-inject
 * License: MIT
 * Copyright:
   * Copyright 2014

__gulp-jsmin 0.1.5__
 * https://github.com/chilijung/gulp-jsmin
 * License: MIT
 * Copyright:
   * chilijung

__gulp-livereload 3.8.1__
 * https://github.com/vohof/gulp-livereload
 * License: MIT
 * Copyright:
   * Copyright (c) 2014 Cyrus David

__gulp-rename 1.4.0__
 * https://github.com/hparra/gulp-rename
 * License: MIT
 * Copyright:
   * Copyright 2013 Hector Guillermo Parra Alvarez

__gulp-stylus 2.7.1__
 * https://github.com/stevelacy/gulp-stylus
 * License: MIT
 * Copyright:
   * Copyright (c) 2015 Steve Lacy (slacy.me, me@slacy.me)

__gulp-util 3.0.8__
 * https://github.com/gulpjs/gulp-util
 * License: MIT
 * Copyright:
   * Copyright (c) 2014 Fractal <contact@wearefractal.com>

__gulp 3.9.1__
 * https://github.com/gulpjs/gulp
 * License: MIT
 * Copyright:
   * Copyright (c) 2012 Tyler Kellen, contributors
   * Copyright (c) 2014 Jason Jarrett
   * Copyright (c) 2013-2016 Fractal <contact@wearefractal.com>

__gulplog 1.0.0__
 * https://github.com/gulpjs/gulplog
 * License: MIT
 * Copyright:
   * Copyright (c) 2015 Blaine Bublitz, Eric Schoffstall and other contributors

__has-ansi 2.0.0__
 * https://github.com/sindresorhus/has-ansi
 * License: MIT
 * Copyright:
   * Copyright (c) Sindre Sorhus <sindresorhus@gmail.com> (sindresorhus.com)

__has-flag 4.0.0__
 * https://github.com/sindresorhus/has-flag
 * License: MIT
 * Copyright:
   * Copyright (c) Sindre Sorhus <sindresorhus@gmail.com> (sindresorhus.com)

__has-gulplog 0.1.0__
 * https://github.com/gulpjs/has-gulplog
 * License: MIT
 * Copyright:
   * Copyright (c) 2015

__has-property-descriptors 1.0.0__
 * https://github.com/inspect-js/has-property-descriptors
 * License: MIT
 * Copyright:
   * Copyright (c) 2022 Inspect JS

__has-symbols 1.0.3__
 * https://github.com/inspect-js/has-symbols
 * License: MIT
 * Copyright:
   * Copyright (c) 2016 Jordan Harband

__has-value 1.0.0__
 * https://github.com/jonschlinkert/has-value
 * License: MIT
 * Copyright:
   * Copyright (c) 2014-2017, Jon Schlinkert

__has-values 1.0.0__
 * https://github.com/jonschlinkert/has-values
 * License: MIT
 * Copyright:
   * Copyright (c) 2014-2017, Jon Schlinkert

__has 1.0.3__
 * https://github.com/tarruda/has
 * License: MIT
 * Copyright:
   * Copyright (c) 2013 Thiago de Arruda

__homedir-polyfill 1.0.3__
 * https://github.com/doowb/homedir-polyfill
 * License: MIT
 * Copyright:
   * Copyright (c) 2016 Brian Woodward
   * Copyright (c) 2016 - 2019, Brian Woodward (https://github.com/doowb)

__hosted-git-info 2.8.9__
 * https://github.com/npm/hosted-git-info
 * License: ISC
 * Copyright:
   * Copyright (c) 2015, Rebecca Turner

__http-errors 2.0.0__
 * https://github.com/jshttp/http-errors
 * License: MIT
 * Copyright:
   * Copyright (c) 2016 Douglas Christopher Wilson doug@somethingdoug.com
   * Copyright (c) 2014 Jonathan Ong me@jongleberry.com

__http-parser-js 0.5.8__
 * https://github.com/creationix/http-parser-js
 * License: MIT
 * Copyright:
   * Copyright (c) 2015 Tim Caswell (https://github.com/creationix) and other contributors

__http-proxy 1.18.1__
 * https://github.com/http-party/node-http-proxy
 * License: MIT
 * Copyright:
   * Copyright (c) 2010-2016 Charlie Robbins, Jarrett Cruger & the Contributors

__iconv-lite 0.4.24__
 * https://github.com/ashtuchkin/iconv-lite
 * License: MIT
 * Copyright:
   * Copyright (c) 2011 Alexander Shtuchkin

__immutable 3.8.2__
 * https://github.com/facebook/immutable-js
 * License: MIT
 * Copyright:
   * Copyright (c) 2014-present, Facebook, Inc.

__indent-string 2.1.0__
 * https://github.com/sindresorhus/indent-string
 * License: MIT
 * Copyright:
   * Copyright (c) Sindre Sorhus <sindresorhus@gmail.com> (sindresorhus.com)

__indx 0.2.3__
 * https://github.com/jenius/indx
 * License: MIT
 * Copyright:
   * Copyright (c) 2015 Jeff Escalante

__inflight 1.0.6__
 * https://github.com/npm/inflight
 * License: ISC
 * Copyright:
   * Copyright (c) Isaac Z. Schlueter

__inherits 2.0.4__
 * https://github.com/isaacs/inherits
 * License: ISC
 * Copyright:
   * Copyright (c) Isaac Z. Schlueter

__ini 1.3.8__
 * https://github.com/isaacs/ini
 * License: ISC
 * Copyright:
   * Copyright (c) Isaac Z. Schlueter and Contributors

__interpret 1.4.0__
 * https://github.com/gulpjs/interpret
 * License: MIT
 * Copyright:
   * Copyright (c) 2014-2018 Tyler Kellen <tyler@sleekcode.net> , Blaine Bublitz <blaine.bublitz@gmail.com>
            , and Eric Schoffstall <yo@contra.io>

__is-absolute 1.0.0__
 * https://github.com/jonschlinkert/is-absolute
 * License: MIT
 * Copyright:
   * Copyright (c) 2014-2017, Jon Schlinkert.
   * Copyright (c) 2009-2014, TJ Holowaychuk

__is-accessor-descriptor 1.0.0__
 * https://github.com/jonschlinkert/is-accessor-descriptor
 * License: MIT
 * Copyright:
   * Copyright (c) 2015-2017, Jon Schlinkert

__is-arrayish 0.2.1__
 * https://github.com/qix-/node-is-arrayish
 * License: MIT
 * Copyright:
   * Copyright (c) 2015 JD Ballard

__is-binary-path 2.1.0__
 * https://github.com/sindresorhus/is-binary-path
 * License: MIT
 * Copyright:
   * Copyright (c) 2019 Sindre Sorhus <sindresorhus@gmail.com> (https://sindresorhus.com), Paul
            Miller (https://paulmillr.com)

__is-buffer 1.1.6__
 * https://github.com/feross/is-buffer
 * License: MIT
 * Copyright:
   * Copyright (c) Feross Aboukhadijeh (http://feross.org)

__is-core-module 2.11.0__
 * https://github.com/inspect-js/is-core-module
 * License: MIT
 * Copyright:
   * Copyright (c) 2014 Dave Justice

__is-data-descriptor 1.0.0__
 * https://github.com/jonschlinkert/is-data-descriptor
 * License: MIT
 * Copyright:
   * Copyright (c) 2015-present, Jon Schlinkert.

__is-descriptor 1.0.2__
 * https://github.com/jonschlinkert/is-descriptor
 * License: MIT
 * Copyright:
   * Copyright (c) 2017, Jon Schlinkert (https://github.com/jonschlinkert)

__is-extendable 1.0.1__
 * https://github.com/jonschlinkert/is-extendable
 * License: MIT
 * Copyright:
   * Copyright (c) 2015-2017, Jon Schlinkert

__is-extglob 2.1.1__
 * https://github.com/jonschlinkert/is-extglob
 * License: MIT
 * Copyright:
   * Copyright (c) 2014-2016, Jon Schlinkert

__is-finite 1.1.0__
 * https://github.com/sindresorhus/is-finite
 * License: MIT
 * Copyright:
   * Copyright (c) Sindre Sorhus <sindresorhus@gmail.com> (sindresorhus.com)

__is-fullwidth-code-point 3.0.0__
 * https://github.com/sindresorhus/is-fullwidth-code-point
 * License: MIT
 * Copyright:
   * Copyright (c) Sindre Sorhus <sindresorhus@gmail.com> (sindresorhus.com)

__is-glob 4.0.3__
 * https://github.com/micromatch/is-glob
 * License: MIT
 * Copyright:
   * Copyright (c) 2014-2017, Jon Schlinkert

__is-negated-glob 1.0.0__
 * https://github.com/jonschlinkert/is-negated-glob
 * License: MIT
 * Copyright:
   * Copyright (c) 2016 Jon Schlinkert

__is-number-like 1.0.8__
 * https://github.com/vigour-io/is-number-like
 * License: ISC
 * Copyright:
   * Copyright (c) 2016, Vigour.io

__is-number 7.0.0__
 * https://github.com/jonschlinkert/is-number
 * License: MIT
 * Copyright:
   * Copyright (c) 2014-present, Jon Schlinkert

__is-plain-object 2.0.4__
 * https://github.com/jonschlinkert/is-plain-object
 * License: MIT
 * Copyright:
   * Copyright (c) 2014-2017, Jon Schlinkert

__is-relative 1.0.0__
 * https://github.com/jonschlinkert/is-relative
 * License: MIT
 * Copyright:
   * Copyright (c) 2014-2017, Jon Schlinkert.

__is-unc-path 1.0.0__
 * https://github.com/jonschlinkert/is-unc-path
 * License: MIT
 * Copyright:
   * Copyright (c) 2015-2017, Jon Schlinkert.

__is-utf8 0.2.1__
 * https://github.com/wayfind/is-utf8
 * License: MIT
 * Copyright:
   * Copyright (c) 2014 Wei Fanzhe

__is-valid-glob 1.0.0__
 * https://github.com/jonschlinkert/is-valid-glob
 * License: MIT
 * Copyright:
   * Copyright (c) 2015-2017, Jon Schlinkert
   * Copyright (c) 2017, Jon Schlinkert (https://github.com/jonschlinkert).

__is-windows 1.0.2__
 * https://github.com/jonschlinkert/is-windows
 * License: MIT
 * Copyright:
   * Copyright (c) 2015-2018, Jon Schlinkert

__is-wsl 1.1.0__
 * https://github.com/sindresorhus/is-wsl
 * License: MIT
 * Copyright:
   * Copyright (c) Sindre Sorhus <sindresorhus@gmail.com> (sindresorhus.com)

__isarray 1.0.0__
 * https://github.com/juliangruber/isarray
 * License: MIT
 * Copyright:
   * Copyright (c) 2013 Julian Gruber <julian@juliangruber.com>

__isexe 2.0.0__
 * https://github.com/isaacs/isexe
 * License: ISC
 * Copyright:
   * Copyright (c) Isaac Z. Schlueter and Contributors

__isobject 3.0.1__
 * https://github.com/jonschlinkert/isobject
 * License: MIT
 * Copyright:
   * Copyright (c) 2014-2017, Jon Schlinkert

__js-string-escape 1.0.1__
 * https://github.com/joliss/js-string-escape
 * License: MIT
 * Copyright:
   * Copyright (c) 2013 Jo Liss

__jsmin-sourcemap 0.16.0__
 * https://github.com/twolfson/node-jsmin-sourcemap
 * License: UNLICENSE
 * Copyright:
   * (c) 2009-2012 Jeremy Ashkenas, DocumentCloud Inc.
   * Copyright 2011, The Dojo Foundation
   * Copyright 2011, John Resig

__jsmin2 1.1.9__
 * https://github.com/twolfson/node-jsmin2
 * License: The JSON License
 * Copyright:
   * Copyright (c) 2012 Todd Wolfson (todd@twolfson.com)

__json-parse-even-better-errors 2.3.1__
 * https://github.com/npm/json-parse-even-better-errors
 * License: MIT
 * Copyright:
   * Copyright 2017 Kat Marchan
   * Copyright npm, Inc.

__json-stable-stringify-without-jsonify 1.0.1__
 * https://github.com/samn/json-stable-stringify
 * License: MIT
 * Copyright:
   * James Halliday (mail@substack.net)

__jsonfile 3.0.1__
 * https://github.com/jprichardson/node-jsonfile
 * License: MIT
 * Copyright:
   * Copyright (c) 2012-2015, JP Richardson <jprichardson@gmail.com>

__kind-of 6.0.3__
 * https://github.com/jonschlinkert/kind-of
 * License: MIT
 * Copyright:
   * Copyright (c) 2014-2017, Jon Schlinkert

__lazy-cache 1.0.4__
 * https://github.com/jonschlinkert/lazy-cache
 * License: MIT
 * Copyright:
   * Copyright (c) 2015-2016, Jon Schlinkert.
   * Copyright (c) 2016, Jon Schlinkert (https://github.com/jonschlinkert).

__lazystream 1.0.1__
 * https://github.com/jpommerening/node-lazystream
 * License: MIT
 * Copyright:
   * Copyright (c) 2013 J. Pommerening, contributors.

__lead 1.0.0__
 * https://github.com/gulpjs/lead
 * License: MIT
 * Copyright:
   * Copyright (c) 2017 Blaine Bublitz <blaine.bublitz@gmail.com> , Eric Schoffstall <yo@contra.io> and other contributors

__license-checker 25.0.1__
 * https://github.com/davglass/license-checker
 * License: BSD-3-Clause
 * Copyright:
   * Copyright 2012 Yahoo Inc.
   * Copyright (c) Isaac Z. Schlueter and Contributors
   * Copyright (c) 2013, Yahoo! Inc.
   * Copyright (c) 2012, Yahoo! Inc.

__liftoff 2.5.0__
 * https://github.com/js-cli/js-liftoff
 * License: MIT
 * Copyright:
   * Copyright (c) 2014 Tyler Kellen

__limiter 1.1.5__
 * https://github.com/jhurliman/node-rate-limiter
 * License: MIT
 * Copyright:
   * Copyright (c) 2011 by John Hurliman
   * Copyright (c) 2013 John Hurliman. <jhurliman@jhurliman.org>

__livereload-js 2.4.0__
 * https://github.com/livereload/livereload-js
 * License: MIT
 * Copyright:
   * Copyright (c) 2010-2012 Andrey Tarantsov

__load-json-file 1.1.0__
 * https://github.com/sindresorhus/load-json-file
 * License: MIT
 * Copyright:
   * Copyright (c) Sindre Sorhus <sindresorhus@gmail.com> (sindresorhus.com)

__localtunnel 2.0.2__
 * https://github.com/localtunnel/localtunnel
 * License: MIT
 * Copyright:
   * Copyright (c) 2018 Roman Shtylman

__lodash._baseassign 3.2.0__
 * https://github.com/lodash/lodash
 * License: MIT
 * Copyright:
   * Copyright 2012-2015 The Dojo Foundation <http://dojofoundation.org/>
   * Copyright 2009-2015 Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors
   * copyright 2009-2015 Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors <http://underscorejs.org/>

__lodash._basecopy 3.0.1__
 * https://github.com/lodash/lodash
 * License: MIT
 * Copyright:
   * Copyright 2012-2015 The Dojo Foundation <http://dojofoundation.org/>
   * Copyright 2009-2015 Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors
   * copyright 2009-2015 Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors <http://underscorejs.org/>

__lodash._basetostring 3.0.1__
 * https://github.com/lodash/lodash
 * License: MIT
 * Copyright:
   * Copyright 2012-2015 The Dojo Foundation <http://dojofoundation.org/>
   * Copyright 2009-2015 Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors
   * copyright 2009-2015 Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors <http://underscorejs.org/>

__lodash._basevalues 3.0.0__
 * https://github.com/lodash/lodash
 * License: MIT
 * Copyright:
   * Copyright 2012-2015 The Dojo Foundation <http://dojofoundation.org/>
   * Copyright 2009-2015 Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors
   * copyright 2009-2015 Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors <http://underscorejs.org/>

__lodash._bindcallback 3.0.1__
 * https://github.com/lodash/lodash
 * License: MIT
 * Copyright:
   * Copyright 2012-2015 The Dojo Foundation <http://dojofoundation.org/>
   * Copyright 2009-2015 Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors
   * copyright 2009-2015 Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors <http://underscorejs.org/>

__lodash._createassigner 3.1.1__
 * https://github.com/lodash/lodash
 * License: MIT
 * Copyright:
   * Copyright 2012-2015 The Dojo Foundation <http://dojofoundation.org/>
   * Copyright 2009-2015 Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors
   * copyright 2009-2015 Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors <http://underscorejs.org/>

__lodash._escapehtmlchar 2.4.1__
 * https://github.com/lodash/lodash-cli
 * License: MIT
 * Copyright:
   * Copyright 2012-2013 The Dojo Foundation <http://dojofoundation.org/>
   * Copyright 2009-2013 Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors
   * copyright 2009-2013 Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors <http://underscorejs.org/>

__lodash._escapestringchar 2.4.1__
 * https://github.com/lodash/lodash-cli
 * License: MIT
 * Copyright:
   * Copyright 2012-2013 The Dojo Foundation <http://dojofoundation.org/>
   * Copyright 2009-2013 Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors
   * copyright 2009-2013 Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors <http://underscorejs.org/>

__lodash._getnative 3.9.1__
 * https://github.com/lodash/lodash
 * License: MIT
 * Copyright:
   * Copyright 2012-2015 The Dojo Foundation <http://dojofoundation.org/>
   * Copyright 2009-2015 Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors
   * copyright 2009-2015 Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors <http://underscorejs.org/>

__lodash._htmlescapes 2.4.1__
 * https://github.com/lodash/lodash-cli
 * License: MIT
 * Copyright:
   * Copyright 2012-2013 The Dojo Foundation <http://dojofoundation.org/>
   * Copyright 2009-2013 Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors
   * copyright 2009-2013 Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors <http://underscorejs.org/>

__lodash._isiterateecall 3.0.9__
 * https://github.com/lodash/lodash
 * License: MIT
 * Copyright:
   * Copyright 2012-2015 The Dojo Foundation <http://dojofoundation.org/>
   * Copyright 2009-2015 Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors
   * copyright 2009-2015 Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors <http://underscorejs.org/>

__lodash._isnative 2.4.1__
 * https://github.com/lodash/lodash-cli
 * License: MIT
 * Copyright:
   * Copyright 2012-2013 The Dojo Foundation <http://dojofoundation.org/>
   * Copyright 2009-2013 Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors
   * copyright 2009-2013 Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors <http://underscorejs.org/>

__lodash._objecttypes 2.4.1__
 * https://github.com/lodash/lodash-cli
 * License: MIT
 * Copyright:
   * Copyright 2012-2013 The Dojo Foundation <http://dojofoundation.org/>
   * Copyright 2009-2013 Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors
   * copyright 2009-2013 Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors <http://underscorejs.org/>

__lodash._reescape 3.0.0__
 * https://github.com/lodash/lodash
 * License: MIT
 * Copyright:
   * Copyright 2012-2015 The Dojo Foundation <http://dojofoundation.org/>
   * Copyright 2009-2015 Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors
   * copyright 2009-2015 Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors <http://underscorejs.org/>

__lodash._reevaluate 3.0.0__
 * https://github.com/lodash/lodash
 * License: MIT
 * Copyright:
   * Copyright 2012-2015 The Dojo Foundation <http://dojofoundation.org/>
   * Copyright 2009-2015 Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors
   * copyright 2009-2015 Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors <http://underscorejs.org/>

__lodash._reinterpolate 3.0.0__
 * https://github.com/lodash/lodash
 * License: MIT
 * Copyright:
   * Copyright 2012-2015 The Dojo Foundation <http://dojofoundation.org/>
   * Copyright 2009-2015 Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors
   * copyright 2009-2015 Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors <http://underscorejs.org/>

__lodash._reunescapedhtml 2.4.1__
 * https://github.com/lodash/lodash-cli
 * License: MIT
 * Copyright:
   * Copyright 2012-2013 The Dojo Foundation <http://dojofoundation.org/>
   * Copyright 2009-2013 Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors
   * copyright 2009-2013 Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors <http://underscorejs.org/>

__lodash._root 3.0.1__
 * https://github.com/lodash/lodash
 * License: MIT
 * Copyright:
   * Copyright 2009-2016 Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors
   * copyright 2009-2016 Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors <http://underscorejs.org/>
   * Copyright 2012-2016 The Dojo Foundation <http://dojofoundation.org/>

__lodash._shimkeys 2.4.1__
 * https://github.com/lodash/lodash-cli
 * License: MIT
 * Copyright:
   * Copyright 2012-2013 The Dojo Foundation <http://dojofoundation.org/>
   * Copyright 2009-2013 Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors
   * copyright 2009-2013 Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors <http://underscorejs.org/>

__lodash.assign 4.2.0__
 * https://github.com/lodash/lodash
 * License: MIT
 * Copyright:
   * Copyright Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors
   * Copyright jQuery Foundation and other contributors <https://jquery.org/>
   * copyright Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors <http://underscorejs.org/>

__lodash.clone 4.5.0__
 * https://github.com/lodash/lodash
 * License: MIT
 * Copyright:
   * Copyright Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors
   * Copyright jQuery Foundation and other contributors <https://jquery.org/>
   * copyright Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors <http://underscorejs.org/>

__lodash.defaults 4.2.0__
 * https://github.com/lodash/lodash
 * License: MIT
 * Copyright:
   * Copyright Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors
   * Copyright jQuery Foundation and other contributors <https://jquery.org/>
   * copyright Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors <http://underscorejs.org/>

__lodash.escape 3.2.0__
 * https://github.com/lodash/lodash
 * License: MIT
 * Copyright:
   * Copyright 2009-2016 Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors
   * copyright 2009-2016 Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors <http://underscorejs.org/>
   * Copyright 2012-2016 The Dojo Foundation <http://dojofoundation.org/>

__lodash.flatten 4.4.0__
 * https://github.com/lodash/lodash
 * License: MIT
 * Copyright:
   * Copyright Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors
   * Copyright jQuery Foundation and other contributors <https://jquery.org/>
   * copyright Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors <http://underscorejs.org/>

__lodash.isarguments 3.1.0__
 * https://github.com/lodash/lodash
 * License: MIT
 * Copyright:
   * Copyright Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors
   * Copyright jQuery Foundation and other contributors <https://jquery.org/>
   * copyright Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors <http://underscorejs.org/>

__lodash.isarray 3.0.4__
 * https://github.com/lodash/lodash
 * License: MIT
 * Copyright:
   * Copyright 2012-2015 The Dojo Foundation <http://dojofoundation.org/>
   * Copyright 2009-2015 Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors
   * copyright 2009-2015 Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors <http://underscorejs.org/>

__lodash.isfinite 3.3.2__
 * https://github.com/lodash/lodash
 * License: MIT
 * Copyright:
   * Copyright Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors
   * Copyright jQuery Foundation and other contributors <https://jquery.org/>
   * copyright Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors <http://underscorejs.org/>

__lodash.isobject 2.4.1__
 * https://github.com/lodash/lodash-cli
 * License: MIT
 * Copyright:
   * Copyright 2012-2013 The Dojo Foundation <http://dojofoundation.org/>
   * Copyright 2009-2013 Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors
   * copyright 2009-2013 Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors <http://underscorejs.org/>

__lodash.keys 3.1.2__
 * https://github.com/lodash/lodash
 * License: MIT
 * Copyright:
   * Copyright 2012-2015 The Dojo Foundation <http://dojofoundation.org/>
   * Copyright 2009-2015 Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors
   * copyright 2009-2015 Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors <http://underscorejs.org/>

__lodash.merge 4.6.2__
 * https://github.com/lodash/lodash
 * License: MIT
 * Copyright:
   * Copyright Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors
   * Copyright OpenJS Foundation and other contributors <https://openjsf.org/>
   * copyright Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors <http://underscorejs.org/>

__lodash.partialright 4.2.1__
 * https://github.com/lodash/lodash
 * License: MIT
 * Copyright:
   * Copyright Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors
   * Copyright jQuery Foundation and other contributors <https://jquery.org/>
   * copyright Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors <http://underscorejs.org/>

__lodash.pick 4.4.0__
 * https://github.com/lodash/lodash
 * License: MIT
 * Copyright:
   * Copyright Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors
   * Copyright jQuery Foundation and other contributors <https://jquery.org/>
   * copyright Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors <http://underscorejs.org/>

__lodash.restparam 3.6.1__
 * https://github.com/lodash/lodash
 * License: MIT
 * Copyright:
   * Copyright 2012-2015 The Dojo Foundation <http://dojofoundation.org/>
   * Copyright 2009-2015 Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors
   * copyright 2009-2015 Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors <http://underscorejs.org/>

__lodash.template 3.6.2__
 * https://github.com/lodash/lodash
 * License: MIT
 * Copyright:
   * Copyright 2012-2015 The Dojo Foundation <http://dojofoundation.org/>
   * Copyright 2009-2015 Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors
   * copyright 2009-2015 Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors <http://underscorejs.org/>

__lodash.templatesettings 3.1.1__
 * https://github.com/lodash/lodash
 * License: MIT
 * Copyright:
   * Copyright 2009-2016 Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors
   * copyright 2009-2016 Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors <http://underscorejs.org/>
   * Copyright 2012-2016 The Dojo Foundation <http://dojofoundation.org/>

__lodash.uniq 4.5.0__
 * https://github.com/lodash/lodash
 * License: MIT
 * Copyright:
   * Copyright Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors
   * Copyright jQuery Foundation and other contributors <https://jquery.org/>
   * copyright Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors <http://underscorejs.org/>

__lodash.values 2.4.1__
 * https://github.com/lodash/lodash-cli
 * License: MIT
 * Copyright:
   * Copyright 2012-2013 The Dojo Foundation <http://dojofoundation.org/>
   * Copyright 2009-2013 Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors
   * copyright 2009-2013 Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors <http://underscorejs.org/>

__lodash 4.17.21__
 * https://github.com/lodash/lodash
 * License: MIT
 * Copyright:
   * Copyright JS Foundation and other contributors <https://js.foundation/>

__longest 1.0.1__
 * https://github.com/jonschlinkert/longest
 * License: MIT
 * Copyright:
   * Copyright (c) 2015 Jon Schlinkert
   * Copyright (c) 2014-2015, Jon Schlinkert.

__loud-rejection 1.6.0__
 * https://github.com/sindresorhus/loud-rejection
 * License: MIT
 * Copyright:
   * Copyright (c) Sindre Sorhus <sindresorhus@gmail.com> (sindresorhus.com)

__lru-cache 2.7.3__
 * https://github.com/isaacs/node-lru-cache
 * License: ISC
 * Copyright:
   * Copyright (c) Isaac Z. Schlueter and Contributors

__main-bower-files 2.13.3__
 * https://github.com/ck86/main-bower-files
 * License: MIT
 * Copyright:
   * Copyright (c) 2013 Christopher Knotschke <cknoetschke@gmail.com>

__make-iterator 1.0.1__
 * https://github.com/jonschlinkert/make-iterator
 * License: MIT
 * Copyright:
   * Copyright (c) 2014-2018, Jon Schlinkert.
   * Copyright (c) 2012, 2013 moutjs team and contributors (http://moutjs.com)
   * Copyright (c) 2018, Jon Schlinkert (https://github.com/jonschlinkert).

__map-cache 0.2.2__
 * https://github.com/jonschlinkert/map-cache
 * License: MIT
 * Copyright:
   * Copyright (c) 2015-2016, Jon Schlinkert

__map-obj 1.0.1__
 * https://github.com/sindresorhus/map-obj
 * License: MIT
 * Copyright:
   * Copyright (c) Sindre Sorhus <sindresorhus@gmail.com> (sindresorhus.com)

__map-stream 0.1.0__
 * https://github.com/dominictarr/map-stream
 * License: MIT
 * Copyright:
   * Copyright (c) 2011 Dominic Tarr

__map-visit 1.0.0__
 * https://github.com/jonschlinkert/map-visit
 * License: MIT
 * Copyright:
   * Copyright (c) 2015-2017, Jon Schlinkert

__media-typer 0.3.0__
 * https://github.com/jshttp/media-typer
 * License: MIT
 * Copyright:
   * Copyright (c) 2014 Douglas Christopher Wilson

__meow 3.7.0__
 * https://github.com/sindresorhus/meow
 * License: MIT
 * Copyright:
   * Copyright (c) Sindre Sorhus <sindresorhus@gmail.com> (sindresorhus.com)

__micromatch 4.0.5__
 * https://github.com/micromatch/micromatch
 * License: MIT
 * Copyright:
   * Copyright (c) 2014-present, Jon Schlinkert

__mime-db 1.52.0__
 * https://github.com/jshttp/mime-db
 * License: MIT
 * Copyright:
   * Copyright (c) 2014 Jonathan Ong me@jongleberry.com

__mime-types 2.1.35__
 * https://github.com/jshttp/mime-types
 * License: MIT
 * Copyright:
   * Copyright (c) 2015 Douglas Christopher Wilson <doug@somethingdoug.com>
   * Copyright (c) 2014 Jonathan Ong <me@jongleberry.com>

__mime 1.4.1__
 * https://github.com/broofa/node-mime
 * License: MIT
 * Copyright:
   * Copyright (c) 2010 Benjamin Thomas, Robert Kieffer

__mini-lr 0.1.9__
 * https://github.com/elwayman02/mini-lr
 * License: MIT
 * Copyright:
   * Copyright (c) 2009-2014 TJ Holowaychuk <tj@vision-media.ca> https://raw.githubusercontent.com/visionmedia/express/master/lib/middleware/query.js
   * Copyright (c) 2012-2013 Mickael Daniel

__minimatch 3.1.2__
 * https://github.com/isaacs/minimatch
 * License: ISC
 * Copyright:
   * Copyright (c) Isaac Z. Schlueter and Contributors

__minimist 1.2.8__
 * https://github.com/minimistjs/minimist
 * License: MIT
 * Copyright:
   * Copyright (c) 2020 James Halliday

__mitt 1.2.0__
 * https://github.com/developit/mitt
 * License: MIT
 * Copyright:
   * (c) Jason Miller (https://jasonformat.com/)

__mixin-deep 1.3.2__
 * https://github.com/jonschlinkert/mixin-deep
 * License: MIT
 * Copyright:
   * Copyright (c) 2014-present, Jon Schlinkert.

__mkdirp 1.0.4__
 * https://github.com/isaacs/node-mkdirp
 * License: MIT
 * Copyright:
   * Copyright James Halliday (mail@substack.net) and Isaac Z. Schlueter (i@izs.me)

__ms 2.1.3__
 * https://github.com/vercel/ms
 * License: MIT
 * Copyright:
   * Copyright (c) 2020 Vercel, Inc.

__multimatch 2.1.0__
 * https://github.com/sindresorhus/multimatch
 * License: MIT
 * Copyright:
   * Copyright (c) Sindre Sorhus <sindresorhus@gmail.com> (https://sindresorhus.com)

__multipipe 0.1.2__
 * https://github.com/juliangruber/multipipe
 * License: MIT
 * Copyright:
   * Copyright (c) 2014 Julian Gruber <julian@juliangruber.com>
   * Copyright (c) 2014 Segment.io Inc. <friends@segment.io>

__nanomatch 1.2.13__
 * https://github.com/micromatch/nanomatch
 * License: MIT
 * Copyright:
   * Copyright (c) 2016-2018, Jon Schlinkert

__natives 1.1.6__
 * https://github.com/addaleax/natives
 * License: ISC
 * Copyright:
   * Copyright 2018 Isaac Z. Schlueter

__negotiator 0.6.3__
 * https://github.com/jshttp/negotiator
 * License: MIT
 * Copyright:
   * Copyright (c) 2012-2014 Federico Romero
   * Copyright (c) 2012-2014 Isaac Z. Schlueter
   * Copyright (c) 2014-2015 Douglas Christopher Wilson

__node-gyp-build 4.6.0__
 * https://github.com/prebuild/node-gyp-build
 * License: MIT
 * Copyright:
   * Copyright (c) 2017 Mathias Buus

__nopt 4.0.3__
 * https://github.com/npm/nopt
 * License: ISC
 * Copyright:
   * Copyright (c) Isaac Z. Schlueter and Contributors

__normalize-package-data 2.5.0__
 * https://github.com/npm/normalize-package-data
 * License: BSD-2-Clause
 * Copyright:
   * Copyright (c) Meryn Stol 'Author

__normalize-path 3.0.0__
 * https://github.com/jonschlinkert/normalize-path
 * License: MIT
 * Copyright:
   * Copyright (c) 2014-2018, Jon Schlinkert.

__now-and-later 2.0.1__
 * https://github.com/gulpjs/now-and-later
 * License: MIT
 * Copyright:
   * Copyright (c) 2014 Blaine Bublitz, Eric Schoffstall and other contributors

__npm-normalize-package-bin 1.0.1__
 * https://github.com/npm/npm-normalize-package-bin
 * License: ISC
 * Copyright:
   * Copyright (c) npm, Inc.

__object-assign 4.1.1__
 * https://github.com/sindresorhus/object-assign
 * License: MIT
 * Copyright:
   * Copyright (c) Sindre Sorhus <sindresorhus@gmail.com> (sindresorhus.com)

__object-copy 0.1.0__
 * https://github.com/jonschlinkert/object-copy
 * License: MIT
 * Copyright:
   * Copyright (c) 2016, Jon Schlinkert

__object-inspect 1.12.3__
 * https://github.com/inspect-js/object-inspect
 * License: MIT
 * Copyright:
   * Copyright (c) 2013 James Halliday

__object-keys 1.1.1__
 * https://github.com/ljharb/object-keys
 * License: MIT
 * Copyright:
   * Copyright (c) 2013 Jordan Harband

__object-visit 1.0.1__
 * https://github.com/jonschlinkert/object-visit
 * License: MIT
 * Copyright:
   * Copyright (c) 2015, 2017, Jon Schlinkert

__object.assign 4.1.4__
 * https://github.com/ljharb/object.assign
 * License: MIT
 * Copyright:
   * Copyright (c) 2014 Jordan Harband

__object.defaults 1.1.0__
 * https://github.com/jonschlinkert/object.defaults
 * License: MIT
 * Copyright:
   * Copyright (c) 2014-2015, 2017, Jon Schlinkert.
   * Copyright (c) 2017, Jon Schlinkert (https://github.com/jonschlinkert).

__object.map 1.0.1__
 * https://github.com/jonschlinkert/object.map
 * License: MIT
 * Copyright:
   * Copyright (c) 2014-2017, Jon Schlinkert.
   * Copyright (c) 2014-2017, Jon Schlinkert, contributors.
   * Copyright (c) 2017, Jon Schlinkert (https://github.com/jonschlinkert).

__object.pick 1.3.0__
 * https://github.com/jonschlinkert/object.pick
 * License: MIT
 * Copyright:
   * Copyright (c) 2014-2016, Jon Schlinkert

__on-finished 2.3.0__
 * https://github.com/jshttp/on-finished
 * License: MIT
 * Copyright:
   * Copyright (c) 2014 Douglas Christopher Wilson <doug@somethingdoug.com>
   * Copyright (c) 2013 Jonathan Ong <me@jongleberry.com>

__once 1.4.0__
 * https://github.com/isaacs/once
 * License: ISC
 * Copyright:
   * Copyright (c) Isaac Z. Schlueter and Contributors

__openurl 1.1.1__
 * https://github.com/rauschma/openurl
 * License: MIT
 * Copyright:
   * Axel Rauschmayer (axe@rauschma.de)

__opn 5.3.0__
 * https://github.com/sindresorhus/opn
 * License: MIT
 * Copyright:
   * Copyright (c) Sindre Sorhus <sindresorhus@gmail.com> (sindresorhus.com)

__orchestrator 0.3.8__
 * https://github.com/robrich/orchestrator
 * License: MIT
 * Copyright:
   * Copyright (c) 2013-2015 Richardson & Sons, LLC (http://richardsonandsons.com/)
   * Copyright (c) 2013 Richardson & Sons, LLC (http://richardsonandsons.com/)

__ordered-read-streams 1.0.1__
 * https://github.com/armed/ordered-read-streams
 * License: MIT
 * Copyright:
   * Copyright (c) 2014 Artem Medeusheyev

__os-homedir 1.0.2__
 * https://github.com/sindresorhus/os-homedir
 * License: MIT
 * Copyright:
   * Copyright (c) Sindre Sorhus <sindresorhus@gmail.com> (sindresorhus.com)

__os-tmpdir 1.0.2__
 * https://github.com/sindresorhus/os-tmpdir
 * License: MIT
 * Copyright:
   * Copyright (c) Sindre Sorhus <sindresorhus@gmail.com> (sindresorhus.com)

__osenv 0.1.5__
 * https://github.com/npm/osenv
 * License: ISC
 * Copyright:
   * Copyright (c) Isaac Z. Schlueter and Contributors

__parse-filepath 1.0.2__
 * https://github.com/jonschlinkert/parse-filepath
 * License: MIT
 * Copyright:
   * Copyright (c) 2014-2015, Jon Schlinkert.
   * Copyright (c) 2016, Jon Schlinkert (https://github.com/jonschlinkert).

__parse-json 2.2.0__
 * https://github.com/sindresorhus/parse-json
 * License: MIT
 * Copyright:
   * Copyright (c) Sindre Sorhus <sindresorhus@gmail.com> (https://sindresorhus.com)

__parse-node-version 1.0.1__
 * https://github.com/gulpjs/parse-node-version
 * License: MIT
 * Copyright:
   * Copyright (c) 2018 Blaine Bublitz <blaine.bublitz@gmail.com> and Eric Schoffstall <yo@contra.io>

__parse-passwd 1.0.0__
 * https://github.com/doowb/parse-passwd
 * License: MIT
 * Copyright:
   * Copyright (c) 2016, Brian Woodward (https://github.com/doowb)
   * Copyright (c) 2016 Brian Woodward

__parseurl 1.3.3__
 * https://github.com/pillarjs/parseurl
 * License: MIT
 * Copyright:
   * Copyright (c) 2014-2017 Douglas Christopher Wilson <doug@somethingdoug.com>
   * Copyright (c) 2014 Jonathan Ong <me@jongleberry.com>

__pascalcase 0.1.1__
 * https://github.com/jonschlinkert/pascalcase
 * License: MIT
 * Copyright:
   * Copyright (c) 2015, Jon Schlinkert

__path-dirname 1.0.2__
 * https://github.com/es128/path-dirname
 * License: MIT
 * Copyright:
   * Copyright (c) Elan Shanker and Node.js contributors

__path-exists 2.1.0__
 * https://github.com/sindresorhus/path-exists
 * License: MIT
 * Copyright:
   * Copyright (c) Sindre Sorhus <sindresorhus@gmail.com> (sindresorhus.com)

__path-is-absolute 1.0.1__
 * https://github.com/sindresorhus/path-is-absolute
 * License: MIT
 * Copyright:
   * Copyright (c) Sindre Sorhus <sindresorhus@gmail.com> (sindresorhus.com)

__path-parse 1.0.7__
 * https://github.com/jbgutierrez/path-parse
 * License: MIT
 * Copyright:
   * Copyright (c) 2015 Javier Blanco

__path-root-regex 0.1.2__
 * https://github.com/regexhq/path-root-regex
 * License: MIT
 * Copyright:
   * Copyright (c) 2016, Jon Schlinkert.
   * Copyright (c) 2016, Jon Schlinkert (https://github.com/jonschlinkert).

__path-root 0.1.1__
 * https://github.com/jonschlinkert/path-root
 * License: MIT
 * Copyright:
   * Copyright (c) 2016, Jon Schlinkert.
   * Copyright (c) 2016, Jon Schlinkert (https://github.com/jonschlinkert).

__path-type 1.1.0__
 * https://github.com/sindresorhus/path-type
 * License: MIT
 * Copyright:
   * Copyright (c) Sindre Sorhus <sindresorhus@gmail.com> (sindresorhus.com)

__pause-stream 0.0.11__
 * Apache2"
 * License: Apache-2.0
 * Copyright:
   * Copyright (c) 2013 Dominic Tarr

__picomatch 2.3.1__
 * https://github.com/micromatch/picomatch
 * License: MIT
 * Copyright:
   * Copyright (c) 2017-present, Jon Schlinkert.

__pify 2.3.0__
 * https://github.com/sindresorhus/pify
 * License: MIT
 * Copyright:
   * Copyright (c) Sindre Sorhus <sindresorhus@gmail.com> (sindresorhus.com)

__pinkie-promise 2.0.1__
 * https://github.com/floatdrop/pinkie-promise
 * License: MIT
 * Copyright:
   * Copyright (c) Vsevolod Strukchinsky <floatdrop@gmail.com>

__pinkie 2.0.4__
 * https://github.com/floatdrop/pinkie
 * License: MIT
 * Copyright:
   * Copyright (c) Vsevolod Strukchinsky <floatdrop@gmail.com>

__plugin-error 0.1.2__
 * https://github.com/jonschlinkert/plugin-error
 * License: MIT
 * Copyright:
   * Copyright (c) 2015, Jon Schlinkert

__portscanner 2.2.0__
 * https://github.com/baalexander/node-portscanner
 * License: MIT
 * Copyright:
   * Copyright (c) 2011 Brandon Ace Alexander

__posix-character-classes 0.1.1__
 * https://github.com/jonschlinkert/posix-character-classes
 * License: MIT
 * Copyright:
   * Copyright (c) 2016-2017, Jon Schlinkert

__pretty-hrtime 1.0.3__
 * https://github.com/robrich/pretty-hrtime
 * License: MIT
 * Copyright:
   * Copyright (c) 2013 Richardson & Sons, LLC (http://richardsonandsons.com/)

__process-nextick-args 2.0.1__
 * https://github.com/calvinmetcalf/process-nextick-args
 * License: MIT
 * Copyright:
   * Copyright (c) 2015 Calvin Metcalf

__pump 2.0.1__
 * https://github.com/mafintosh/pump
 * License: MIT
 * Copyright:
   * Copyright (c) 2014 Mathias Buus

__pumpify 1.5.1__
 * https://github.com/mafintosh/pumpify
 * License: MIT
 * Copyright:
   * Copyright (c) 2014 Mathias Buus

__qs 6.11.0__
 * https://github.com/ljharb/qs
 * License: BSD-3-Clause
 * Copyright:
   * Copyright (c) 2014 Nathan LaFreniere and other contributors.

__range-parser 1.2.1__
 * https://github.com/jshttp/range-parser
 * License: MIT
 * Copyright:
   * Copyright (c) 2015-2016 Douglas Christopher Wilson doug@somethingdoug.com
   * Copyright (c) 2012-2014 TJ Holowaychuk <tj@vision-media.ca>

__raw-body 2.5.2__
 * https://github.com/stream-utils/raw-body
 * License: MIT
 * Copyright:
   * Copyright (c) 2014-2015 Douglas Christopher Wilson <doug@somethingdoug.com>
   * Copyright (c) 2013-2014 Jonathan Ong <me@jongleberry.com>

__read-installed 4.0.3__
 * https://github.com/isaacs/read-installed
 * License: ISC
 * Copyright:
   * Copyright (c) Isaac Z. Schlueter

__read-package-json 2.1.2__
 * https://github.com/npm/read-package-json
 * License: ISC
 * Copyright:
   * Copyright (c) Isaac Z. Schlueter

__read-pkg-up 1.0.1__
 * https://github.com/sindresorhus/read-pkg-up
 * License: MIT
 * Copyright:
   * Copyright (c) Sindre Sorhus <sindresorhus@gmail.com> (sindresorhus.com)

__read-pkg 1.1.0__
 * https://github.com/sindresorhus/read-pkg
 * License: MIT
 * Copyright:
   * Copyright (c) Sindre Sorhus <sindresorhus@gmail.com> (sindresorhus.com)

__readable-stream 2.3.8__
 * https://github.com/nodejs/readable-stream
 * License: MIT
 * Copyright:
   * Copyright Joyent, Inc. and other Node contributors

__readdir-scoped-modules 1.1.0__
 * https://github.com/npm/readdir-scoped-modules
 * License: ISC
 * Copyright:
   * Copyright (c) Isaac Z. Schlueter and Contributors

__readdirp 3.6.0__
 * https://github.com/paulmillr/readdirp
 * License: MIT
 * Copyright:
   * Copyright (c) 2012-2019 Thorsten Lorenz, Paul Miller (https://paulmillr.com)

__rechoir 0.6.2__
 * https://github.com/tkellen/node-rechoir
 * License: MIT
 * Copyright:
   * Copyright (c) 2015 Tyler Kellen

__redent 1.0.0__
 * https://github.com/sindresorhus/redent
 * License: MIT
 * Copyright:
   * Copyright (c) Sindre Sorhus <sindresorhus@gmail.com> (sindresorhus.com)

__regex-not 1.0.2__
 * https://github.com/jonschlinkert/regex-not
 * License: MIT
 * Copyright:
   * Copyright (c) 2016, 2018, Jon Schlinkert

__remove-bom-buffer 3.0.0__
 * https://github.com/jonschlinkert/remove-bom-buffer
 * License: MIT
 * Copyright:
   * Copyright (c) 2015-2017, Jon Schlinkert.
   * Copyright (c) 2017, Jon Schlinkert (https://github.com/jonschlinkert).

__remove-bom-stream 1.2.0__
 * https://github.com/gulpjs/remove-bom-stream
 * License: MIT
 * Copyright:
   * Copyright (c) 2017 Blaine Bublitz <blaine.bublitz@gmail.com> , Eric Schoffstall <yo@contra.io> and other contributors

__remove-trailing-separator 1.1.0__
 * https://github.com/darsain/remove-trailing-separator
 * License: ISC
 * Copyright:
   * Copyright (c) 2017 Tomas Sardyha <darsain@gmail.com>

__repeat-element 1.1.4__
 * https://github.com/jonschlinkert/repeat-element
 * License: MIT
 * Copyright:
   * Copyright (c) 2015-present, Jon Schlinkert

__repeat-string 1.6.1__
 * https://github.com/jonschlinkert/repeat-string
 * License: MIT
 * Copyright:
   * Copyright (c) 2014-2016, Jon Schlinkert

__repeating 2.0.1__
 * https://github.com/sindresorhus/repeating
 * License: MIT
 * Copyright:
   * Copyright (c) Sindre Sorhus <sindresorhus@gmail.com> (sindresorhus.com)

__replace-ext 1.0.1__
 * https://github.com/gulpjs/replace-ext
 * License: MIT
 * Copyright:
   * Copyright (c) 2014 Blaine Bublitz <blaine.bublitz@gmail.com> , Eric Schoffstall <yo@contra.io>
            and other contributors

__require-directory 2.1.1__
 * https://github.com/troygoode/node-require-directory
 * License: MIT
 * Copyright:
   * Copyright (c) 2011 Troy Goode <troygoode@gmail.com>

__requires-port 1.0.0__
 * https://github.com/unshiftio/requires-port
 * License: MIT
 * Copyright:
   * Copyright (c) 2015 Unshift.io, Arnout Kazemier

__resolve-dir 1.0.1__
 * https://github.com/jonschlinkert/resolve-dir
 * License: MIT
 * Copyright:
   * Copyright (c) 2016, Jon Schlinkert (https://github.com/jonschlinkert)
   * Copyright (c) 2015, Jon Schlinkert
   * Copyright (c) 2015-2016, Jon Schlinkert

__resolve-options 1.1.0__
 * https://github.com/gulpjs/resolve-options
 * License: MIT
 * Copyright:
   * Copyright (c) 2017 Blaine Bublitz <blaine.bublitz@gmail.com> , Eric Schoffstall <yo@contra.io> and other contributors

__resolve-url 0.2.1__
 * https://github.com/lydell/resolve-url
 * License: MIT
 * Copyright:
   * Copyright (c) 2014 Simon Lydell

__resolve 1.22.1__
 * https://github.com/browserify/resolve
 * License: MIT
 * Copyright:
   * Copyright (c) 2012 James Halliday

__resp-modifier 6.0.2__
 * https://github.com/shakyshane/resp-modifier
 * License: MIT
 * Copyright:
   * Copyright (c) 2013 Shane Osbourne

__ret 0.1.15__
 * https://github.com/fent/ret.js
 * License: MIT
 * Copyright:
   * Copyright (c) 2011 by Roly Fentanes

__right-align 0.1.3__
 * https://github.com/jonschlinkert/right-align
 * License: MIT
 * Copyright:
   * Copyright (c) 2015 Jon Schlinkert
   * Copyright (c) 2015, Jon Schlinkert.

__rx 4.1.0__
 * https://github.com/Reactive-Extensions/RxJS
 * License: Apache-2.0
 * Copyright:
   * Copyright (c) Microsoft.
   * Microsoft Open Technologies would like to thank its contributors, a list of whom are at
            http://rx.codeplex.com/wikipage?title=Contributors.

__rxjs 5.5.12__
 * https://github.com/ReactiveX/RxJS
 * License: Apache-2.0
 * Copyright:
   * Copyright (c) 2015-2018 Google, Inc., Netflix, Inc., Microsoft Corp. and contributors

__safe-buffer 5.2.1__
 * https://github.com/feross/safe-buffer
 * License: MIT
 * Copyright:
   * Copyright (c) Feross Aboukhadijeh

__safe-regex 1.1.0__
 * https://github.com/substack/safe-regex
 * License: MIT
 * Copyright:
   * Copyright 2019-present is held by the authors of the safe-regex module

__safer-buffer 2.1.2__
 * https://github.com/ChALkeR/safer-buffer
 * License: MIT
 * Copyright:
   * Copyright (c) 2018 Nikita Skovoroda <chalkerx@gmail.com>

__sax 1.2.4__
 * https://github.com/isaacs/sax-js
 * License: ISC
 * Copyright:
   * Copyright (c) Isaac Z. Schlueter and Contributors
   * Copyright Mathias Bynens <https://mathiasbynens.be/>

__semver 6.3.0__
 * https://github.com/npm/node-semver
 * License: ISC
 * Copyright:
   * Copyright (c) Isaac Z. Schlueter and Contributors

__send 0.16.2__
 * https://github.com/pillarjs/send
 * License: MIT
 * Copyright:
   * Copyright (c) 2012 TJ Holowaychuk
   * Copyright (c) 2014-2022 Douglas Christopher Wilson

__sequencify 0.0.7__
 * https://github.com/robrich/sequencify
 * License: MIT
 * Copyright:
   * Copyright (c) 2013 Richardson & Sons, LLC (http://richardsonandsons.com/)

__serve-index 1.9.1__
 * https://github.com/expressjs/serve-index
 * License: MIT
 * Copyright:
   * Copyright (c) 2010 Sencha Inc.
   * Copyright (c) 2011 TJ Holowaychuk
   * Copyright (c) 2014-2015 Douglas Christopher Wilson
   * Copyright (c) 2011 LearnBoost

__serve-static 1.13.2__
 * https://github.com/expressjs/serve-static
 * License: MIT
 * Copyright:
   * Copyright (c) 2010 Sencha Inc.
   * Copyright (c) 2011 TJ Holowaychuk
   * Copyright (c) 2014-2016 Douglas Christopher Wilson
   * Copyright (c) 2011 LearnBoost

__server-destroy 1.0.1__
 * https://github.com/isaacs/server-destroy
 * License: ISC
 * Copyright:
   * Copyright (c) Isaac Z. Schlueter and Contributors

__set-value 2.0.1__
 * https://github.com/jonschlinkert/set-value
 * License: MIT
 * Copyright:
   * Copyright (c) 2014-2017, Jon Schlinkert

__setprototypeof 1.2.0__
 * https://github.com/wesleytodd/setprototypeof
 * License: ISC
 * Copyright:
   * Copyright (c) 2015, Wes Todd

__side-channel 1.0.4__
 * https://github.com/ljharb/side-channel
 * License: MIT
 * Copyright:
   * Copyright (c) 2019 Jordan Harband

__sigmund 1.0.1__
 * https://github.com/isaacs/sigmund
 * License: ISC
 * Copyright:
   * Copyright (c) Isaac Z. Schlueter and Contributors

__signal-exit 3.0.7__
 * https://github.com/tapjs/signal-exit
 * License: ISC
 * Copyright:
   * Copyright (c) 2015, Contributors

__slide 1.1.6__
 * https://github.com/isaacs/slide-flow-control
 * License: ISC
 * Copyright:
   * Copyright (c) Isaac Z. Schlueter

__snapdragon-node 2.1.1__
 * https://github.com/jonschlinkert/snapdragon-node
 * License: MIT
 * Copyright:
   * Copyright (c) 2017, Jon Schlinkert

__snapdragon-util 3.0.1__
 * https://github.com/jonschlinkert/snapdragon-util
 * License: MIT
 * Copyright:
   * Copyright (c) 2017, Jon Schlinkert

__snapdragon 0.8.2__
 * https://github.com/jonschlinkert/snapdragon
 * License: MIT
 * Copyright:
   * Copyright (c) 2015-2018, Jon Schlinkert

__socket.io-adapter 2.5.2__
 * https://github.com/socketio/socket.io-adapter
 * License: MIT
 * Copyright:
   * Copyright (c) 2014 Guillermo Rauch <guillermo@learnboost.com>

__socket.io-client 4.6.1__
 * https://github.com/socketio/socket.io-client
 * License: MIT
 * Copyright:
   * Copyright (c) 2014 Guillermo Rauch

__socket.io-parser 4.2.2__
 * https://github.com/socketio/socket.io-parser
 * License: MIT
 * Copyright:
   * Copyright (c) 2014 Guillermo Rauch <guillermo@learnboost.com>

__socket.io 4.6.1__
 * https://github.com/socketio/socket.io
 * License: MIT
 * Copyright:
   * Copyright (c) 2014-2018 Automattic <dev@cloudup.com>

__source-map-index-generator 0.1.2__
 * https://github.com/twolfson/source-map-index-generator
 * License: MIT
 * Copyright:
   * Copyright (c) 2013 Todd Wolfson

__source-map-resolve 0.5.3__
 * https://github.com/lydell/source-map-resolve
 * License: MIT
 * Copyright:
   * Copyright (c) 2019 ZHAO Jinxiang
   * Copyright (c) 2014, 2015, 2016, 2017, 2018, 2019 Simon Lydell

__source-map-url 0.4.1__
 * https://github.com/lydell/source-map-url
 * License: MIT
 * Copyright:
   * Copyright (c) 2014 Simon Lydell

__source-map 0.7.4__
 * https://github.com/mozilla/source-map
 * License: BSD-3-Clause
 * Copyright:
   * Copyright (c) 2009-2011, Mozilla Foundation and contributors

__sparkles 1.0.1__
 * https://github.com/gulpjs/sparkles
 * License: MIT
 * Copyright:
   * Copyright (c) 2014 Blaine Bublitz <blaine.bublitz@gmail.com> and Eric Schoffstall <yo@contra.io>

__spdx-compare 1.0.0__
 * https://github.com/kemitchell/spdx-compare.js
 * License: MIT
 * Copyright:
   * Copyright (c) 2015 Kyle E. Mitchell

__spdx-correct 3.1.1__
 * https://github.com/jslicense/spdx-correct.js
 * License: Apache-2.0
 * Copyright:
   * Copyright spdx-correct.js contributors

__spdx-exceptions 2.3.0__
 * https://github.com/kemitchell/spdx-exceptions.json
 * License: CC-BY-3.0
 * Copyright:
   * Copyright (c) 2010-2015 Linux Foundation and its Contributors

__spdx-expression-parse 3.0.1__
 * https://github.com/jslicense/spdx-expression-parse.js
 * License: MIT
 * Copyright:
   * Copyright (c) 2015 Kyle E. Mitchell

__spdx-license-ids 3.0.12__
 * https://github.com/jslicense/spdx-license-ids
 * License: Creative Commons Zero v1.0 Universal
 * Copyright:
   * Shinnosuke Watanabe (https://github.com/shinnn)

__spdx-ranges 2.1.1__
 * https://github.com/kemitchell/spdx-ranges.js
 * Licenses:
   * CC-BY-3.0
   * MIT
 * Copyright:
   * Copyright (c) 2015 Kyle E. Mitchell

__spdx-satisfies 4.0.1__
 * https://github.com/kemitchell/spdx-satisfies.js
 * License: MIT
 * Copyright:
   * Copyright (c) spdx-satisfies.js

__split-string 3.1.0__
 * https://github.com/jonschlinkert/split-string
 * License: MIT
 * Copyright:
   * Copyright (c) 2015-2017, Jon Schlinkert

__split 1.0.1__
 * https://github.com/dominictarr/split
 * License: MIT
 * Copyright:
   * Copyright (c) 2011 Dominic Tarr

__static-extend 0.1.2__
 * https://github.com/jonschlinkert/static-extend
 * License: MIT
 * Copyright:
   * Copyright (c) 2016, Jon Schlinkert

__statuses 2.0.1__
 * https://github.com/jshttp/statuses
 * License: MIT
 * Copyright:
   * Copyright (c) 2014 Jonathan Ong <me@jongleberry.com>
   * Copyright (c) 2016 Douglas Christopher Wilson <doug@somethingdoug.com>

__stream-combiner 0.2.2__
 * https://github.com/dominictarr/stream-combiner
 * License: MIT
 * Copyright:
   * Copyright (c) 2012 Dominic Tarr

__stream-consume 0.1.1__
 * https://github.com/aroneous/stream-consume
 * License: MIT
 * Copyright:
   * Copyright (c) 2014 Aron Nopanen

__stream-shift 1.0.1__
 * https://github.com/mafintosh/stream-shift
 * License: MIT
 * Copyright:
   * Copyright (c) 2016 Mathias Buus

__stream-throttle 0.1.3__
 * https://github.com/tjgq/node-stream-throttle
 * License: BSD-3-Clause
 * Copyright:
   * Copyright (c) 2013 Tiago Quelhas.

__string-width 4.2.3__
 * https://github.com/sindresorhus/string-width
 * License: MIT
 * Copyright:
   * Copyright (c) Sindre Sorhus <sindresorhus@gmail.com> (sindresorhus.com)

__string_decoder 1.1.1__
 * https://github.com/nodejs/string_decoder
 * License: MIT
 * Copyright:
   * Copyright Joyent, Inc. and other Node contributors

__strip-ansi 6.0.1__
 * https://github.com/chalk/strip-ansi
 * License: MIT
 * Copyright:
   * Copyright (c) Sindre Sorhus <sindresorhus@gmail.com> (sindresorhus.com)

__strip-bom 2.0.0__
 * https://github.com/sindresorhus/strip-bom
 * License: MIT
 * Copyright:
   * Copyright (c) Sindre Sorhus <sindresorhus@gmail.com> (sindresorhus.com)

__strip-indent 1.0.1__
 * https://github.com/sindresorhus/strip-indent
 * License: MIT
 * Copyright:
   * Copyright (c) Sindre Sorhus <sindresorhus@gmail.com> (sindresorhus.com)

__strip-json-comments 1.0.4__
 * https://github.com/sindresorhus/strip-json-comments
 * License: MIT
 * Copyright:
   * Copyright (c) Sindre Sorhus <sindresorhus@gmail.com> (sindresorhus.com)

__stylus 0.54.8__
 * https://github.com/stylus/stylus
 * License: MIT
 * Copyright:
   * Copyright (c) Automattic <developer.wordpress.com>

__supports-color 7.2.0__
 * https://github.com/chalk/supports-color
 * License: MIT
 * Copyright:
   * Copyright (c) Sindre Sorhus <sindresorhus@gmail.com> (sindresorhus.com)

__supports-preserve-symlinks-flag 1.0.0__
 * https://github.com/inspect-js/node-supports-preserve-symlinks-flag
 * License: MIT
 * Copyright:
   * Copyright (c) 2022 Inspect JS

__symbol-observable 1.0.1__
 * https://github.com/blesh/symbol-observable
 * License: MIT
 * Copyright:
   * Copyright (c) Sindre Sorhus <sindresorhus@gmail.com> (sindresorhus.com)
   * Copyright (c) Ben Lesh <ben@benlesh.com>

__temp-write 0.1.1__
 * https://github.com/sindresorhus/temp-write
 * License: MIT
 * Copyright:
   * (c) Sindre Sorhus (http://sindresorhus.com)

__tempfile 0.1.3__
 * https://github.com/sindresorhus/tempfile
 * License: MIT
 * Copyright:
   * (c) Sindre Sorhus (http://sindresorhus.com)

__through2-filter 3.0.0__
 * https://github.com/brycebaril/through2-filter
 * License: MIT
 * Copyright:
   * Copyright (c) Bryce B. Baril <bryce@ravenwall.com>

__through2 2.0.5__
 * https://github.com/rvagg/through2
 * License: MIT
 * Copyright:
   * Copyright (c) Rod Vagg (the "Original Author") and additional contributors

__through 2.3.8__
 * https://github.com/dominictarr/through
 * License: MIT
 * Copyright:
   * Copyright (c) 2011 Dominic Tarr

__tildify 1.2.0__
 * https://github.com/sindresorhus/tildify
 * License: MIT
 * Copyright:
   * (c) Sindre Sorhus (https://sindresorhus.com)
   * Copyright (c) Sindre Sorhus <sindresorhus@gmail.com> (sindresorhus.com)

__time-stamp 1.1.0__
 * https://github.com/jonschlinkert/time-stamp
 * License: MIT
 * Copyright:
   * Copyright (c) 2015-2017, Jon Schlinkert.

__to-absolute-glob 2.0.2__
 * https://github.com/jonschlinkert/to-absolute-glob
 * License: MIT
 * Copyright:
   * Copyright (c) 2016, Jon Schlinkert (https://github.com/jonschlinkert).
   * Copyright (c) 2015-2016, Jon Schlinkert

__to-object-path 0.3.0__
 * https://github.com/jonschlinkert/to-object-path
 * License: MIT
 * Copyright:
   * Copyright (c) 2015-2016, Jon Schlinkert

__to-regex-range 5.0.1__
 * https://github.com/micromatch/to-regex-range
 * License: MIT
 * Copyright:
   * Copyright (c) 2015-present, Jon Schlinkert
   * Copyright (c) 2019, Jon Schlinkert (https://github.com/jonschlinkert)

__to-regex 3.0.2__
 * https://github.com/jonschlinkert/to-regex
 * License: MIT
 * Copyright:
   * Copyright (c) 2016-2018, Jon Schlinkert

__to-through 2.0.0__
 * https://github.com/gulpjs/to-through
 * License: MIT
 * Copyright:
   * Copyright (c) 2017 Blaine Bublitz <blaine.bublitz@gmail.com> , Eric Schoffstall <yo@contra.io> and other contributors

__toidentifier 1.0.1__
 * https://github.com/component/toidentifier
 * License: MIT
 * Copyright:
   * Copyright (c) 2016 Douglas Christopher Wilson <doug@somethingdoug.com>

__treeify 1.1.0__
 * https://github.com/notatestuser/treeify
 * License: MIT
 * Copyright:
   * Copyright (c) 2012-2018 Luke Plaster <notatestuser@gmail.com>

__trim-newlines 1.0.0__
 * https://github.com/sindresorhus/trim-newlines
 * License: MIT
 * Copyright:
   * Copyright (c) Sindre Sorhus <sindresorhus@gmail.com> (sindresorhus.com)

__type-is 1.6.18__
 * https://github.com/jshttp/type-is
 * License: MIT
 * Copyright:
   * Copyright (c) 2014-2015 Douglas Christopher Wilson <doug@somethingdoug.com>
   * Copyright (c) 2014 Jonathan Ong <me@jongleberry.com>

__typescript 4.9.5__
 * https://github.com/Microsoft/TypeScript
 * License: Apache-2.0
 * Copyright:
   * Copyright (c) Microsoft Corporation.

__ua-parser-js 1.0.33__
 * https://github.com/faisalman/ua-parser-js
 * License: MIT
 * Copyright:
   * Copyright (c) 2012-2021 Faisal Salman <f@faisalman.com>
   * Copyright (c) 2012-2021 Faisal Salman

__uglify-js 2.8.29__
 * https://github.com/mishoo/UglifyJS2
 * License: BSD-2-Clause
 * Copyright:
   * Copyright 2012-2019 (c) Mihai Bazon <mihai.bazon@gmail.com>

__uglify-to-browserify 1.0.2__
 * https://github.com/ForbesLindesay/uglify-to-browserify
 * License: MIT
 * Copyright:
   * Copyright (c) 2013 Forbes Lindesay

__unc-path-regex 0.1.2__
 * https://github.com/regexhq/unc-path-regex
 * License: MIT
 * Copyright:
   * Copyright (c) 2015 Jon Schlinkert

__union-value 1.0.1__
 * https://github.com/jonschlinkert/union-value
 * License: MIT
 * Copyright:
   * Copyright (c) 2015-2017, Jon Schlinkert

__unique-stream 2.3.1__
 * https://github.com/eugeneware/unique-stream
 * License: MIT
 * Copyright:
   * Copyright 2014 Eugene Ware

__universalify 0.1.2__
 * https://github.com/RyanZim/universalify
 * License: MIT
 * Copyright:
   * Copyright (c) 2017, Ryan Zimmerman <opensrc@ryanzim.com>

__unpipe 1.0.0__
 * https://github.com/stream-utils/unpipe
 * License: MIT
 * Copyright:
   * Copyright (c) 2015 Douglas Christopher Wilson <doug@somethingdoug.com>

__unset-value 1.0.0__
 * https://github.com/jonschlinkert/unset-value
 * License: MIT
 * Copyright:
   * Copyright (c) 2015, 2017, Jon Schlinkert

__urix 0.1.0__
 * https://github.com/lydell/urix
 * License: MIT
 * Copyright:
   * Copyright (c) 2013 Simon Lydell

__use 3.1.1__
 * https://github.com/jonschlinkert/use
 * License: MIT
 * Copyright:
   * Copyright (c) 2015-present, Jon Schlinkert

__user-home 1.1.1__
 * https://github.com/sindresorhus/user-home
 * License: MIT
 * Copyright:
   * (c) Sindre Sorhus (http://sindresorhus.com)
   * Copyright (c) Sindre Sorhus <sindresorhus@gmail.com> (sindresorhus.com)

__utf-8-validate 5.0.10__
 * https://github.com/websockets/utf-8-validate
 * License: MIT
 * Copyright:
   * Copyright (c) 2011 Einar Otto Stangvik <einaros@gmail.com> (http://2x.io)

__util-deprecate 1.0.2__
 * https://github.com/TooTallNate/util-deprecate
 * License: MIT
 * Copyright:
   * Copyright (c) 2014 Nathan Rajlich <nathan@tootallnate.net>

__util-extend 1.0.3__
 * https://github.com/isaacs/util-extend
 * License: MIT
 * Copyright:
   * Copyright Joyent, Inc. and other Node contributors

__utils-merge 1.0.1__
 * https://github.com/jaredhanson/utils-merge
 * License: MIT
 * Copyright:
   * Copyright (c) 2013-2017 Jared Hanson < http://jaredhanson.net/ (http://jaredhanson.net/)>

__uuid 1.4.2__
 * https://github.com/shtylman/node-uuid
 * License: MIT
 * Copyright:
   * Copyright (c) 2010-2016 Robert Kieffer and other contributors

__v8flags 2.1.1__
 * https://github.com/tkellen/node-v8flags
 * License: MIT
 * Copyright:
   * Copyright (c) 2014 Tyler Kellen

__validate-npm-package-license 3.0.4__
 * https://github.com/kemitchell/validate-npm-package-license.js
 * License: Apache-2.0
 * Copyright:
   * Copyright 2019 Kyle Mitchell <kyle@kemitchell.com>

__value-or-function 3.0.0__
 * https://github.com/gulpjs/value-or-function
 * License: MIT
 * Copyright:
   * Copyright (c) 2015 Blaine Bublitz, Eric Schoffstall and other contributors

__vary 1.1.2__
 * https://github.com/jshttp/vary
 * License: MIT
 * Copyright:
   * Copyright (c) 2014-2017 Douglas Christopher Wilson

__vinyl-fs 3.0.3__
 * https://github.com/gulpjs/vinyl-fs
 * License: MIT
 * Copyright:
   * Copyright (c) 2013-2017 Blaine Bublitz <blaine.bublitz@gmail.com> , Eric Schoffstall <yo@contra.io> and other contributors

__vinyl-sourcemap 1.1.0__
 * https://github.com/gulpjs/vinyl-sourcemap
 * License: MIT
 * Copyright:
   * Copyright (c) 2014, Florian Reiterer
   * Copyright (c) 2017 Blaine Bublitz <blaine.bublitz@gmail.com> , Eric Schoffstall <yo@contra.io> and other contributors

__vinyl-sourcemaps-apply 0.2.1__
 * https://github.com/floridoo/vinyl-sourcemaps-apply
 * License: ISC
 * Copyright:
   * Florian Reiterer (me@florianreiterer.com)

__vinyl 2.2.1__
 * https://github.com/gulpjs/vinyl
 * License: MIT
 * Copyright:
   * Copyright (c) 2013 Blaine Bublitz <blaine.bublitz@gmail.com> , Eric Schoffstall <yo@contra.io> and other contributors

__websocket-driver 0.7.4__
 * https://github.com/faye/websocket-driver-node
 * License: Apache-2.0
 * Copyright:
   * Copyright 2010-2020 James Coglan

__websocket-extensions 0.1.4__
 * https://github.com/faye/websocket-extensions-node
 * License: Apache-2.0
 * Copyright:
   * Copyright 2014-2020 James Coglan

__when 3.7.8__
 * https://github.com/cujojs/when
 * License: MIT
 * Copyright:
   * Copyright (c) 2011 Brian Cavalier

__which 1.3.1__
 * https://github.com/isaacs/node-which
 * License: ISC
 * Copyright:
   * Copyright (c) Isaac Z. Schlueter and Contributors

__window-size 0.1.0__
 * https://github.com/jonschlinkert/window-size
 * License: MIT
 * Copyright:
   * Copyright (c) 2014 Jon Schlinkert

__wordwrap 0.0.2__
 * https://github.com/substack/node-wordwrap
 * License: MIT
 * Copyright:
   * Copyright (C) 2020 James Halliday

__wrap-ansi 7.0.0__
 * https://github.com/chalk/wrap-ansi
 * License: MIT
 * Copyright:
   * Copyright (c) Sindre Sorhus <sindresorhus@gmail.com> (sindresorhus.com)

__wrappy 1.0.2__
 * https://github.com/npm/wrappy
 * License: ISC
 * Copyright:
   * Copyright (c) Isaac Z. Schlueter and Contributors

__ws 8.11.0__
 * https://github.com/websockets/ws
 * License: MIT
 * Copyright:
   * Copyright (c) 2011 Einar Otto Stangvik <einaros@gmail.com>

__xmlhttprequest-ssl 2.0.0__
 * https://github.com/mjwwit/node-XMLHttpRequest
 * License: MIT
 * Copyright:
   * Copyright (c) 2010 passive.ly LLC

__xtend 4.0.2__
 * https://github.com/Raynos/xtend
 * License: MIT
 * Copyright:
   * Copyright (c) 2012-2014 Raynos

__y18n 5.0.8__
 * https://github.com/yargs/y18n
 * License: ISC
 * Copyright:
   * Copyright (c) 2015, Contributors

__yargs-parser 21.1.1__
 * https://github.com/yargs/yargs-parser
 * License: ISC
 * Copyright:
   * Copyright (c) 2016, Contributors

__yargs 3.10.0__
 * https://github.com/bcoe/yargs
 * License: MIT
 * Copyright:
   * Copyright 2010 James Halliday (mail@substack.net); Modified work Copyright 2014 Contributors
            (ben@npmjs.com)


______

__MIT License__

```
Copyright <YEAR> <COPYRIGHT HOLDER>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
```
______

__ISC License__

```
Copyright <YEAR> <OWNER>

Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee is hereby granted, provided that the above copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
```
______

__BSD-2-Clause__

```
Copyright <YEAR> <COPYRIGHT HOLDER>

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
```
______

__Apache License 2.0__

```
                                 Apache License
                           Version 2.0, January 2004
                        http://www.apache.org/licenses/

   TERMS AND CONDITIONS FOR USE, REPRODUCTION, AND DISTRIBUTION

   1. Definitions.

      "License" shall mean the terms and conditions for use, reproduction,
      and distribution as defined by Sections 1 through 9 of this document.

      "Licensor" shall mean the copyright owner or entity authorized by
      the copyright owner that is granting the License.

      "Legal Entity" shall mean the union of the acting entity and all
      other entities that control, are controlled by, or are under common
      control with that entity. For the purposes of this definition,
      "control" means (i) the power, direct or indirect, to cause the
      direction or management of such entity, whether by contract or
      otherwise, or (ii) ownership of fifty percent (50%) or more of the
      outstanding shares, or (iii) beneficial ownership of such entity.

      "You" (or "Your") shall mean an individual or Legal Entity
      exercising permissions granted by this License.

      "Source" form shall mean the preferred form for making modifications,
      including but not limited to software source code, documentation
      source, and configuration files.

      "Object" form shall mean any form resulting from mechanical
      transformation or translation of a Source form, including but
      not limited to compiled object code, generated documentation,
      and conversions to other media types.

      "Work" shall mean the work of authorship, whether in Source or
      Object form, made available under the License, as indicated by a
      copyright notice that is included in or attached to the work
      (an example is provided in the Appendix below).

      "Derivative Works" shall mean any work, whether in Source or Object
      form, that is based on (or derived from) the Work and for which the
      editorial revisions, annotations, elaborations, or other modifications
      represent, as a whole, an original work of authorship. For the purposes
      of this License, Derivative Works shall not include works that remain
      separable from, or merely link (or bind by name) to the interfaces of,
      the Work and Derivative Works thereof.

      "Contribution" shall mean any work of authorship, including
      the original version of the Work and any modifications or additions
      to that Work or Derivative Works thereof, that is intentionally
      submitted to Licensor for inclusion in the Work by the copyright owner
      or by an individual or Legal Entity authorized to submit on behalf of
      the copyright owner. For the purposes of this definition, "submitted"
      means any form of electronic, verbal, or written communication sent
      to the Licensor or its representatives, including but not limited to
      communication on electronic mailing lists, source code control systems,
      and issue tracking systems that are managed by, or on behalf of, the
      Licensor for the purpose of discussing and improving the Work, but
      excluding communication that is conspicuously marked or otherwise
      designated in writing by the copyright owner as "Not a Contribution."

      "Contributor" shall mean Licensor and any individual or Legal Entity
      on behalf of whom a Contribution has been received by Licensor and
      subsequently incorporated within the Work.

   2. Grant of Copyright License. Subject to the terms and conditions of
      this License, each Contributor hereby grants to You a perpetual,
      worldwide, non-exclusive, no-charge, royalty-free, irrevocable
      copyright license to reproduce, prepare Derivative Works of,
      publicly display, publicly perform, sublicense, and distribute the
      Work and such Derivative Works in Source or Object form.

   3. Grant of Patent License. Subject to the terms and conditions of
      this License, each Contributor hereby grants to You a perpetual,
      worldwide, non-exclusive, no-charge, royalty-free, irrevocable
      (except as stated in this section) patent license to make, have made,
      use, offer to sell, sell, import, and otherwise transfer the Work,
      where such license applies only to those patent claims licensable
      by such Contributor that are necessarily infringed by their
      Contribution(s) alone or by combination of their Contribution(s)
      with the Work to which such Contribution(s) was submitted. If You
      institute patent litigation against any entity (including a
      cross-claim or counterclaim in a lawsuit) alleging that the Work
      or a Contribution incorporated within the Work constitutes direct
      or contributory patent infringement, then any patent licenses
      granted to You under this License for that Work shall terminate
      as of the date such litigation is filed.

   4. Redistribution. You may reproduce and distribute copies of the
      Work or Derivative Works thereof in any medium, with or without
      modifications, and in Source or Object form, provided that You
      meet the following conditions:

      (a) You must give any other recipients of the Work or
          Derivative Works a copy of this License; and

      (b) You must cause any modified files to carry prominent notices
          stating that You changed the files; and

      (c) You must retain, in the Source form of any Derivative Works
          that You distribute, all copyright, patent, trademark, and
          attribution notices from the Source form of the Work,
          excluding those notices that do not pertain to any part of
          the Derivative Works; and

      (d) If the Work includes a "NOTICE" text file as part of its
          distribution, then any Derivative Works that You distribute must
          include a readable copy of the attribution notices contained
          within such NOTICE file, excluding those notices that do not
          pertain to any part of the Derivative Works, in at least one
          of the following places: within a NOTICE text file distributed
          as part of the Derivative Works; within the Source form or
          documentation, if provided along with the Derivative Works; or,
          within a display generated by the Derivative Works, if and
          wherever such third-party notices normally appear. The contents
          of the NOTICE file are for informational purposes only and
          do not modify the License. You may add Your own attribution
          notices within Derivative Works that You distribute, alongside
          or as an addendum to the NOTICE text from the Work, provided
          that such additional attribution notices cannot be construed
          as modifying the License.

      You may add Your own copyright statement to Your modifications and
      may provide additional or different license terms and conditions
      for use, reproduction, or distribution of Your modifications, or
      for any such Derivative Works as a whole, provided Your use,
      reproduction, and distribution of the Work otherwise complies with
      the conditions stated in this License.

   5. Submission of Contributions. Unless You explicitly state otherwise,
      any Contribution intentionally submitted for inclusion in the Work
      by You to the Licensor shall be under the terms and conditions of
      this License, without any additional terms or conditions.
      Notwithstanding the above, nothing herein shall supersede or modify
      the terms of any separate license agreement you may have executed
      with Licensor regarding such Contributions.

   6. Trademarks. This License does not grant permission to use the trade
      names, trademarks, service marks, or product names of the Licensor,
      except as required for reasonable and customary use in describing the
      origin of the Work and reproducing the content of the NOTICE file.

   7. Disclaimer of Warranty. Unless required by applicable law or
      agreed to in writing, Licensor provides the Work (and each
      Contributor provides its Contributions) on an "AS IS" BASIS,
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
      implied, including, without limitation, any warranties or conditions
      of TITLE, NON-INFRINGEMENT, MERCHANTABILITY, or FITNESS FOR A
      PARTICULAR PURPOSE. You are solely responsible for determining the
      appropriateness of using or redistributing the Work and assume any
      risks associated with Your exercise of permissions under this License.

   8. Limitation of Liability. In no event and under no legal theory,
      whether in tort (including negligence), contract, or otherwise,
      unless required by applicable law (such as deliberate and grossly
      negligent acts) or agreed to in writing, shall any Contributor be
      liable to You for damages, including any direct, indirect, special,
      incidental, or consequential damages of any character arising as a
      result of this License or out of the use or inability to use the
      Work (including but not limited to damages for loss of goodwill,
      work stoppage, computer failure or malfunction, or any and all
      other commercial damages or losses), even if such Contributor
      has been advised of the possibility of such damages.

   9. Accepting Warranty or Additional Liability. While redistributing
      the Work or Derivative Works thereof, You may choose to offer,
      and charge a fee for, acceptance of support, warranty, indemnity,
      or other liability obligations and/or rights consistent with this
      License. However, in accepting such obligations, You may act only
      on Your own behalf and on Your sole responsibility, not on behalf
      of any other Contributor, and only if You agree to indemnify,
      defend, and hold each Contributor harmless for any liability
      incurred by, or claims asserted against, such Contributor by reason
      of your accepting any such warranty or additional liability.

   END OF TERMS AND CONDITIONS

   APPENDIX: How to apply the Apache License to your work.

      To apply the Apache License to your work, attach the following
      boilerplate notice, with the fields enclosed by brackets "[]"
      replaced with your own identifying information. (Don't include
      the brackets!)  The text should be enclosed in the appropriate
      comment syntax for the file format. We also recommend that a
      file or class name and description of purpose be included on the
      same "printed page" as the copyright notice for easier
      identification within third-party archives.

   Copyright [yyyy] [name of copyright owner]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
```
______

__LGPL-2.1__

```
                  GNU LESSER GENERAL PUBLIC LICENSE
                       Version 2.1, February 1999

 Copyright (C) 1991, 1999 Free Software Foundation, Inc.
 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 Everyone is permitted to copy and distribute verbatim copies
 of this license document, but changing it is not allowed.

[This is the first released version of the Lesser GPL.  It also counts
 as the successor of the GNU Library Public License, version 2, hence
 the version number 2.1.]

                            Preamble

  The licenses for most software are designed to take away your
freedom to share and change it.  By contrast, the GNU General Public
Licenses are intended to guarantee your freedom to share and change
free software--to make sure the software is free for all its users.

  This license, the Lesser General Public License, applies to some
specially designated software packages--typically libraries--of the
Free Software Foundation and other authors who decide to use it.  You
can use it too, but we suggest you first think carefully about whether
this license or the ordinary General Public License is the better
strategy to use in any particular case, based on the explanations below.

  When we speak of free software, we are referring to freedom of use,
not price.  Our General Public Licenses are designed to make sure that
you have the freedom to distribute copies of free software (and charge
for this service if you wish); that you receive source code or can get
it if you want it; that you can change the software and use pieces of
it in new free programs; and that you are informed that you can do
these things.

  To protect your rights, we need to make restrictions that forbid
distributors to deny you these rights or to ask you to surrender these
rights.  These restrictions translate to certain responsibilities for
you if you distribute copies of the library or if you modify it.

  For example, if you distribute copies of the library, whether gratis
or for a fee, you must give the recipients all the rights that we gave
you.  You must make sure that they, too, receive or can get the source
code.  If you link other code with the library, you must provide
complete object files to the recipients, so that they can relink them
with the library after making changes to the library and recompiling
it.  And you must show them these terms so they know their rights.

  We protect your rights with a two-step method: (1) we copyright the
library, and (2) we offer you this license, which gives you legal
permission to copy, distribute and/or modify the library.

  To protect each distributor, we want to make it very clear that
there is no warranty for the free library.  Also, if the library is
modified by someone else and passed on, the recipients should know
that what they have is not the original version, so that the original
author's reputation will not be affected by problems that might be
introduced by others.

  Finally, software patents pose a constant threat to the existence of
any free program.  We wish to make sure that a company cannot
effectively restrict the users of a free program by obtaining a
restrictive license from a patent holder.  Therefore, we insist that
any patent license obtained for a version of the library must be
consistent with the full freedom of use specified in this license.

  Most GNU software, including some libraries, is covered by the
ordinary GNU General Public License.  This license, the GNU Lesser
General Public License, applies to certain designated libraries, and
is quite different from the ordinary General Public License.  We use
this license for certain libraries in order to permit linking those
libraries into non-free programs.

  When a program is linked with a library, whether statically or using
a shared library, the combination of the two is legally speaking a
combined work, a derivative of the original library.  The ordinary
General Public License therefore permits such linking only if the
entire combination fits its criteria of freedom.  The Lesser General
Public License permits more lax criteria for linking other code with
the library.

  We call this license the "Lesser" General Public License because it
does Less to protect the user's freedom than the ordinary General
Public License.  It also provides other free software developers Less
of an advantage over competing non-free programs.  These disadvantages
are the reason we use the ordinary General Public License for many
libraries.  However, the Lesser license provides advantages in certain
special circumstances.

  For example, on rare occasions, there may be a special need to
encourage the widest possible use of a certain library, so that it becomes
a de-facto standard.  To achieve this, non-free programs must be
allowed to use the library.  A more frequent case is that a free
library does the same job as widely used non-free libraries.  In this
case, there is little to gain by limiting the free library to free
software only, so we use the Lesser General Public License.

  In other cases, permission to use a particular library in non-free
programs enables a greater number of people to use a large body of
free software.  For example, permission to use the GNU C Library in
non-free programs enables many more people to use the whole GNU
operating system, as well as its variant, the GNU/Linux operating
system.

  Although the Lesser General Public License is Less protective of the
users' freedom, it does ensure that the user of a program that is
linked with the Library has the freedom and the wherewithal to run
that program using a modified version of the Library.

  The precise terms and conditions for copying, distribution and
modification follow.  Pay close attention to the difference between a
"work based on the library" and a "work that uses the library".  The
former contains code derived from the library, whereas the latter must
be combined with the library in order to run.

                  GNU LESSER GENERAL PUBLIC LICENSE
   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

  0. This License Agreement applies to any software library or other
program which contains a notice placed by the copyright holder or
other authorized party saying it may be distributed under the terms of
this Lesser General Public License (also called "this License").
Each licensee is addressed as "you".

  A "library" means a collection of software functions and/or data
prepared so as to be conveniently linked with application programs
(which use some of those functions and data) to form executables.

  The "Library", below, refers to any such software library or work
which has been distributed under these terms.  A "work based on the
Library" means either the Library or any derivative work under
copyright law: that is to say, a work containing the Library or a
portion of it, either verbatim or with modifications and/or translated
straightforwardly into another language.  (Hereinafter, translation is
included without limitation in the term "modification".)

  "Source code" for a work means the preferred form of the work for
making modifications to it.  For a library, complete source code means
all the source code for all modules it contains, plus any associated
interface definition files, plus the scripts used to control compilation
and installation of the library.

  Activities other than copying, distribution and modification are not
covered by this License; they are outside its scope.  The act of
running a program using the Library is not restricted, and output from
such a program is covered only if its contents constitute a work based
on the Library (independent of the use of the Library in a tool for
writing it).  Whether that is true depends on what the Library does
and what the program that uses the Library does.

  1. You may copy and distribute verbatim copies of the Library's
complete source code as you receive it, in any medium, provided that
you conspicuously and appropriately publish on each copy an
appropriate copyright notice and disclaimer of warranty; keep intact
all the notices that refer to this License and to the absence of any
warranty; and distribute a copy of this License along with the
Library.

  You may charge a fee for the physical act of transferring a copy,
and you may at your option offer warranty protection in exchange for a
fee.

  2. You may modify your copy or copies of the Library or any portion
of it, thus forming a work based on the Library, and copy and
distribute such modifications or work under the terms of Section 1
above, provided that you also meet all of these conditions:

    a) The modified work must itself be a software library.

    b) You must cause the files modified to carry prominent notices
    stating that you changed the files and the date of any change.

    c) You must cause the whole of the work to be licensed at no
    charge to all third parties under the terms of this License.

    d) If a facility in the modified Library refers to a function or a
    table of data to be supplied by an application program that uses
    the facility, other than as an argument passed when the facility
    is invoked, then you must make a good faith effort to ensure that,
    in the event an application does not supply such function or
    table, the facility still operates, and performs whatever part of
    its purpose remains meaningful.

    (For example, a function in a library to compute square roots has
    a purpose that is entirely well-defined independent of the
    application.  Therefore, Subsection 2d requires that any
    application-supplied function or table used by this function must
    be optional: if the application does not supply it, the square
    root function must still compute square roots.)

These requirements apply to the modified work as a whole.  If
identifiable sections of that work are not derived from the Library,
and can be reasonably considered independent and separate works in
themselves, then this License, and its terms, do not apply to those
sections when you distribute them as separate works.  But when you
distribute the same sections as part of a whole which is a work based
on the Library, the distribution of the whole must be on the terms of
this License, whose permissions for other licensees extend to the
entire whole, and thus to each and every part regardless of who wrote
it.

Thus, it is not the intent of this section to claim rights or contest
your rights to work written entirely by you; rather, the intent is to
exercise the right to control the distribution of derivative or
collective works based on the Library.

In addition, mere aggregation of another work not based on the Library
with the Library (or with a work based on the Library) on a volume of
a storage or distribution medium does not bring the other work under
the scope of this License.

  3. You may opt to apply the terms of the ordinary GNU General Public
License instead of this License to a given copy of the Library.  To do
this, you must alter all the notices that refer to this License, so
that they refer to the ordinary GNU General Public License, version 2,
instead of to this License.  (If a newer version than version 2 of the
ordinary GNU General Public License has appeared, then you can specify
that version instead if you wish.)  Do not make any other change in
these notices.

  Once this change is made in a given copy, it is irreversible for
that copy, so the ordinary GNU General Public License applies to all
subsequent copies and derivative works made from that copy.

  This option is useful when you wish to copy part of the code of
the Library into a program that is not a library.

  4. You may copy and distribute the Library (or a portion or
derivative of it, under Section 2) in object code or executable form
under the terms of Sections 1 and 2 above provided that you accompany
it with the complete corresponding machine-readable source code, which
must be distributed under the terms of Sections 1 and 2 above on a
medium customarily used for software interchange.

  If distribution of object code is made by offering access to copy
from a designated place, then offering equivalent access to copy the
source code from the same place satisfies the requirement to
distribute the source code, even though third parties are not
compelled to copy the source along with the object code.

  5. A program that contains no derivative of any portion of the
Library, but is designed to work with the Library by being compiled or
linked with it, is called a "work that uses the Library".  Such a
work, in isolation, is not a derivative work of the Library, and
therefore falls outside the scope of this License.

  However, linking a "work that uses the Library" with the Library
creates an executable that is a derivative of the Library (because it
contains portions of the Library), rather than a "work that uses the
library".  The executable is therefore covered by this License.
Section 6 states terms for distribution of such executables.

  When a "work that uses the Library" uses material from a header file
that is part of the Library, the object code for the work may be a
derivative work of the Library even though the source code is not.
Whether this is true is especially significant if the work can be
linked without the Library, or if the work is itself a library.  The
threshold for this to be true is not precisely defined by law.

  If such an object file uses only numerical parameters, data
structure layouts and accessors, and small macros and small inline
functions (ten lines or less in length), then the use of the object
file is unrestricted, regardless of whether it is legally a derivative
work.  (Executables containing this object code plus portions of the
Library will still fall under Section 6.)

  Otherwise, if the work is a derivative of the Library, you may
distribute the object code for the work under the terms of Section 6.
Any executables containing that work also fall under Section 6,
whether or not they are linked directly with the Library itself.

  6. As an exception to the Sections above, you may also combine or
link a "work that uses the Library" with the Library to produce a
work containing portions of the Library, and distribute that work
under terms of your choice, provided that the terms permit
modification of the work for the customer's own use and reverse
engineering for debugging such modifications.

  You must give prominent notice with each copy of the work that the
Library is used in it and that the Library and its use are covered by
this License.  You must supply a copy of this License.  If the work
during execution displays copyright notices, you must include the
copyright notice for the Library among them, as well as a reference
directing the user to the copy of this License.  Also, you must do one
of these things:

    a) Accompany the work with the complete corresponding
    machine-readable source code for the Library including whatever
    changes were used in the work (which must be distributed under
    Sections 1 and 2 above); and, if the work is an executable linked
    with the Library, with the complete machine-readable "work that
    uses the Library", as object code and/or source code, so that the
    user can modify the Library and then relink to produce a modified
    executable containing the modified Library.  (It is understood
    that the user who changes the contents of definitions files in the
    Library will not necessarily be able to recompile the application
    to use the modified definitions.)

    b) Use a suitable shared library mechanism for linking with the
    Library.  A suitable mechanism is one that (1) uses at run time a
    copy of the library already present on the user's computer system,
    rather than copying library functions into the executable, and (2)
    will operate properly with a modified version of the library, if
    the user installs one, as long as the modified version is
    interface-compatible with the version that the work was made with.

    c) Accompany the work with a written offer, valid for at
    least three years, to give the same user the materials
    specified in Subsection 6a, above, for a charge no more
    than the cost of performing this distribution.

    d) If distribution of the work is made by offering access to copy
    from a designated place, offer equivalent access to copy the above
    specified materials from the same place.

    e) Verify that the user has already received a copy of these
    materials or that you have already sent this user a copy.

  For an executable, the required form of the "work that uses the
Library" must include any data and utility programs needed for
reproducing the executable from it.  However, as a special exception,
the materials to be distributed need not include anything that is
normally distributed (in either source or binary form) with the major
components (compiler, kernel, and so on) of the operating system on
which the executable runs, unless that component itself accompanies
the executable.

  It may happen that this requirement contradicts the license
restrictions of other proprietary libraries that do not normally
accompany the operating system.  Such a contradiction means you cannot
use both them and the Library together in an executable that you
distribute.

  7. You may place library facilities that are a work based on the
Library side-by-side in a single library together with other library
facilities not covered by this License, and distribute such a combined
library, provided that the separate distribution of the work based on
the Library and of the other library facilities is otherwise
permitted, and provided that you do these two things:

    a) Accompany the combined library with a copy of the same work
    based on the Library, uncombined with any other library
    facilities.  This must be distributed under the terms of the
    Sections above.

    b) Give prominent notice with the combined library of the fact
    that part of it is a work based on the Library, and explaining
    where to find the accompanying uncombined form of the same work.

  8. You may not copy, modify, sublicense, link with, or distribute
the Library except as expressly provided under this License.  Any
attempt otherwise to copy, modify, sublicense, link with, or
distribute the Library is void, and will automatically terminate your
rights under this License.  However, parties who have received copies,
or rights, from you under this License will not have their licenses
terminated so long as such parties remain in full compliance.

  9. You are not required to accept this License, since you have not
signed it.  However, nothing else grants you permission to modify or
distribute the Library or its derivative works.  These actions are
prohibited by law if you do not accept this License.  Therefore, by
modifying or distributing the Library (or any work based on the
Library), you indicate your acceptance of this License to do so, and
all its terms and conditions for copying, distributing or modifying
the Library or works based on it.

  10. Each time you redistribute the Library (or any work based on the
Library), the recipient automatically receives a license from the
original licensor to copy, distribute, link with or modify the Library
subject to these terms and conditions.  You may not impose any further
restrictions on the recipients' exercise of the rights granted herein.
You are not responsible for enforcing compliance by third parties with
this License.

  11. If, as a consequence of a court judgment or allegation of patent
infringement or for any other reason (not limited to patent issues),
conditions are imposed on you (whether by court order, agreement or
otherwise) that contradict the conditions of this License, they do not
excuse you from the conditions of this License.  If you cannot
distribute so as to satisfy simultaneously your obligations under this
License and any other pertinent obligations, then as a consequence you
may not distribute the Library at all.  For example, if a patent
license would not permit royalty-free redistribution of the Library by
all those who receive copies directly or indirectly through you, then
the only way you could satisfy both it and this License would be to
refrain entirely from distribution of the Library.

If any portion of this section is held invalid or unenforceable under any
particular circumstance, the balance of the section is intended to apply,
and the section as a whole is intended to apply in other circumstances.

It is not the purpose of this section to induce you to infringe any
patents or other property right claims or to contest validity of any
such claims; this section has the sole purpose of protecting the
integrity of the free software distribution system which is
implemented by public license practices.  Many people have made
generous contributions to the wide range of software distributed
through that system in reliance on consistent application of that
system; it is up to the author/donor to decide if he or she is willing
to distribute software through any other system and a licensee cannot
impose that choice.

This section is intended to make thoroughly clear what is believed to
be a consequence of the rest of this License.

  12. If the distribution and/or use of the Library is restricted in
certain countries either by patents or by copyrighted interfaces, the
original copyright holder who places the Library under this License may add
an explicit geographical distribution limitation excluding those countries,
so that distribution is permitted only in or among countries not thus
excluded.  In such case, this License incorporates the limitation as if
written in the body of this License.

  13. The Free Software Foundation may publish revised and/or new
versions of the Lesser General Public License from time to time.
Such new versions will be similar in spirit to the present version,
but may differ in detail to address new problems or concerns.

Each version is given a distinguishing version number.  If the Library
specifies a version number of this License which applies to it and
"any later version", you have the option of following the terms and
conditions either of that version or of any later version published by
the Free Software Foundation.  If the Library does not specify a
license version number, you may choose any version ever published by
the Free Software Foundation.

  14. If you wish to incorporate parts of the Library into other free
programs whose distribution conditions are incompatible with these,
write to the author to ask for permission.  For software which is
copyrighted by the Free Software Foundation, write to the Free
Software Foundation; we sometimes make exceptions for this.  Our
decision will be guided by the two goals of preserving the free status
of all derivatives of our free software and of promoting the sharing
and reuse of software generally.

                            NO WARRANTY

  15. BECAUSE THE LIBRARY IS LICENSED FREE OF CHARGE, THERE IS NO
WARRANTY FOR THE LIBRARY, TO THE EXTENT PERMITTED BY APPLICABLE LAW.
EXCEPT WHEN OTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR
OTHER PARTIES PROVIDE THE LIBRARY "AS IS" WITHOUT WARRANTY OF ANY
KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
PURPOSE.  THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE
LIBRARY IS WITH YOU.  SHOULD THE LIBRARY PROVE DEFECTIVE, YOU ASSUME
THE COST OF ALL NECESSARY SERVICING, REPAIR OR CORRECTION.

  16. IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN
WRITING WILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MAY MODIFY
AND/OR REDISTRIBUTE THE LIBRARY AS PERMITTED ABOVE, BE LIABLE TO YOU
FOR DAMAGES, INCLUDING ANY GENERAL, SPECIAL, INCIDENTAL OR
CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OR INABILITY TO USE THE
LIBRARY (INCLUDING BUT NOT LIMITED TO LOSS OF DATA OR DATA BEING
RENDERED INACCURATE OR LOSSES SUSTAINED BY YOU OR THIRD PARTIES OR A
FAILURE OF THE LIBRARY TO OPERATE WITH ANY OTHER SOFTWARE), EVEN IF
SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH
DAMAGES.

                     END OF TERMS AND CONDITIONS

           How to Apply These Terms to Your New Libraries

  If you develop a new library, and you want it to be of the greatest
possible use to the public, we recommend making it free software that
everyone can redistribute and change.  You can do so by permitting
redistribution under these terms (or, alternatively, under the terms of the
ordinary General Public License).

  To apply these terms, attach the following notices to the library.  It is
safest to attach them to the start of each source file to most effectively
convey the exclusion of warranty; and each file should have at least the
"copyright" line and a pointer to where the full notice is found.

    <one line to give the library's name and a brief idea of what it does.>
    Copyright (C) <year>  <name of author>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA

Also add information on how to contact you by electronic and paper mail.

You should also get your employer (if you work as a programmer) or your
school, if any, to sign a "copyright disclaimer" for the library, if
necessary.  Here is a sample; alter the names:

  Yoyodyne, Inc., hereby disclaims all copyright interest in the
  library `Frob' (a library for tweaking knobs) written by James Random
  Hacker.

  <signature of Ty Coon>, 1 April 1990
  Ty Coon, President of Vice

That's all there is to it!
```
______

__MPL-1.1__

```
                          MOZILLA PUBLIC LICENSE
                                Version 1.1

                              ---------------

1. Definitions.

     1.0.1. "Commercial Use" means distribution or otherwise making the
     Covered Code available to a third party.

     1.1. "Contributor" means each entity that creates or contributes to
     the creation of Modifications.

     1.2. "Contributor Version" means the combination of the Original
     Code, prior Modifications used by a Contributor, and the Modifications
     made by that particular Contributor.

     1.3. "Covered Code" means the Original Code or Modifications or the
     combination of the Original Code and Modifications, in each case
     including portions thereof.

     1.4. "Electronic Distribution Mechanism" means a mechanism generally
     accepted in the software development community for the electronic
     transfer of data.

     1.5. "Executable" means Covered Code in any form other than Source
     Code.

     1.6. "Initial Developer" means the individual or entity identified
     as the Initial Developer in the Source Code notice required by Exhibit
     A.

     1.7. "Larger Work" means a work which combines Covered Code or
     portions thereof with code not governed by the terms of this License.

     1.8. "License" means this document.

     1.8.1. "Licensable" means having the right to grant, to the maximum
     extent possible, whether at the time of the initial grant or
     subsequently acquired, any and all of the rights conveyed herein.

     1.9. "Modifications" means any addition to or deletion from the
     substance or structure of either the Original Code or any previous
     Modifications. When Covered Code is released as a series of files, a
     Modification is:
          A. Any addition to or deletion from the contents of a file
          containing Original Code or previous Modifications.

          B. Any new file that contains any part of the Original Code or
          previous Modifications.

     1.10. "Original Code" means Source Code of computer software code
     which is described in the Source Code notice required by Exhibit A as
     Original Code, and which, at the time of its release under this
     License is not already Covered Code governed by this License.

     1.10.1. "Patent Claims" means any patent claim(s), now owned or
     hereafter acquired, including without limitation,  method, process,
     and apparatus claims, in any patent Licensable by grantor.

     1.11. "Source Code" means the preferred form of the Covered Code for
     making modifications to it, including all modules it contains, plus
     any associated interface definition files, scripts used to control
     compilation and installation of an Executable, or source code
     differential comparisons against either the Original Code or another
     well known, available Covered Code of the Contributor's choice. The
     Source Code can be in a compressed or archival form, provided the
     appropriate decompression or de-archiving software is widely available
     for no charge.

     1.12. "You" (or "Your")  means an individual or a legal entity
     exercising rights under, and complying with all of the terms of, this
     License or a future version of this License issued under Section 6.1.
     For legal entities, "You" includes any entity which controls, is
     controlled by, or is under common control with You. For purposes of
     this definition, "control" means (a) the power, direct or indirect,
     to cause the direction or management of such entity, whether by
     contract or otherwise, or (b) ownership of more than fifty percent
     (50%) of the outstanding shares or beneficial ownership of such
     entity.

2. Source Code License.

     2.1. The Initial Developer Grant.
     The Initial Developer hereby grants You a world-wide, royalty-free,
     non-exclusive license, subject to third party intellectual property
     claims:
          (a)  under intellectual property rights (other than patent or
          trademark) Licensable by Initial Developer to use, reproduce,
          modify, display, perform, sublicense and distribute the Original
          Code (or portions thereof) with or without Modifications, and/or
          as part of a Larger Work; and

          (b) under Patents Claims infringed by the making, using or
          selling of Original Code, to make, have made, use, practice,
          sell, and offer for sale, and/or otherwise dispose of the
          Original Code (or portions thereof).

          (c) the licenses granted in this Section 2.1(a) and (b) are
          effective on the date Initial Developer first distributes
          Original Code under the terms of this License.

          (d) Notwithstanding Section 2.1(b) above, no patent license is
          granted: 1) for code that You delete from the Original Code; 2)
          separate from the Original Code;  or 3) for infringements caused
          by: i) the modification of the Original Code or ii) the
          combination of the Original Code with other software or devices.

     2.2. Contributor Grant.
     Subject to third party intellectual property claims, each Contributor
     hereby grants You a world-wide, royalty-free, non-exclusive license

          (a)  under intellectual property rights (other than patent or
          trademark) Licensable by Contributor, to use, reproduce, modify,
          display, perform, sublicense and distribute the Modifications
          created by such Contributor (or portions thereof) either on an
          unmodified basis, with other Modifications, as Covered Code
          and/or as part of a Larger Work; and

          (b) under Patent Claims infringed by the making, using, or
          selling of  Modifications made by that Contributor either alone
          and/or in combination with its Contributor Version (or portions
          of such combination), to make, use, sell, offer for sale, have
          made, and/or otherwise dispose of: 1) Modifications made by that
          Contributor (or portions thereof); and 2) the combination of
          Modifications made by that Contributor with its Contributor
          Version (or portions of such combination).

          (c) the licenses granted in Sections 2.2(a) and 2.2(b) are
          effective on the date Contributor first makes Commercial Use of
          the Covered Code.

          (d)    Notwithstanding Section 2.2(b) above, no patent license is
          granted: 1) for any code that Contributor has deleted from the
          Contributor Version; 2)  separate from the Contributor Version;
          3)  for infringements caused by: i) third party modifications of
          Contributor Version or ii)  the combination of Modifications made
          by that Contributor with other software  (except as part of the
          Contributor Version) or other devices; or 4) under Patent Claims
          infringed by Covered Code in the absence of Modifications made by
          that Contributor.

3. Distribution Obligations.

     3.1. Application of License.
     The Modifications which You create or to which You contribute are
     governed by the terms of this License, including without limitation
     Section 2.2. The Source Code version of Covered Code may be
     distributed only under the terms of this License or a future version
     of this License released under Section 6.1, and You must include a
     copy of this License with every copy of the Source Code You
     distribute. You may not offer or impose any terms on any Source Code
     version that alters or restricts the applicable version of this
     License or the recipients' rights hereunder. However, You may include
     an additional document offering the additional rights described in
     Section 3.5.

     3.2. Availability of Source Code.
     Any Modification which You create or to which You contribute must be
     made available in Source Code form under the terms of this License
     either on the same media as an Executable version or via an accepted
     Electronic Distribution Mechanism to anyone to whom you made an
     Executable version available; and if made available via Electronic
     Distribution Mechanism, must remain available for at least twelve (12)
     months after the date it initially became available, or at least six
     (6) months after a subsequent version of that particular Modification
     has been made available to such recipients. You are responsible for
     ensuring that the Source Code version remains available even if the
     Electronic Distribution Mechanism is maintained by a third party.

     3.3. Description of Modifications.
     You must cause all Covered Code to which You contribute to contain a
     file documenting the changes You made to create that Covered Code and
     the date of any change. You must include a prominent statement that
     the Modification is derived, directly or indirectly, from Original
     Code provided by the Initial Developer and including the name of the
     Initial Developer in (a) the Source Code, and (b) in any notice in an
     Executable version or related documentation in which You describe the
     origin or ownership of the Covered Code.

     3.4. Intellectual Property Matters
          (a) Third Party Claims.
          If Contributor has knowledge that a license under a third party's
          intellectual property rights is required to exercise the rights
          granted by such Contributor under Sections 2.1 or 2.2,
          Contributor must include a text file with the Source Code
          distribution titled "LEGAL" which describes the claim and the
          party making the claim in sufficient detail that a recipient will
          know whom to contact. If Contributor obtains such knowledge after
          the Modification is made available as described in Section 3.2,
          Contributor shall promptly modify the LEGAL file in all copies
          Contributor makes available thereafter and shall take other steps
          (such as notifying appropriate mailing lists or newsgroups)
          reasonably calculated to inform those who received the Covered
          Code that new knowledge has been obtained.

          (b) Contributor APIs.
          If Contributor's Modifications include an application programming
          interface and Contributor has knowledge of patent licenses which
          are reasonably necessary to implement that API, Contributor must
          also include this information in the LEGAL file.

               (c)    Representations.
          Contributor represents that, except as disclosed pursuant to
          Section 3.4(a) above, Contributor believes that Contributor's
          Modifications are Contributor's original creation(s) and/or
          Contributor has sufficient rights to grant the rights conveyed by
          this License.

     3.5. Required Notices.
     You must duplicate the notice in Exhibit A in each file of the Source
     Code.  If it is not possible to put such notice in a particular Source
     Code file due to its structure, then You must include such notice in a
     location (such as a relevant directory) where a user would be likely
     to look for such a notice.  If You created one or more Modification(s)
     You may add your name as a Contributor to the notice described in
     Exhibit A.  You must also duplicate this License in any documentation
     for the Source Code where You describe recipients' rights or ownership
     rights relating to Covered Code.  You may choose to offer, and to
     charge a fee for, warranty, support, indemnity or liability
     obligations to one or more recipients of Covered Code. However, You
     may do so only on Your own behalf, and not on behalf of the Initial
     Developer or any Contributor. You must make it absolutely clear than
     any such warranty, support, indemnity or liability obligation is
     offered by You alone, and You hereby agree to indemnify the Initial
     Developer and every Contributor for any liability incurred by the
     Initial Developer or such Contributor as a result of warranty,
     support, indemnity or liability terms You offer.

     3.6. Distribution of Executable Versions.
     You may distribute Covered Code in Executable form only if the
     requirements of Section 3.1-3.5 have been met for that Covered Code,
     and if You include a notice stating that the Source Code version of
     the Covered Code is available under the terms of this License,
     including a description of how and where You have fulfilled the
     obligations of Section 3.2. The notice must be conspicuously included
     in any notice in an Executable version, related documentation or
     collateral in which You describe recipients' rights relating to the
     Covered Code. You may distribute the Executable version of Covered
     Code or ownership rights under a license of Your choice, which may
     contain terms different from this License, provided that You are in
     compliance with the terms of this License and that the license for the
     Executable version does not attempt to limit or alter the recipient's
     rights in the Source Code version from the rights set forth in this
     License. If You distribute the Executable version under a different
     license You must make it absolutely clear that any terms which differ
     from this License are offered by You alone, not by the Initial
     Developer or any Contributor. You hereby agree to indemnify the
     Initial Developer and every Contributor for any liability incurred by
     the Initial Developer or such Contributor as a result of any such
     terms You offer.

     3.7. Larger Works.
     You may create a Larger Work by combining Covered Code with other code
     not governed by the terms of this License and distribute the Larger
     Work as a single product. In such a case, You must make sure the
     requirements of this License are fulfilled for the Covered Code.

4. Inability to Comply Due to Statute or Regulation.

     If it is impossible for You to comply with any of the terms of this
     License with respect to some or all of the Covered Code due to
     statute, judicial order, or regulation then You must: (a) comply with
     the terms of this License to the maximum extent possible; and (b)
     describe the limitations and the code they affect. Such description
     must be included in the LEGAL file described in Section 3.4 and must
     be included with all distributions of the Source Code. Except to the
     extent prohibited by statute or regulation, such description must be
     sufficiently detailed for a recipient of ordinary skill to be able to
     understand it.

5. Application of this License.

     This License applies to code to which the Initial Developer has
     attached the notice in Exhibit A and to related Covered Code.

6. Versions of the License.

     6.1. New Versions.
     Netscape Communications Corporation ("Netscape") may publish revised
     and/or new versions of the License from time to time. Each version
     will be given a distinguishing version number.

     6.2. Effect of New Versions.
     Once Covered Code has been published under a particular version of the
     License, You may always continue to use it under the terms of that
     version. You may also choose to use such Covered Code under the terms
     of any subsequent version of the License published by Netscape. No one
     other than Netscape has the right to modify the terms applicable to
     Covered Code created under this License.

     6.3. Derivative Works.
     If You create or use a modified version of this License (which you may
     only do in order to apply it to code which is not already Covered Code
     governed by this License), You must (a) rename Your license so that
     the phrases "Mozilla", "MOZILLAPL", "MOZPL", "Netscape",
     "MPL", "NPL" or any confusingly similar phrase do not appear in your
     license (except to note that your license differs from this License)
     and (b) otherwise make it clear that Your version of the license
     contains terms which differ from the Mozilla Public License and
     Netscape Public License. (Filling in the name of the Initial
     Developer, Original Code or Contributor in the notice described in
     Exhibit A shall not of themselves be deemed to be modifications of
     this License.)

7. DISCLAIMER OF WARRANTY.

     COVERED CODE IS PROVIDED UNDER THIS LICENSE ON AN "AS IS" BASIS,
     WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
     WITHOUT LIMITATION, WARRANTIES THAT THE COVERED CODE IS FREE OF
     DEFECTS, MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE OR NON-INFRINGING.
     THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE COVERED CODE
     IS WITH YOU. SHOULD ANY COVERED CODE PROVE DEFECTIVE IN ANY RESPECT,
     YOU (NOT THE INITIAL DEVELOPER OR ANY OTHER CONTRIBUTOR) ASSUME THE
     COST OF ANY NECESSARY SERVICING, REPAIR OR CORRECTION. THIS DISCLAIMER
     OF WARRANTY CONSTITUTES AN ESSENTIAL PART OF THIS LICENSE. NO USE OF
     ANY COVERED CODE IS AUTHORIZED HEREUNDER EXCEPT UNDER THIS DISCLAIMER.

8. TERMINATION.

     8.1.  This License and the rights granted hereunder will terminate
     automatically if You fail to comply with terms herein and fail to cure
     such breach within 30 days of becoming aware of the breach. All
     sublicenses to the Covered Code which are properly granted shall
     survive any termination of this License. Provisions which, by their
     nature, must remain in effect beyond the termination of this License
     shall survive.

     8.2.  If You initiate litigation by asserting a patent infringement
     claim (excluding declatory judgment actions) against Initial Developer
     or a Contributor (the Initial Developer or Contributor against whom
     You file such action is referred to as "Participant")  alleging that:

     (a)  such Participant's Contributor Version directly or indirectly
     infringes any patent, then any and all rights granted by such
     Participant to You under Sections 2.1 and/or 2.2 of this License
     shall, upon 60 days notice from Participant terminate prospectively,
     unless if within 60 days after receipt of notice You either: (i)
     agree in writing to pay Participant a mutually agreeable reasonable
     royalty for Your past and future use of Modifications made by such
     Participant, or (ii) withdraw Your litigation claim with respect to
     the Contributor Version against such Participant.  If within 60 days
     of notice, a reasonable royalty and payment arrangement are not
     mutually agreed upon in writing by the parties or the litigation claim
     is not withdrawn, the rights granted by Participant to You under
     Sections 2.1 and/or 2.2 automatically terminate at the expiration of
     the 60 day notice period specified above.

     (b)  any software, hardware, or device, other than such Participant's
     Contributor Version, directly or indirectly infringes any patent, then
     any rights granted to You by such Participant under Sections 2.1(b)
     and 2.2(b) are revoked effective as of the date You first made, used,
     sold, distributed, or had made, Modifications made by that
     Participant.

     8.3.  If You assert a patent infringement claim against Participant
     alleging that such Participant's Contributor Version directly or
     indirectly infringes any patent where such claim is resolved (such as
     by license or settlement) prior to the initiation of patent
     infringement litigation, then the reasonable value of the licenses
     granted by such Participant under Sections 2.1 or 2.2 shall be taken
     into account in determining the amount or value of any payment or
     license.

     8.4.  In the event of termination under Sections 8.1 or 8.2 above,
     all end user license agreements (excluding distributors and resellers)
     which have been validly granted by You or any distributor hereunder
     prior to termination shall survive termination.

9. LIMITATION OF LIABILITY.

     UNDER NO CIRCUMSTANCES AND UNDER NO LEGAL THEORY, WHETHER TORT
     (INCLUDING NEGLIGENCE), CONTRACT, OR OTHERWISE, SHALL YOU, THE INITIAL
     DEVELOPER, ANY OTHER CONTRIBUTOR, OR ANY DISTRIBUTOR OF COVERED CODE,
     OR ANY SUPPLIER OF ANY OF SUCH PARTIES, BE LIABLE TO ANY PERSON FOR
     ANY INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES OF ANY
     CHARACTER INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF GOODWILL,
     WORK STOPPAGE, COMPUTER FAILURE OR MALFUNCTION, OR ANY AND ALL OTHER
     COMMERCIAL DAMAGES OR LOSSES, EVEN IF SUCH PARTY SHALL HAVE BEEN
     INFORMED OF THE POSSIBILITY OF SUCH DAMAGES. THIS LIMITATION OF
     LIABILITY SHALL NOT APPLY TO LIABILITY FOR DEATH OR PERSONAL INJURY
     RESULTING FROM SUCH PARTY'S NEGLIGENCE TO THE EXTENT APPLICABLE LAW
     PROHIBITS SUCH LIMITATION. SOME JURISDICTIONS DO NOT ALLOW THE
     EXCLUSION OR LIMITATION OF INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO
     THIS EXCLUSION AND LIMITATION MAY NOT APPLY TO YOU.

10. U.S. GOVERNMENT END USERS.

     The Covered Code is a "commercial item," as that term is defined in
     48 C.F.R. 2.101 (Oct. 1995), consisting of "commercial computer
     software" and "commercial computer software documentation," as such
     terms are used in 48 C.F.R. 12.212 (Sept. 1995). Consistent with 48
     C.F.R. 12.212 and 48 C.F.R. 227.7202-1 through 227.7202-4 (June 1995),
     all U.S. Government End Users acquire Covered Code with only those
     rights set forth herein.

11. MISCELLANEOUS.

     This License represents the complete agreement concerning subject
     matter hereof. If any provision of this License is held to be
     unenforceable, such provision shall be reformed only to the extent
     necessary to make it enforceable. This License shall be governed by
     California law provisions (except to the extent applicable law, if
     any, provides otherwise), excluding its conflict-of-law provisions.
     With respect to disputes in which at least one party is a citizen of,
     or an entity chartered or registered to do business in the United
     States of America, any litigation relating to this License shall be
     subject to the jurisdiction of the Federal Courts of the Northern
     District of California, with venue lying in Santa Clara County,
     California, with the losing party responsible for costs, including
     without limitation, court costs and reasonable attorneys' fees and
     expenses. The application of the United Nations Convention on
     Contracts for the International Sale of Goods is expressly excluded.
     Any law or regulation which provides that the language of a contract
     shall be construed against the drafter shall not apply to this
     License.

12. RESPONSIBILITY FOR CLAIMS.

     As between Initial Developer and the Contributors, each party is
     responsible for claims and damages arising, directly or indirectly,
     out of its utilization of rights under this License and You agree to
     work with Initial Developer and Contributors to distribute such
     responsibility on an equitable basis. Nothing herein is intended or
     shall be deemed to constitute any admission of liability.

13. MULTIPLE-LICENSED CODE.

     Initial Developer may designate portions of the Covered Code as
     "Multiple-Licensed".  "Multiple-Licensed" means that the Initial
     Developer permits you to utilize portions of the Covered Code under
     Your choice of the MPL or the alternative licenses, if any, specified
     by the Initial Developer in the file described in Exhibit A.

EXHIBIT A -Mozilla Public License.

     ``The contents of this file are subject to the Mozilla Public License
     Version 1.1 (the "License"); you may not use this file except in
     compliance with the License. You may obtain a copy of the License at
     https://www.mozilla.org/MPL/

     Software distributed under the License is distributed on an "AS IS"
     basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
     License for the specific language governing rights and limitations
     under the License.

     The Original Code is ______________________________________.

     The Initial Developer of the Original Code is ________________________.
     Portions created by ______________________ are Copyright (C) ______
     _______________________. All Rights Reserved.

     Contributor(s): ______________________________________.

     Alternatively, the contents of this file may be used under the terms
     of the _____ license (the  "[___] License"), in which case the
     provisions of [______] License are applicable instead of those
     above.  If you wish to allow use of your version of this file only
     under the terms of the [____] License and not to allow others to use
     your version of this file under the MPL, indicate your decision by
     deleting  the provisions above and replace  them with the notice and
     other provisions required by the [___] License.  If you do not delete
     the provisions above, a recipient may use your version of this file
     under either the MPL or the [___] License."

     [NOTE: The text of this Exhibit A may differ slightly from the text of
     the notices in the Source Code files of the Original Code. You should
     use the text of this Exhibit A rather than the text found in the
     Original Code Source Code for Your Modifications.]
```
______

__GPL-2.0__

```
                    GNU GENERAL PUBLIC LICENSE
                       Version 2, June 1991

 Copyright (C) 1989, 1991 Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 Everyone is permitted to copy and distribute verbatim copies
 of this license document, but changing it is not allowed.

                            Preamble

  The licenses for most software are designed to take away your
freedom to share and change it.  By contrast, the GNU General Public
License is intended to guarantee your freedom to share and change free
software--to make sure the software is free for all its users.  This
General Public License applies to most of the Free Software
Foundation's software and to any other program whose authors commit to
using it.  (Some other Free Software Foundation software is covered by
the GNU Lesser General Public License instead.)  You can apply it to
your programs, too.

  When we speak of free software, we are referring to freedom, not
price.  Our General Public Licenses are designed to make sure that you
have the freedom to distribute copies of free software (and charge for
this service if you wish), that you receive source code or can get it
if you want it, that you can change the software or use pieces of it
in new free programs; and that you know you can do these things.

  To protect your rights, we need to make restrictions that forbid
anyone to deny you these rights or to ask you to surrender the rights.
These restrictions translate to certain responsibilities for you if you
distribute copies of the software, or if you modify it.

  For example, if you distribute copies of such a program, whether
gratis or for a fee, you must give the recipients all the rights that
you have.  You must make sure that they, too, receive or can get the
source code.  And you must show them these terms so they know their
rights.

  We protect your rights with two steps: (1) copyright the software, and
(2) offer you this license which gives you legal permission to copy,
distribute and/or modify the software.

  Also, for each author's protection and ours, we want to make certain
that everyone understands that there is no warranty for this free
software.  If the software is modified by someone else and passed on, we
want its recipients to know that what they have is not the original, so
that any problems introduced by others will not reflect on the original
authors' reputations.

  Finally, any free program is threatened constantly by software
patents.  We wish to avoid the danger that redistributors of a free
program will individually obtain patent licenses, in effect making the
program proprietary.  To prevent this, we have made it clear that any
patent must be licensed for everyone's free use or not licensed at all.

  The precise terms and conditions for copying, distribution and
modification follow.

                    GNU GENERAL PUBLIC LICENSE
   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

  0. This License applies to any program or other work which contains
a notice placed by the copyright holder saying it may be distributed
under the terms of this General Public License.  The "Program", below,
refers to any such program or work, and a "work based on the Program"
means either the Program or any derivative work under copyright law:
that is to say, a work containing the Program or a portion of it,
either verbatim or with modifications and/or translated into another
language.  (Hereinafter, translation is included without limitation in
the term "modification".)  Each licensee is addressed as "you".

Activities other than copying, distribution and modification are not
covered by this License; they are outside its scope.  The act of
running the Program is not restricted, and the output from the Program
is covered only if its contents constitute a work based on the
Program (independent of having been made by running the Program).
Whether that is true depends on what the Program does.

  1. You may copy and distribute verbatim copies of the Program's
source code as you receive it, in any medium, provided that you
conspicuously and appropriately publish on each copy an appropriate
copyright notice and disclaimer of warranty; keep intact all the
notices that refer to this License and to the absence of any warranty;
and give any other recipients of the Program a copy of this License
along with the Program.

You may charge a fee for the physical act of transferring a copy, and
you may at your option offer warranty protection in exchange for a fee.

  2. You may modify your copy or copies of the Program or any portion
of it, thus forming a work based on the Program, and copy and
distribute such modifications or work under the terms of Section 1
above, provided that you also meet all of these conditions:

    a) You must cause the modified files to carry prominent notices
    stating that you changed the files and the date of any change.

    b) You must cause any work that you distribute or publish, that in
    whole or in part contains or is derived from the Program or any
    part thereof, to be licensed as a whole at no charge to all third
    parties under the terms of this License.

    c) If the modified program normally reads commands interactively
    when run, you must cause it, when started running for such
    interactive use in the most ordinary way, to print or display an
    announcement including an appropriate copyright notice and a
    notice that there is no warranty (or else, saying that you provide
    a warranty) and that users may redistribute the program under
    these conditions, and telling the user how to view a copy of this
    License.  (Exception: if the Program itself is interactive but
    does not normally print such an announcement, your work based on
    the Program is not required to print an announcement.)

These requirements apply to the modified work as a whole.  If
identifiable sections of that work are not derived from the Program,
and can be reasonably considered independent and separate works in
themselves, then this License, and its terms, do not apply to those
sections when you distribute them as separate works.  But when you
distribute the same sections as part of a whole which is a work based
on the Program, the distribution of the whole must be on the terms of
this License, whose permissions for other licensees extend to the
entire whole, and thus to each and every part regardless of who wrote it.

Thus, it is not the intent of this section to claim rights or contest
your rights to work written entirely by you; rather, the intent is to
exercise the right to control the distribution of derivative or
collective works based on the Program.

In addition, mere aggregation of another work not based on the Program
with the Program (or with a work based on the Program) on a volume of
a storage or distribution medium does not bring the other work under
the scope of this License.

  3. You may copy and distribute the Program (or a work based on it,
under Section 2) in object code or executable form under the terms of
Sections 1 and 2 above provided that you also do one of the following:

    a) Accompany it with the complete corresponding machine-readable
    source code, which must be distributed under the terms of Sections
    1 and 2 above on a medium customarily used for software interchange; or,

    b) Accompany it with a written offer, valid for at least three
    years, to give any third party, for a charge no more than your
    cost of physically performing source distribution, a complete
    machine-readable copy of the corresponding source code, to be
    distributed under the terms of Sections 1 and 2 above on a medium
    customarily used for software interchange; or,

    c) Accompany it with the information you received as to the offer
    to distribute corresponding source code.  (This alternative is
    allowed only for noncommercial distribution and only if you
    received the program in object code or executable form with such
    an offer, in accord with Subsection b above.)

The source code for a work means the preferred form of the work for
making modifications to it.  For an executable work, complete source
code means all the source code for all modules it contains, plus any
associated interface definition files, plus the scripts used to
control compilation and installation of the executable.  However, as a
special exception, the source code distributed need not include
anything that is normally distributed (in either source or binary
form) with the major components (compiler, kernel, and so on) of the
operating system on which the executable runs, unless that component
itself accompanies the executable.

If distribution of executable or object code is made by offering
access to copy from a designated place, then offering equivalent
access to copy the source code from the same place counts as
distribution of the source code, even though third parties are not
compelled to copy the source along with the object code.

  4. You may not copy, modify, sublicense, or distribute the Program
except as expressly provided under this License.  Any attempt
otherwise to copy, modify, sublicense or distribute the Program is
void, and will automatically terminate your rights under this License.
However, parties who have received copies, or rights, from you under
this License will not have their licenses terminated so long as such
parties remain in full compliance.

  5. You are not required to accept this License, since you have not
signed it.  However, nothing else grants you permission to modify or
distribute the Program or its derivative works.  These actions are
prohibited by law if you do not accept this License.  Therefore, by
modifying or distributing the Program (or any work based on the
Program), you indicate your acceptance of this License to do so, and
all its terms and conditions for copying, distributing or modifying
the Program or works based on it.

  6. Each time you redistribute the Program (or any work based on the
Program), the recipient automatically receives a license from the
original licensor to copy, distribute or modify the Program subject to
these terms and conditions.  You may not impose any further
restrictions on the recipients' exercise of the rights granted herein.
You are not responsible for enforcing compliance by third parties to
this License.

  7. If, as a consequence of a court judgment or allegation of patent
infringement or for any other reason (not limited to patent issues),
conditions are imposed on you (whether by court order, agreement or
otherwise) that contradict the conditions of this License, they do not
excuse you from the conditions of this License.  If you cannot
distribute so as to satisfy simultaneously your obligations under this
License and any other pertinent obligations, then as a consequence you
may not distribute the Program at all.  For example, if a patent
license would not permit royalty-free redistribution of the Program by
all those who receive copies directly or indirectly through you, then
the only way you could satisfy both it and this License would be to
refrain entirely from distribution of the Program.

If any portion of this section is held invalid or unenforceable under
any particular circumstance, the balance of the section is intended to
apply and the section as a whole is intended to apply in other
circumstances.

It is not the purpose of this section to induce you to infringe any
patents or other property right claims or to contest validity of any
such claims; this section has the sole purpose of protecting the
integrity of the free software distribution system, which is
implemented by public license practices.  Many people have made
generous contributions to the wide range of software distributed
through that system in reliance on consistent application of that
system; it is up to the author/donor to decide if he or she is willing
to distribute software through any other system and a licensee cannot
impose that choice.

This section is intended to make thoroughly clear what is believed to
be a consequence of the rest of this License.

  8. If the distribution and/or use of the Program is restricted in
certain countries either by patents or by copyrighted interfaces, the
original copyright holder who places the Program under this License
may add an explicit geographical distribution limitation excluding
those countries, so that distribution is permitted only in or among
countries not thus excluded.  In such case, this License incorporates
the limitation as if written in the body of this License.

  9. The Free Software Foundation may publish revised and/or new versions
of the General Public License from time to time.  Such new versions will
be similar in spirit to the present version, but may differ in detail to
address new problems or concerns.

Each version is given a distinguishing version number.  If the Program
specifies a version number of this License which applies to it and "any
later version", you have the option of following the terms and conditions
either of that version or of any later version published by the Free
Software Foundation.  If the Program does not specify a version number of
this License, you may choose any version ever published by the Free Software
Foundation.

  10. If you wish to incorporate parts of the Program into other free
programs whose distribution conditions are different, write to the author
to ask for permission.  For software which is copyrighted by the Free
Software Foundation, write to the Free Software Foundation; we sometimes
make exceptions for this.  Our decision will be guided by the two goals
of preserving the free status of all derivatives of our free software and
of promoting the sharing and reuse of software generally.

                            NO WARRANTY

  11. BECAUSE THE PROGRAM IS LICENSED FREE OF CHARGE, THERE IS NO WARRANTY
FOR THE PROGRAM, TO THE EXTENT PERMITTED BY APPLICABLE LAW.  EXCEPT WHEN
OTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR OTHER PARTIES
PROVIDE THE PROGRAM "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED
OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.  THE ENTIRE RISK AS
TO THE QUALITY AND PERFORMANCE OF THE PROGRAM IS WITH YOU.  SHOULD THE
PROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF ALL NECESSARY SERVICING,
REPAIR OR CORRECTION.

  12. IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING
WILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MAY MODIFY AND/OR
REDISTRIBUTE THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES,
INCLUDING ANY GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING
OUT OF THE USE OR INABILITY TO USE THE PROGRAM (INCLUDING BUT NOT LIMITED
TO LOSS OF DATA OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY
YOU OR THIRD PARTIES OR A FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHER
PROGRAMS), EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE
POSSIBILITY OF SUCH DAMAGES.

                     END OF TERMS AND CONDITIONS

            How to Apply These Terms to Your New Programs

  If you develop a new program, and you want it to be of the greatest
possible use to the public, the best way to achieve this is to make it
free software which everyone can redistribute and change under these terms.

  To do so, attach the following notices to the program.  It is safest
to attach them to the start of each source file to most effectively
convey the exclusion of warranty; and each file should have at least
the "copyright" line and a pointer to where the full notice is found.

    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) <year>  <name of author>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

Also add information on how to contact you by electronic and paper mail.

If the program is interactive, make it output a short notice like this
when it starts in an interactive mode:

    Gnomovision version 69, Copyright (C) year name of author
    Gnomovision comes with ABSOLUTELY NO WARRANTY; for details type `show w'.
    This is free software, and you are welcome to redistribute it
    under certain conditions; type `show c' for details.

The hypothetical commands `show w' and `show c' should show the appropriate
parts of the General Public License.  Of course, the commands you use may
be called something other than `show w' and `show c'; they could even be
mouse-clicks or menu items--whatever suits your program.

You should also get your employer (if you work as a programmer) or your
school, if any, to sign a "copyright disclaimer" for the program, if
necessary.  Here is a sample; alter the names:

  Yoyodyne, Inc., hereby disclaims all copyright interest in the program
  `Gnomovision' (which makes passes at compilers) written by James Hacker.

  <signature of Ty Coon>, 1 April 1989
  Ty Coon, President of Vice

This General Public License does not permit incorporating your program into
proprietary programs.  If your program is a subroutine library, you may
consider it more useful to permit linking proprietary applications with the
library.  If this is what you want to do, use the GNU Lesser General
Public License instead of this License.
```
______

__BSD-3-Clause__

```
Copyright <YEAR> <COPYRIGHT HOLDER>

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
```
______

__Unlicense__

```
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
```
______

__CC-BY-3.0__

```
Creative Commons Legal Code

Attribution 3.0 Unported

    CREATIVE COMMONS CORPORATION IS NOT A LAW FIRM AND DOES NOT PROVIDE
    LEGAL SERVICES. DISTRIBUTION OF THIS LICENSE DOES NOT CREATE AN
    ATTORNEY-CLIENT RELATIONSHIP. CREATIVE COMMONS PROVIDES THIS
    INFORMATION ON AN "AS-IS" BASIS. CREATIVE COMMONS MAKES NO WARRANTIES
    REGARDING THE INFORMATION PROVIDED, AND DISCLAIMS LIABILITY FOR
    DAMAGES RESULTING FROM ITS USE.

License

THE WORK (AS DEFINED BELOW) IS PROVIDED UNDER THE TERMS OF THIS CREATIVE
COMMONS PUBLIC LICENSE ("CCPL" OR "LICENSE"). THE WORK IS PROTECTED BY
COPYRIGHT AND/OR OTHER APPLICABLE LAW. ANY USE OF THE WORK OTHER THAN AS
AUTHORIZED UNDER THIS LICENSE OR COPYRIGHT LAW IS PROHIBITED.

BY EXERCISING ANY RIGHTS TO THE WORK PROVIDED HERE, YOU ACCEPT AND AGREE
TO BE BOUND BY THE TERMS OF THIS LICENSE. TO THE EXTENT THIS LICENSE MAY
BE CONSIDERED TO BE A CONTRACT, THE LICENSOR GRANTS YOU THE RIGHTS
CONTAINED HERE IN CONSIDERATION OF YOUR ACCEPTANCE OF SUCH TERMS AND
CONDITIONS.

1. Definitions

 a. "Adaptation" means a work based upon the Work, or upon the Work and
    other pre-existing works, such as a translation, adaptation,
    derivative work, arrangement of music or other alterations of a
    literary or artistic work, or phonogram or performance and includes
    cinematographic adaptations or any other form in which the Work may be
    recast, transformed, or adapted including in any form recognizably
    derived from the original, except that a work that constitutes a
    Collection will not be considered an Adaptation for the purpose of
    this License. For the avoidance of doubt, where the Work is a musical
    work, performance or phonogram, the synchronization of the Work in
    timed-relation with a moving image ("synching") will be considered an
    Adaptation for the purpose of this License.
 b. "Collection" means a collection of literary or artistic works, such as
    encyclopedias and anthologies, or performances, phonograms or
    broadcasts, or other works or subject matter other than works listed
    in Section 1(f) below, which, by reason of the selection and
    arrangement of their contents, constitute intellectual creations, in
    which the Work is included in its entirety in unmodified form along
    with one or more other contributions, each constituting separate and
    independent works in themselves, which together are assembled into a
    collective whole. A work that constitutes a Collection will not be
    considered an Adaptation (as defined above) for the purposes of this
    License.
 c. "Distribute" means to make available to the public the original and
    copies of the Work or Adaptation, as appropriate, through sale or
    other transfer of ownership.
 d. "Licensor" means the individual, individuals, entity or entities that
    offer(s) the Work under the terms of this License.
 e. "Original Author" means, in the case of a literary or artistic work,
    the individual, individuals, entity or entities who created the Work
    or if no individual or entity can be identified, the publisher; and in
    addition (i) in the case of a performance the actors, singers,
    musicians, dancers, and other persons who act, sing, deliver, declaim,
    play in, interpret or otherwise perform literary or artistic works or
    expressions of folklore; (ii) in the case of a phonogram the producer
    being the person or legal entity who first fixes the sounds of a
    performance or other sounds; and, (iii) in the case of broadcasts, the
    organization that transmits the broadcast.
 f. "Work" means the literary and/or artistic work offered under the terms
    of this License including without limitation any production in the
    literary, scientific and artistic domain, whatever may be the mode or
    form of its expression including digital form, such as a book,
    pamphlet and other writing; a lecture, address, sermon or other work
    of the same nature; a dramatic or dramatico-musical work; a
    choreographic work or entertainment in dumb show; a musical
    composition with or without words; a cinematographic work to which are
    assimilated works expressed by a process analogous to cinematography;
    a work of drawing, painting, architecture, sculpture, engraving or
    lithography; a photographic work to which are assimilated works
    expressed by a process analogous to photography; a work of applied
    art; an illustration, map, plan, sketch or three-dimensional work
    relative to geography, topography, architecture or science; a
    performance; a broadcast; a phonogram; a compilation of data to the
    extent it is protected as a copyrightable work; or a work performed by
    a variety or circus performer to the extent it is not otherwise
    considered a literary or artistic work.
 g. "You" means an individual or entity exercising rights under this
    License who has not previously violated the terms of this License with
    respect to the Work, or who has received express permission from the
    Licensor to exercise rights under this License despite a previous
    violation.
 h. "Publicly Perform" means to perform public recitations of the Work and
    to communicate to the public those public recitations, by any means or
    process, including by wire or wireless means or public digital
    performances; to make available to the public Works in such a way that
    members of the public may access these Works from a place and at a
    place individually chosen by them; to perform the Work to the public
    by any means or process and the communication to the public of the
    performances of the Work, including by public digital performance; to
    broadcast and rebroadcast the Work by any means including signs,
    sounds or images.
 i. "Reproduce" means to make copies of the Work by any means including
    without limitation by sound or visual recordings and the right of
    fixation and reproducing fixations of the Work, including storage of a
    protected performance or phonogram in digital form or other electronic
    medium.

2. Fair Dealing Rights. Nothing in this License is intended to reduce,
limit, or restrict any uses free from copyright or rights arising from
limitations or exceptions that are provided for in connection with the
copyright protection under copyright law or other applicable laws.

3. License Grant. Subject to the terms and conditions of this License,
Licensor hereby grants You a worldwide, royalty-free, non-exclusive,
perpetual (for the duration of the applicable copyright) license to
exercise the rights in the Work as stated below:

 a. to Reproduce the Work, to incorporate the Work into one or more
    Collections, and to Reproduce the Work as incorporated in the
    Collections;
 b. to create and Reproduce Adaptations provided that any such Adaptation,
    including any translation in any medium, takes reasonable steps to
    clearly label, demarcate or otherwise identify that changes were made
    to the original Work. For example, a translation could be marked "The
    original work was translated from English to Spanish," or a
    modification could indicate "The original work has been modified.";
 c. to Distribute and Publicly Perform the Work including as incorporated
    in Collections; and,
 d. to Distribute and Publicly Perform Adaptations.
 e. For the avoidance of doubt:

     i. Non-waivable Compulsory License Schemes. In those jurisdictions in
        which the right to collect royalties through any statutory or
        compulsory licensing scheme cannot be waived, the Licensor
        reserves the exclusive right to collect such royalties for any
        exercise by You of the rights granted under this License;
    ii. Waivable Compulsory License Schemes. In those jurisdictions in
        which the right to collect royalties through any statutory or
        compulsory licensing scheme can be waived, the Licensor waives the
        exclusive right to collect such royalties for any exercise by You
        of the rights granted under this License; and,
   iii. Voluntary License Schemes. The Licensor waives the right to
        collect royalties, whether individually or, in the event that the
        Licensor is a member of a collecting society that administers
        voluntary licensing schemes, via that society, from any exercise
        by You of the rights granted under this License.

The above rights may be exercised in all media and formats whether now
known or hereafter devised. The above rights include the right to make
such modifications as are technically necessary to exercise the rights in
other media and formats. Subject to Section 8(f), all rights not expressly
granted by Licensor are hereby reserved.

4. Restrictions. The license granted in Section 3 above is expressly made
subject to and limited by the following restrictions:

 a. You may Distribute or Publicly Perform the Work only under the terms
    of this License. You must include a copy of, or the Uniform Resource
    Identifier (URI) for, this License with every copy of the Work You
    Distribute or Publicly Perform. You may not offer or impose any terms
    on the Work that restrict the terms of this License or the ability of
    the recipient of the Work to exercise the rights granted to that
    recipient under the terms of the License. You may not sublicense the
    Work. You must keep intact all notices that refer to this License and
    to the disclaimer of warranties with every copy of the Work You
    Distribute or Publicly Perform. When You Distribute or Publicly
    Perform the Work, You may not impose any effective technological
    measures on the Work that restrict the ability of a recipient of the
    Work from You to exercise the rights granted to that recipient under
    the terms of the License. This Section 4(a) applies to the Work as
    incorporated in a Collection, but this does not require the Collection
    apart from the Work itself to be made subject to the terms of this
    License. If You create a Collection, upon notice from any Licensor You
    must, to the extent practicable, remove from the Collection any credit
    as required by Section 4(b), as requested. If You create an
    Adaptation, upon notice from any Licensor You must, to the extent
    practicable, remove from the Adaptation any credit as required by
    Section 4(b), as requested.
 b. If You Distribute, or Publicly Perform the Work or any Adaptations or
    Collections, You must, unless a request has been made pursuant to
    Section 4(a), keep intact all copyright notices for the Work and
    provide, reasonable to the medium or means You are utilizing: (i) the
    name of the Original Author (or pseudonym, if applicable) if supplied,
    and/or if the Original Author and/or Licensor designate another party
    or parties (e.g., a sponsor institute, publishing entity, journal) for
    attribution ("Attribution Parties") in Licensor's copyright notice,
    terms of service or by other reasonable means, the name of such party
    or parties; (ii) the title of the Work if supplied; (iii) to the
    extent reasonably practicable, the URI, if any, that Licensor
    specifies to be associated with the Work, unless such URI does not
    refer to the copyright notice or licensing information for the Work;
    and (iv) , consistent with Section 3(b), in the case of an Adaptation,
    a credit identifying the use of the Work in the Adaptation (e.g.,
    "French translation of the Work by Original Author," or "Screenplay
    based on original Work by Original Author"). The credit required by
    this Section 4 (b) may be implemented in any reasonable manner;
    provided, however, that in the case of a Adaptation or Collection, at
    a minimum such credit will appear, if a credit for all contributing
    authors of the Adaptation or Collection appears, then as part of these
    credits and in a manner at least as prominent as the credits for the
    other contributing authors. For the avoidance of doubt, You may only
    use the credit required by this Section for the purpose of attribution
    in the manner set out above and, by exercising Your rights under this
    License, You may not implicitly or explicitly assert or imply any
    connection with, sponsorship or endorsement by the Original Author,
    Licensor and/or Attribution Parties, as appropriate, of You or Your
    use of the Work, without the separate, express prior written
    permission of the Original Author, Licensor and/or Attribution
    Parties.
 c. Except as otherwise agreed in writing by the Licensor or as may be
    otherwise permitted by applicable law, if You Reproduce, Distribute or
    Publicly Perform the Work either by itself or as part of any
    Adaptations or Collections, You must not distort, mutilate, modify or
    take other derogatory action in relation to the Work which would be
    prejudicial to the Original Author's honor or reputation. Licensor
    agrees that in those jurisdictions (e.g. Japan), in which any exercise
    of the right granted in Section 3(b) of this License (the right to
    make Adaptations) would be deemed to be a distortion, mutilation,
    modification or other derogatory action prejudicial to the Original
    Author's honor and reputation, the Licensor will waive or not assert,
    as appropriate, this Section, to the fullest extent permitted by the
    applicable national law, to enable You to reasonably exercise Your
    right under Section 3(b) of this License (right to make Adaptations)
    but not otherwise.

5. Representations, Warranties and Disclaimer

UNLESS OTHERWISE MUTUALLY AGREED TO BY THE PARTIES IN WRITING, LICENSOR
OFFERS THE WORK AS-IS AND MAKES NO REPRESENTATIONS OR WARRANTIES OF ANY
KIND CONCERNING THE WORK, EXPRESS, IMPLIED, STATUTORY OR OTHERWISE,
INCLUDING, WITHOUT LIMITATION, WARRANTIES OF TITLE, MERCHANTIBILITY,
FITNESS FOR A PARTICULAR PURPOSE, NONINFRINGEMENT, OR THE ABSENCE OF
LATENT OR OTHER DEFECTS, ACCURACY, OR THE PRESENCE OF ABSENCE OF ERRORS,
WHETHER OR NOT DISCOVERABLE. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION
OF IMPLIED WARRANTIES, SO SUCH EXCLUSION MAY NOT APPLY TO YOU.

6. Limitation on Liability. EXCEPT TO THE EXTENT REQUIRED BY APPLICABLE
LAW, IN NO EVENT WILL LICENSOR BE LIABLE TO YOU ON ANY LEGAL THEORY FOR
ANY SPECIAL, INCIDENTAL, CONSEQUENTIAL, PUNITIVE OR EXEMPLARY DAMAGES
ARISING OUT OF THIS LICENSE OR THE USE OF THE WORK, EVEN IF LICENSOR HAS
BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.

7. Termination

 a. This License and the rights granted hereunder will terminate
    automatically upon any breach by You of the terms of this License.
    Individuals or entities who have received Adaptations or Collections
    from You under this License, however, will not have their licenses
    terminated provided such individuals or entities remain in full
    compliance with those licenses. Sections 1, 2, 5, 6, 7, and 8 will
    survive any termination of this License.
 b. Subject to the above terms and conditions, the license granted here is
    perpetual (for the duration of the applicable copyright in the Work).
    Notwithstanding the above, Licensor reserves the right to release the
    Work under different license terms or to stop distributing the Work at
    any time; provided, however that any such election will not serve to
    withdraw this License (or any other license that has been, or is
    required to be, granted under the terms of this License), and this
    License will continue in full force and effect unless terminated as
    stated above.

8. Miscellaneous

 a. Each time You Distribute or Publicly Perform the Work or a Collection,
    the Licensor offers to the recipient a license to the Work on the same
    terms and conditions as the license granted to You under this License.
 b. Each time You Distribute or Publicly Perform an Adaptation, Licensor
    offers to the recipient a license to the original Work on the same
    terms and conditions as the license granted to You under this License.
 c. If any provision of this License is invalid or unenforceable under
    applicable law, it shall not affect the validity or enforceability of
    the remainder of the terms of this License, and without further action
    by the parties to this agreement, such provision shall be reformed to
    the minimum extent necessary to make such provision valid and
    enforceable.
 d. No term or provision of this License shall be deemed waived and no
    breach consented to unless such waiver or consent shall be in writing
    and signed by the party to be charged with such waiver or consent.
 e. This License constitutes the entire agreement between the parties with
    respect to the Work licensed here. There are no understandings,
    agreements or representations with respect to the Work not specified
    here. Licensor shall not be bound by any additional provisions that
    may appear in any communication from You. This License may not be
    modified without the mutual written agreement of the Licensor and You.
 f. The rights granted under, and the subject matter referenced, in this
    License were drafted utilizing the terminology of the Berne Convention
    for the Protection of Literary and Artistic Works (as amended on
    September 28, 1979), the Rome Convention of 1961, the WIPO Copyright
    Treaty of 1996, the WIPO Performances and Phonograms Treaty of 1996
    and the Universal Copyright Convention (as revised on July 24, 1971).
    These rights and subject matter take effect in the relevant
    jurisdiction in which the License terms are sought to be enforced
    according to the corresponding provisions of the implementation of
    those treaty provisions in the applicable national law. If the
    standard suite of rights granted under applicable copyright law
    includes additional rights not granted under this License, such
    additional rights are deemed to be included in the License; this
    License is not intended to restrict the license of any rights under
    applicable law.


Creative Commons Notice

    Creative Commons is not a party to this License, and makes no warranty
    whatsoever in connection with the Work. Creative Commons will not be
    liable to You or any party on any legal theory for any damages
    whatsoever, including without limitation any general, special,
    incidental or consequential damages arising in connection to this
    license. Notwithstanding the foregoing two (2) sentences, if Creative
    Commons has expressly identified itself as the Licensor hereunder, it
    shall have all rights and obligations of Licensor.

    Except for the limited purpose of indicating to the public that the
    Work is licensed under the CCPL, Creative Commons does not authorize
    the use by either party of the trademark "Creative Commons" or any
    related trademark or logo of Creative Commons without the prior
    written consent of Creative Commons. Any permitted use will be in
    compliance with Creative Commons' then-current trademark usage
    guidelines, as may be published on its website or otherwise made
    available upon request from time to time. For the avoidance of doubt,
    this trademark restriction does not form part of this License.

    Creative Commons may be contacted at https://creativecommons.org/.
```
______

__CC0-1.0__

```
Creative Commons Legal Code

CC0 1.0 Universal

    CREATIVE COMMONS CORPORATION IS NOT A LAW FIRM AND DOES NOT PROVIDE
    LEGAL SERVICES. DISTRIBUTION OF THIS DOCUMENT DOES NOT CREATE AN
    ATTORNEY-CLIENT RELATIONSHIP. CREATIVE COMMONS PROVIDES THIS
    INFORMATION ON AN "AS-IS" BASIS. CREATIVE COMMONS MAKES NO WARRANTIES
    REGARDING THE USE OF THIS DOCUMENT OR THE INFORMATION OR WORKS
    PROVIDED HEREUNDER, AND DISCLAIMS LIABILITY FOR DAMAGES RESULTING FROM
    THE USE OF THIS DOCUMENT OR THE INFORMATION OR WORKS PROVIDED
    HEREUNDER.

Statement of Purpose

The laws of most jurisdictions throughout the world automatically confer
exclusive Copyright and Related Rights (defined below) upon the creator
and subsequent owner(s) (each and all, an "owner") of an original work of
authorship and/or a database (each, a "Work").

Certain owners wish to permanently relinquish those rights to a Work for
the purpose of contributing to a commons of creative, cultural and
scientific works ("Commons") that the public can reliably and without fear
of later claims of infringement build upon, modify, incorporate in other
works, reuse and redistribute as freely as possible in any form whatsoever
and for any purposes, including without limitation commercial purposes.
These owners may contribute to the Commons to promote the ideal of a free
culture and the further production of creative, cultural and scientific
works, or to gain reputation or greater distribution for their Work in
part through the use and efforts of others.

For these and/or other purposes and motivations, and without any
expectation of additional consideration or compensation, the person
associating CC0 with a Work (the "Affirmer"), to the extent that he or she
is an owner of Copyright and Related Rights in the Work, voluntarily
elects to apply CC0 to the Work and publicly distribute the Work under its
terms, with knowledge of his or her Copyright and Related Rights in the
Work and the meaning and intended legal effect of CC0 on those rights.

1. Copyright and Related Rights. A Work made available under CC0 may be
protected by copyright and related or neighboring rights ("Copyright and
Related Rights"). Copyright and Related Rights include, but are not
limited to, the following:

  i. the right to reproduce, adapt, distribute, perform, display,
     communicate, and translate a Work;
 ii. moral rights retained by the original author(s) and/or performer(s);
iii. publicity and privacy rights pertaining to a person's image or
     likeness depicted in a Work;
 iv. rights protecting against unfair competition in regards to a Work,
     subject to the limitations in paragraph 4(a), below;
  v. rights protecting the extraction, dissemination, use and reuse of data
     in a Work;
 vi. database rights (such as those arising under Directive 96/9/EC of the
     European Parliament and of the Council of 11 March 1996 on the legal
     protection of databases, and under any national implementation
     thereof, including any amended or successor version of such
     directive); and
vii. other similar, equivalent or corresponding rights throughout the
     world based on applicable law or treaty, and any national
     implementations thereof.

2. Waiver. To the greatest extent permitted by, but not in contravention
of, applicable law, Affirmer hereby overtly, fully, permanently,
irrevocably and unconditionally waives, abandons, and surrenders all of
Affirmer's Copyright and Related Rights and associated claims and causes
of action, whether now known or unknown (including existing as well as
future claims and causes of action), in the Work (i) in all territories
worldwide, (ii) for the maximum duration provided by applicable law or
treaty (including future time extensions), (iii) in any current or future
medium and for any number of copies, and (iv) for any purpose whatsoever,
including without limitation commercial, advertising or promotional
purposes (the "Waiver"). Affirmer makes the Waiver for the benefit of each
member of the public at large and to the detriment of Affirmer's heirs and
successors, fully intending that such Waiver shall not be subject to
revocation, rescission, cancellation, termination, or any other legal or
equitable action to disrupt the quiet enjoyment of the Work by the public
as contemplated by Affirmer's express Statement of Purpose.

3. Public License Fallback. Should any part of the Waiver for any reason
be judged legally invalid or ineffective under applicable law, then the
Waiver shall be preserved to the maximum extent permitted taking into
account Affirmer's express Statement of Purpose. In addition, to the
extent the Waiver is so judged Affirmer hereby grants to each affected
person a royalty-free, non transferable, non sublicensable, non exclusive,
irrevocable and unconditional license to exercise Affirmer's Copyright and
Related Rights in the Work (i) in all territories worldwide, (ii) for the
maximum duration provided by applicable law or treaty (including future
time extensions), (iii) in any current or future medium and for any number
of copies, and (iv) for any purpose whatsoever, including without
limitation commercial, advertising or promotional purposes (the
"License"). The License shall be deemed effective as of the date CC0 was
applied by Affirmer to the Work. Should any part of the License for any
reason be judged legally invalid or ineffective under applicable law, such
partial invalidity or ineffectiveness shall not invalidate the remainder
of the License, and in such case Affirmer hereby affirms that he or she
will not (i) exercise any of his or her remaining Copyright and Related
Rights in the Work or (ii) assert any associated claims and causes of
action with respect to the Work, in either case contrary to Affirmer's
express Statement of Purpose.

4. Limitations and Disclaimers.

 a. No trademark or patent rights held by Affirmer are waived, abandoned,
    surrendered, licensed or otherwise affected by this document.
 b. Affirmer offers the Work as-is and makes no representations or
    warranties of any kind concerning the Work, express, implied,
    statutory or otherwise, including without limitation warranties of
    title, merchantability, fitness for a particular purpose, non
    infringement, or the absence of latent or other defects, accuracy, or
    the present or absence of errors, whether or not discoverable, all to
    the greatest extent permissible under applicable law.
 c. Affirmer disclaims responsibility for clearing rights of other persons
    that may apply to the Work or any use thereof, including without
    limitation any person's Copyright and Related Rights in the Work.
    Further, Affirmer disclaims responsibility for obtaining any necessary
    consents, permissions or other rights required for any use of the
    Work.
 d. Affirmer understands and acknowledges that Creative Commons is not a
    party to this document and has no duty or obligation with respect to
    this CC0 or use of the Work.
```