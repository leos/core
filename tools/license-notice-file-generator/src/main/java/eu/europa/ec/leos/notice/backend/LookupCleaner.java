package eu.europa.ec.leos.notice.backend;

import eu.europa.ec.leos.notice.backend.thirdparty.ThirdPartyConverter;
import eu.europa.ec.leos.notice.backend.thirdparty.ThirdPartyLibrary;
import eu.europa.ec.leos.notice.backend.xml.LookupXmlMapping;
import eu.europa.ec.leos.notice.backend.xml.MavenXmlCopyrightsMapping;
import eu.europa.ec.leos.notice.common.PathRetriever;
import eu.europa.ec.leos.notice.common.Product;
import eu.europa.ec.leos.notice.frontend.xml.CopyrightsResult;
import eu.europa.ec.leos.notice.generator.LibraryNoticeV3;
import eu.europa.ec.leos.notice.generator.NoticeFileGeneratorV3;
import eu.europa.ec.leos.notice.license.*;
import eu.europa.ec.leos.notice.remote.NoticeService;
import eu.europa.ec.leos.notice.remote.RemoteNotice;
import org.silentsoft.oss.LicenseDictionary;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Generate a Notice file for Maven dependencies read from a THIRD-PARTY.txt file with content like:
 * <pre>
 * (Apache License, Version 2.0) tomcat-embed-websocket (org.apache.tomcat.embed:tomcat-embed-websocket:9.0.64 - https://tomcat.apache.org/)
 * (Apache License, version 2.0) JBoss Logging 3 (org.jboss.logging:jboss-logging:3.4.1.Final - http://www.jboss.org)
 * (BSD 2-Clause) LatencyUtils (org.latencyutils:LatencyUtils:2.0.3 - http://latencyutils.github.io/LatencyUtils/)
 * (BSD-3-Clause) Hamcrest (org.hamcrest:hamcrest:2.2 - http://hamcrest.org/JavaHamcrest/)
 * (BSD License) AntLR Parser Generator (antlr:antlr:2.7.7 - http://www.antlr.org/)
 * </pre>
 */
public class LookupCleaner {
    private final Logger log = LoggerFactory.getLogger(getClass());
    private final Product product;
    private final Path thirdPartyTxtFile;
    private final String existingJsonResponse;
    private final boolean useExistingResponses;
    private final MavenXmlCopyrightsMapping xmlCopyrightsMapping;

    public LookupCleaner(Product product, Path thirdPartyTxtFile, MavenXmlCopyrightsMapping xmlCopyrightsMapping,
                         String existingJsonResponse, boolean useExistingResponses) {
        Objects.requireNonNull(product);
        Objects.requireNonNull(thirdPartyTxtFile);
        Objects.requireNonNull(xmlCopyrightsMapping);
        this.product = product;
        this.thirdPartyTxtFile = thirdPartyTxtFile;
        this.xmlCopyrightsMapping = xmlCopyrightsMapping;
        this.existingJsonResponse = existingJsonResponse;
        this.useExistingResponses = useExistingResponses;
        registerExtraLicenses();
    }

    public void generateNotice(OutputStream os) throws IOException {
        final PrintStream ps = new PrintStream(os, true);
        final NoticeFileGeneratorV3.NoticeFileBuilder noticeBuilder = NoticeFileGeneratorV3.newInstance(product.getName(), product.getOwner());
//        noticeBuilder.addText("Version: " + product.getVersion());
        noticeBuilder.addText(product.getLicenseContent());
        noticeBuilder.addText("This product includes dynamically linked software developed by third parties which is provided under their respective licences:");

        final List<ThirdPartyLibrary> thirdPartyLibraries = loadThirdPartyLibraries();
        log.warn("Total UNIQUE thirdPartyLibraries: {}", thirdPartyLibraries.size());
        final List<String> namespaceAndNames = extractNamespaceAndNes(thirdPartyLibraries);
        final List<RemoteNotice> remoteNotices = retrieveMavenNotices(namespaceAndNames);

        merge(thirdPartyLibraries, remoteNotices)
                .forEach(noticeBuilder::addLibrary);

        String markdown = noticeBuilder.generate();
        ps.print(markdown);
    }

    private List<ThirdPartyLibrary> loadThirdPartyLibraries() throws IOException {
        return new ThirdPartyConverter().convertTxt(thirdPartyTxtFile);
    }

    private static List<String> extractNamespaceAndNes(List<ThirdPartyLibrary> thirdPartyLibraries) {
        return thirdPartyLibraries.stream()
                                  .map(ThirdPartyLibrary::fullDependencyPath)
                                  .collect(Collectors.toList());
    }

    private List<RemoteNotice> retrieveMavenNotices(List<String> namespaceAndNames) throws IOException {
        final NoticeService noticeService = new NoticeService();
        return noticeService.retrieveNotices(namespaceAndNames, NoticeService.NoticeProvider.MAVENCENTRAL, existingJsonResponse, useExistingResponses);
    }

    private void registerExtraLicenses() {
        LicenseDictionary.put(new BSDJavaLicense(), new CDDL1_0License(), new SAXLicense(), new W3CLicense());
    }

    private Collection<LibraryNoticeV3> merge(Collection<ThirdPartyLibrary> libraries, List<RemoteNotice> remoteNotices) {
        final Map<String, RemoteNotice> fullNameToRemoteNotice = remoteNotices
                .stream()
                .collect(Collectors.toMap(RemoteNotice::getFullName,
                        Function.identity()));
        return libraries
                .stream()
                .map(thirdPartyLibrary ->
                        toLibraryNotice(thirdPartyLibrary, fullNameToRemoteNotice.get(thirdPartyLibrary.fullDependencyPath())))
                .collect(Collectors.toList());
    }

    private LibraryNoticeV3 toLibraryNotice(ThirdPartyLibrary thirdPartyLibrary, RemoteNotice remoteNotice) {
        LibraryNoticeV3 libNotice = new LibraryNoticeV3()
                .withName(thirdPartyLibrary.groupIdAndArtifactId())
                .withVersion(thirdPartyLibrary.getVersion())
                .withWebsite(thirdPartyLibrary.getProjectUrl())
                .withLicenseAlias(thirdPartyLibrary.getLicenseAliases());

        final Optional<CopyrightsResult> copyrightMatch = xmlCopyrightsMapping.match(
                thirdPartyLibrary.getGroupId(), thirdPartyLibrary.getArtifactId(), thirdPartyLibrary.getVersion());

        if (copyrightMatch.isPresent()) {
            final CopyrightsResult copyrightsResult = copyrightMatch.get();
            libNotice.withCopyrights(copyrightsResult.getCopyrights());
            if (!copyrightsResult.getLicenseAliases().isEmpty()) {
                libNotice.withLicenseAlias(copyrightsResult.getLicenseAliases());
            }
        } else if (null != remoteNotice) {
            // use mainly copyright from RemoteNotice
            if (remoteNotice.hasCopyrights()) {
                libNotice.withCopyrights(remoteNotice.getCopyrights());
            }
            if (remoteNotice.hasWebsite() && !remoteNotice.getWebsite().equals(thirdPartyLibrary.getProjectUrl())) {
                log.warn("Got different website for {}. From XML: {}, from remote: {}",
                        thirdPartyLibrary.groupIdAndArtifactId(),
                        thirdPartyLibrary.getProjectUrl(),
                        remoteNotice.getWebsite());
            }
        }

        libNotice.validate();
        return libNotice;
    }

    public static void main(String[] args) throws URISyntaxException, IOException, InterruptedException, ParserConfigurationException, SAXException, TransformerException {
        final Path xmlCopyrights = new PathRetriever().fromClasspath("/copyrights-lookup-annotation-npmjs_dirty.xml");
        LookupXmlMapping mappings = new LookupXmlMapping(xmlCopyrights);

        mappings.convertNoticeToXml(System.out);

//        final Path trustedAppTxtFile = Paths.get("target/generated-sources/license/THIRD-PARTY.txt");
//        final Product trustedApp = new Product("LEOS", "2022 European Union", "1.0", EUPLv1_2Content.content());
//
//        final LookupCleaner trustedAppNoticeGenerator = new LookupCleaner(trustedApp, trustedAppTxtFile, mappings);
//        try (PrintStream ps = new PrintStream("NOTICE_BE.md")) {
//            trustedAppNoticeGenerator.generateNotice(ps);
//        }
    }
}
