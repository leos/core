import { HttpClient } from '@angular/common/http';
import {
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { Subject, takeUntil } from 'rxjs';

import { AppConfigService } from '@/core/services/app-config.service';
import { CKEditorService } from '@/features/akn-document/services/ckeditor.service';
import {
  TrackChangeAction,
  TrackChangesActionsService,
} from '@/features/akn-document/services/track-changes-actions.service';
import { DocumentConfig, LeosConfig, Permission } from '@/shared';
import { DocumentService } from '@/shared/services/document.service';
import {EuiDialogService} from "@eui/components/eui-dialog";
import {TranslateService} from "@ngx-translate/core";

@Component({
  selector: 'app-track-changes-actions',
  styleUrls: ['./track-changes-actions.component.scss'],
  // eslint-disable-next-line @angular-eslint/no-host-metadata-property
  host: {
    '(document:click)': 'clickedOutside()',
    '(document:keydown)': 'clickedOutside()',
  },
  templateUrl: './track-changes-actions.component.html',
})
export class TrackChangesActionsComponent implements OnInit, OnDestroy {
  documentConfig: DocumentConfig;
  leosConfig: LeosConfig;

  isShown = false;
  trackChangesDr: NodeListOf<Element>;
  canAcceptTrackChanges: boolean;
  canRejectTrackChanges: boolean;
  currentElement: HTMLElement;
  trackChangeAction: TrackChangeAction;
  movedToId: string;
  movedFromId: string;

  private destroy$ = new Subject();

  private mouseLocation: { left: number; top: number } = { left: 0, top: 0 };

  constructor(
    private http: HttpClient,
    private doc: DocumentService,
    private ref: ChangeDetectorRef,
    private trackChangesActionsService: TrackChangesActionsService,
    private ckEditorService: CKEditorService,
    private appConfigService: AppConfigService,
    private dialogService: EuiDialogService,
    private translateService: TranslateService,
  ) {
    trackChangesActionsService.show.subscribe((trackChanges) => {
      this.trackChangesDr = trackChanges.trackChanges;
      this.addTrackChangesEvents();
    });
  }

  ngOnInit() {
    this.doc.permissions$.subscribe((perms) => this.setMenuState(perms));
    this.doc.documentConfig$
      .pipe(takeUntil(this.destroy$))
      .subscribe((config) => {
        this.documentConfig = config;
      });
    this.appConfigService.config
      .pipe(takeUntil(this.destroy$))
      .subscribe((config) => {
        this.leosConfig = config;
      });
  }

  setMenuState(permissions: Permission[]) {
    this.canAcceptTrackChanges =
      permissions.includes('CAN_ACCEPT_CHANGES') &&
      (!this.documentConfig?.clonedProposal ||
        (this.documentConfig?.clonedProposal &&
          this.leosConfig?.user.roles.includes('SUPPORT')));
    this.canRejectTrackChanges = permissions.includes('CAN_REJECT_CHANGES');
  }

  seeTrackChanges() {
    return this.ckEditorService.getSeeTrackChangesState();
  }

  addTrackChangesEvents() {
    this.trackChangesDr?.forEach((tc) => {
      tc.addEventListener('contextmenu', (e) => {
        if (this.seeTrackChanges()) {
          e.preventDefault();
          this.showMenu(e);
        }
      });
    });
  }

  get locationCss() {
    return {
      position: 'fixed',
      display: this.isShown ? 'block' : 'none',
      left: this.mouseLocation.left + 'px',
      top: this.mouseLocation.top + 'px',
    };
  }

  clickedOutside() {
    this.isShown = false; // hide the menu
  }

  // show the menu and set the location of the mouse
  showMenu(event) {
    this.isShown = true;
    this.currentElement = event.currentTarget;
    this.mouseLocation = {
      left: event.clientX,
      top: event.clientY,
    };
    this.getAction(event.currentTarget);
    this.ref.markForCheck();
  }

  getAction(elt: HTMLElement) {
    const action = elt.getAttribute('leos:action');
    this.movedToId = elt.getAttribute('leos:softmove_to');
    this.movedFromId = elt.getAttribute('leos:softmove_from');
    if (action === 'insert') {
      this.trackChangeAction = TrackChangeAction.ADD;
    } else if (action === 'delete') {
      this.trackChangeAction = TrackChangeAction.DEL;
    }
    if (!!this.movedToId) {
      this.trackChangeAction = TrackChangeAction.MOVED_TO;
    }
    if (!!this.movedFromId) {
      this.trackChangeAction = TrackChangeAction.MOVED_FROM;
    }
  }

  canUserAcceptChanges() {
    return this.canAcceptTrackChanges;
  }

  canUserRejectChanges() {
    return this.canRejectTrackChanges;
  }

  onAccept() {
    if (this.currentElement.getAttribute("leos:optional") === "true"
      || (this.currentElement.children && this.currentElement.children[1] && this.currentElement.children[1].getAttribute("leos:optional") === "true")) {
      this.dialogService.openDialog({
        title: this.translateService.instant(
          'page.editor.element-delete-dialog.optional.title',
        ),
        content: this.translateService.instant(
          'page.editor.element-delete-dialog.optional.body',
        ),
        hasDismissButton: false,
        accept: () => { },
      });
    } else {
      this.trackChangesActionsService.applyTrackChangeAction(
        this.trackChangeAction,
        {
          elementType: this.currentElement.tagName.toLowerCase(),
          elementId: this.currentElement.id,
        },
        this.doc,
      );
    }
  }

  onReject() {
    this.trackChangesActionsService.rejectTrackChangeAction(
      this.trackChangeAction,
      {
        elementType: this.currentElement.tagName.toLowerCase(),
        elementId: this.currentElement.id,
      },
      this.doc,
    );
  }

  ngOnDestroy(): void {
    this.destroy$.next(null);
    this.destroy$.complete();
  }
}
