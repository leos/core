package eu.europa.ec.leos.domain.repository;

public enum LeosExportStatus {
    FILE_READY,
    NOTIFIED,
    PROCESSED_OK,
    PROCESSED_ERROR
}
